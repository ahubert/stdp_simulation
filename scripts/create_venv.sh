#!/bin/bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd "${SELF%/*/*}"

python3 -m venv venv
source venv/bin/activate

pip install -U pip
pip install -U -r ./requirements.txt
