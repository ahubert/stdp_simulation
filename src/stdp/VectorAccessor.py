class VectorAccessor():
    '''
    Accessor class for a indexable object.

    In this example, suppose we have a vector with 3 elements that we will refer to as
    foo, bar and baz.
    '''
    def __init__(self, vector):
        '''
        Args:
            vector:
                The indexable object (e.g. a numpy vector or a simple list.).
        '''
        self.vector = vector

VECTOR_ELEMENT_NAMES = ['B1', 'B2', 'B3' , 'B4' , 'B5' , 'B6' , 'B7' , 'B8' , 'B9' , 'B10' , 'B11' , 'B12' , 'B13',
                        'Ca_cyt' , 'Ca_ER' , 'h' , 'IP3' , 'V' , 'rAMPA' , 'rNMDA' ,
                        'PP1' , 'I1P' , 'DAG', 'DAGLP' , '2-AG' , 'fpre' , 'AEA' , 'Glu' , 'Curr_spikepost', 'cst']

# Dynamically add properties.
def VectorAccessor_add_property(VECTOR_ELEMENT_NAMES):
  for i, name in enumerate(VECTOR_ELEMENT_NAMES):
      def getter(self, i=i):
          return self.vector[i]

      def setter(self, value, i=i):
          self.vector[i] = value

      setattr(VectorAccessor, name, property(getter, setter))

VectorAccessor_add_property(VECTOR_ELEMENT_NAMES)