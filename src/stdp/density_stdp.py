import numpy as np
import scipy
import scipy.signal
import scipy.stats
import matplotlib.pyplot as plt
import matplotlib
import sklearn.cluster

from stdp.Class_Analysis import Router
from stdp.create_mouse_data import Mouse
from stdp.computation_tools import *
from stdp.utility_tools import *
from stdp.fig_paper_Charlotte import around_ratio
from stdp.fig_paper_Charlotte import selec_on_criteria

import stdp.Class_Correlation as Corr
############################################################
######## OLD VERSION BASED ON W_PRE VARIATION   ############
############################################################

class jump():
    '''
    Class used to caracterise jump in plasticity
    '''
    def __init__(self, start, stop, height, ground):
        self.start = start
        self.stop = stop
        self.height = height
        self.ground = ground

def value_at_time(time, data, t):
     '''
     return the interpolated value of array at time "t".
     '''
     t_inf = time[time<t][-1]
     inf = data[time<t][-1]
     t_sup = time[time>t][0]
     sup = data[time>t][0]
     return inf + (t-t_inf)/(t_sup-t_inf)*(sup-inf)

def convolveTri(time, data, f_params = [0.02, 0.1, 1]):
    '''
    Convolve a function evaluated at time points as data and convolve it with triangle func.
    
    f_params:
        [a, b, c]: [start of triangle, end of triangle, max heigh of triangle]

    return (res, interp_time)
    '''
    res = dict()
    interp_time = np.linspace(np.min(time), np.max(time), int((np.max(time)-np.min(time))*100))
    interp_val = np.interp(interp_time, time, data)
    tri_val = np.zeros(50)  #(len(interp_time))
    tri_val[int(25-f_params[0]*100):25] = np.linspace(0, f_params[2], 25-int(25-f_params[0]*100))
    tri_val[25:int(25+f_params[1]*100)] = np.linspace(f_params[2], 0, int(25+f_params[1]*100)-25)

    #tri_val = tri_val

    res['values'] = np.convolve(interp_val, tri_val)[49:-49]#[:len(interp_time)]
    res['time'] = interp_time[24:-25]
    return res

def is_jump(time, data):
    res_convolv = convolveTri(time, data, f_params=[0.05, 0.05, 1])
    #plt.plot(res_convolv['time'], res_convolv['values'], label = 'res convovlve')
    #plt.title('res convolve')
    #plt.show()
    len_time = np.max(time)-np.min(time)
    min_time = np.min(time)
    diff = res_convolv['values'][1:] - res_convolv['values'][:-1]
    #plt.plot(res_convolv['time'][0:-1], diff, label = 'diff')
    #plt.title('diff')
    #plt.show()
    mean_diff = convolveTri(res_convolv['time'][:-1], diff)
    #plt.plot(mean_diff['time'], mean_diff['values'], label = 'mean diff')
    #plt.title('mean diff')

    potential_start = np.where(mean_diff['values'] > 0.04)[0]
    group = potential_start[1:] - potential_start[:-1]

    #plt.scatter( mean_diff['time'][potential_start][:-1] ,group, label = 'group')

    if len(potential_start) == 0:
         LOGGER.debug('No jump detected')
         return ()
    
    jump_list = ()
    start = potential_start[0]
    stop = potential_start[0]
    mem = 2
    for it, k in enumerate(group):
        if it == (len(group)-1):
                stop =  mean_diff['time'][potential_start][-1]
                jump_list = jump_list + (jump(start, stop, value_at_time(time, data, stop) - value_at_time(time, data,start), value_at_time(time, data, start)) ,)
        if mem == 1 and k>1 :
            stop = mean_diff['time'][potential_start][it-1] #potential_start[it]
            jump_list = jump_list + (jump(start, stop, value_at_time(time, data, stop) - value_at_time(time, data,start), value_at_time(time, data, start)) ,)
            #plt.vlines(stop, 0, 10, color = 'green')
        if mem > 1 and k == 1:
            start = mean_diff['time'][potential_start][it] #potential_start[it]
            #plt.vlines(start, 0, 10, color = 'black')
        mem = k
    
    #plt.legend()
    #plt.show()
    return jump_list

############################################################

class cat_res():
    def __init__(self, wpre_nAG, wpre_tAG, wpre_tCa, wpre_Ca, wpre_tAGCa):
        self.wpre_nAG = wpre_nAG
        self.wpre_tAG = wpre_tAG
        self.wpre_tCa = wpre_tCa
        self.wpre_Ca = wpre_Ca
        self.wpre_tAGCa = wpre_tAGCa
    """
    wpre_nAG: number of switch to Omega LTP
    wpre_tAG: time past in Omega LTP
    wpre_tCa: time past in hight Ca (taufpre<100)
    wpre_Ca: integral of 1/taufpre, to give info on how much low
    wpre_tAGCa: time past with Omega LTP while hight Ca
    """
    def f(self):
        return 0


def compute_carCat(T , Omega, taufpre):
    '''compute the variables of a result for creating cat_res class object
    '''
    IOmega = (Omega > 2)*1
    Itaufpre = (taufpre < 100)*1
    dIOmega = IOmega[1:]-IOmega[:-1]
    int_cat = compute_int_X(T, np.array([IOmega, 1/taufpre, Itaufpre, IOmega*Itaufpre]), T[0], T[-1])
    wpre_nAG = np.sum(dIOmega == 1)
    wpre_tAG = int_cat[0]*(T[-1]-T[0])
    wpre_Ca = int_cat[1]
    wpre_tCa = int_cat[2]
    wpre_tAGCa = int_cat[3]
    W = [wpre_nAG, wpre_tAG, wpre_tCa, wpre_Ca, wpre_tAGCa]
    cat = cat_res(W)
    return cat


def compute_tLTP(T , Omega, taufpre):
    '''compute the times start of LTP event.
    '''
    IOmega = (Omega > 2)*1
    Itaufpre = (taufpre < 100)*1
    ILTP = IOmega*Itaufpre
    dILTP = ILTP[1:] - ILTP[:-1]
    T_ref = (T[1:] + T[:-1])/2
    t_LTP = T_ref[dILTP == 1]

    return t_LTP

def compute_durLTP(T , Omega, taufpre, ti, delta, len_dim, LTD_type = "LTD"):
    '''compute the times where LTP happens.
    LTD_type can be Neutral if it come from Ca increase without LTD threshold activated
    '''
    vect_LTP = np.zeros(len_dim)
    vect_LTD = np.zeros(len_dim)
    IOmega_LTP = (Omega > 2)*1
    if LTD_type == 'LTD':
        IOmega_LTD = (Omega < 0.5)*1
    elif LTD_type == 'Neutral':
        IOmega_LTD = (Omega <= 0.5)*1
    else:
        LOGGER.warning('LTD_type not recognized. Results will be wrong.')
    Itaufpre = (taufpre < 100)*1
    ILTP = IOmega_LTP*Itaufpre
    ILTD = IOmega_LTD*Itaufpre
    T_LTP = T[ILTP == 1]
    T_LTD = T[ILTD == 1]

    #T_STDP[np.array((T_LTP - ti)/delta).astype(int)] = 1
    #T_STDP[np.array((T_LTD - ti)/delta).astype(int)] = -1
    vect_LTP[np.array((T_LTP - ti)/delta).astype(int)] = 1
    vect_LTD[np.array((T_LTD - ti)/delta).astype(int)] = 1
    return (vect_LTP, vect_LTD)


def compute_durLTP2(T , Omega, taufpre, ti, delta, len_dim, LTD_type = "LTD"):
    '''compute the times where LTP happens.
    LTD_type can be Neutral if it come from Ca increase without LTD threshold activated
    '''
    vect_LTP = np.zeros(len_dim)
    vect_LTD = np.zeros(len_dim)
    IOmega_LTP = (Omega > 2)*1
    if LTD_type == 'LTD':
        IOmega_LTD = (Omega < 0.5)*1
    elif LTD_type == 'Neutral':
        IOmega_LTD = (Omega <= 0.5)*1
    else:
        LOGGER.warning('LTD_type not recognized. Results will be wrong.')
    Itaufpre = (taufpre < 100)*1
    ILTP = IOmega_LTP*Itaufpre
    ILTD = IOmega_LTD*Itaufpre
    T_LTP = T[ILTP == 1]
    T_LTD = T[ILTD == 1]

    #T_STDP[np.array((T_LTP - ti)/delta).astype(int)] = 1
    #T_STDP[np.array((T_LTD - ti)/delta).astype(int)] = -1
    vect_LTP[np.array((T_LTP - ti)/delta).astype(int)] = 1
    vect_LTD[np.array((T_LTD - ti)/delta).astype(int)] = 1
    return (vect_LTP, vect_LTD)

def res_to_carCat(router:Router, mouse:Mouse):
    """This function want to sort results into category
    """
    
    result_id = router.get_couple_id()
    W_cat = np.zeros((len(result_id), 5))

    for it, couple in enumerate(result_id):
        T = router.load_result_couple(*couple)[2]
        Omega, taufpre = compute_Omega(router, couple)
        W_cat[it] = compute_carCat(T, Omega, taufpre)

    return W_cat

def res_to_durLTP2(router: Router, mouse: Mouse, delta, len_dim, LTD_type = 'LTD'):

    #delta:  precision, in s

    T_mouse_LTP = np.zeros(len_dim)
    T_mouse_LTD = np.zeros(len_dim)
    result_id = np.array(router.get_couple_id())
    #corr_id = mouse.get_correlated_couples()
    #result_id = [(corr_id['Cortex_ID'][it],corr_id['Striatum_ID'][it]) for it in range(len(corr_id['Cortex_ID']))]

    #Checker for Team A and B+
    cortex_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    #print((cortex_clusters))
    striatum_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    selec_cortex = np.isin(cortex_clusters, mouse.selec_Team(cortex_clusters))
    selec_striatum = np.isin(striatum_clusters, mouse.selec_Team(striatum_clusters))
    result_id = result_id[np.logical_and(selec_cortex, selec_striatum)]
    
    for couple in result_id:
        try:
            T = router.load_result_couple(*couple)[2]
            Omega, taufpre = compute_Omega(router, couple)
            res = compute_durLTP(T, Omega, taufpre, mouse.ti-300, delta, len_dim, LTD_type = LTD_type)
            T_mouse_LTP += res[0]
            T_mouse_LTD += res[1]
        except FileNotFoundError:
            LOGGER.warning('%s not found', couple)


    return (T_mouse_LTP, T_mouse_LTD)

def res_to_durLTP(router: Router, mouse: Mouse, delta, len_dim, id_selection = 'pairing_increase' ,LTD_type = 'LTD'):

    #delta:  precision, in s

    T_mouse_LTP = np.zeros(len_dim)
    T_mouse_LTD = np.zeros(len_dim)
    if id_selection == 'pairing_increase':
        result_id = around_ratio(mouse)
        print(f'number of id with pairing increase selection: {len(result_id)}')
    elif id_selection == 'all':
        result_id = np.array(router.get_couple_id())
    elif id_selection == 'mid':
        result_id = around_ratio(mouse, 'mid')
    else:
        print(f'selection id as: {id_selection}, is not recognized')

    print(f'{id_selection} selection, {len(result_id)} id.')
    #corr_id = mouse.get_correlated_couples()
    #result_id = np.array([(corr_id['Cortex_ID'][it],corr_id['Striatum_ID'][it]) for it in range(len(corr_id['Cortex_ID']))])
    
    #Checker for Team A and B+
    cortex_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    #print((cortex_clusters))
    striatum_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    selec_cortex = np.isin(cortex_clusters, mouse.selec_Team(cortex_clusters))
    selec_striatum = np.isin(striatum_clusters, mouse.selec_Team(striatum_clusters))
    result_id = result_id[np.logical_and(selec_cortex, selec_striatum)]
    
    T_plot = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    for couple in result_id:
        try:
            T = router.load_result_couple(*couple)[2]
            Omega, taufpre = compute_Omega(router, couple)
            res = compute_durLTP(T, Omega, taufpre, mouse.ti-300, delta, len_dim, LTD_type = LTD_type)
            #plt.plot(T_plot, res[0])
            if np.sum(res[0])>0:
                T_mouse_LTP += res[0]/np.sqrt(np.sum(res[0]))
            if np.sum(res[1])>0:
                T_mouse_LTD += res[1]/np.sqrt(np.sum(res[1]))
        except FileNotFoundError:
            LOGGER.warning('%s not found', couple)


    return (T_mouse_LTP, T_mouse_LTD)

def res_to_durLTP_sepRES(router: Router, mouse: Mouse, delta, len_dim, id_selection = 'pairing_increase' ,LTD_type = 'LTD'):

    #delta:  precision, in s

    if id_selection == 'pairing_increase':
        result_id = around_ratio(mouse)
        print(f'number of id with pairing increase selection: {len(result_id)}')
    elif id_selection == 'all':
        result_id = np.array(router.get_couple_id())
    elif id_selection == 'mid':
        result_id = around_ratio(mouse, 'mid')
    elif id_selection == 'V7260':
        result_id = around_ratio(mouse, 'V7260')
    else:
        print(f'selection id as: {id_selection}, is not recognized. Try to take it as list of couples')
        result_id = id_selection
    
    T_mouse_LTP = np.zeros((len(result_id), len_dim))
    T_mouse_LTD = np.zeros((len(result_id), len_dim))
    #corr_id = mouse.get_correlated_couples()
    #result_id = np.array([(corr_id['Cortex_ID'][it],corr_id['Striatum_ID'][it]) for it in range(len(corr_id['Cortex_ID']))])
    
    '''#Checker for Team A and B+
    cortex_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    #print((cortex_clusters))
    striatum_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    selec_cortex = np.isin(cortex_clusters, mouse.selec_Team(cortex_clusters))
    selec_striatum = np.isin(striatum_clusters, mouse.selec_Team(striatum_clusters))
    result_id = result_id[np.logical_and(selec_cortex, selec_striatum)]'''
    

    it = 0
    T_plot = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    for couple in result_id:
        try:
            T = router.load_result_couple(*couple)[2]
            Omega, taufpre = compute_Omega(router, couple)
            res = compute_durLTP(T, Omega, taufpre, mouse.ti-300, delta, len_dim, LTD_type = LTD_type)
            #plt.plot(T_plot, res[0])
            if np.sum(res[0])>0:
                T_mouse_LTP[it, :] = res[0]#/np.sqrt(np.sum(res[0]))
            if np.sum(res[1])>0:
                T_mouse_LTD[it, :] = res[1]#/np.sqrt(np.sum(res[1]))
            it += 1
        except FileNotFoundError:
            LOGGER.warning('%s not found', couple)


    return (T_mouse_LTP, T_mouse_LTD)

def res_to_tLTP(router: Router, mouse: Mouse):
        
    #result_id = router.get_couple_id()
    corr_id = mouse.get_correlated_couples()
    result_id = [(corr_id['Cortex_ID'][it],corr_id['Striatum_ID'][it]) for it in range(len(corr_id['Cortex_ID']))]
    T_mouse = ()

    for couple in result_id:
        T = router.load_result_couple(*couple)[2]
        Omega, taufpre = compute_Omega(router, couple)
        T_mouse += (*compute_tLTP(T, Omega, taufpre),)

    return T_mouse

def main():
    '''We look at number of LTP event, LTP are only counted from start, duration not taken into account.
    '''
    name = 'V7260'
    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    #router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f') #1K
    #router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    #router_Mouse1 = Router('490892905216247639','c289df2d94674edb9c16c64e32711432') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    
    mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    
    path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
    synchro = pd.read_csv(path, usecols=[0, 4])
    #print(synchro.index)
    speed = synchro.to_numpy()
    #length = speed.shape[0]/10000
    #print(speed.shape[0])
    speed = speed[np.arange(0, speed.shape[0], 500), :]
    speed = speed.T

    RES = res_to_tLTP(router_V7260, mouse)
    #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')

    #load_object()
    plt.hist( RES, bins=200)
    plt.vlines([mouse.ti, mouse.tf], 0, 200, color = 'red')
    plt.plot(speed[0], speed[1])
    plt.show()

    return 0

def main2():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''
    name = 'V7260'
    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    #router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f') #1K
    #router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    #router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    
    mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')

    #RELATED TO SPEED
    path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
    synchro = pd.read_csv(path, usecols=[0, 4])
    #print(synchro.index)
    speed = synchro.to_numpy()
    #length = speed.shape[0]/10000
    #print(speed.shape[0])
    speed = speed[np.arange(0, speed.shape[0], 500), :]
    speed = speed.T
    recallage = 0
    speed = speed[:, np.logical_and(speed[0]>(mouse.ti-300+recallage), speed[0]<(mouse.tf+320+recallage))]
    #plt.plot(speed[0]-recallage, speed[1], alpha = 0.1, label = 'mouse speed') 

    delta = 0.1
    len_dim = int((mouse.tf-mouse.ti+620)/delta +1)

    router = router_V7260
    RES_selected = res_to_durLTP(router, mouse, delta, len_dim)#, LTD_type='Neutral')
    #RES_all = res_to_durLTP(router, mouse, delta, len_dim, 'all')
    RES_mid = res_to_durLTP(router, mouse, delta, len_dim, 'mid')
    #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')
    
    T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    #load_object()
    #plt.plot( T, RES[0], label = 'LTP', alpha = 0.7 )
    #plt.plot( T, RES[1], label = 'LTD', alpha = 0.7 )
    #plt.plot( T, RES[0]*2-RES[1], label = 'LTP relativated' )

    #Just for more smoother visualisation
    #RES_selected = scipy.signal.savgol_filter(RES_selected, 100, 2)
    #RES_all = scipy.signal.savgol_filter(RES_all, 100, 2)    

    fig, axs = plt.subplots( 3,1, width_ratios = [3] )

    #Plot diff

    axs[0].hlines([0], mouse.ti-300, mouse.tf+320, color = 'black')
    axs[0].vlines([mouse.ti, mouse.tf], 0, 100, color = 'red')
    
    '''plt.plot( T, RES_selected[0]/np.sum(RES_selected[0]),
             label = 'LTP, selected couples ' )
              #label = 'LTP relativated selected' )
    plt.plot( T, (RES_all[0])/np.sum(RES_all[0]),
             label = 'LTP, all couples ' )
              #label = 'LTP relativated all' )'''
    

    #axs[0].plot( T, (RES_all[0]-RES_all[1])/20,
    #         label = 'LTP - LTD, all couples ' )
    #          #label = 'LTP relativated all' )
    axs[0].plot( T, (RES_mid[0]-RES_mid[1]),
             label = 'LTP - LTD, classic couples ' )
    axs[0].plot( T, (RES_selected[0]-RES_selected[1]),
             label = 'LTP - LTD, increasing couples ' )
              #label = 'LTP relativated selected' )
    axs[0].set_title(f'mouse {name}, LTP and LTD diff events meter')
    axs[0].legend()

    axs[0].hlines([0], mouse.ti-300, mouse.tf+320, color = 'black')
    axs[0].vlines([mouse.ti, mouse.tf], 0, 1, color = 'red')
    

    axs[1].vlines([mouse.ti, mouse.tf], 0, 100, color = 'red')
    #axs[1].plot( T, RES_all[0]/20,
    #         label = 'LTP, all couples ' )
    #          #label = 'LTP relativated all' )
    axs[1].plot( T, RES_mid[0],
             label = 'LTP, classic couples ' )
    axs[1].plot( T, RES_selected[0],
             label = 'LTP, increasing couples ' )
              #label = 'LTP relativated selected' )
    axs[1].set_title(f'mouse {name}, LTP events meter')
    axs[1].legend()


    axs[2].vlines([mouse.ti, mouse.tf], 0, 100, color = 'red')
    #axs[2].plot( T, RES_all[1]/20,
    #         label = 'LTP, all couples ' )
    #          #label = 'LTP relativated all' )
    axs[2].plot( T, RES_mid[1],
             label = 'LTP, classic couples ' )
    axs[2].plot( T, RES_selected[1],
             label = 'LTP, increasing couples ' )
              #label = 'LTP relativated selected' )
    axs[2].set_title(f'mouse {name}, LTD events meter')
    axs[2].legend()

    #stimulation, curiosité, grooming, mouvements

    grooming = np.array([854.6, 857.3, 2067.1, 2070.8, 2384.6, 2386.6])
    near_tape = np.array([1341.8, 1342.6])
    sniffing = np.array([1342.5, 1345.6])
    stimulis = np.array([1366.42, 1367.2, 1526.1, 1551.7])
    #1436.0 2nd contact retait à 1557.7

    for it in range(3):
        #axs[it].vlines(marche, -2, 150, colors = 'blue',  alpha = 0.15)
        axs[it].vlines(stimulis, -1, 150, colors = 'purple', alpha = 0.15)
        #axs[it].vlines(grooming, -3, 150, colors = 'orange', alpha = 0.15)
        axs[it].vlines(sniffing, -3, 150, colors = 'pink', alpha = 0.2)
        axs[it].vlines(near_tape, -3, 150, colors = 'yellow',  alpha = 0.15)
        axs[it].plot(speed[0]-recallage, speed[1]/2, alpha = 0.4)#, label = 'mouse speed')
        axs[it].vlines([1436.0, 1557.7], 0, 100, color = 'red')

    #plt.savefig('/home/ahubert/Documents/Paper_Charlotte/LTP_counter_fig/V7250_pltFig')
    plt.show()

    return 0


def selec_sparse(RES):
    n_sum = np.sum(RES, axis = 1).T
    n_sparse = n_sum < np.median(n_sum)
    return n_sparse

def main_sepRes():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''
    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    router_Black =  Router('490892905216247639','1cb6b6e3c29e45dd83f8710f2463942e')
    router_Red =  Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    router_V4607 =  Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    #router_Blue = Router('490892905216247639' ,'c486b7474b9a4713ba905be3114e9a5a') #2K
    router_Blue = Router('490892905216247639','96e389831f8a4a2fa8709224f05f61fc') #1K
    router_V4609 = Router('490892905216247639','c22b39c7b56b4e54ad2e34582d80430d') #1K 

    delta = 0.2

    L_mouse = [('V7265', router_V7265, 700), ('V4606', router_V4606, 300), ('Mouse1', router_Mouse1, 200), ('V7260', router_V7260, 0),
               ('Black', router_Black, 1200), ('Red', router_Red, 600), ('V4607', router_V4607, 0), ('Blue', router_Blue, 900),
               ('V4609', router_V4609, 0)]
    
    L_mouse = [('V7265', router_V7265, 700)]

    for name, router, recallage in L_mouse:

        mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    
        #RELATED TO SPEED
        
        path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
        synchro = pd.read_csv(path, usecols=[0, 4])
        #print(synchro.index)
        #synchro = pd.to_numeric(synchro, errors='coerce')
        speed = synchro.to_numpy()
        if name == 'Black':
            speed = speed[270000:2887000]
            speed = speed.astype(float)
        if name == 'Red':
            speed = speed.astype(float)
        #length = speed.shape[0]/10000
        #print(speed.shape[0])
        speed = speed[np.arange(0, speed.shape[0], 500), :]
        speed = speed.T

        speed = speed[:, np.logical_and(speed[0]>(mouse.ti-300+recallage), speed[0]<(mouse.tf+320+recallage))]
        #speed[1] = scipy.signal.savgol_filter(speed[1], 20, 1)

        len_dim = int((mouse.tf-mouse.ti+620)/delta +1)
        #len_dim = 375


        try:
            RES_selected = np.load(mouse.path_mouse / 'RES_selected.npy')
            RES_all = np.load(mouse.path_mouse / 'RES_all.npy')
            RES_mid = np.load(mouse.path_mouse / 'RES_mid.npy')
        except:
            print('in exept')
            exit()
            RES_selected = res_to_durLTP_sepRES(router, mouse, delta, len_dim)[1]#, LTD_type='Neutral')
            RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'all')[1]
            #RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'V7260')
            RES_mid = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'mid')[1]
            #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')
            np.save(mouse.path_mouse / 'RES_selected.npy', RES_selected)
            np.save(mouse.path_mouse / 'RES_all.npy', RES_all)
            np.save(mouse.path_mouse / 'RES_mid.npy', RES_mid)
        #sel = selec_on_criteria(mouse, ['increase', 'mid']) 
        #RES_selected = RES_all[sel[0]]
        #RES_mid = RES_all[sel[1]]

        print(name)
        #print(RES_all.shape)

        #ind_sparse = selec_sparse(RES_all[0])
        print( f' n zeros all is: {np.sum(np.sum(RES_all, axis = 1) == 0)/RES_all.shape[0]}')
        print( f' n zeros increase is: {np.sum(np.sum(RES_selected, axis = 1) == 0)/RES_selected.shape[0]}')
        print( f' n zeros mid is: {np.sum(np.sum(RES_mid, axis = 1) == 0)/RES_mid.shape[0]}')


        #RES_all = np.sum(RES_all, axis = 0)
        #RES_selected = np.sum(RES_selected, axis = 0)
        #RES_mid = np.sum(RES_mid, axis = 0)

    #Normalisation part
        pen_all = np.sum(RES_all, axis = 1)
        pen_selected = np.sum(RES_selected, axis = 1)
        pen_mid = np.sum(RES_mid, axis = 1)

        RES_all = np.sum(RES_all[pen_all.T>0]/np.array([pen_all[pen_all>0]]).T, axis = 0)
        RES_selected = np.sum(RES_selected[pen_selected.T>0]/np.array([pen_selected[pen_selected>0]]).T, axis = 0)
        RES_mid = np.sum(RES_mid[pen_mid.T>0]/np.array([pen_mid[pen_mid>0]]).T, axis = 0)
    #End of normalisation Part
        
        RES_all = scipy.signal.savgol_filter(RES_all, 3, 1)
        RES_selected = scipy.signal.savgol_filter(RES_selected, 3, 1)
        RES_mid = scipy.signal.savgol_filter(RES_mid, 3, 1)

        print('MAXS ARE:')
        print(np.min(RES_all)/20)
        print(np.min(RES_mid)/2)
        print(np.min(RES_selected))


        """RES_all = RES_all[275*5 : 350*5]
        RES_selected = RES_selected[275*5 : 350*5]
        RES_mid = RES_mid[275*5 : 350*5]
        """
        
        '''
        M = np.zeros((len(RES_all[0]), len(RES_all[0])))
        for k in range(len(RES_all[0])):
            for j in range(len(RES_all[0])):
                M[k, j] = np.sum(RES_all[0][k] * RES_all[0][j])'''

        #Kmeans = sklearn.cluster.KMeans( n_clusters = 3)#, algorithm = 'elkan')
        #ind_Kmean = Kmeans.fit_predict(RES_all[0])
        #np.save('/home/ahubert/Documents/SpikeSorting/Kmean_ind/Red_indKmean', ind_Kmean)
        #ind_Kmean = np.load('/home/ahubert/Documents/SpikeSorting/Kmean_ind/Black_indKmean.npy')
        #print(np.unique(ind_Kmean, return_counts=True))


        #print(scipy.stats.pearsonr(np.sum(RES_all[0][ind_Kmean == 1], axis = 0)[1:], speed[1]))
        #print(scipy.stats.pearsonr(np.sum(RES_all[0][ind_Kmean != 1], axis = 0)[1:], speed[1]))

        #print(scipy.stats.spearmanr(np.sum(RES_mid[0], axis = 0), speed[1]))
        #print(scipy.stats.spearmanr(np.sum(RES_selected[0], axis = 0), speed[1]))

        T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
        #T = np.linspace(mouse.ti-25, mouse.ti+50, len_dim)

        max_RES = np.max(np.array([RES_all/20, RES_mid/2, RES_selected]), axis = 0)

        fig, (ax1, ax3, ax2) = plt.subplots(3, width_ratios = [3])

        ax1.vlines([mouse.ti, mouse.tf,1436,1557.7], 0, np.max(max_RES)*1.2, color = 'red', alpha = 0.6)
        #ax1.vlines([349.21, 470.85], 0, np.max(max_RES)*1.2, color = 'red', alpha = 0.6)
        
        if name != 'Black':
            ax2.plot(speed[0]-recallage, speed[1]/np.max(speed[1])*np.max(max_RES), alpha = 1, label = 'mouse speed') 
            ax2.set_xlabel('time (s)')
            ax2.set_ylabel('speed')

        near_tape = np.array([1341.8, 1342.6])
        sniffing = np.array([1342.5, 1345.6])
        stimulis = np.array([1340, 1341, 1366.42, 1367.2, 1526.1, 1551.7])
        
        #1436.0 2nd contact retait à 1557.7
        color_plot = ['orange', 'green', 'red']
        label_plot = ['all', 'increase', 'mid']

        timeline_list = [near_tape, stimulis,  sniffing]
        label_fill = ['near_tape', 'stimulis',  'sniffing']
        color_fill = ['green', 'purple',  'blue']

        it=0
        
        ax1.plot(T, RES_all/20, label = 'all', alpha = 0.8)
        ax1.plot(T, RES_selected, label = 'increase', alpha = 0.8)
        #plt.fill(T, RES_selected, label = 'increase', alpha = 0.4)
        ax1.plot(T, RES_mid/2, label = 'mid', alpha = 0.8)
        ax1.set_xlabel('time (s)')
        ax1.set_ylabel('ecb-LTP counter')
        
        #plot speed above main plot
        ax1.plot(speed[0]-recallage, speed[1]/np.max(speed[1])*np.max(max_RES), alpha = 0.3, label = 'mouse speed', color = 'black')
        print(f'max speed: {np.max(speed[1])}, min speed {np.min(speed[1])}')

        '''for vec in timeline_list:
            under_fill = ()
            upper_fill = ()
            for t in range(int(len(vec)/2)):
                sel_t = np.logical_and(T>vec[t*2], T<vec[t*2+1])
                under_fill += (np.concatenate((np.array([T[sel_t][0]]), T[sel_t],np.array([T[sel_t][-1]]))),
                            np.concatenate((np.array([0]),max_RES[sel_t],np.array([0]))) )
                
                upper_fill += (np.array([T[sel_t][0],T[sel_t][0],T[sel_t][-1],T[sel_t][-1]]),
                        #np.array([np.max(max_RES[sel_t]),np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])]) )
                            [-0.1, 0.0, 0.0, -0.1] )

                #plt.fill(np.concatenate((np.array([T[sel_t][0]]),T[sel_t],np.array([T[sel_t][-1]]))),
                #        np.concatenate((np.array([-5]),max_RES[sel_t],np.array([-5]))) , alpha = 0.5 , label = label_list[it] )
                #plt.fill(np.array([T[sel_t][0],T[sel_t][0],T[sel_t][-1],T[sel_t][-1]]),
                #         np.array([np.max(max_RES[sel_t]),np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])]))
            ax1.fill( *under_fill, color = color_fill[it], alpha = 0.2)#, label = label_fill[it] )
            ax1.fill( [],[], color = color_fill[it], alpha = 0.3, label = label_fill[it] )
            ax1.fill( *upper_fill, color = color_fill[it], alpha = 0.8 )
            it += 1
        '''
        '''
        it = 0
        for vec in timeline_list:
            under_fill = ()
            upper_fill = ()
            for t in range(int(len(vec)/2)):
                sel_t = np.logical_and(T>vec[t*2], T<vec[t*2+1])
                under_fill += (np.concatenate((np.array([T[sel_t][0]]), T[sel_t],np.array([T[sel_t][-1]]))),
                            np.concatenate((np.array([0]),(speed[1]/np.max(speed[1])*np.max(max_RES))[sel_t],np.array([0]))) )
                
                upper_fill += (np.array([T[sel_t][0],T[sel_t][0],T[sel_t][-1],T[sel_t][-1]]),
                        #np.array([np.max(max_RES[sel_t]),np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])]) )
                            [-0.1, 0.0, 0.0, -0.1] )

                #plt.fill(np.concatenate((np.array([T[sel_t][0]]),T[sel_t],np.array([T[sel_t][-1]]))),
                #        np.concatenate((np.array([-5]),max_RES[sel_t],np.array([-5]))) , alpha = 0.5 , label = label_list[it] )
                #plt.fill(np.array([T[sel_t][0],T[sel_t][0],T[sel_t][-1],T[sel_t][-1]]),
                #         np.array([np.max(max_RES[sel_t]),np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])+5,np.max(max_RES[sel_t])]))
            ax2.fill( *under_fill, color = color_fill[it], alpha = 0.2)#, label = label_fill[it] )
            ax2.fill( [],[], color = color_fill[it], alpha = 0.3, label = label_fill[it] )
            ax2.fill( *upper_fill, color = color_fill[it], alpha = 0.8 )
            it += 1
        '''

        '''plt.plot(T, np.sum(RES_all[0]/20, axis = 0), label = 'all', alpha = 0.8)
        plt.plot(T, np.sum(RES_selected[0], axis = 0), label = 'increase', alpha = 0.8)
        plt.fill(T, np.sum(RES_selected[0], axis = 0), label = 'increase', alpha = 0.4)
        plt.plot(T, np.sum(RES_mid[0], axis = 0), label = 'mid', alpha = 0.8)
        '''

        #plt.plot(T, np.sum(RES_selected[0], axis = 0), label = '0', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 0], axis = 0), label = 'speed artefacts', alpha = 1)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 1], axis = 0), label = 'learning?', alpha = 1)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 2], axis = 0), label = '2', alpha = 0.6)  

        #plt.plot(T, np.sum(RES_all[0][ind_groom], axis = 0), label = 'groom', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][np.invert(ind_groom)], axis = 0), label = 'move', alpha = 0.6) 
        
        #plt.plot(T, np.sum(RES_all[0][selec_speed], axis = 0), label = 'corrspeed', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][selec_no_speed], axis = 0), label = 'nocorrspeed', alpha = 0.6)
        
        ##################### ZSCORE PART ##########################
        ax1.legend()
        ax1.set_title(name)
        ax2.legend()

        zscore_selected = scipy.stats.zscore(RES_selected)
        zscore_all = scipy.stats.zscore(RES_all)
        zscore_mid = scipy.stats.zscore(RES_mid)

        zscore_selected[zscore_selected<0] = 0
        zscore_all[zscore_all<0] = 0
        zscore_mid[zscore_mid < 0] = 0

        zscore_selected[zscore_selected>5] = 5
        zscore_all[zscore_all > 5] = 5
        zscore_mid[zscore_mid > 5] = 5

        #zscore_all[zscore_all<0] = 0
        #zscore_all[zscore_all>5] = 5

        X = np_moving_average(zscore_selected, 10)
        #X = X[np.arange(0, len(X), 5)]
        d = len(X)
        X = np.tile(X, (int(len(X)/20),1))
        X = np.concatenate((X,))

        Y = np_moving_average(zscore_mid, 10)
        #Y = Y[np.arange(0, len(Y), 5)]
        Y = np.tile(Y, (int(len(Y)/20),1))

        Z = np_moving_average(zscore_all, 10)
        #Z = Z[np.arange(0, len(Z), 5)]
        Z = np.tile(Z, (int(len(Z)/20),1))

        W = np.ones(d)*np.min([X,Y,Z])
        W = np.tile(W, (int(d/100),1))

        XYZ = np.concatenate((X,W,Y,W,Z), axis = 0)

        m = matplotlib.cm.ScalarMappable(cmap='hot')
        m.set_clim(vmin = np.min(XYZ), vmax = np.max(XYZ))
        ax3.imshow(XYZ, cmap = 'hot', label = 'zscore', extent = [mouse.ti - 300, mouse.tf + 320, 0, (mouse.tf-mouse.ti+600)/10 ])
        ax3.set_title(f'Z score, {name}')
        #plt.colorbar(XYZ, cmap = 'hot', ax = ax3)
        fig.colorbar(m, orientation = 'horizontal')

        plt.show()
        #T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    return 0

def main_heatmap():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''

    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    router_Black =  Router('490892905216247639','1cb6b6e3c29e45dd83f8710f2463942e')
    router_Red =  Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    router_V4607 =  Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    #router_Blue = Router('490892905216247639' ,'c486b7474b9a4713ba905be3114e9a5a') #2K
    router_Blue = Router('490892905216247639','96e389831f8a4a2fa8709224f05f61fc') #1K
    router_V4609 = Router('490892905216247639','c22b39c7b56b4e54ad2e34582d80430d') #1K 

    delta = 0.2

    L_mouse = [('V7265', router_V7265, 700), ('V4606', router_V4606, 300), ('Mouse1', router_Mouse1, 200), ('V7260', router_V7260, 0),
               ('Black', router_Black, 1200), ('Red', router_Red, 600), ('V4607', router_V4607, 0), ('Blue', router_Blue, 900),
               ('V4609', router_V4609, 0)]
    L_mouse=[('V4609', router_V4609, 0)]
    #for mouse,router in [('V7265', router_V7265), ('V4606', router_V4606), ('Mouse1', router_Mouse1), ('V7260', router_V7260)]:
    for name,router, recallage in L_mouse:

        mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        len_dim = int((mouse.tf-mouse.ti+620)/delta +1)

        try:
            RES_selected = np.load(mouse.path_mouse / 'RES_selected.npy')
            RES_all = np.load(mouse.path_mouse / 'RES_all.npy')/20
            RES_mid = np.load(mouse.path_mouse / 'RES_mid.npy')/2
        except:
            RES_selected = res_to_durLTP_sepRES(router, mouse, delta, len_dim)[0]#, LTD_type='Neutral')
            RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'all')[0]
            #RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'V7260')
            RES_mid = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'mid')[0]
            #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')
            np.save(mouse.path_mouse / 'RES_selected.npy', RES_selected)
            np.save(mouse.path_mouse / 'RES_all.npy', RES_all)
            np.save(mouse.path_mouse / 'RES_mid.npy', RES_mid)
        #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')


        #print(scipy.stats.pearsonr(np.sum(RES_all[0][ind_Kmean == 1], axis = 0)[1:], speed[1]))
        #print(scipy.stats.pearsonr(np.sum(RES_all[0][ind_Kmean != 1], axis = 0)[1:], speed[1]))

        #print(scipy.stats.spearmanr(np.sum(RES_mid[0], axis = 0), speed[1]))
        #print(scipy.stats.spearmanr(np.sum(RES_selected[0], axis = 0), speed[1]))


        #print(np.sum(np.sum(RES_all[0][ind_Kmean == 0], axis = 1)<=0) )
        #print(np.sum(np.sum(RES_all[0][ind_Kmean == 2], axis = 1)<=0) )


        '''
        plt.vlines(sniffing, -2, 100, colors = 'red',  alpha = 0.2)
        #plt.vlines(marche, -2, 100, colors = 'blue',  alpha = 0.2)
        plt.vlines(stimulis, -1, 100, colors = 'purple',  alpha = 0.2)
        #plt.vlines(grooming, -3, 100, colors = 'orange', alpha = 0.2)
        plt.vlines(near_tape, -3, 100, colors = 'yellow', alpha = 0.2)
        '''
        #plt.vlines([mouse.ti, mouse.tf], 0, 200, color = 'red', alpha = 0.6)


        #result_id = np.array(router.get_couple_id())
        #list_corr_speed = np.array([214,459,907,256,471,310,334,370,220,231,935,278,280,284,
        #285,948, 1030,713,902,1013,906,295,487,364,234,279,320,591])
        #selec_speed = np.array(np.invert([np.isin(result_id[it][0], list_corr_speed) for it in range(len(result_id))] ))

        T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)

    

        #RES_all = np.sum(RES_all, axis = 0)
        #RES_selected = np.sum(RES_selected, axis = 0)
        #RES_mid = np.sum(RES_mid, axis = 0)

    #Normalisation part
        
        pen_all = np.sum(RES_all, axis = 1)
        pen_selected = np.sum(RES_selected, axis = 1)
        pen_mid = np.sum(RES_mid, axis = 1)

        RES_all = np.sum(RES_all[pen_all.T>0]/np.array([pen_all[pen_all>0]]).T, axis = 0)
        RES_selected = np.sum(RES_selected[pen_selected.T>0]/np.array([pen_selected[pen_selected>0]]).T, axis = 0)
        RES_mid = np.sum(RES_mid[pen_mid.T>0]/np.array([pen_mid[pen_mid>0]]).T, axis = 0)
        
    #End of normalisation part
        
        RES_all = scipy.signal.savgol_filter(RES_all, 3, 1)
        RES_selected = scipy.signal.savgol_filter(RES_selected, 3, 1)
        RES_mid = scipy.signal.savgol_filter(RES_mid, 3, 1)

        zscore_selected = scipy.stats.zscore(RES_selected)
        zscore_all = scipy.stats.zscore(RES_all)
        zscore_mid = scipy.stats.zscore(RES_mid)

        s = 4
        zscore_selected[zscore_selected>s] = s
        zscore_mid[zscore_mid>s] = s
        zscore_all[zscore_all>s] = s

        zscore_selected[zscore_selected<0] = 0
        zscore_mid[zscore_mid<0] = 0
        zscore_all[zscore_all<0] = 0
        
        #plt.hist([zscore_all, zscore_mid, zscore_selected], label = ['all', 'mid', 'sel'], stacked = True, alpha = 0.5)
        #plt.title(name)
        #plt.show()
        
        
        X = np_moving_average(zscore_selected, 10)
        X = X[np.arange(0, len(X), 5)]
        d = len(X)
        X = np.tile(X, (int(len(X)/20),1))
        X = np.concatenate((X,))

        Y = np_moving_average(zscore_mid, 10)
        Y = Y[np.arange(0, len(Y), 5)]
        Y = np.tile(Y, (int(len(Y)/20),1))

        Z = np_moving_average(zscore_all, 10)
        Z = Z[np.arange(0, len(Z), 5)]
        Z = np.tile(Z, (int(len(Z)/20),1))

        W = np.ones(d)*np.min([X,Y,Z])
        W = np.tile(W, (int(d/100),1))

        XYZ = np.concatenate((X,W,Y,W,Z), axis = 0)
        plt.imshow(XYZ, cmap = 'hot', label = 'zscore')
        plt.title(f'Z score, {name}')
        plt.colorbar(orientation = 'horizontal')#, fraction=.1)
        #plt.imshow([zscore_all, zscore_mid, zscore_selected], cmap = 'seismic')
        
        #plt.plot(T, np.sum(RES_selected[0], axis = 0), label = '0', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 0], axis = 0), label = 'speed artefacts', alpha = 1)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 1], axis = 0), label = 'learning?', alpha = 1)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 2], axis = 0), label = '2', alpha = 0.6)  

        #plt.plot(T, np.sum(RES_all[0][ind_groom], axis = 0), label = 'groom', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][np.invert(ind_groom)], axis = 0), label = 'move', alpha = 0.6) 
        
        #plt.plot(T, np.sum(RES_all[0][selec_speed], axis = 0), label = 'corrspeed', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][selec_no_speed], axis = 0), label = 'nocorrspeed', alpha = 0.6)
        plt.legend()
        plt.show()
    #T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)

    
    return 0

def main_CorrCouples():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''
    name = 'V7260'
    router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    #router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f') #1K
    #router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    #router_Mouse1 = Router('490892905216247639','c289df2d94674edb9c16c64e32711432') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K 
    router_Blue = Router('490892905216247639','96e389831f8a4a2fa8709224f05f61fc') #1K

    mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')

    #RELATED TO SPEED
    path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
    synchro = pd.read_csv(path, usecols=[0, 4])
    #print(synchro.index)
    speed = synchro.to_numpy()
    #length = speed.shape[0]/10000
    #print(speed.shape[0])
    speed = speed[np.arange(0, speed.shape[0], 500), :]
    speed = speed.T
    recallage = 0
    speed = speed[:, np.logical_and(speed[0]>(mouse.ti-300+recallage), speed[0]<(mouse.tf+320+recallage))]
    plt.plot(speed[0]-recallage, speed[1], alpha = 0.3, label = 'mouse speed') 

    delta = 0.1
    len_dim = int((mouse.tf-mouse.ti+620)/delta +1)

    router = router_V7260

    couples_list = mouse.get_correlated_couples('All')
    #couples_list = couples_list[couples_list['Keep'] == 1]
    striatum_list = couples_list['Striatum_ID'].to_numpy()
    cortex_list = couples_list['Cortex_ID'].to_numpy()
    couples_list = np.array([(cortex_list[it],striatum_list[it]) for it in range(len(striatum_list)) ])
    #RES_selected = res_to_durLTP_sepRES(router, mouse, delta, len_dim)#, LTD_type='Neutral')
    RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, couples_list)#, 'all')
    #RES_mid = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'mid')
    #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')

    '''
    M = np.zeros((len(RES_all[0]), len(RES_all[0])))
    for k in range(len(RES_all[0])):
        for j in range(len(RES_all[0])):
            M[k, j] = np.sum(RES_all[0][k] * RES_all[0][j])'''

    Kmeans = sklearn.cluster.KMeans( n_clusters = 2)#, algorithm = 'elkan')
    ind_Kmean = Kmeans.fit_predict(RES_all[0])
    #np.save('/home/ahubert/Documents/SpikeSorting/V7260_indKmean', ind_Kmean)
    #ind_Kmean = np.load('/home/ahubert/Documents/SpikeSorting/Mouse1_indKmean.npy')

    ind_groom = np.zeros(len(RES_all[0]) ,dtype = bool)
    it = 0
    #for ILTP in RES_all[0]:
    

    plt.vlines([mouse.ti, mouse.tf], 0, 200, color = 'red')

    #result_id = np.array(router.get_couple_id())
    #list_corr_speed = np.array([214,459,907,256,471,310,334,370,220,231,935,278,280,284,
    #285,948, 1030,713,902,1013,906,295,487,364,234,279,320,591])
    #selec_speed = np.array(np.invert([np.isin(result_id[it][0], list_corr_speed) for it in range(len(result_id))] ))

    T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)

    plt.plot(T, np.sum(RES_all[0][ind_Kmean == 0], axis = 0), label = '0', alpha = 0.6)
    plt.plot(T, np.sum(RES_all[0][ind_Kmean == 1], axis = 0), label = '1', alpha = 0.6)
    #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 2], axis = 0), label = '2', alpha = 0.6) 

    #plt.plot(T, np.sum(RES_all[0][ind_groom], axis = 0), label = 'groom', alpha = 0.6)
    #plt.plot(T, np.sum(RES_all[0][np.invert(ind_groom)], axis = 0), label = 'move', alpha = 0.6) 
    
    #plt.plot(T, np.sum(RES_all[0][selec_speed], axis = 0), label = 'corrspeed', alpha = 0.6)
    #plt.plot(T, np.sum(RES_all[0][selec_no_speed], axis = 0), label = 'nocorrspeed', alpha = 0.6)
    plt.legend()
    plt.show()
    #T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)

def main_heatmap2():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''

    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    router_Black =  Router('490892905216247639','1cb6b6e3c29e45dd83f8710f2463942e')
    router_Red =  Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    router_V4607 =  Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    #router_Blue = Router('490892905216247639' ,'c486b7474b9a4713ba905be3114e9a5a') #2K
    router_Blue = Router('490892905216247639','96e389831f8a4a2fa8709224f05f61fc') #1K
    router_V4609 = Router('490892905216247639','c22b39c7b56b4e54ad2e34582d80430d') #1K 

    delta = 0.2

    L_mouse = [('V7265', router_V7265, 700), ('V4606', router_V4606, 300), ('Mouse1', router_Mouse1, 200), ('V7260', router_V7260, 0),
               ('Black', router_Black, 1200), ('Red', router_Red, 600), ('V4607', router_V4607, 0), ('Blue', router_Blue, 900),
               ('V4609', router_V4609, 0)]
    
    #L_mouse = [('Black', router_Black, 1200), ('Red', router_Red, 600), ('V4607', router_V4607, 0), ('Blue', router_Blue, 900),
    #           ('V4609', router_V4609, 0),('V4609', router_V4609, 0)]

    L_mouse = [('V7265', router_V7265, 700), ('V4606', router_V4606, 300), ('Mouse1', router_Mouse1, 200), ('V7260', router_V7260, 0)]#,
               #('V7260', router_V7260, 0)]

    #for mouse,router in [('V7265', router_V7265), ('V4606', router_V4606), ('Mouse1', router_Mouse1), ('V7260', router_V7260)]:
    it = 0
    #int_zscore = np.zeros((len(L_mouse), 40*5))
    int_zscore = np.zeros(len(L_mouse))
    int_zscore2 = np.zeros(len(L_mouse))
    it_int_zscore2 = 0
    it_int_zscore = 0

    fig, axs = plt.subplots(len(L_mouse))
    for name,router, recallage in L_mouse:

        mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        len_dim = int((mouse.tf-mouse.ti+620)/delta +1)

        try:
            RES_selected = np.load(mouse.path_mouse / 'ZZRES_selected_LTD_true.npy')
            RES_all = np.load(mouse.path_mouse / 'ZZRES_all_LTD_true.npy')/20
            RES_mid = np.load(mouse.path_mouse / 'ZZRES_mid_LTD_true.npy')/2
        except:
            RES_selected = res_to_durLTP_sepRES(router, mouse, delta, len_dim)[0]#, LTD_type='Neutral')
            RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'all')[0]
            #RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'V7260')
            RES_mid = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'mid')[0]
            #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')
            np.save(mouse.path_mouse / 'RES_selected_true.npy', RES_selected)
            np.save(mouse.path_mouse / 'RES_all_true.npy', RES_all)
            np.save(mouse.path_mouse / 'RES_mid_true.npy', RES_mid)

        print(f'shape is: {RES_all.shape}')

        #RELATED TO SPEED
        
        path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
        synchro = pd.read_csv(path, usecols=[0, 4])
        #print(synchro.index)
        #synchro = pd.to_numeric(synchro, errors='coerce')
        speed = synchro.to_numpy()
        if name == 'Black':
            speed = speed[270000:2887000]
            speed = speed.astype(float)
        if name == 'Red':
            speed = speed.astype(float)
        #length = speed.shape[0]/10000
        #print(speed.shape[0])
        speed = speed[np.arange(0, speed.shape[0], 500), :]
        speed = speed.T
        speed = speed[:, np.logical_and(speed[0]>=(mouse.ti-300+recallage), speed[0]<=(mouse.tf+320+recallage))]
        print(f'speed related {speed.shape}')
        print(RES_all.shape[1])
        T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)


        #RES_all = np.sum(RES_all, axis = 0)
        #RES_selected = np.sum(RES_selected, axis = 0)
        #RES_mid = np.sum(RES_mid, axis = 0)

    #Normalisation part
        
        pen_all = np.sum(RES_all, axis = 1)
        pen_selected = np.sum(RES_selected, axis = 1)
        pen_mid = np.sum(RES_mid, axis = 1)

        RES_all = np.sum(RES_all[pen_all.T>0]/np.array([pen_all[pen_all>0]]).T, axis = 0)
        RES_selected = np.sum(RES_selected[pen_selected.T>0]/np.array([pen_selected[pen_selected>0]]).T, axis = 0)
        RES_mid = np.sum(RES_mid[pen_mid.T>0]/np.array([pen_mid[pen_mid>0]]).T, axis = 0)

        '''########### ONE MORE TIME FOR LTD #############
        pen_all = np.sum(RES_all_LTD, axis = 1)
        pen_selected = np.sum(RES_selected_LTD, axis = 1)
        pen_mid = np.sum(RES_mid_LTD, axis = 1)

        RES_all_LTD = np.sum(RES_all_LTD[pen_all.T>0]/np.array([pen_all[pen_all>0]]).T, axis = 0)
        RES_selected_LTD = np.sum(RES_selected_LTD[pen_selected.T>0]/np.array([pen_selected[pen_selected>0]]).T, axis = 0)
        RES_mid_LTD = np.sum(RES_mid_LTD[pen_mid.T>0]/np.array([pen_mid[pen_mid>0]]).T, axis = 0)
        

        RES_all = RES_all - RES_all_LTD
        RES_selected = RES_selected - RES_selected_LTD
        '''

    #End of normalisation part
        
        RES_all = scipy.signal.savgol_filter(RES_all, 10, 1)
        RES_selected = scipy.signal.savgol_filter(RES_selected, 10, 1)
        RES_mid = scipy.signal.savgol_filter(RES_mid, 10, 1)

        #zscore_selected = scipy.stats.zscore(RES_selected)
        #zscore_all = scipy.stats.zscore(RES_all)
        #zscore_mid = scipy.stats.zscore(RES_mid)

        if name == 'Red' or name =='Blue':
            RES_selected = RES_selected[:-1]
            RES_all = RES_all[:-1]
            T = T[:-1]

        #speed_sel = scipy.stats.zscore(speed[1]) >= 2
        if name != 'Black':
            speed_sel = speed[1] > 20
            zscore_selected_speed = (RES_selected-np.mean(RES_selected[speed_sel]))/np.std(RES_selected[speed_sel])
            zscore_selected_max = (RES_selected-np.mean(RES_selected))/np.std(RES_selected)
            zscore_selected = np.minimum(zscore_selected_speed, zscore_selected_max)
            zscore_selected = zscore_selected_max

            zscore_all_speed = (RES_all-np.mean(RES_all[speed_sel]))/np.std(RES_all[speed_sel])
            zscore_all_max = (RES_all-np.mean(RES_all))/np.std(RES_all)
            zscore_all = np.minimum(zscore_all_speed, zscore_all_max)
            #zscore_all = zscore_all_max
            zscore_all = zscore_all_max
            
        else:
            zscore_selected = (RES_selected-np.mean(RES_selected))/np.std(RES_selected)
            zscore_all = (RES_all-np.mean(RES_all))/np.std(RES_all)

        int_zscore[it_int_zscore] = np.max(zscore_selected[np.logical_and(T>(mouse.ti+3), T<(mouse.ti+20))])
        #int_zscore[it_int_zscore] = np.mean(zscore_selected[np.logical_and(T>(mouse.ti), T<(mouse.ti+20))])
        #int_zscore[it_int_zscore] = np.cumsum(zscore_selected[np.logical_and(T>(mouse.ti), T<(mouse.ti+40))])
        #plt.plot(int_zscore[it_int_zscore], label = name)

        int_zscore2[it_int_zscore2] = np.max(zscore_all[np.logical_and(T>(mouse.ti+3), T<(mouse.ti+20))])

        s = 5
        
        #len_bin_cut = 3100 #for 605s , 3600 for long contact
        c_map_name = 'jet'


        #### SO IT ALL HAVE SAME SHAPE, 605s ####
        #zscore_all = zscore_all[:3100]
        #zscore_selected = zscore_selected[:3100]
        #zscore_mid = zscore_mid[:3100]
        
        #################################### OR 705s 
        #zscore_all = zscore_all[:len_bin_cut]
        #zscore_selected = zscore_selected[:len_bin_cut]        

        zscore_selected[zscore_selected<0] = 0
        zscore_selected[zscore_selected>s] = s

        zscore_all[zscore_all<0] = 0
        zscore_all[zscore_all>s] = s
        #print(int_zscore[it_int_zscore])
        it_int_zscore += 1
        it_int_zscore2 += 1
        #zscore_all = scipy.signal.savgol_filter(zscore_all, 3, 1)
        #zscore_selected = scipy.signal.savgol_filter(zscore_selected, 3, 1)
        
        
        X = np_moving_average(zscore_selected, 3)
        #X = X[(275*5):(350*5)]
        #f len(X)<len_bin_cut:
        #    print('XXX short')
        #    X = np.concatenate((X,np.zeros(len_bin_cut-len(X))))
        #X = X[np.arange(0, len(X), 5)]
        d = len(X)
        X = np.tile(X, (int(len(X)/20),1))
        X = np.concatenate((X,))

        Y = np_moving_average(zscore_all, 3)
        #Y = Y[(275*5):(350*5)]
        #f len(Y)<len_bin_cut:
        #    Y = np.concatenate((Y,np.zeros(len_bin_cut-len(Y))))
        #Y = Y[np.arange(0, len(Y), 5)]
        d = len(Y)
        Y = np.tile(Y, (int(len(Y)/20),1))
        Y = np.concatenate((Y,))

        W = np.zeros(d)
        W = np.tile(W, (int(d/100),1))

        XY = np.concatenate((X, W, Y), axis = 0)

        #print(XY.shape)
        axs[it].imshow(XY, label = 'zscore', vmin = 0, vmax = s, cmap = c_map_name) #cmap = 'seismic'
        axs[it].set_title(f'Z score, {name}')
        it += 1
        
        #plt.colorbar(orientation = 'horizontal')#, fraction=.1)

        #plt.imshow([zscore_all, zscore_mid, zscore_selected], cmap = 'seismic')
        
        #plt.plot(T, np.sum(RES_selected[0], axis = 0), label = '0', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 0], axis = 0), label = 'speed artefacts', alpha = 1)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 1], axis = 0), label = 'learning?', alpha = 1)
        #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 2], axis = 0), label = '2', alpha = 0.6)  

        #plt.plot(T, np.sum(RES_all[0][ind_groom], axis = 0), label = 'groom', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][np.invert(ind_groom)], axis = 0), label = 'move', alpha = 0.6) 
        
        #plt.plot(T, np.sum(RES_all[0][selec_speed], axis = 0), label = 'corrspeed', alpha = 0.6)
        #plt.plot(T, np.sum(RES_all[0][selec_no_speed], axis = 0), label = 'nocorrspeed', alpha = 0.6)
    m = matplotlib.cm.ScalarMappable(cmap= c_map_name)
    m.set_clim(vmin = 0, vmax = s)
    fig.colorbar(m, orientation = 'horizontal')
    print(f'FIG SIZE IS: {fig.get_size_inches()*fig.dpi}')
    plt.legend()
    plt.show()
    exit()
    print(int_zscore)
    print(int_zscore2)
    #np.save('/home/ahubert/Documents/Paper_Charlotte/data_np/max_zscore')
    plt.scatter(np.concatenate((np.ones(len(L_mouse)), np.zeros(len(L_mouse)))), np.concatenate((int_zscore, int_zscore2 )),
                c = ['green', 'lightgreen', 'black', 'green', 'lightgreen', 'lightgreen', 'red', 'black', 'red',
                     'green', 'lightgreen', 'black', 'green', 'lightgreen', 'lightgreen', 'red', 'black', 'red'])#, 'black', 'red'  ])

    #plt.title('repartition zscore int by R1')
    plt.show()
    #T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)

    
    return 0

def main_Kmeans():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''
    #name = 'Black'
    #router_V7265 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #2K
    router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    router_Black =  Router('490892905216247639','1cb6b6e3c29e45dd83f8710f2463942e')
    router_Red =  Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    router_V4607 =  Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    router_Blue = Router('490892905216247639','96e389831f8a4a2fa8709224f05f61fc') #1K 
    #router_Blue = Router('490892905216247639','c486b7474b9a4713ba905be3114e9a5a') #2K
    router_V4609 = Router('490892905216247639','c22b39c7b56b4e54ad2e34582d80430d') #1K 

    L_mouse = [('V7265', router_V7265, 700), ('V4606', router_V4606, 300), ('Mouse1', router_Mouse1, 200), ('V7260', router_V7260, 0),
            ('Black', router_Black, 1200), ('Red', router_Red, 600), ('V4607', router_V4607, 0), ('Blue', router_Blue, 900),
            ('V4609', router_V4609, 0)]

    for name, router, recallage in L_mouse:

        mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    
        #RELATED TO SPEED
        
        path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
        synchro = pd.read_csv(path, usecols=[0, 4])
        #print(synchro.index)
        #synchro = pd.to_numeric(synchro, errors='coerce')
        speed = synchro.to_numpy()
        if name == 'Black':
            speed = speed[270000:2887000]
            speed = speed.astype(float)
        if name == 'Red':
            speed = speed.astype(float)
        #length = speed.shape[0]/10000
        #print(speed.shape[0])
        speed = speed[np.arange(0, speed.shape[0], 500), :]
        speed = speed.T

        speed = speed[:, np.logical_and(speed[0]>(mouse.ti-300+recallage), speed[0]<(mouse.tf+320+recallage))]
        #speed[1] = scipy.signal.savgol_filter(speed[1], 20, 1)

        delta = 0.2
        len_dim = int((mouse.tf-mouse.ti+620)/delta +1)

        #router = router_V7265

        try:
            RES_selected = np.load(mouse.path_mouse / 'RES_selected.npy')
            RES_all = np.load(mouse.path_mouse / 'RES_all.npy')
            RES_mid = np.load(mouse.path_mouse / 'RES_mid.npy')
        except:
            RES_selected = res_to_durLTP_sepRES(router, mouse, delta, len_dim)[0]#, LTD_type='Neutral')
            RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'all')[0]
            #RES_all = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'V7260')
            RES_mid = res_to_durLTP_sepRES(router, mouse, delta, len_dim, 'mid')[0]
            #save_object(RES, f'/home/ahubert/Documents/SpikeSorting/{name}/data_analyse/t_LTP_1K')
            np.save(mouse.path_mouse / 'RES_selected.npy', RES_selected)
            np.save(mouse.path_mouse / 'RES_all.npy', RES_all)
            np.save(mouse.path_mouse / 'RES_mid.npy', RES_mid)
        #sel = selec_on_criteria(mouse, ['increase', 'mid']) 
        #RES_selected = RES_all[sel[0]]
        #RES_mid = RES_all[sel[1]]

        print(name)
        #print(RES_all.shape)
        print(len_dim)
        print(RES_all.shape)
        #ind_sparse = selec_sparse(RES_all[0])
        print( f' n zeros all is: {np.sum(np.sum(RES_all, axis = 1) == 0)/RES_all.shape[0]}')
        print( f' n zeros increase is: {np.sum(np.sum(RES_selected, axis = 1) == 0)/RES_selected.shape[0]}')
        print( f' n zeros mid is: {np.sum(np.sum(RES_mid, axis = 1) == 0)/RES_mid.shape[0]}')



        #RES_all = np.sum(RES_all, axis = 0)
        #RES_selected = np.sum(RES_selected, axis = 0)
        #RES_mid = np.sum(RES_mid, axis = 0)

    #Normalisation part
        pen_all = np.sqrt(np.sum(RES_all, axis = 1))
        Kmeans = sklearn.cluster.KMeans( n_clusters = 3)#, algorithm = 'elkan')
        ind_Kmean = Kmeans.fit_predict(RES_all[pen_all.T>0])

        RES_all = RES_all[pen_all.T>0]/np.array([pen_all[pen_all>0]]).T

    #End of normalisation Part
    
        #RES_all = scipy.signal.savgol_filter(RES_all, 3, 1)
        #RES_selected = scipy.signal.savgol_filter(RES_selected, 3, 1)
        #RES_mid = scipy.signal.savgol_filter(RES_mid, 3, 1)



        #np.save('/home/ahubert/Documents/SpikeSorting/Kmean_ind/Red_indKmean', ind_Kmean)
        #ind_Kmean = np.load('/home/ahubert/Documents/SpikeSorting/Kmean_ind/Black_indKmean.npy')
        #print(np.unique(ind_Kmean, return_counts=True))

        #print(scipy.stats.pearsonr(np.sum(RES_all[0][ind_Kmean == 1], axis = 0)[1:], speed[1]))
        #print(scipy.stats.pearsonr(np.sum(RES_all[0][ind_Kmean != 1], axis = 0)[1:], speed[1]))

        #print(scipy.stats.spearmanr(np.sum(RES_mid[0], axis = 0), speed[1]))
        #print(scipy.stats.spearmanr(np.sum(RES_selected[0], axis = 0), speed[1]))

        T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)

        max_RES = np.max(np.array([np.sum(RES_all[ind_Kmean == 0], axis = 0),
                                    np.sum(RES_all[ind_Kmean == 1], axis = 0),
                                      np.sum(RES_all[ind_Kmean == 2], axis = 0)]), axis = 0)

        if name != 'Black':
            plt.plot(speed[0]-recallage, speed[1]/np.max(speed[1])*np.max(max_RES), alpha = 0.5, label = 'mouse speed') 

        plt.vlines([mouse.ti, mouse.tf], 0, np.max(max_RES)*1.2, color = 'red', alpha = 0.7)
        
        color_plot = ['orange', 'green', 'red']
        label_plot = ['all', 'increase', 'mid']

        it=0
        
        plt.plot(T, np.sum(RES_all[ind_Kmean == 0], axis = 0), label = '0', alpha = 0.8)
        plt.plot(T, np.sum(RES_all[ind_Kmean == 1], axis = 0), label = '1', alpha = 0.8)
        #plt.fill(T, RES_selected, label = 'increase', alpha = 0.4)
        plt.plot(T, np.sum(RES_all[ind_Kmean == 2], axis = 0), label = '2', alpha = 0.8)
        plt.title(name)
        plt.legend()

        plt.show()

    return 0

############################################# SQUARE ROOT PLOT #########################################
def plot_squareroot_n():
    import matplotlib.scale as mscale
    import matplotlib.transforms as mtransforms
    import matplotlib.ticker as ticker
    
    class SquareRootScale(mscale.ScaleBase):
        """
        ScaleBase class for generating square root scale.
        """
    
        name = 'squareroot'
    
        def __init__(self, axis, **kwargs):
            # note in older versions of matplotlib (<3.1), this worked fine.
            # mscale.ScaleBase.__init__(self)

            # In newer versions (>=3.1), you also need to pass in `axis` as an arg
            mscale.ScaleBase.__init__(self, axis)
    
        def set_default_locators_and_formatters(self, axis):
            axis.set_major_locator(ticker.AutoLocator())
            axis.set_major_formatter(ticker.ScalarFormatter())
            axis.set_minor_locator(ticker.AutoMinorLocator(10))# ticker.NullLocator())
            axis.set_minor_formatter(ticker.ScalarFormatter())# NullFormatter())
    
        def limit_range_for_scale(self, vmin, vmax, minpos):
            return  max(0., vmin), vmax
    
        class SquareRootTransform(mtransforms.Transform):
            input_dims = 1
            output_dims = 1
            is_separable = True
    
            def transform_non_affine(self, a): 
                return np.array(a)**0.5
    
            def inverted(self):
                return SquareRootScale.InvertedSquareRootTransform()
    
        class InvertedSquareRootTransform(mtransforms.Transform):
            input_dims = 1
            output_dims = 1
            is_separable = True
    
            def transform(self, a):
                return np.array(a)**2
    
            def inverted(self):
                return SquareRootScale.SquareRootTransform()
    
        def get_transform(self):
            return self.SquareRootTransform()
    
    mscale.register_scale(SquareRootScale)

    fig, axs = plt.subplots(1,2)
    i_ax = 0
    max_same = 0
    for zone in ['cortex', 'striatum']:
        lenabs_mouse = ()
        for name in ['Mouse1', 'V7265', 'V4606', 'V7260']:
            print(f'Mouse: {name}')
            mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
            id_cluster = mouse.get_cluster(zone)
            print(len(id_cluster))
            if zone == 'striatum' and name == 'Mouse1':
                lenabs = np.zeros(len(id_cluster)+1)
                lenabs[-1] = max_same
            else:
                lenabs = np.zeros(len(id_cluster))
            i = 0
            for spike_id in np.unique(id_cluster):
                spikes = mouse.spikes_from_ID(spike_id)
                #lenabs[i] = np.sum(np.logical_and(spikes>(mouse.ti-300), spikes<(mouse.tf+300)))
                lenabs[i] = np.sum(np.logical_and(spikes>(mouse.ti), spikes<(mouse.tf)))+0.2
                i+=1
            lenabs_mouse += ((lenabs), )
            if zone == 'cortex':
                max_same = np.max([np.max(lenabs),max_same])

        if zone == 'cortex':
            axs[i_ax].boxplot(lenabs_mouse, labels = [f'Mouse1 n58', f'V7265 n77', f'V4606 n98', f'V7260 n77'])
        else:
            axs[i_ax].boxplot(lenabs_mouse, labels = [f'Mouse1 n74', f'V7265 n51', f'V4606 n27', f'V7260 n111'])
        axs[i_ax].set_title(zone)
        axs[i_ax].hlines(1, 0.5,4.5)
        axs[i_ax].set_yscale('squareroot')
        i_ax += 1
        plt.ylabel('absolute #spikes during contact')


    plt.show()
    return 0
    
    ##############################################################################################################

if __name__ == '__main__':
    #main_CorrCouples()
    #main_sepRes()
    #main2()
    #main_heatmap2()
    #main_Kmeans()

    name ='V7265'
    id_increase = np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase.npy')#[:193]
    #id_all = np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Mid.npy')[9]
    #id_increase = [ 590, 1005]
    mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    #start_win_corr = mouse.ti-300
    #stop_win_corr = mouse.ti -10
    #fig, axs = plt.subplots(2)
    it_ax = 0
    name_axs = ['contact', 'baseline']
    
    for time_winval in [[mouse.ti, mouse.ti + 20], [mouse.ti-300, mouse.ti-10]]:
        M = Corr.compute_Tdiff(name, time_winval, dt_win = 0.2, couples_list = id_increase)
        A = np.zeros(len(M.couples))
        for i in range(len(M.couples)):
            n_spikes = 0
            spikes = mouse.spikes_from_ID(M.couples[i][0])
            n_spikes += np.sum(np.logical_and(spikes>time_winval[0], spikes<time_winval[1]))
            spikes = mouse.spikes_from_ID(M.couples[i][1])
            n_spikes += np.sum(np.logical_and(spikes>time_winval[0], spikes<time_winval[1]))
            A[i] = len(M.dt[i])/n_spikes
        #A = A/(time_winval[1] - time_winval[0]) #Abs events version per time
        if it_ax == 0:
            sortA = np.argsort(A)
        plt.bar(np.arange(0,len(M.couples)), A[sortA], label = name_axs[it_ax] )
        it_ax += 1
    plt.legend()
    plt.ylabel('ratio of paired spikes')
    plt.xlabel('pairs')
    plt.title('comparison of #paired spikes between baseline and contact for mid actives pairs')
    plt.show()
    exit()

    for start_win_corr, stop_win_corr in [[mouse.ti - 300, mouse.ti-10], [mouse.ti, mouse.ti + 20]]:

        si_c = mouse.spikes_from_ID(id_increase[0])
        si_s = mouse.spikes_from_ID(id_increase[1])

        si_c = si_c[np.logical_and(si_c<(stop_win_corr), si_c>(start_win_corr))]
        si_s = si_s[np.logical_and(si_s<(stop_win_corr), si_s>(start_win_corr))]

        FR_increase_c = firing_rate(si_c, [start_win_corr, stop_win_corr], 0.05, 0.025)
        FR_increase_s = firing_rate(si_s, [start_win_corr, stop_win_corr], 0.05, 0.025)
        
        ind_mid = int((stop_win_corr - start_win_corr)*40)
        print(np.sum(np.convolve(FR_increase_s[0], FR_increase_c[0])[(ind_mid-8):(ind_mid+8)])/(np.sum(si_c)+np.sum(si_s)))
        axs[it_ax].bar(np.arange(-200, 200, 25)+25/2,
                np.convolve(FR_increase_s[0], FR_increase_c[0])[(ind_mid-8):(ind_mid+8)]/(stop_win_corr-start_win_corr),#/(np.sum(si_c)+np.sum(si_s)),
                25 , label = 'increase', alpha = 0.7)
        #plt.plot(np.arange(-200, 200, 25), np.convolve(FR_increase_s[0], FR_increase_c[0])[32:48], label = 'increase', color = 'red')

        si_c = mouse.spikes_from_ID(id_all[0])
        si_s = mouse.spikes_from_ID(id_all[1])

        si_c = si_c[np.logical_and(si_c<(stop_win_corr), si_c>(start_win_corr))]
        si_s = si_s[np.logical_and(si_s<(stop_win_corr), si_s>(start_win_corr))]

        FR_increase_c = firing_rate(si_c, [start_win_corr, stop_win_corr], 0.05, 0.025)
        FR_increase_s = firing_rate(si_s, [start_win_corr, stop_win_corr], 0.05, 0.025)
        print(np.sum(np.convolve(FR_increase_s[0], FR_increase_c[0])[(ind_mid-8):(ind_mid+8)])/(np.sum(si_c)+np.sum(si_s)))
        #plt.bar(np.convolve(FR_increase_s , FR_increase_c)[16:25], label = 'not increase')
        axs[it_ax].bar(np.arange(-200, 200, 25)+25/2,
                np.convolve(FR_increase_s[0], FR_increase_c[0])[(ind_mid-8):(ind_mid+8)]/(stop_win_corr-start_win_corr),#/(np.sum(si_c)+np.sum(si_s)),
                25 , label = 'all', alpha = 0.7)
        print((stop_win_corr-start_win_corr))
        ###### FOR GOOD SCALE ##########
        #axs[it_ax].bar(np.zeros(1), 500, 15 , label = 'scale', alpha = 0.7)

        plt.xlabel('shift (ms)')
        plt.ylabel('correlation / t')
        #plt.plot(np.arange(-200, 200, 25), np.convolve(FR_increase_s[0], FR_increase_c[0])[32:48], label = 'all', color = 'red')
        axs[it_ax].set_title(name_axs[it_ax])
        it_ax += 1
        plt.legend()
    #plt.vlines(mouse.ti*10, 0, 10)
    plt.show()


    '''router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    '''

    '''for router, name in [(router_V7265, 'V7265'), (router_V4606, 'V4606'), (router_Mouse1, 'Mouse1'), (router_V7260, 'V7260')]:
        print(np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase.npy'))
        print(np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_All.npy'))
        print(np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Mid.npy'))


        #mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        #increase
        result_id = around_ratio(mouse)
        np.save(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase', result_id)
        #'all':
        result_id = np.array(router.get_couple_id())
        np.save(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_All', result_id)
        #'mid':
        result_id = around_ratio(mouse, 'mid')
        np.save(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Mid', result_id)
        
    '''


    '''for name in ['V4606']:#,'V7265', 'Mouse1', 'V7260']:
        print(f'Mouse: {name}')
        FRR = np.zeros(61980)
        mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        increase_id = np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_All.npy')
        print(np.unique(increase_id))
        increase_id = [63,71]
        for spike_id in np.unique(increase_id):
            spikes = mouse.spikes_from_ID(spike_id)
            #FR = firing_rate(spikes, [mouse.ti-300, mouse.ti + 320],0.2, 0.01)
            print(len(spikes))
            if (len(spikes)/(mouse.t_bounds[1]))>26.5 :
                print(f'{spike_id} : {len(spikes)/(mouse.t_bounds[1])} FR')
    '''

    
    