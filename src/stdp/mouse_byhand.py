import numpy as np
import pandas as pd
import pickle
import stdp.Class_Analysis
#from stdp.create_mouse_data import Mouse
import matplotlib.pyplot as plt
from stdp.computation_tools import *

#####################################    COMMENTARY    ###############################
""" This code prepare data of mouse. Supposed to be executed once and save data in the good format, then later data will be load in this new format.
"""

##################################### UTILITY FUNCTIONs ###############################
def save_object(obj, file):
    try:
        with open(file, "wb") as f:
            pickle.dump(obj, f)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)

def load_object(filename):
    try:
        with open(filename, "rb") as f:
            return pickle.load(f)
    except Exception as ex:
        print("Error during unpickling object (Possibly unsupported):", ex)

#######################################################################################

def TTL_to_time(name, TTL):
    name = 'Mouse1'
    path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
    synchro = pd.read_csv(path, usecols=[0,5])
    synchro.columns = ['time', 'TTL']
    TTL_ind = np.where(synchro.TTL == 1)[0]

    print(synchro.time[TTL_ind[76]]-200)
    print(synchro.time[TTL_ind[91]]-200)
    return 0

def remove_DB(array, rf):
    '''
    remove element in array that are less espaced than rf.
    '''
    array = np.sort(array)
    new_array = array.copy()
    for it in range(len(array)-1):
        dt = array[it + 1] - array[it]
        if dt < rf:
            new_array[it+1] = 0
            array[it + 1] = array[it]
    return new_array[new_array > 0]

def test_frame_to_time(name, frame):
    path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
    synchro = pd.read_csv(path, usecols=[0,1])
    #print(synchro.index)
    
    #synchro = synchro.to_numpy()

    synchro.columns = ['time', 'frame']
    time = np.zeros(len(frame))
    it = 0
    for f in frame:
        #print(synchro['time'][np.where(synchro['frame']==f)[0]])
        time[it] = np.mean(synchro['time'][np.where(synchro['frame']==f)[0]])
        #print(synchro[:, 0][np.where(synchro[:, 1]==f)[0]])
        #time[it] = np.mean(synchro[:, 0][np.where(synchro[:, 1]==f)[0]])
        it = it+1

    return time

#######################################################################################

def check_time_contact():
    name = 'Mouse1' #200
    dec = 200
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    print(f'{name} mouse: {mouse.ti}, {mouse.tf}')
    print(f'{name}: {mouse.ti+dec}, {mouse.tf+dec}')
    print(test_frame_to_time(name, [25910, 26029]))

    name = 'Red' #600
    dec = 600
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    print(f'{name} mouse: {mouse.ti}, {mouse.tf}')
    print(f'{name}: {mouse.ti+dec}, {mouse.tf+dec}')
    print(test_frame_to_time(name, [27243, 29168]))

    name = 'V7265' #700
    dec = 700
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    print(f'{name} mouse: {mouse.ti}, {mouse.tf}')
    print(f'{name}: {mouse.ti+dec}, {mouse.tf+dec}')
    print(test_frame_to_time(name, [21257, 21361]))


    name = 'Black' #1200
    dec = 1200
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    print(f'{name} mouse: {mouse.ti}, {mouse.tf}')
    print(f'{name}: {mouse.ti+dec}, {mouse.tf+dec}')
    print(test_frame_to_time(name, [37280, 40330]))

    name = 'Blue' #900
    dec = 900
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    print(f'{name} mouse: {mouse.ti}, {mouse.tf}')
    print(f'{name}: {mouse.ti+dec}, {mouse.tf+dec}')
    print(test_frame_to_time(name, [30100, 33027]))

    name = 'V4606' #300
    dec = 300
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    print(f'{name} mouse: {mouse.ti}, {mouse.tf}')
    print(f'{name}: {mouse.ti+dec}, {mouse.tf+dec}')
    print(test_frame_to_time(name, [23000, 23245]))


#print(mouse.mouse_name)
#depot, marche, face scotch, start groom, stop groom , course, petit groom
#print(test_frame_to_time('V7260', [20190]))
#print(np.sum(np.isin(mouse.resume.Channel_ID, mouse.brain_region[0])))

#V4606
def V4606():
    #V4606
    list_clust = np.array([649,656,12,644,645,35,61,63,71,736,738,83,86,113,780,779,725,782,817,856,197,888,214,901,222,459,603,229,905,228,241,907,256,471,301,302,310,314,
    323,326,334,344,347,992,352,370,16,25,684,690,36,730,93,120,784,805,806,816,166,171,833,442,845,449,875,220,596,231,935,278,280,284,285,637,
    948,954,316,976,369,1030,634,593,15,675,53,69,81,82,713,723,839,841,217,225,902,545,287,291,952,962,324,981,482,1013,365,372,1034,85,
    668,110,728,696,748, 873,906,553,295,474,331,988,487,360,364,807,168,234,938,279,320,591,999])
    # [351 921 769] bug, not found
    #clust 780 lost 1953 DB and 1953 refractory out of 4122
    #clust 856 lost 1076 DB and 1078 refractory out of 3393
    #clust 907 lost 218 DB and 218 refractory out of 1055
    #clust 992 lost 1491 DB and 1491 refractory out of 15624
    #clust 816 lost 99 DB and 99 refractory out of 212
    #clust 833 lost 56 DB and 59 refractory out of 980
    #clust 935 lost 87 DB and 87 refractory out of 196
    #clust 948 lost 67 DB and 68 refractory out of 273
    #clust 1030 lost 31 DB and 31 refractory out of 260
    #clust 331 lost 0 DB and 21 refractory out of 23095

    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/V4606/data_KS/spike_times.npy')/30000
    spike_times = spike_times.reshape((len(spike_times,)))
    spike_clusters = np.load('/home/ahubert/Documents/SpikeSorting/V4606/data_KS/spike_clusters.npy')

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])

    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'V4606'
    brain_region = (range(0,58), range(58,384))
    interneurons = np.array([63, 71, 222, 197, 962, 1034]) #779, 725, 110, pic at start: 197, 222 , 331
    t_bounds = [0, 1200]
    contact_time = [474.33, 482.49] #[775.0, 782.49] #[901.48, 965.62]

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        #print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        FR = firing_rate(clean_times, [0, 1200], 1, 0.2 )
        '''if len(clean_times)/1200 > 15 or np.max(FR[0]) > 100:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/1200}')
            plt.plot(FR[1], FR[0])
            plt.title(f'{name}, clust {clust}')
            plt.show()'''
        
        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #INDENT

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'V4606_mouse')
    return 0


#V7265
def V7265(): #V7265
    list_clust = np.array([4,616,1091,232,636,239,657,626,179,684,672,243,411,700,362,1211,208,268,1021,196,224,1054,650,320,1064,849,747,174,324,457,1132,192,148,1045,37,160,86,111,
    378,732,628,400,826,828,11,832,970,922,1135,1434,1381,436,1352,1338,1312,1287,704,1241,1240,240,255,261,279,1149,1148,1142,1123,358,1153,685,506,540,532,
    529,1190,1248,670,158,143,1337,1388,110,1416,108,982,448,345,309,95,325,619,50,1161,183,1288,617,30,786,602,331,1187,294,968,1005,304,1036,371,527,212,
    1282,1224,489,771,1198,908,1378,638,297,1103,603,218,227,1262,1194,989,757,1441,138,100,1225,1212,1125,601,590,1203,263,575,1023,265,823,1426,1156,1136,252])
    #[168 114] bug, not found
    #clust 4 lost 4 DB and 34 refractory out of 66553
    #clust 179 lost 0 DB and 20 refractory out of 6786
    #clust 1021 lost 0 DB and 43 refractory out of 2876
    #clust 1064 lost 9 DB and 24 refractory out of 11796
    #clust 849 lost 960 DB and 961 refractory out of 4189
    #clust 1045 lost 144 DB and 144 refractory out of 937
    #clust 1148 lost 189 DB and 196 refractory out of 463
    #clust 1416 lost 49 DB and 50 refractory out of 210
    #clust 617 lost 0 DB and 10 refractory out of 7361
    #clust 30 lost 0 DB and 289 refractory out of 5720
    #clust 908 lost 98 DB and 100 refractory out of 691
    #clust 1378 lost 365 DB and 976 refractory out of 10757
    #clust 603 lost 0 DB and 146 refractory out of 9301
    #clust 218 lost 2 DB and 22 refractory out of 21396
    #clust 227 lost 0 DB and 18 refractory out of 11698
    #clust 1262 lost 68 DB and 71 refractory out of 2527
    #clust 1441 lost 15 DB and 24 refractory out of 13563
    #clust 1212 lost 611 DB and 5551 refractory out of 17126
    #clust 590 lost 0 DB and 23 refractory out of 8825

    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/V7265/data_KS/spike_times.npy')/30000
    spike_times = spike_times.reshape((len(spike_times,)))
    spike_clusters = np.load('/home/ahubert/Documents/SpikeSorting/V7265/data_KS/spike_clusters.npy')

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])

    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'V7265'
    brain_region = (range(0,181), range(200,384))
    interneurons = np.array([4]) #1262, 1378 un peu bizarres, old: 448, 218
    #218, FR max: 72.0, mean fr: 19.420909090909092
    #1262, FR max: 121.0, mean fr: 2.230909090909091
    t_bounds = [0, 1100]
    contact_time = [635.8, 642.75] #[619.78, 626.70],

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        '''FR = firing_rate(clean_times, t_bounds, 2, 0.4 )
        if len(clean_times)/(t_bounds[1]-t_bounds[0]) > 26 or np.max(FR[0]) > 200:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(t_bounds[1]-t_bounds[0])}')
            plt.plot(FR[1], FR[0])
            plt.show()'''
        

        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'V7265_mouse')
    return 0


#RED
def RED(): #RED
    list_clust = np.array([3,12,18,30,40,44,67,112,131,145,150,181,186,197,198,201,212,230,234,239,262,266,268,270,272,275,281,293,303,309,312,315,320,329,350,354,366,367,371,377,380,393,
    403,405,406,407,410,418,419,424,434,442,444,448,452,464,479,480,488,501,502,514,520,521,524,545,554,557,558,560,563,564,568,570,580,581,584,586,601,611,616,617,
    624,659,662,675,685,723,724,743,759,768,790,814,875,880,891,904,918,969,1043,1089,1103,1124,1137,1152,1430,1474,1483,1517,1529,1559,1564,1585,1599,1624,
    1625,1630,1632,1651,1669,1671,1672,1674,1690,1692,1694,1701,1709,1713,1716,1755,1756,1799,1803,25,32,49,109,148,154,155,162,184,218,242,274,278,325,332,
    351,395,400,401,408,432,435,440,445,460,528,539,561,565,594,600,612,634,635,638,663,706,714,737,740,753,755,764,793,816,818,821,827,843,852,854,898,920,935,938,
    997,1001,1105,1140,1254,1335,1369,1428,1435,1473,1496,1511,1535,1590,1597,1617,1658,1661,1673,1682,1698,1714,1715,1735,1740,1742,1743,1766,1783,1795,1802,
    1812,1815,1820,1821,1825])
    #[765,1438,226,431,1689] bug not found

    #clust 262 lost 2 DB and 19 refractory out of 7953
    #clust 524 lost 0 DB and 12 refractory out of 6082
    #clust 440 lost 0 DB and 31 refractory out of 11189
    #clust 634 lost 2 DB and 37 refractory out of 28889
    #clust 714 lost 16 DB and 18 refractory out of 1312
    #clust 1140 lost 50 DB and 50 refractory out of 110
    #clust 1428 lost 186 DB and 186 refractory out of 295

    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/Red/data_KS/spike_times.npy')/30000
    spike_times = spike_times.reshape((len(spike_times,)))
    spike_clusters = np.load('/home/ahubert/Documents/SpikeSorting/Red/data_KS/spike_clusters.npy')

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch', 'fr'])
    #fr_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/data_KS/cluster_info.tsv', sep ='\t', usecols = ['fr'])

    fr_sort = np.argsort(cluster_info['fr'])
    a = np.array([cluster_info['cluster_id'][fr_sort], cluster_info['fr'][fr_sort]] )
    #print(a[:, -10:])

    for k in list_clust:
        if len(np.where( cluster_info['cluster_id'] == k )[0]) == 0:
            print(k)

    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'Red'
    brain_region = (range(0,211), range(250,384))
    interneurons = np.array([867, 554, 634]) #np.array([418, 554, 634, 155]) OLD MANUAL SELECTION
    #clust 155, FR max: 87.0, mean fr: 18.81304347826087 weird FR

    #clust 309, FR max: 50.0, mean fr: 16.69 chan 141
    #clust 435, FR max: 68.0, mean fr: 18.16 chan 206
    #clust 440, FR max: 79.0, mean fr: 16.15 chan 209

    t_bounds = [0, 690] #t0 600
    #print(test_frame_to_time('Red', [27243,29168])-600 )
    contact_time = [533.73, 613.85]

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
            print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        #FR = firing_rate(clean_times, t_bounds, 2, 0.4 )
        #if len(clean_times)/(t_bounds[1]-t_bounds[0]) > 26 or np.max(FR[0]) > 150:
        #    print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(t_bounds[1]-t_bounds[0])}')
        #    plt.plot(FR[1], FR[0])
        #    plt.show()
    
        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'Red_mouse')

    return 0


# BLUE find ti tf with TTL
"""name = 'Blue'
path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
synchro = pd.read_csv(path, usecols=[0,5])
synchro.columns = ['time', 'TTL']
TTL_ind = np.where(synchro.TTL == 1)[0]

print(synchro.time[TTL_ind[113]]-900)
print(synchro.time[TTL_ind[138]]-900)
"""

#BLUE
def Blue(): #BLUE
    '''list_clust = np.array([3,5,6,10,17,18,23,25,29,31,34,35,36,39,40,41,42,48,49,51,53,55,57,58,59,67,68,69,70,71,76,77,84,86,88,92,94,98,102,106,108,110,129,133,
    145,159,170,183,184,193,196,204,215,218,219,225,226,228,230,232,234,266,267,268,272,290,295,297,298,359,373,383,400,401,405,411,412,414,425,429,441,442,443,448,
    459,473,486,494,495,516,533,549,552,553,568,572,576,596,598,600,614,625,631,633,636,637,37,632,4,220,595])
    #[] bug not found
    
    #list_chan_init = np.array([2,4,5,13,21,25,25,32,44,43,45,46,48,49,50,51,53,62,64,66,70,71,74,76,75,79,81,81,81,82,84,85,87,89,90,91,92,95,94,103,105,109,114,
    #                           118,145,166,181,203,205,224,224,237,249,251,251,254,256,257,261,261,265,49,53,78,81,84,196,221,232,233,18,245,77,223,181,224,243,
    #                           243,252,13,84,89,91,93,102,137,137,170,201,233,233,262,277,221,58,58,98,250,261,246,165,242,154,161,237,257,173,217,48,204,3,252,246])

    #clust 6 lost 2 DB and 14 refractory out of 31161
    #clust 18 lost 1 DB and 31 refractory out of 131097
    #clust 23 lost 0 DB and 18 refractory out of 44965
    #clust 40 lost 0 DB and 17 refractory out of 26380
    #clust 41 lost 1 DB and 145 refractory out of 99462
    #clust 48 lost 3 DB and 80 refractory out of 77768
    #clust 49 lost 2 DB and 12 refractory out of 42305
    #clust 53 lost 2 DB and 76 refractory out of 64585
    #clust 55 lost 61 DB and 134 refractory out of 80670
    #clust 57 lost 1 DB and 9 refractory out of 26932
    #clust 59 lost 0 DB and 32 refractory out of 52852
    #clust 68 lost 0 DB and 31 refractory out of 45854
    #clust 69 lost 0 DB and 71 refractory out of 41526
    #clust 70 lost 1 DB and 51 refractory out of 33459
    #clust 71 lost 31 DB and 205 refractory out of 111989
    #clust 129 lost 98 DB and 98 refractory out of 10474
    #clust 219 lost 1 DB and 8 refractory out of 14915
    #clust 266 lost 0 DB and 11 refractory out of 17815
    #clust 267 lost 0 DB and 69 refractory out of 18788
    #clust 268 lost 0 DB and 81 refractory out of 59949
    #clust 429 lost 1 DB and 54 refractory out of 70160
    #clust 441 lost 8 DB and 89 refractory out of 70039
    #clust 553 lost 0 DB and 7 refractory out of 66820
    #clust 568 lost 7 DB and 177 refractory out of 15042
    #clust 572 lost 0 DB and 15 refractory out of 4785
    #clust 633 lost 0 DB and 7 refractory out of 9652
    #clust 637 lost 0 DB and 11 refractory out of 7134'''

    list_clust = np.array([3,4,5,6,16,18,23,25,34,36,39,49,51,58,67,68,84,86,88,92,359,266,553,94,102,98,145,156,159,169,170,662,290,292,179,486,184,
                           667,549,400,193,406,297,495,204,203,669,373,215,219,414,225,226,230,516,676,533,534,425,11,9,22,641,29,41,42,48,53,55,59,
                           383,268,71,441,442,443,448,645,108,110,133,135,648,136,459,282,154,285,654,658,166,408,196,405,298,218,220,302,228,633,
                           2,260,10,261,17,24,28,35,37,40,267,57,66,69,70,427,80,642,643,97,447,109,129,651,286,162,288,289,632,637,631,595,242,307])
    
    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/Blue/data_KS/spike_times.npy')/30000
    spike_times = spike_times.reshape((len(spike_times,)))
    spike_clusters = np.load('/home/ahubert/Documents/SpikeSorting/Blue/data_KS/spike_clusters.npy')

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Blue/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'Blue'
    brain_region = (range(0,201), range(220,384))
    #interneurons = np.array([6, 18, 23, 40, 41, 48, 49, 53, 55, 57, 59, 68, 69, 70, 71, 84, 86, 268, 429, 441, 553, 633, 37]) #58 or not? 37 is limit, FR 27
    interneurons = np.array([6, 11, 18, 23, 28, 37, 40, 41, 48, 49, 58, 53, 55, 57, 59, 68, 69, 70, 71, 84, 86, 136, 166, 268, 441, 553, 633, 642]) #641, 633
    t_bounds = [0, 900] #900, 1800
    contact_time = [349.21, 470.85] #OLD [241.52, 491.61] #proxy with TTl +-10s
    

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        #if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
        #    print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        '''FR = firing_rate(clean_times, t_bounds, 2, 0.4 )
        if len(clean_times)/(t_bounds[1]-t_bounds[0]) > 26 or np.max(FR[0]) > 150:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(t_bounds[1]-t_bounds[0])}')
            plt.plot(FR[1], FR[0])
            plt.show()'''
        
        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    print(spike_times)
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'Blue_mouse')

    return 0



#BLACK
def Black(): #BLACK
    list_clust = np.array([10,8,833,19,924,935,941,88,90,958,95,102,101,100,118,109,112,160,168,169,178,181,395,203,216,222,238,241,249,250,412,1072,259,265,274,
                        1086,432,289,292,295,484,296,1103,11,835,348,20,27,895,48,904,908,352,920,927,954,103,108,111,114,122,978,125,126,367,996,998,132,140,144,141,
                        147,375,1018,163,184,182,1037,205,215,1059,224,1068,1067,228,229,1070,408,244,495,419,263,427,278,1084,280,283,288,436,480,438,298,299,
                        301,302,1101,1104,339,508,324,347,23,22,880,30,893,897,820,906,354,58,60,355,83,362,92,961,967,129,124,368,465,133,997,142,
                        372,1012,153,150,383,165,385,470,209,214,1057,400,227,239,242,1073,477,422,272,502,284,435,1087,297,439,1099,314,443,444,335,341,475,
                        940,426,326,329,567,332])

    #[135 413 279 311 225 406 281 336] bug not found

    #clust 954 lost 1 DB and 8 refractory out of 462
    #clust 140 lost 5 DB and 12 refractory out of 13730
    #clust 163 lost 0 DB and 8 refractory out of 11707
    #clust 205 lost 2 DB and 22 refractory out of 27362
    #clust 495 lost 0 DB and 13 refractory out of 13887
    #clust 263 lost 1 DB and 14 refractory out of 10399
    #clust 280 lost 0 DB and 8 refractory out of 10301
    #clust 508 lost 0 DB and 13 refractory out of 9724
    #clust 153 lost 0 DB and 13 refractory out of 3605
    #clust 239 lost 2 DB and 10 refractory out of 14826
    #clust 242 lost 0 DB and 14 refractory out of 6418
    #clust 272 lost 1 DB and 11 refractory out of 13525
    #clust 502 lost 0 DB and 7 refractory out of 9714
    #clust 332 lost 0 DB and 9 refractory out of 1080

    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/Black/data_KS/spike_times.npy')#/30000
    spike_times = spike_times.reshape((len(spike_times,)))
    spike_clusters = np.load('/home/ahubert/Documents/SpikeSorting/Black/data_KS/spike_clusters.npy')

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Black/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])

    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'Black'
    brain_region = (range(0,200), range(220,360))
    interneurons = np.array([412, 265, 140, 163, 205, 419]) #372, 163, 567  (83 OK?)

    t_bounds = [0, 900] #1200, 2100
    contact_time = [348.53, 475.35] #[524.10, 604.18]

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
            print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        
        '''FR = firing_rate(clean_times, [t_bounds[0], 600], 2, 0.4 )
        if len(clean_times)/(600) > 26 or np.max(FR[0]) > 180:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(600)}')
            plt.plot(FR[1], FR[0])
            plt.show()'''
        
        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    #save_object(mouse, mouse.path_mouse / 'data_mouse' /'Black_mouse')

    return 0


#MOUSE 1
def Mouse1(): #MOUSE 1
    list_clust = np.array([24,29,585,36,46,48,68,107,860,882,139,145,156,177,196,206,913,933,218,235,249,718,1072,380,384,390,391,396,659,1101,408,417,667,420,
                        422,426,731,429,1114,437,445,466,465,468,485,486,683,1155,501,513,1160,1153,1166,1187,11,584,785,44,800,72,81,593,85,816,834,596,836,
                        874,167,905,190,930,939,941,261,269,969,273,978,296,294,999,1012,1018,1043,1056,637,640,357,1090,370,1091,646,648,378,383,382,1097,
                        1102,1103,436,1140,1143,498,688,1159,514,555,1190,755,779,810,862,871,133,148,912,717,267,1020,333,1069,1086,371,654,1099,403,
                        401,424,446,682,526,1156,1184])

    #[] bug not found

    #clust 860 lost 292 DB and 292 refractory out of 897
    #clust 882 lost 143 DB and 143 refractory out of 370
    #clust 933 lost 132 DB and 132 refractory out of 349
    #clust 1072 lost 529 DB and 529 refractory out of 1118
    #clust 1114 lost 411 DB and 411 refractory out of 10399
    #clust 513 lost 0 DB and 8 refractory out of 8842
    #clust 1160 lost 2476 DB and 2810 refractory out of 9749
    #clust 1153 lost 397 DB and 397 refractory out of 982
    #clust 1166 lost 1175 DB and 1175 refractory out of 2823
    #clust 1187 lost 155 DB and 155 refractory out of 336
    #clust 785 lost 13 DB and 13 refractory out of 65
    #clust 800 lost 49 DB and 50 refractory out of 630
    #clust 874 lost 84 DB and 85 refractory out of 248
    #clust 167 lost 3 DB and 12 refractory out of 6681
    #clust 930 lost 971 DB and 973 refractory out of 3918
    #clust 261 lost 1 DB and 12 refractory out of 11119
    #clust 999 lost 216 DB and 244 refractory out of 517
    #clust 640 lost 0 DB and 9 refractory out of 9766
    #clust 357 lost 1 DB and 21 refractory out of 30833
    #clust 370 lost 1 DB and 25 refractory out of 15091
    #clust 1102 lost 5 DB and 50 refractory out of 21224
    #clust 1140 lost 342 DB and 350 refractory out of 6191
    #clust 688 lost 0 DB and 10 refractory out of 5116
    #clust 1159 lost 2 DB and 9 refractory out of 3323
    #clust 555 lost 370 DB and 372 refractory out of 6992
    #clust 862 lost 538 DB and 545 refractory out of 2750
    #clust 871 lost 119 DB and 276 refractory out of 534
    #clust 133 lost 0 DB and 6 refractory out of 7722
    #clust 912 lost 2 DB and 37 refractory out of 12948
    #clust 333 lost 0 DB and 11 refractory out of 21774
    #clust 1069 lost 2 DB and 34 refractory out of 13051
    #clust 371 lost 0 DB and 6 refractory out of 6574
    #clust 446 lost 1 DB and 27 refractory out of 9972
    #clust 1156 lost 508 DB and 516 refractory out of 2356
    #clust 1184 lost 91 DB and 91 refractory out of 740

    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/Mouse1/data_KS/spike_times.npy')/30000
    spike_times = spike_times.reshape((len(spike_times,)))
    spike_clusters = np.load('/home/ahubert/Documents/SpikeSorting/Mouse1/data_KS/spike_clusters.npy')

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Mouse1/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])

    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'Mouse1'
    brain_region = (range(0,200), range(200,383))
    interneurons = np.array([139, 930]) #
    #clust 426, FR max: 268.0, mean fr: 18.9
    #clust 930, FR max: 329.0, mean fr: 2.4

    t_bounds = [0, 1200] #200, 1400
    contact_time = [661.89, 665.85] #

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
            print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        
        FR = firing_rate(clean_times, [t_bounds[0], 1200], 2, 0.4 )
        if len(clean_times)/(1200) > 26 or np.max(FR[0]) > 180:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(1200)}')
            plt.plot(FR[1], FR[0])
            plt.show()
        
        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'Mouse1_mouse')

    return 0


def V4607(): #V4607
    list_clust = np.array([633,696,572,30,255,316,702,711,778,267,91,513,98,105,836,115,118,124,279,142,147,289,166,867,869,887,895,902,906,
                           918,235,927,633,649,477,673,683,685,36,708,42,712,718,52,62,263,771,774,86,266,581,513,832,837,114,863,167,169,
                           177,878,882,900,213,215,926,604,609,642,644,691,39,43,590,53,59,61,765,760,775,783,791,796,804,826,125,140,852,904,
                           912,917,928])
    #BUG NOT FOUND: 648
    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/V4607/data_KS/spike_times.npy')/30000
    spike_clusters = np.array([np.load('/home/ahubert/Documents/SpikeSorting/V4607/data_KS/spike_clusters.npy')]).T
    spike_clusters = spike_clusters[spike_times < 1200]

    spike_times = spike_times[spike_times < 1200]
    spike_times = spike_times.reshape((len(spike_times,)))


    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4607/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
    
    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'V4607'
    brain_region = (range(0,85), range(90,383))
    interneurons = np.array([167])

    t_bounds = [0,1200]
    contact_time = [691.12, 818.34]

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
            print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        
        '''FR = firing_rate(clean_times, [t_bounds[0], 1200], 2, 0.4 )
        if len(clean_times)/(1200) > 26 or np.max(FR[0]) > 180:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(1200)}')
            plt.plot(FR[1], FR[0])
            plt.show()'''

        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'V4607_mouse')

    return 0


def V7260(): #V7260
    list_clust = np.array([1453,28,41,42,839,1476,75,1502,855,87,97,1560,1817,222,921,1846,1862,1975,1977,998,1963,336,126,1028,1882,515,516,519,
                           524,544,563,564,1121,598,1135,1651,1653,619,623,626,637,625,1663,650,684,679,692,713,723,1689,1722,
                           1452,1435,6,1459,1983,1985,48,70,1503,84,1505,1523,865,1563,1990,2016,1270,146,167,178,1782,1792,1795,2017,918,1278,
                           1839,930,1850,1869,277,2021,2022,1978,310,1976,1974,1964,335,1016,1019,1932,1925,1916,1055,1904,2029,480,479,1596,1086,
                           487,1087,512,1604,1103,1609,1104,1618,1111,1623,573,1629,572,571,1129,596,1130,1131,627,645,675,710,1178,698,1241,1735,
                           732,1778,1446,17,828,1982,1462,833,32,1475,1485,850,852,1531,1541,104,110,2015,1396,1584,888,905,1827,1831,2018,
                           1836,928,236,1847,1863,1876,2020,276,291,2023,312,1300,2026,1001,1005,1955,1961,1047,1903,2028,468,470,1589,1593,
                           478,1325,1602,523,1100,533,538,1105,1620,559,1119,1647,576,592,1650,1143,1655,1147,647,666,1672,1678,1779,1183,
                           1706,1714,661])
    
    #Bug NOT FOUND: [1483, 649, 1375, 1887, 531, 78, 1851, 452, 1745] #juste pas beau 1763
    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/V7260/data_KS/spike_times.npy')/30000
    spike_clusters = np.array([np.load('/home/ahubert/Documents/SpikeSorting/V7260/data_KS/spike_clusters.npy')]).T
    spike_clusters = spike_clusters[spike_times < 1500]

    spike_times = spike_times[spike_times < 1500]
    spike_times = spike_times.reshape((len(spike_times,)))


    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7260/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
    
    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'V7260'
    brain_region = (range(0,210), range(230,383))
    interneurons = np.array([75])

    t_bounds = [0,1500]
    contact_time = [1345.94, 1348.27]

    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    for clust in list_clust:
        len_init = len(spike_times[spike_clusters == clust])
        clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        #if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
        #    print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        
        '''FR = firing_rate(clean_times, [t_bounds[0], 1200], 2, 0.4 )
        if len(clean_times)/(1200) > 26 or np.max(FR[0]) > 180:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(1200)}')
            plt.plot(FR[1], FR[0])
            plt.show()'''

        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'V7260_mouse')

    return 0


def V4609():
    list_clust = np.array([3,15,636,638,641,642,37,51,52,53,66,70,80,83,439,87,442,88,666,99,107,672,127,134,699,142,146,175,719,186,195,200,207,
                           217,226,500,232,235,243,244,262,276,279,313,321,357,28,410,20,35,658,420,48,660,423,575,428,61,429,94,440,91,98,662,
                           96,105,670,680,128,463,465,700,704,706,472,708,710,170,477,173,183,720,721,203,208,489,210,211,214,491,219,494,228,727,734,
                           247,269,273,278,284,300,308,321,322,330,533,342,347,537,538,541,552,756,757,381])
    #620 bug
    
    spike_times = np.load('/home/ahubert/Documents/SpikeSorting/V4609/data_KS/spike_times.npy')#/30000
    spike_clusters = np.array([np.load('/home/ahubert/Documents/SpikeSorting/V4609/data_KS/spike_clusters.npy')]).T

    spike_times = spike_times.reshape((len(spike_times,)))

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4609/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
    
    for k in list_clust:
        if np.sum(cluster_info['cluster_id'] == k ) == 0:
            print(k)

    indices = [ np.where( cluster_info['cluster_id'] == k )[0][0] for k in list_clust]
    list_chan = np.array([ cluster_info['ch'][k] for k in indices ])

    resume = pd.DataFrame([list_clust, list_chan])
    resume = resume.transpose()
    resume.columns = ['Cluster_ID', 'Channel_ID']

    name = 'V4609'
    brain_region = (range(0,45), range(50,383))
    interneurons = np.array([276, 494, 330, 342])
    #clust 276, FR max: 112.0, mean fr: 39.0125
    #clust 494, FR max: 94.0, mean fr: 27.525
    #clust 330, FR max: 342.0, mean fr: 73.46083333333333
    #clust 342, FR max: 397.0, mean fr: 6.818333333333333 #max not where we look...
    print(f'tbound: {np.min(spike_times)}, {np.max(spike_times)}')
    t_bounds = [0,2192]
    contact_time = [641, 1120.5]
    
    new_times = np.array([])
    new_clust = np.array([])
    new_chan = np.array([])

    spike_clusters = spike_clusters.reshape((len(spike_clusters), ))
    
    for clust in list_clust:
        print(clust)
        #len_init = len(spike_times[spike_clusters == clust])
        #clean_times_DB = remove_DB(spike_times[spike_clusters == clust], 0.0002)
        clean_times = remove_DB(spike_times[spike_clusters == clust], 0.0015)
        #if len_init - len(clean_times_DB)>5 or (len_init - len(clean_times))>5:
        #    print(f'clust {clust} lost {len_init - len(clean_times_DB)} DB and {len_init - len(clean_times)} refractory out of {len_init}')
        
        '''FR = firing_rate(clean_times, [t_bounds[0], 1200], 2, 0.4 )
        if len(clean_times)/(1200) > 26 or np.max(FR[0]) > 180:
            print(f'clust {clust}, FR max: {np.max(FR[0])}, mean fr: {len(clean_times)/(1200)}')
            plt.plot(FR[1], FR[0])
            plt.show()
        '''
        #print(f'cluster: {clust}, lost {len(spike_times[spike_clusters == clust]) - len(clean_times)} spikes' )
        clean_clust = np.array([clust]*len(clean_times))
        #print(np.sum(list_clust == clust))
        clean_chan = np.array([list_chan[list_clust == clust][0]]*len(clean_times))

        new_times = np.concatenate((new_times, clean_times))
        new_clust = np.concatenate((new_clust, clean_clust))
        new_chan = np.concatenate((new_chan, clean_chan))
        #indent

    new_clust = new_clust[np.argsort(new_times)]
    new_chan = new_chan[np.argsort(new_times)]
    new_times = np.sort(new_times)

    spike_times = pd.DataFrame([new_times, new_clust, new_chan])
    spike_times = spike_times.transpose()
    spike_times.columns = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    spike_times.Cluster_ID.astype(int)
    spike_times.Channel_ID.astype(int)


    mouse = Mouse( name, spike_times, resume, brain_region, interneurons, t_bounds, contact_time )
    save_object(mouse, mouse.path_mouse / 'data_mouse' /'V4609_mouse')


def mouse_to_csv(list_mouse):
    for name in list_mouse:#['Blue', 'V4607']:#['V7265','V4606', 'Red', 'Blue', 'Black', 'Mouse1']:
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        mouse.resume = mouse.resume.loc()[np.invert(np.isin(mouse.resume.Cluster_ID, mouse.interneurons))]
        mouse.resume.to_csv(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_resume.csv', sep = ',')
        DF=pd.DataFrame(mouse.interneurons)
        DF.to_csv(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_interneurons.csv', sep = ',')
        mouse.spike_time = mouse.spike_times.loc()[np.invert(np.isin(mouse.spike_times.Cluster_ID, mouse.interneurons))]
        mouse.spike_times.to_csv(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_spike_times.csv', sep = ',')
        print(f'{name}, ti = {mouse.ti}, tf = {mouse.tf}')

    return 0

def clean_sol(router: Router):
    list_couples = router.get_couple_id()
    for couple in list_couples:
        y = router.load_solution_couple(couple)
        if y.shape[0] > 10:
            y = y[24:27, :]
            np.save(router.get_artifacts_dir / f'sol_{int(couple[0])}_{int(couple[1])}' , y)
            LOGGER.info('Saved  res_%s', couple)
        else:
            LOGGER.info('no need to clean %s', couple)

    return 0
