import numpy as np
import pandas as pd
from stdp.create_mouse_data import Mouse
from Class_Analysis import Router
from computation_tools import *
from data_analyse import load_object, save_object
import stdp.Class_Correlation as Corr
#from stdp.plot_center import plot_ID

import warnings
warnings.filterwarnings("ignore")

'''
router_red = Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
mouse_red = load_object(f'/home/ahubert/Documents/SpikeSorting/Red/data_mouse/Red_mouse')

router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f')
mouse_V4606 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4606/data_mouse/V4606_mouse')

router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f')
mouse_V7265 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')

router_Mouse1 = Router('490892905216247639','c289df2d94674edb9c16c64e32711432')
mouse_Mouse1 = load_object(f'/home/ahubert/Documents/SpikeSorting/Mouse1/data_mouse/Mouse1_mouse')

router_V4607 = Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
mouse_V4607 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4607/data_mouse/V4607_mouse')
'''

def create_Tdiff(name, time_win):
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')

    cortex_list = mouse.get_cluster('cortex')
    striatum_list = mouse.get_cluster('striatum')
    it = 0
    T_diff = [(cortex_list, striatum_list),()]

    for id_cortex in cortex_list:
        spikes_cortex = mouse.spikes_from_ID(id_cortex)
        spikes_cortex = spikes_cortex[np.logical_and(spikes_cortex< time_win[1], spikes_cortex> time_win[0])]
        for id_striatum in striatum_list:       
            spikes_striatum = mouse.spikes_from_ID(id_striatum)
            spikes_striatum = spikes_striatum[ np.logical_and(spikes_striatum< time_win[1], spikes_striatum> time_win[0]) ]
            t_corr = ()

            for t in spikes_striatum:
                t_diff = spikes_cortex - t
                t_diff = t_diff[np.abs(t_diff) < 0.015]
                t_corr += (*t_diff , )
            t_corr = np.array(t_corr)
            T_diff[1] += (t_corr, )
            it += 1

    #save_object(T_diff, f'/home/ahubert/Documents/Code_STDP/model-potcadep/data/hebian_corr/T_diff_{name}')
    return T_diff

def compute_dt_pair(mouse: Mouse, time_win):
    '''
    Wee look at ratio of paired spike
    '''
    name = mouse.mouse_name
    it = 0
    #T_diff = load_object(f'/home/ahubert/Documents/Code_STDP/model-potcadep/data/hebian_corr/T_diff_{name}')
    T_diff = create_Tdiff(name, time_win)
    cortex_list, striatum_list = T_diff[0]
    dt_pair = np.zeros((len(cortex_list)*len(striatum_list), 5))
    ind_real = np.zeros(len(cortex_list)*len(striatum_list))

    for id_cortex in cortex_list:
        cortex_spikes = mouse.spikes_from_ID(int(id_cortex))
        cortex_spikes = cortex_spikes[np.logical_and(cortex_spikes < time_win[1], cortex_spikes > time_win[0])]
        n_cortex = len(cortex_spikes)
        for id_striatum in striatum_list:
            t_corr = T_diff[1][it]
            striatum_spikes = mouse.spikes_from_ID(int(id_striatum))
            striatum_spikes = striatum_spikes[np.logical_and(striatum_spikes < time_win[1], striatum_spikes > time_win[0])]
            n_striatum = len(striatum_spikes)

            dt_pair[it, 0] = np.sum( np.logical_and( t_corr<0.2, t_corr>0.0001) )
            dt_pair[it, 1] = np.sum( np.logical_and( t_corr>-0.2, t_corr<-0.0001) )
            dt_pair[it, 2] = np.sum( np.logical_and( t_corr<0.2, t_corr>-0.0001) )

            '''dt_pair[it, 0] = np.sum( np.logical_and( t_corr<0.015, t_corr>0.0001) )
            dt_pair[it, 1] = np.sum( np.logical_and( t_corr>-0.015, t_corr<-0.0001) )
            dt_pair[it, 2] = np.sum( np.logical_and( t_corr<0.0001, t_corr>-0.0001) )'''
            dt_pair[it, 3] = n_cortex
            dt_pair[it, 4] = n_striatum
            it +=1

    return dt_pair

def plot_diff_dt_pair(list_name):

    ratio_pos_cortex_mouse = ()
    ratio_neg_cortex_mouse = ()
    ratio_pos_striatum_mouse = ()
    ratio_neg_striatum_mouse = ()

    for it_mouse, name in enumerate(list_name):
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        time_win_before = [mouse.ti-205, mouse.ti-5]
        time_win_after = [mouse.ti-5, mouse.ti + 20]
        dt_pair_before = compute_dt_pair(mouse, time_win_before)
        dt_pair_after = compute_dt_pair(mouse, time_win_after)

        #ind_before = np.invert(np.logical_or(dt_pair_before[:, 3] == 0, dt_pair_before[:, 4] == 0))
        #ind_after = np.invert(np.logical_or(dt_pair_after[:, 3] == 0, dt_pair_after[:, 4] == 0))

        ratio_pos_cortex = (dt_pair_before[:, 0]/dt_pair_before[:, 3]/
                            dt_pair_after[:, 0]*dt_pair_after[:, 3])
        ratio_neg_cortex = (dt_pair_before[:, 1]/dt_pair_before[:, 3]/
                            dt_pair_after[:, 1]*dt_pair_after[:, 3])
        
        ratio_pos_striatum = (dt_pair_before[:, 0]/dt_pair_before[:, 4]/
                            dt_pair_after[:, 0]*dt_pair_after[:, 4])
        ratio_neg_striatum = (dt_pair_before[:, 1]/dt_pair_before[:, 4]/
                            dt_pair_after[:, 1]*dt_pair_after[:, 4])
        
        ratio_pos_cortex = ratio_pos_cortex[np.isfinite(ratio_pos_cortex)]
        ratio_neg_cortex = ratio_neg_cortex[np.isfinite(ratio_neg_cortex)]
        ratio_pos_striatum = ratio_pos_striatum[np.isfinite(ratio_pos_striatum)]
        ratio_neg_striatum = ratio_neg_striatum[np.isfinite(ratio_neg_striatum)]
        ratio_pos_cortex_mouse += (ratio_pos_cortex, )
        ratio_neg_cortex_mouse += (ratio_neg_cortex, )
        ratio_pos_striatum_mouse += (ratio_pos_striatum, )
        ratio_neg_striatum_mouse += (ratio_neg_striatum, )

    fig, axs = plt.subplots(2,2)
    axs[0,0].boxplot(ratio_pos_cortex_mouse)
    axs[0,0].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[0,0].set_title('ratio_pos_cortex_mouse')
    axs[0,1].boxplot(ratio_neg_cortex_mouse)
    axs[0,1].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[0,1].set_title('ratio_neg_cortex_mouse')
    axs[1,0].boxplot(ratio_pos_striatum_mouse)
    axs[1,0].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[1,0].set_title('ratio_pos_striatum_mouse')
    axs[1,1].boxplot(ratio_neg_striatum_mouse)
    axs[1,1].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[1,1].set_title('ratio_neg_striatum_mouse')
    fig.suptitle(f'ratio ratio of paired over nspikes')
    plt.show()

    return 0

def plot_diff_dt_pair_real(list_name):

    ratio_pos_cortex_mouse = ()
    ratio_neg_cortex_mouse = ()
    ratio_pos_striatum_mouse = ()
    ratio_neg_striatum_mouse = ()

    for it_mouse, name in enumerate(list_name):
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        time_win_before = [mouse.ti-305, mouse.ti-5]
        time_win_after = [mouse.ti, mouse.tf + 60]

        #Part related to real pair
        list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
        list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
        real_pair = list.to_numpy()
        check_real = real_pair[:, 0]**3 + real_pair[:, 1]**3

        cortex_list = mouse.get_cluster('cortex')
        striatum_list = mouse.get_cluster('striatum')

        dt_pair_before = compute_dt_pair(mouse, time_win_before)
        dt_pair_after = compute_dt_pair(mouse, time_win_after)

        #ind_before = np.invert(np.logical_or(dt_pair_before[:, 3] == 0, dt_pair_before[:, 4] == 0))
        #ind_after = np.invert(np.logical_or(dt_pair_after[:, 3] == 0, dt_pair_after[:, 4] == 0))

        it = 0
        ind_real = np.zeros(len(cortex_list)*len(striatum_list), dtype = bool)
        for id_cortex in cortex_list:
            for id_striatum in striatum_list:
                if np.isin(id_cortex**3+id_striatum**3, check_real):
                    ind_real[it] = True
                it +=1

        dt_pair_after = dt_pair_after[ind_real, :]
        dt_pair_before = dt_pair_before[ind_real, :]
        
        ratio_pos_cortex = (dt_pair_after[:, 0]/dt_pair_after[:, 3]/
                            dt_pair_before[:, 0]*dt_pair_before[:, 3])
        ratio_neg_cortex = (dt_pair_after[:, 1]/dt_pair_after[:, 3]/
                            dt_pair_before[:, 1]*dt_pair_before[:, 3])
        
        ratio_pos_striatum = (dt_pair_after[:, 0]/dt_pair_after[:, 4]/
                              dt_pair_before[:, 0]*dt_pair_before[:, 4])
        ratio_neg_striatum = (dt_pair_after[:, 1]/dt_pair_after[:, 4]/
                              dt_pair_before[:, 1]*dt_pair_before[:, 4])
        
        TTest_pos_cortex_after =dt_pair_after[:, 0]/dt_pair_after[:, 3]
        TTest_pos_cortex_before = dt_pair_before[:, 0]/dt_pair_before[:, 3]
        TTest_neg_cortex_after = dt_pair_after[:, 1]/dt_pair_after[:, 3]
        TTest_neg_cortex_before = dt_pair_before[:, 1]/dt_pair_before[:, 3]
        TTest_pos_striatum_after = dt_pair_after[:, 0]/dt_pair_after[:, 4]
        TTest_pos_striatum_before = dt_pair_before[:, 0]/dt_pair_before[:, 4]
        TTest_neg_striatum_after = dt_pair_after[:, 1]/dt_pair_after[:, 4]
        TTest_neg_striatum_before = dt_pair_before[:, 1]/dt_pair_before[:, 4]

        selec_pos_cortex = np.logical_and(np.isfinite(TTest_pos_cortex_after),np.isfinite(TTest_pos_cortex_before))
        selec_neg_cortex = np.logical_and(np.isfinite(TTest_neg_cortex_after),np.isfinite(TTest_neg_cortex_before))
        TTest_pos_cortex_before = TTest_pos_cortex_before[selec_pos_cortex]
        TTest_pos_cortex_after = TTest_pos_cortex_after[selec_pos_cortex]
        TTest_neg_cortex_before = TTest_neg_cortex_before[selec_neg_cortex]
        TTest_neg_cortex_after = TTest_neg_cortex_after[selec_neg_cortex]
        TTest_pos_striatum_before = TTest_pos_striatum_before[np.isfinite(TTest_pos_striatum_after)]
        TTest_pos_striatum_after = TTest_pos_striatum_after[np.isfinite(TTest_pos_striatum_after)]
        TTest_neg_striatum_before = TTest_neg_striatum_before[np.isfinite(TTest_neg_striatum_after)]
        TTest_neg_striatum_after = TTest_neg_striatum_after[np.isfinite(TTest_neg_striatum_after)]

        #plt.hist(TTest_pos_cortex_before, alpha = 0.5)
        #plt.hist(TTest_pos_cortex_after, alpha = 0.5)
        #plt.show()
        print(f'{name}')
        print(np.sum(np.invert(np.isfinite(TTest_neg_cortex_after))))
        print(np.sum(np.invert(np.isfinite(TTest_neg_cortex_before))))

        TTest_pos_cortex = stats.ttest_ind(TTest_pos_cortex_after, TTest_pos_cortex_before)
        print(f'stat: {TTest_pos_cortex.statistic}, pvalue: {TTest_pos_cortex.pvalue}')

        TTest_neg_cortex = stats.ttest_ind(TTest_neg_cortex_after, TTest_neg_cortex_before)
        print(f'stat: {TTest_neg_cortex.statistic}, pvalue: {TTest_neg_cortex.pvalue}')

        TTest_pos_striatum = stats.ttest_ind(TTest_pos_striatum_after, TTest_pos_striatum_before)
        print(f'stat: {TTest_pos_striatum.statistic}, pvalue: {TTest_pos_striatum.pvalue}')

        TTest_neg_striatum = stats.ttest_ind(TTest_neg_striatum_after, TTest_neg_striatum_before)
        print(f'stat: {TTest_neg_striatum.statistic}, pvalue: {TTest_neg_striatum.pvalue}')

        #print(Mouse)

        ratio_pos_cortex = ratio_pos_cortex[np.isfinite(ratio_pos_cortex)]
        ratio_neg_cortex = ratio_neg_cortex[np.isfinite(ratio_neg_cortex)]
        ratio_pos_striatum = ratio_pos_striatum[np.isfinite(ratio_pos_striatum)]
        ratio_neg_striatum = ratio_neg_striatum[np.isfinite(ratio_neg_striatum)]
        ratio_pos_cortex_mouse += (ratio_pos_cortex, )
        ratio_neg_cortex_mouse += (ratio_neg_cortex, )
        ratio_pos_striatum_mouse += (ratio_pos_striatum, )
        ratio_neg_striatum_mouse += (ratio_neg_striatum, )

    fig, axs = plt.subplots(2,2)
    axs[0,0].boxplot(ratio_pos_cortex_mouse, showmeans = True)
    axs[0,0].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[0,0].set_title('ratio_pos_cortex_mouse')
    axs[0,0].hlines([1], 0.5, len(list_name)+0.5)
    axs[0,1].boxplot(ratio_neg_cortex_mouse, showmeans = True)
    axs[0,1].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[0,1].set_title('ratio_neg_cortex_mouse')
    axs[0,1].hlines([1], 0.5, len(list_name)+0.5)
    axs[1,0].boxplot(ratio_pos_striatum_mouse, showmeans = True)
    axs[1,0].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[1,0].set_title('ratio_pos_striatum_mouse')
    axs[1,0].hlines([1], 0.5, len(list_name)+0.5)
    axs[1,1].boxplot(ratio_neg_striatum_mouse, showmeans = True)
    axs[1,1].set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    axs[1,1].hlines([1], 0.5, len(list_name)+0.5)
    axs[1,1].set_title('ratio_neg_striatum_mouse')
    fig.suptitle(f'ratio ratio of paired over nspikes, correlated pair')
    plt.show()

    return 0

def plot_diff_dt_pair_real2(list_name):

    ratio_mouse = ()
    ratio_mouse_real = ()

    for it_mouse, name in enumerate(list_name):
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        time_win_before = [mouse.ti-205, mouse.ti-5]
        time_win_after = [mouse.ti-5, mouse.tf + 5]

        '''#Part related to real pair
        list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
        list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
        real_pair = list.to_numpy()
        check_real = real_pair[:, 0]**3 + real_pair[:, 1]**3 '''

        real_pair = mouse.get_correlated_couples('Post')
        real_pair = real_pair[real_pair['Keep'] == 1]
        check_real = real_pair['Striatum_ID']**3+real_pair['Cortex_ID']**3

        cortex_list = mouse.get_cluster('cortex')
        striatum_list = mouse.get_cluster('striatum')

        dt_pair_before = compute_dt_pair(mouse, time_win_before)
        dt_pair_after = compute_dt_pair(mouse, time_win_after)

        #ind_before = np.invert(np.logical_or(dt_pair_before[:, 3] == 0, dt_pair_before[:, 4] == 0))
        #ind_after = np.invert(np.logical_or(dt_pair_after[:, 3] == 0, dt_pair_after[:, 4] == 0))

        it = 0
        ind_real = np.zeros(len(cortex_list)*len(striatum_list), dtype = bool)

        for id_cortex in cortex_list:
            for id_striatum in striatum_list:
                if np.isin(id_cortex**3+id_striatum**3, check_real):
                    ind_real[it] = True
                it +=1



        '''M_after = (dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])
        M_before = (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])/(dt_pair_before[:, 3]+dt_pair_before[:, 4])
        print(np.sum(np.invert(np.isfinite(M_before))))
        print(np.sum(np.invert(np.isfinite(M_after))))
        M_before = M_before[np.isfinite(M_after)]
        M_after = M_after[np.isfinite(M_after)]
        M_stat = stats.ttest_rel(M_before, M_after)
        print(f'{name} \n, stat:{M_stat.statistic}, pvalue: {M_stat.pvalue}')

        #plt.scatter( M_before, M_after  )
        #plt.title(f'{name}')
        #plt.show()'''

        ratio = ((dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])/
                        (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])*(dt_pair_before[:, 3]+dt_pair_before[:, 4]))
        
        ratio = ratio[np.isfinite(ratio)]
        ratio_mouse += (ratio, )

        dt_pair_after = dt_pair_after[ind_real, :]
        dt_pair_before = dt_pair_before[ind_real, :]
    
        ratio_real = ((dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])/
                 (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])*(dt_pair_before[:, 3]+dt_pair_before[:, 4]))
        
        ratio_real = ratio_real[np.isfinite(ratio_real)]
        ratio_mouse_real += (ratio_real, )

    fig, (ax0, ax1) = plt.subplots(1, 2)
    ax0.boxplot(ratio_mouse, showmeans = True)
    ax0.hlines([1], 0.5, len(list_name)+0.5)
    ax0.set_title('All pairs')
    ax0.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    ax1.boxplot(ratio_mouse_real, showmeans = True)
    ax1.hlines([1], 0.5, len(list_name)+0.5)
    ax1.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    ax1.set_title('Correlated pairs')
    fig.suptitle('Variation of ratio paired spikes')
    plt.show()

    return 0

def plot_diff_dt_pair_real3(list_name):
    ''' Same as other but with 3 plot for 3 groups'''
    ratio_mouse = ()
    ratio_mouse_post = ()
    ratio_mouse_baseline = ()

    for it_mouse, name in enumerate(list_name):
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        time_win_before = [mouse.ti-305, mouse.ti-5]
        time_win_after = [mouse.ti, mouse.ti + 20]

        '''#Part related to real pair
        list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
        list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
        real_pair = list.to_numpy()
        check_real = real_pair[:, 0]**3 + real_pair[:, 1]**3 '''

        real_baseline = mouse.get_correlated_couples('Baseline')
        real_baseline = real_baseline[real_baseline['Keep'] == 1]
        check_baseline = real_baseline['Striatum_ID']**3+real_baseline['Cortex_ID']**3

        real_post = mouse.get_correlated_couples('Post')
        real_post = real_post[real_post['Keep'] == 1]
        check_post = real_post['Striatum_ID']**3+real_post['Cortex_ID']**3

        cortex_list = mouse.get_cluster('cortex')
        striatum_list = mouse.get_cluster('striatum')

        dt_pair_before = compute_dt_pair(mouse, time_win_before)
        dt_pair_after = compute_dt_pair(mouse, time_win_after)

        #ind_before = np.invert(np.logical_or(dt_pair_before[:, 3] == 0, dt_pair_before[:, 4] == 0))
        #ind_after = np.invert(np.logical_or(dt_pair_after[:, 3] == 0, dt_pair_after[:, 4] == 0))

        it = 0
        ind_baseline = np.zeros(len(cortex_list)*len(striatum_list), dtype = bool)
        for id_cortex in cortex_list:
            for id_striatum in striatum_list:
                if np.isin(id_cortex**3+id_striatum**3, check_baseline):
                    ind_baseline[it] = True
                it +=1

        it = 0
        ind_post = np.zeros(len(cortex_list)*len(striatum_list), dtype = bool)
        for id_cortex in cortex_list:
            for id_striatum in striatum_list:
                if np.isin(id_cortex**3+id_striatum**3, check_post):
                    ind_post[it] = True
                it +=1


        '''M_after = (dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])
        M_before = (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])/(dt_pair_before[:, 3]+dt_pair_before[:, 4])
        print(np.sum(np.invert(np.isfinite(M_before))))
        print(np.sum(np.invert(np.isfinite(M_after))))
        M_before = M_before[np.isfinite(M_after)]
        M_after = M_after[np.isfinite(M_after)]
        M_stat = stats.ttest_rel(M_before, M_after)
        print(f'{name} \n, stat:{M_stat.statistic}, pvalue: {M_stat.pvalue}')

        #plt.scatter( M_before, M_after  )
        #plt.title(f'{name}')
        #plt.show()'''

        ratio = ((dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])/
                        (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])*(dt_pair_before[:, 3]+dt_pair_before[:, 4]))
        
        ratio = ratio[np.isfinite(ratio)]
        ratio_mouse += (ratio, )

        #Baseline corr selection
        dt_pair_after_baseline = dt_pair_after[ind_baseline, :]
        dt_pair_before_baseline = dt_pair_before[ind_baseline, :]
    
        ratio_baseline = ((dt_pair_after_baseline[:, 0]+dt_pair_after_baseline[:, 1]+dt_pair_after_baseline[:, 2])/
                          (dt_pair_after_baseline[:, 3]+dt_pair_after_baseline[:, 4])/
                 (dt_pair_before_baseline[:, 0]+dt_pair_before_baseline[:, 1]+dt_pair_before_baseline[:, 2])*
                 (dt_pair_before_baseline[:, 3]+dt_pair_before_baseline[:, 4]))
        
        ratio_baseline = ratio_baseline[np.isfinite(ratio_baseline)]
        ratio_mouse_baseline += (ratio_baseline, )

        #Post corr selection
        dt_pair_after_post = dt_pair_after[ind_post, :]
        dt_pair_before_post = dt_pair_before[ind_post, :]
    
        ratio_post = ((dt_pair_after_post[:, 0]+dt_pair_after_post[:, 1]+dt_pair_after_post[:, 2])/
                      (dt_pair_after_post[:, 3]+dt_pair_after_post[:, 4])/
                 (dt_pair_before_post[:, 0]+dt_pair_before_post[:, 1]+dt_pair_before_post[:, 2])*
                 (dt_pair_before_post[:, 3]+dt_pair_before_post[:, 4]))
        
        ratio_post = ratio_post[np.isfinite(ratio_post)]
        ratio_mouse_post += (ratio_post, )

    ind_post_outlier = np.where(ratio_mouse_post[0]>1.5)[0]
    print(ind_post_outlier)
    print(real_post)
    #print(real_post[ind_post_outlier])
    

    fig, (ax0, ax1, ax2) = plt.subplots(1, 3)
    ax0.boxplot(ratio_mouse, showmeans = True)
    ax0.hlines([1], 0.5, len(list_name)+0.5)
    ax0.set_title('All pairs')
    ax0.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)

    ax1.boxplot(ratio_mouse_post, showmeans = True)
    ax1.hlines([1], 0.5, len(list_name)+0.5)
    ax1.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    ax1.set_title('Correlated pairs Post')

    ax2.boxplot(ratio_mouse_baseline, showmeans = True)
    ax2.hlines([1], 0.5, len(list_name)+0.5)
    ax2.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
    ax2.set_title('Correlated pairs Baseline')

    fig.suptitle('Variation of ratio paired spikes, 200ms Ref')
    plt.show()

    return 0

def plot_diff_dt_pair_real_hist(list_name):
    ''' Same as other but with 3 plot for 3 groups'''
    ratio_mouse = ()
    ratio_mouse_corr = ()

    for it_mouse, name in enumerate(list_name):
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
        time_win_before = [mouse.ti-305, mouse.ti-5]
        time_win_after = [mouse.ti, mouse.ti + 20]

        '''#Part related to real pair
        list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
        list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
        real_pair = list.to_numpy()
        check_real = real_pair[:, 0]**3 + real_pair[:, 1]**3 '''

        real_corr = mouse.get_correlated_couples('All')
        check_corr = real_corr['Striatum_ID']**3+real_corr['Cortex_ID']**3

        cortex_list = mouse.get_cluster('cortex')
        striatum_list = mouse.get_cluster('striatum')

        dt_pair_before = compute_dt_pair(mouse, time_win_before)
        dt_pair_after = compute_dt_pair(mouse, time_win_after)

        #ind_before = np.invert(np.logical_or(dt_pair_before[:, 3] == 0, dt_pair_before[:, 4] == 0))
        #ind_after = np.invert(np.logical_or(dt_pair_after[:, 3] == 0, dt_pair_after[:, 4] == 0))



        it = 0
        ind_corr = np.zeros(len(cortex_list)*len(striatum_list), dtype = bool)
        for id_cortex in cortex_list:
            for id_striatum in striatum_list:
                if np.isin(id_cortex**3+id_striatum**3, check_corr):
                    ind_corr[it] = True
                it +=1


        '''M_after = (dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])
        M_before = (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])/(dt_pair_before[:, 3]+dt_pair_before[:, 4])
        print(np.sum(np.invert(np.isfinite(M_before))))
        print(np.sum(np.invert(np.isfinite(M_after))))
        M_before = M_before[np.isfinite(M_after)]
        M_after = M_after[np.isfinite(M_after)]
        M_stat = stats.ttest_rel(M_before, M_after)
        print(f'{name} \n, stat:{M_stat.statistic}, pvalue: {M_stat.pvalue}')

        #plt.scatter( M_before, M_after  )
        #plt.title(f'{name}')
        #plt.show()'''

        ratio = ((dt_pair_after[:, 0]+dt_pair_after[:, 1]+dt_pair_after[:, 2])/(dt_pair_after[:, 3]+dt_pair_after[:, 4])/
                        (dt_pair_before[:, 0]+dt_pair_before[:, 1]+dt_pair_before[:, 2])*(dt_pair_before[:, 3]+dt_pair_before[:, 4]))
        
        ratio = ratio[np.isfinite(ratio)]
        ratio_mouse += (ratio, )


        #corr selection
        dt_pair_after_corr = dt_pair_after[ind_corr, :]
        dt_pair_before_corr = dt_pair_before[ind_corr, :]
    
        ratio_corr = ((dt_pair_after_corr[:, 0]+dt_pair_after_corr[:, 1]+dt_pair_after_corr[:, 2])/
                      (dt_pair_after_corr[:, 3]+dt_pair_after_corr[:, 4])/
                 (dt_pair_before_corr[:, 0]+dt_pair_before_corr[:, 1]+dt_pair_before_corr[:, 2])*
                 (dt_pair_before_corr[:, 3]+dt_pair_before_corr[:, 4]))
        
        ratio_corr = ratio_corr[np.isfinite(ratio_corr)]
        ratio_mouse_corr += (ratio_corr, )

    ind_corr_outlier = np.where(ratio_mouse_corr[0]>1.5)[0]

    threshold_values = [10,10,5,5]
    for ratio_L in [ratio_mouse, ratio_mouse_corr]:
        for it, mouse_name in enumerate(list_name):
            print(mouse_name)
            print('\n _______________________________________________________ \n ')
            print(ratio_mouse[it][ratio_mouse[it] > threshold_values[it]])
            plt.hist(ratio_L[it], bins = 200)
            plt.title(mouse_name)
            plt.show()

    return 0

def Var_FR(list_name):
    fig, (ax1, ax2) = plt.subplots(1,2)

    for zone in ['cortex', 'striatum']:
        ratio_mouse = ()
        FR_mouse = ()
        for it_mouse, name in enumerate(list_name):
            mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
            t_before = [mouse.ti - 310, mouse.ti - 10]
            t_after = [mouse.ti, mouse.tf+5]
            list_id = mouse.get_cluster(zone)
            FR_id = np.zeros(len(list_id))
            ratio_id = np.zeros(len(list_id))
            FR_before_id = np.zeros(len(list_id))
            FR_after_id = np.zeros(len(list_id))

                #REAL PAIR THINGS
            list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
            list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
            real_pair = list.to_numpy()
            real_pair = np.unique(real_pair)
            ind_real = np.isin(list_id, real_pair)


            for it_id, id in enumerate(list_id):
                spikes = mouse.spikes_from_ID(id)
                #spikes = spikes[ np.logical_and(spikes<mouse.ti - 25, spikes < mouse.ti < 5)]
                FR_before = np.sum(np.logical_and(spikes>t_before[0], spikes <t_before[1]))/(t_before[1]-t_before[0])
                FR_after =  np.sum(np.logical_and(spikes>t_after[0], spikes < t_after[1]))/(t_after[1]-t_after[0])
                #FR_before = firing_rate(spikes, [mouse.ti - 25, mouse.ti - 5], 0.5, 0.1)
                #FR_after = firing_rate(spikes, [mouse.ti , mouse.ti +20], 0.5, 0.1)

                ratio_id[it_id] = np.mean(FR_after)/np.mean(FR_before)
                FR_before_id[it_id] = FR_before
                FR_after_id[it_id] = FR_after
                FR_id[it_id] = FR_before
            
            ratio_id = ratio_id[ind_real]
            ratio_id = ratio_id[np.isfinite(ratio_id)]
            ratio_mouse += (ratio_id, )
            FR_mouse += (FR_id, )
            TTest = stats.ttest_rel(FR_after_id, FR_before_id)
            print(f'mouse: {name}, \n stat: {TTest.statistic}, pvalue: {TTest.pvalue}')


        if zone == 'cortex':
            ax = ax1
        if zone == 'striatum':
            ax = ax2

        ax.boxplot([ratio_mouse[0][np.isfinite(ratio_mouse[0])],
                    ratio_mouse[1][np.isfinite(ratio_mouse[1])],
                    ratio_mouse[2][np.isfinite(ratio_mouse[2])],
                    ratio_mouse[3][np.isfinite(ratio_mouse[3])],
                    ratio_mouse[4][np.isfinite(ratio_mouse[4])],
                    ratio_mouse[5][np.isfinite(ratio_mouse[5])],
                    ratio_mouse[6][np.isfinite(ratio_mouse[6])]],
                    showmeans = True)
        #ax1.boxplot(ratio_mouse)
        ax.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
        ax.hlines([1], 0.5, len(list_name)+0.5)
        ax.set_title('Variations of Cortex Firing rate')

    fig.suptitle('Ratio of Firing rate during contact over 5min before')

    plt.show()

def Var_FR2(list_name):
    fig, axs = plt.subplots(2,2)

    for is_real in [False, True]:
        for zone in ['cortex', 'striatum']:
            ratio_mouse = ()
            FR_mouse = ()
            for it_mouse, name in enumerate(list_name):
                mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
                t_before = [mouse.ti - 310, mouse.ti - 10]
                t_after = [mouse.tf, mouse.tf+20]
                list_id = mouse.get_cluster(zone)
                FR_id = np.zeros(len(list_id))
                ratio_id = np.zeros(len(list_id))
                FR_before_id = np.zeros(len(list_id))
                FR_after_id = np.zeros(len(list_id))

                    #REAL PAIR THINGS
                list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
                list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
                real_pair = list.to_numpy()
                real_pair = np.unique(real_pair)
                ind_real = np.isin(list_id, real_pair)


                for it_id, id in enumerate(list_id):
                    spikes = mouse.spikes_from_ID(id)
                    #FR_before = np.sum(np.logical_and(spikes>t_before[0], spikes <t_before[1]))/(t_before[1]-t_before[0])
                    #FR_before = firing_rate(spikes, [mouse.ti-310, mouse.ti-10], mouse.tf-mouse.ti, 1 )[0]/(mouse.tf-mouse.ti)
                    FR_before = firing_rate(spikes, t_before, mouse.tf-mouse.ti, 1 )[0]/(mouse.tf-mouse.ti)
                    FR_before = np.quantile(FR_before, 0.95)
                    #FR_before = np.max(FR_before) #max on baseline
                    FR_after =  np.sum(np.logical_and(spikes>t_after[0], spikes < t_after[1]))/(t_after[1]-t_after[0]) #contact
                    #FR_before = firing_rate(spikes, [mouse.ti - 25, mouse.ti - 5], 0.5, 0.1)
                    #FR_after = firing_rate(spikes, [mouse.ti , mouse.ti +20], 0.5, 0.1)

                    ratio_id[it_id] = np.mean(FR_after)/np.mean(FR_before)
                    FR_before_id[it_id] = FR_before
                    FR_after_id[it_id] = FR_after
                    FR_id[it_id] = FR_before
                
                if is_real == True:
                    ratio_id = ratio_id[ind_real]
                ratio_id = ratio_id[np.isfinite(ratio_id)]
                ratio_mouse += (ratio_id, )
                FR_mouse += (FR_id, )
                TTest = stats.ttest_rel(FR_after_id, FR_before_id)
                print(f'mouse: {name} {zone}, \n stat: {TTest.statistic}, pvalue: {TTest.pvalue}')

            if is_real == False:
                if zone == 'cortex':
                    ax = axs[0,0]
                if zone == 'striatum':
                    ax = axs[0,1]
            elif is_real == True:
                if zone == 'cortex':
                    ax = axs[1,0]
                if zone == 'striatum':
                    ax = axs[1,1]

            ax.boxplot([ratio_mouse[0][np.isfinite(ratio_mouse[0])],
                        ratio_mouse[1][np.isfinite(ratio_mouse[1])],
                        ratio_mouse[2][np.isfinite(ratio_mouse[2])],
                        ratio_mouse[3][np.isfinite(ratio_mouse[3])],
                        ratio_mouse[4][np.isfinite(ratio_mouse[4])],
                        ratio_mouse[5][np.isfinite(ratio_mouse[5])],
                        ratio_mouse[6][np.isfinite(ratio_mouse[6])]],
                        showmeans = True)
            #ax1.boxplot(ratio_mouse)
            ax.set_xticks(np.arange(start = 1, stop = len(list_name)+1), list_name)
            ax.hlines([1], 0.5, len(list_name)+0.5)
            if is_real == True:
                ax.set_title(f'Variations of {zone} rate, correlated pairs')
            else:
                ax.set_title(f'Variations of {zone} rate')

    fig.suptitle('Ratio of Firing rate')# during contact over 5min before')

    plt.show()


################################# AROUND RATIO ########################################
#                            WITH BASELINE AND POST

def around_ratio(mouse: Mouse, selec = 'increase'):
    '''We get couples that have increase of paired spike by 20% or more.
    '''
    name = mouse.mouse_name
    #mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    #couples_list = np.array([(cortex_list[it],striatum_list[it]) for it in range(len(striatum_list)) ])
    T_diff_glob = Corr.compute_Tdiff(name, [mouse.ti, mouse.ti + 20], 0.2)#, couples_list = couples_list)
    T_diff_before = Corr.compute_Tdiff(name, [mouse.ti-300, mouse.ti - 10], 0.2)

    ratio_contact = np.zeros(len(T_diff_glob.couples))
    ratio_before = np.zeros(len(T_diff_glob.couples))
    ratio_ratio = np.zeros(len(T_diff_glob.couples))
    it = 0

    for couple in T_diff_glob.couples:

        spikes = mouse.spikes_from_ID(couple[0])
        n_spikes_contact = np.sum(np.logical_and(spikes > mouse.ti, spikes < mouse.ti+20))
        n_spikes_before = np.sum(np.logical_and(spikes > (mouse.ti-300), spikes < (mouse.ti-10)))
        spikes = mouse.spikes_from_ID(couple[1])
        n_spikes_contact += np.sum(np.logical_and(spikes > mouse.ti, spikes < mouse.ti+20))
        n_spikes_before += np.sum(np.logical_and(spikes > (mouse.ti-300), spikes < (mouse.ti-10)))

        ratio_contact [it] = len(T_diff_glob.dt[it])/n_spikes_contact
        ratio_before [it] = len(T_diff_before.dt[it])/n_spikes_before
        ratio_ratio[it] = ratio_contact[it]/ratio_before[it]
        
        it += 1

    ratio_ratio = ratio_contact

    ratio_ratio[np.invert(np.isfinite(ratio_ratio))] = -1

    ratio_contact[np.invert(np.isfinite(ratio_contact))] = -0.12
    ratio_before[np.invert(np.isfinite(ratio_before))] = -1
    #print(np.percentile(ratio, 90))
    #increasing_couples = T_diff_glob.couples[ratio_contact > np.percentile(ratio_contact, 90)]
    #increasing_couples = T_diff_glob.couples[ratio < np.percentile(ratio, 50)]
    if selec == 'increase':
        increasing_couples = T_diff_before.couples[ratio_ratio > np.percentile(ratio_ratio, 95)]
    if selec == 'mid':
        skip = np.sum(ratio_ratio <= 0)/len(ratio_ratio)*50
        increasing_couples = T_diff_before.couples[np.logical_and(ratio_ratio < np.percentile(ratio_ratio, 55 + skip),
                                                                   ratio_ratio > np.percentile(ratio_ratio, 45 + skip))]                                                 
    if selec == 'V7260':
        increasing_couples = T_diff_before.couples[ratio_ratio > np.percentile(ratio_ratio, 75)]

    '''if selec == 'increase':
        increasing_couples = T_diff_before.couples[ratio_ratio > 1.1]
    if selec == 'mid':
        skip = np.sum(ratio_ratio <= 0)/len(ratio_ratio)*50
        increasing_couples = T_diff_before.couples[np.logical_and(ratio_ratio < 1.1,
                                                                   ratio_ratio > 0.7)]
    if selec == 'V7260':
        increasing_couples = T_diff_before.couples[ratio_ratio > np.percentile(ratio_ratio, 75)]'''


    '''import matplotlib.scale as mscale
    plt.hist(ratio_contact, bins = 50)
    per95 = np.percentile(ratio_contact, 95)
    plt.vlines(per95, 0, 800, color='red')
    plt.xlabel('Ratio of paired spikes')
    plt.ylabel('#pairs')
    plt.title('histogramm of ratio paired spikes, 20s from contact')
    #plt.yscale('log')

    plt.show()'''

    print(f'{mouse.mouse_name}')
    print(f'percentile 95: {np.percentile(ratio_ratio, 95)}')
    print(f'percentile 47.5: {np.percentile(ratio_ratio, 47.5)}')
    print(f'percentile 52.5: {np.percentile(ratio_ratio, 52.5)}')
    print(f'number of uninvalid clusters ratio: {np.sum(ratio_ratio <= 0)}')

    return increasing_couples

def selec_on_criteria(mouse: Mouse, selec = 'increase'):
    '''We get couples that have increase of paired spike by 20% or more.
    '''
    name = mouse.mouse_name
    #mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    #couples_list = np.array([(cortex_list[it],striatum_list[it]) for it in range(len(striatum_list)) ])
    list_sel = ()

    try:
        np.load(mouse.path_mouse / 'ratio_udgsfgratio.npy')
        ratio_ratio = np.load(mouse.path_mouse / 'ratio_ratio.npy')
        ratio_contact = np.load(mouse.path_mouse / 'ratio_contact.npy')
        ratio_before = np.load(mouse.path_mouse / 'ratio_before.npy')
    except:

        T_diff_glob = Corr.compute_Tdiff(name, [mouse.ti, mouse.ti + 20], 0.2)#, couples_list = couples_list)
        T_diff_before = Corr.compute_Tdiff(name, [mouse.ti-300, mouse.ti - 10], 0.2)

        ratio_contact = np.zeros(len(T_diff_glob.couples))
        ratio_before = np.zeros(len(T_diff_glob.couples))
        ratio_ratio = np.zeros(len(T_diff_glob.couples))
        #n_spikes_tot = np.zeros(len(T_diff_glob.couples))
        #ratio_spikes = np.zeros(len(T_diff_glob.couples))

        it = 0
        for couple in T_diff_glob.couples:

            spikes = mouse.spikes_from_ID(couple[0])
            #n_spikes = len(spikes)
            n_spikes_contact = np.sum(np.logical_and(spikes > mouse.ti, spikes < mouse.ti+20))
            n_spikes_before = np.sum(np.logical_and(spikes > (mouse.ti-300), spikes < (mouse.ti-10)))
            spikes = mouse.spikes_from_ID(couple[1])
            #n_spikes += len(spikes)
            n_spikes_contact += np.sum(np.logical_and(spikes > mouse.ti, spikes < mouse.ti+20))
            n_spikes_before += np.sum(np.logical_and(spikes > (mouse.ti-300), spikes < (mouse.ti-10)))

            ratio_contact [it] = len(T_diff_glob.dt[it])/n_spikes_contact
            #n_spikes_tot[it] = n_spikes
            #ratio_spikes[it] = n_spikes_contact/n_spikes_before
            ratio_before [it] = len(T_diff_before.dt[it])/n_spikes_before
            ratio_ratio[it] = ratio_contact[it]/ratio_before[it]
            it += 1

        ratio_ratio = ratio_contact

        ratio_ratio[np.invert(np.isfinite(ratio_ratio))] = -1

        ratio_contact[np.invert(np.isfinite(ratio_contact))] = -1
        ratio_before[np.invert(np.isfinite(ratio_before))] = -1

        np.save(mouse.path_mouse / 'ratio_ratio', ratio_ratio)
        np.save(mouse.path_mouse / 'ratio_contact', ratio_contact)
        np.save(mouse.path_mouse / 'ratio_before', ratio_before)

    print(f'{mouse.mouse_name}')
    print(f'percentile 95: {np.percentile(ratio_ratio, 95)}')
    print(f'percentile 47.5: {np.percentile(ratio_ratio, 47.5)}')
    print(f'percentile 52.5: {np.percentile(ratio_ratio, 52.5)}')
    print(f'number of uninvalid clusters ratio: {np.sum(ratio_ratio <= 0)}')

    for s in selec:
        if s == 'increase':
            increasing_couples = ratio_ratio > np.percentile(ratio_ratio, 95)
        if s == 'mid':
            skip = np.sum(ratio_ratio <= 0)/len(ratio_ratio)*50
            increasing_couples = np.logical_and(ratio_ratio < np.percentile(ratio_ratio, 55 + skip),
                                                                    ratio_ratio > np.percentile(ratio_ratio, 45 + skip))
        list_sel += (increasing_couples, )

    return list_sel

def around_ratio1(mouse: Mouse):
    '''We get couples that have increase of paired spike by 20% or more.
    '''
    name = mouse.mouse_name
    #mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    couples_list = mouse.get_correlated_couples('Post')
    couples_list = couples_list[couples_list['Keep'] == 1]
    striatum_list = couples_list['Striatum_ID'].to_numpy()
    cortex_list = couples_list['Cortex_ID'].to_numpy()
    couples_list = np.array([(cortex_list[it],striatum_list[it]) for it in range(len(striatum_list)) ])
    T_diff_before_long = Corr.compute_Tdiff(name, [mouse.ti - 305, mouse.ti-5], dt_win=0.2, couples_list = couples_list)
    T_diff_before_short = Corr.compute_Tdiff(name, [mouse.ti - 65, mouse.ti-5], dt_win=0.2, couples_list = couples_list)
    T_diff_contact = Corr.compute_Tdiff(name, [mouse.ti, mouse.ti+60], dt_win=0.2, couples_list = couples_list)
    print(Corr.ratio_outlier(T_diff_before_long, T_diff_contact))
    print('GOOD TILL HERE ______________________________')
    print(Corr.ratio_outlier(T_diff_before_short, T_diff_contact))
    return 0

def around_ratio3(mouse: Mouse):
    '''We get couples that have increase of paired spike by 20% or more.
    '''
    name = mouse.mouse_name
    #mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    striatum_list = mouse.get_cluster('striatum').to_numpy()
    cortex_list = mouse.get_cluster('cortex').to_numpy()
    #couples_list = mouse.get_correlated_couples('All')
    #striatum_list = couples_list['Striatum_ID'].to_numpy()
    #cortex_list = couples_list['Cortex_ID'].to_numpy()
    couples_list = np.array([(cortex_id,striatum_id) for cortex_id in cortex_list for striatum_id in striatum_list ])
    #T_diff_before_long = Corr.compute_Tdiff(name, [mouse.ti - 305, mouse.ti-5], dt_win=0.2, couples_list = couples_list)
    T_diff_before_long = Corr.compute_Tdiff(name, [mouse.tf-290, mouse.tf+10], dt_win=0.2, couples_list = couples_list)

    #T_diff_before_short = Corr.compute_Tdiff(name, [mouse.ti - 65, mouse.ti-5], dt_win=0.2, couples_list = couples_list)
    T_diff_contact = Corr.compute_Tdiff(name, [mouse.tf+30, mouse.tf+90], dt_win=0.2, couples_list = couples_list)
    print(Corr.ratio_outlier(T_diff_before_long, T_diff_contact, ratio_value = 5))
    print('GOOD TILL HERE ______________________________')
    #print(Corr.ratio_outlier(T_diff_before_short, T_diff_contact))
    return 0

def around_ratio2(router: Router, mouse: Mouse, list_couples):
    for couple in list_couples:
        try:
            plot_ID(router, mouse, couple, with_FR = True)
        except FileNotFoundError:
            LOGGER.info(f'{couple} not found')

    return 0

#######################################################################################
# ALL AND ALL

def get_outlier_pairedSpike(list_name):
    for name in list_name:
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    return 0

#######################################################################################
def main():
    name = 'V7265'
    for name in ['V4606', 'V7265', 'Mouse1', 'V7260']:
    #router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f')
        mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')

        list_increase = around_ratio(mouse)
        np.save(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase', list_increase)

    exit()
    #around_ratio2(router_V7265, mouse_V7265, list_couples)

    #np.save(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase2', around_ratio(mouse))
    listI2 = np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase2.npy')
    listI = np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_Increase.npy')
    print(name)
    print('len tot:')
    print(len(np.load(f'/home/ahubert/Documents/SpikeSorting/to_send/{name}/id_All.npy')))

    sim = 0
    for couple in listI:
        for couple2 in listI2:
            if couple[0] == couple2[0]:
                if couple[1] == couple2[1]:
                    sim += 1

    print(sim)
    print(len(listI))

    return 0

#Var_FR2(['V4606', 'V7265', 'Mouse1', 'V7260', 'Red', 'Blue', 'V4607'])

#plot_diff_dt_pair_real2(['V4606', 'V7265', 'Mouse1', 'V7260', 'Red', 'Blue', 'V4607'])#, 'Black'])

#

if __name__ == '__main__':
    main()
    #plot_diff_dt_pair_real3(['V4606', 'V7265', 'Mouse1'])#,'V7260'])
    #plot_diff_dt_pair_real_hist(['V4606', 'V7265', 'Mouse1', 'V4607'])
    
