import logging
import pathlib
#from pathlib import Path

from multiprocessing import Pool

import seaborn as sns
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
import pickle
import tslearn
from tslearn.metrics import dtw, cdist_dtw
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as mpatches

import mlflow

import stdp.integration
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot
import stdp.computation_center
import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from stdp.Class_Analysis import Router
from computation_tools import *
from stdp.Class_optiGlu import optiGlu
import stdp.density_stdp as dens

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)

######################### Init part ##########################

if __name__ == "__main__":
    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    V4606_mouse = Mouse(*stdp.create_mouse_data.prepare_V4606())
    V7265_mouse = Mouse(*stdp.create_mouse_data.prepare_V7265())
    exp_id = '562231799100305614'

######################## Functions def ########################
                            #Utility

def save_object(obj, file):
    try:
        with open(file, "wb") as f:
            pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)

def load_object(filename):
    try:
        with open(filename, "rb") as f:
            return pickle.load(f)
    except Exception as ex:
        print("Error during unpickling object (Possibly unsupported):", ex)

###############################################################

P1=1e-10 #1e-3
P3=5 #2
P4=2 #2e-3
alphaAEACB1=0.10
LTDstart=5e-3 #5e-3
LTDstop=10e-3 #13e-3
LTPstart=19e-3
LTDMax=0.20 #0.25
LTPMax=15


y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])

def plot_func(mouse: Mouse):
    Router_exp = Router('490892905216247639','94d644c941dd43818e4c6d03233fc48c')#'96ca3a8daf3c4b5baa8b5f1a53de5929')
    list_of_couple = Router_exp.get_couple_id()
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    print(list_of_couple)
    for couple in list_of_couple:
        #print('GOT IN ONCE AT LEAST')
        #print(couple)
        RES = Router_exp.load_artifacts(couple[0], couple[1])
        mean_before = compute_int_RES(RES,  mouse.ti-200, mouse.ti-5)
        if mean_before[1] > 2.2:
            list_pre_saturated += (couple, )
        else:
            list_pre_nosat += (couple, )

    print(f'proportion of non saturated: {len(list_pre_nosat)}/{len(list_of_couple)}')
    #for couple in [list_pre_saturated[i] for i in [ 40]]: #For fig.1.pdf
    for couple in list_of_couple:
        RES = Router_exp.load_artifacts(couple[0], couple[1])
        y = Router_exp.load_artifacts(couple[0], couple[1], seg = 'sol')
        #cortex_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == couple[0]]
        #striatum_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == couple[1]]
        #FR_cortex = firing_rate(cortex_times, [0, 1100], 5, 1)
        #FR_striatum = firing_rate(striatum_times, [0, 1100], 5, 1)
        plt.plot(RES[2], RES[0][:]/RES[0][0], color = 'navy')
        plt.plot(RES[2], RES[1][:]/RES[1][0], color = 'firebrick')
        #plt.plot(np.arange(2.5, mouse.t_bounds[1]-mouse.t_bounds[0]-2, 1), FR_cortex, color = 'green')
        #plt.plot(np.arange(2.5, mouse.t_bounds[1]-mouse.t_bounds[0]-2, 1), FR_striatum, color = 'orange')
        '''taufpre=P1/((P1/1e-4)**P3+(y[24]+alphaAEACB1*y[26, :])**P3)+P4

        #Omega = (RES[0]*np.exp(RES[2]/taufpre) - y0init[25])/(np.exp(RES[2]/taufpre)-1) #WRONG dont take disontinuities into account
        Omega=(0.5-LTDMax*np.heaviside(y[24]+alphaAEACB1*y[26]-LTDstart, 1)*
                        np.heaviside(LTDstop-y[24]-alphaAEACB1*y[26], 1)+
                        np.heaviside(y[24]+alphaAEACB1*y[26]-LTPstart, 1)*LTPMax)'''

        #plt.plot(RES[2],Omega)
        #taufpre[taufpre > 100] = 100
        #plt.plot(RES[2], taufpre)
        plt.vlines([mouse.ti, mouse.tf] , 0, 30 , colors=['red', 'red'], label = 'duration of contact with tape')
        plt.legend(['pre-syn weight','post-syn weight'])#, 'fr cortex', 'fr striatum'])
        plt.ylabel('synaptic weight (%)')
        plt.xlabel('time (s) ')
        plt.title(f'Brain plasticity along time, id {couple} ')
        plt.show()

def plot_mean_LFP(mouse: Mouse):
    mean_LFP = np.mean(mouse.LFP_cortex, axis = 1)
    plt.plot(mean_LFP, color = 'navy')
    plt.vlines([mouse.ti*2500, mouse.tf*2500], 0, np.max(mean_LFP), colors = 'red')
    plt.xlabel('time*2500')
    plt.ylabel('mean LPP on cortex')
    plt.title('Mean LFP along time')
    plt.show()

def plot_low_LFP(mouse: Mouse):
    mean_LFP = np.mean(mouse.LFP_cortex, axis = 1)
    coord = np.where(mean_LFP < -300)
    print(len(coord[0])/len(mean_LFP))
    coord = coord[0][400]
    print(coord)
    plt.plot(mean_LFP[(coord-2000): (coord+2000)], label = 'LFP')
    plt.hlines(-120, 0, 4000, color = "red", label = 'threshold of modulation')
    plt.legend()
    plt.show()

def plot_boxplot_region_LFP(mouse: Mouse):
    '''plot the repartition of LFP as boxplot, for cortex region and remaining
    '''
    cortex_LFP = np.mean(mouse.LFP_cortex, axis = 1)
    other_LFP = np.mean(mouse.LFP[:, :mouse.brain_region[1][0]], axis = 1)
    plt.boxplot([np.ravel(cortex_LFP), np.ravel(other_LFP)])
    plt.title(f'{mouse.mouse_name} mouse, mean LFP boxplot depending on region')
    plt.xticks([1,2], ['cortex region', 'remaining'])
    plt.show()

def big_cortex(mouse: Mouse):

    if mouse.mouse_name == 'V4606':
        striatum_list = [649, 656, 12, 644, 645, 35, 61, 736, 738, 83, 86, 16, 25,
            684, 690, 36, 730, 15, 675, 53, 69, 81, 82] #V4
    elif mouse.mouse_name == 'Red':
        striatum_list = [3, 12, 18, 30, 40, 44, 67, 112, 131, 145, 150, 181, 186, 197, 198, 201, 212, 230, 234, 239, 262, 266, 268, 270, 272,
            275, 281, 293, 303, 309, 312, 315, 320, 329, 350, 354, 366, 367, 371, 377, 380, 393, 403, 405, 406, 407, 410, 419, 424,
            434, 442, 723, 724, 743, 759, 880, 904, 918, 969, 1043, 1089, 1103, 1124, 1137, 1152, 1430, 1483, 1517, 1529, 1585, 1599,
            1625, 1630, 1651, 1669, 1671, 1672, 1674, 1690, 1694, 1701, 1709, 1713, 1716, 1755, 1756, 1799, 25, 32, 49, 109, 148,
            154, 162, 184, 218, 242, 274, 278, 325, 332, 351, 395, 400, 401, 408, 432, 435, 440, 706, 714, 737, 740, 753, 755, 827,
            843, 898, 920, 935, 938, 1105, 1140, 1254, 1335, 1369, 1428, 1435, 1496, 1590, 1597, 1658, 1661, 1673, 1682, 1698, 1714,
            1715, 1735, 1740, 1742, 1743, 1766, 1783, 1795, 1802, 150, 266] #Red
        striatum_list = [12, 40, 67, 145, 181, 186, 197, 198, 212, 230, 268, 272, 293, 329, 354, 377, 380, 393, 403, 424, 434, 442, 723, 1043,
                         1089, 1103, 1124, 1137, 1152, 1483, 1517, 1599, 1625, 1630, 1651, 1669, 1671, 1672, 1674, 1690, 1694, 1701, 1709, 1713,
                         1755, 1756, 1799, 109, 154, 162, 242, 325, 400, 401, 408, 432, 706, 714, 740, 753, 755, 827, 1140, 1335, 1369, 1428, 1496,
                         1590, 1597, 1658, 1661, 1673, 1682, 1715, 1735, 1740, 1742, 1743, 1802] #the one not saturated
    elif mouse.mouse_name == 'V7265':
        striatum_list = [636,657,626,650,849,747,148,37,86,111,628,826,828,11,832,970,922,1434,1381,436,1352,1338,
        1312,1287,158,143,1337,1388,110,1416,108,982,95,50,1288,30,786,968,1005,1282,771,908,638] #V7

    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    #time_vect = [V4606_mouse.ti-60, V4606_mouse.ti-10, V4606_mouse.tf, V4606_mouse.tf + 50]
    #time_vect = [red_mouse.ti-60, red_mouse.ti-10, red_mouse.tf, red_mouse.tf + 50]
    time_vect = [mouse.ti-60, mouse.ti-10, mouse.tf, mouse.tf + 50]

    #4a403505ce2f4de994ee75ffbaa97ad2 #complete Red
    #4fe67572c10c401e924a289af1ef4bf2 #complete V4606
    #d7792f0df3e84581b6a9293a363a4440 #complete V7
    
    #ede5a3d11f0f4da3b88e2fa939476afe V4606
    for clust in striatum_list:
        RES = np.load(f'/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/5f258b7992144f59a6bd05b65c18a93f/artifacts/res_{mouse.mouse_name}_{clust}.npy') #0
        #RES = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/0b3850a9af1d430db75e6f230875209b/artifacts/res_V7265.npy') #1
        #RES = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/8019d89ae2b54d8a98d5001e233e3409/artifacts/res_V7265.npy') #5
        #RES2 = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/1f2f0f1389af4cc4a962952360cea477/artifacts/res_V7265.npy')

        mean_selec = compute_int_RES(RES, mouse.ti-60, mouse.ti-10)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.6:
            list_pre_saturated += (clust, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (clust, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
    
    print(f'saturated: {list_pre_saturated}')
    print(f'no sat: {list_pre_nosat}')

    '''
        depot_scotch = mouse.frame_to_time(27020)-600 #depot scotch

        
        plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre-syn weight')
        plt.plot(RES[2], RES[1]/RES[1][0], label = 'post-syn weight')
        plt.title(f'striatum cluster n°{clust} (Red)')
        plt.ylabel('plasticity variation (%)')
        plt.xlabel('time (s)')
        plt.legend()
        plt.vlines(depot_scotch, 0, 30, color = 'green')
        plt.vlines([mouse.ti, mouse.tf], 0, 30, color = ['red', 'red'])
        plt.show()


    print(f'len no sat: {len(list_pre_nosat)}, len sat: {len(list_pre_saturated)} ')
    plt.boxplot([-w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after], positions=[1, 1.4])#, notch = True)
    plt.xticks([1,1.4], ['saturated', 'not saturated'])
    plt.title(f'compare btwn times {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()
    '''
    return 0

def analyse_dt(mouse: Mouse, selection_inter = False, ):
    res = load_object(pathlib.Path(mouse.path_mouse, 'data_analyse', 'analyse_dt' ))
    cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[0])]
    cluster_cortex = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    if selection_inter == True:
        selection = np.isin([t[1] for t in res] , [63, 71, 222, 197, 962, 1034])
        selection = np.where(selection == True)[0]
        res = res[selection]
        res_dt = np.concatenate([dt[2] for dt in res])
        res_dt = res_dt[np.abs(res_dt) < 1]
        plt.hist(res_dt, bins = 300, density=True, alpha = 0.5)
        return 0
    res_dt = np.concatenate([dt[2] for dt in res])
    res_dt = res_dt[np.abs(res_dt) < 1]
    plt.hist(res_dt, bins = 300, density=True, alpha = 0.5)
    #plt.show()

    return 0

def plot_FR(mouse: Mouse, list):

    for clust in list:
        spikes = mouse.spikes_from_ID(int(clust))
        FR = firing_rate(spikes, t_bounds=mouse.t_bounds, win_len=0.5, win_step=0.1)
        plt.plot(FR[1], FR[0])
        plt.vlines([mouse.ti, mouse.tf], 0, 20, colors=['red' , 'red'])
        plt.title(f'{clust}')
        plt.show()
    return 0

def plot_FR_cortex(mouse: Mouse):

    if mouse.mouse_name == 'V4606':
        interneurons_list = np.array([63, 71, 222, 197, 962, 1034]) #V4
        depot_scotch = mouse.frame_to_time(19800) #depot scotch
    elif mouse.mouse_name == 'Red':
        interneurons_list = np.array([418, 554, 634, 155])
    elif mouse.mouse_name == 'V7265':
        interneurons_list = np.array([4, 448, 1378]) #V7

    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID > 57]

    FR = firing_rate(cortex_spikes, t_bounds=mouse.t_bounds, win_len=1, win_step=0.2)
    
    plt.plot(FR)
    plt.vlines([mouse.ti*5, mouse.tf*5], 0, np.max(FR), colors=['red' , 'red'])
    #plt.vlines(depot_scotch*5, 0, np.max(FR), color = 'green')
    plt.show()
    return 0

def plot_big_cortex(router: Router, mouse: Mouse):
    #opti = optiGlu(mouse)
    #optim_values = opti.get_optim_values()
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    time_vect = np.array([mouse.ti-20, mouse.ti, mouse.tf, mouse.tf + 20])
    result_id = router.get_result_id()
    print(result_id)
    #result_id = np.array([ 11,108,110,143,158])

    for clust in result_id:
        RES = router.load_result(clust, name = f'{mouse.mouse_name}')
        y = router.load_solution(clust, name = f'{mouse.mouse_name}')
        mean_selec = compute_int_RES(RES, mouse.ti-20, mouse.ti)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.2:
            list_pre_saturated += (clust, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (clust, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
        
        #depot_scotch = mouse.frame_to_time(27020)-600 #depot scotch
        taufpre=P1/((P1/1e-4)**P3+(y[24]+alphaAEACB1*y[26, :])**P3)+P4
        #Omega = (RES[0]*np.exp(RES[2]/taufpre) - y0init[25])/(np.exp(RES[2]/taufpre)-1)
        Omega=(0.5-LTDMax*np.heaviside(y[24]+alphaAEACB1*y[26]-LTDstart, 1)*
                np.heaviside(LTDstop-y[24]-alphaAEACB1*y[26], 1)+
                np.heaviside(y[24]+alphaAEACB1*y[26]-LTPstart, 1)*LTPMax)
        dy = (Omega-y[25])/taufpre #equation of fpre
    
        #print(f'Max omega is: {np.max(Omega)}')
        plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre-syn weight')
        plt.plot(RES[2], RES[1]/RES[1][0], label = 'post-syn weight')
        #plt.plot(RES[2], taufpre/np.max(taufpre)*np.max(RES[0])*2, label = 'Omega')
        #plt.plot(RES[2], Omega, label = 'Omega')
        plt.plot(RES[2], dy)
        #opt_val = optim_values[f'{int(clust)}']
        #plt.title(f'striatum cluster n°{clust}, Glumax = {opt_val[5:]}, {mouse.mouse_name}')
        plt.title(f'striatum cluster n°{clust}, {mouse.mouse_name}')
        plt.ylabel('plasticity variation (%)')
        plt.xlabel('time (s)')
        plt.legend()
        #plt.vlines(depot_scotch, 0, 30, color = 'green')
        plt.vlines([mouse.ti, mouse.tf], 0, 30, color = ['red', 'red'])
        plt.show()
        

    print(f'len no sat: {len(list_pre_nosat)}, len sat: {len(list_pre_saturated)} ')
    plt.boxplot([w_pre_saturated_before, w_pre_saturated_after,w_pre_nosat_before , w_pre_nosat_after, -w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after])#, notch = True)
    #plt.boxplot([-w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after], positions=[1, 1.4])#, notch = True)
    plt.xticks([1,1.4], ['saturated', 'not saturated'])
    plt.title(f'compare btwn times {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()
    return 0

def plot_multi_big_cortex(router_list , mouse_list ):
    try:
        len(router_list) == len(mouse_list)
    except:
        LOGGER.error('list lenght are not the same')
    for it in range(len(router_list)):
        mouse = mouse_list[it]
        router = router_list[it]
        list_pre_saturated = ( )
        list_pre_nosat = ( )
        list_pre_transi = ( )
        w_pre_saturated_after = np.array([])
        w_pre_nosat_after = np.array([])
        w_pre_saturated_before = np.array([])
        w_pre_nosat_before = np.array([])
        time_vect = [mouse.ti-40, mouse.ti-20, mouse.tf, mouse.tf + 20] #maj sur conseil hugues, 20-30s pour stabilisation.
        #time_vect = [mouse.tf-20, mouse.tf+20, mouse.tf + 30,  mouse.tf + 50]#pourquoi avant en fait, 
        #time_vect = [mouse.ti-50, mouse.ti, mouse.tf, np.min([mouse.t_bounds[1] , mouse.tf + 200])] #pourquoi avant en fait, 
        result_id = router.get_result_id()

        for clust in result_id:
            RES = router.load_result(clust, name = mouse.mouse_name)
            """
            
            mean_selec_bef = compute_int_RES(RES, mouse.ti-55, mouse.ti-5)[1]
            mean_selec_aft = compute_int_RES(RES, mouse.tf, mouse.tf + 50)[1]

            if mean_selec_bef > 2.2:
                list_pre_saturated += (clust, )
            else:
                if mean_selec_aft > 2.2:
                    list_pre_transi += (clust, )
                else:
                    list_pre_nosat += (clust, )
            """
            
            mean_selec = compute_int_RES(RES, mouse.ti-50, mouse.ti)[1]
            mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
            mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
            if mean_selec > 2.2:
                list_pre_saturated += (clust, )
                w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
                w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
            else:
                list_pre_nosat += (clust, )
                w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
                w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
        
        print(stats.shapiro(-w_pre_nosat_before + w_pre_nosat_after))

        #plt.boxplot([-w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after], positions=[it, it+0.5])#, notch = True)
        plt.boxplot([-w_pre_nosat_before + w_pre_nosat_after], positions=[it/2])#, notch = True)

        #plt.xticks([it, it+0.5], [f'{mouse.mouse_name} sat', f'{mouse.mouse_name} not sat'])
        #plt.title(f'compare btwn times {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()
    return 0

def plot_multi_big_cortex_FR(router_list , mouse_list ):
    try:
        len(router_list) == len(mouse_list)
    except:
        LOGGER.error('list lenght are not the same')
    toplot = ()
    for it in range(len(router_list)):
        mouse = mouse_list[it]
        router = router_list[it]
        len_stri_nosat = np.array([])
        len_stri_sat = np.array([])
        list_pre_saturated = ( )
        list_pre_nosat = ( )
        list_pre_transi = ( )
        w_pre_saturated_after = np.array([])
        w_pre_nosat_after = np.array([])
        w_pre_saturated_before = np.array([])
        w_pre_nosat_before = np.array([])
        #time_vect = [mouse.ti-30, mouse.ti-10, mouse.tf, mouse.tf + 20] #maj sur conseil hugues, 20-30s pour stabilisation.
        time_vect = [mouse.tf, mouse.tf+20, mouse.tf + 30, mouse.tf + 50] #pourquoi avant en fait, 
        #time_vect = [mouse.ti-50, mouse.ti, mouse.tf, np.min([mouse.t_bounds[1] , mouse.tf + 200])] #pourquoi avant en fait, 
        result_id = router.get_result_id()

        for clust in result_id:
            RES = router.load_result(clust, name = mouse.mouse_name)            
            mean_selec = compute_int_RES(RES, mouse.ti-50, mouse.ti)[1]
            mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
            mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
            if mean_selec > 2.2:
                list_pre_saturated += (clust, )
                w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
                w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
                striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
                striatum_spikes = striatum_spikes[striatum_spikes<mouse.tf+200] #choose stop point
                FR = firing_rate(striatum_spikes, [np.min(striatum_spikes), np.max(striatum_spikes)], 0.5, 0.2 )
                len_stri_sat = np.append(len_stri_sat, np.max(FR[0]))#len(striatum_spikes))
            else:
                list_pre_nosat += (clust, )
                w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
                w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
                striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
                striatum_spikes = striatum_spikes[striatum_spikes<mouse.tf+200] #choose stop point
                FR = firing_rate(striatum_spikes, [np.min(striatum_spikes), np.max(striatum_spikes)], 0.5, 0.2 )
                len_stri_nosat = np.append(len_stri_nosat, np.max(FR[0]))#len(striatum_spikes))

        plt.scatter(len_stri_nosat, -w_pre_nosat_before + w_pre_nosat_after, color = "blue" )
        plt.scatter(len_stri_sat, -w_pre_saturated_before + w_pre_saturated_after, color = "red" )
        plt.show()
        #plt.show()

        #plt.boxplot([-w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after], positions=[it, it+0.5])#, notch = True)
        #plt.boxplot(, positions=[it/2])#, notch = True)

        #plt.xticks([it, it+0.5], [f'{mouse.mouse_name} sat', f'{mouse.mouse_name} not sat'])
        #plt.title(f'compare btwn times {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()

    return 0

def plot_big_cortex_jitter(router: Router, mouse: Mouse):
    opti = optiGlu(mouse)
    optim_values = opti.get_optim_values()
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    time_vect = [mouse.ti-60, mouse.ti-10, mouse.tf, mouse.tf + 50]
    result_id = np.unique(router.get_result_id_jitter())
    print(result_id)
    for clust in result_id:
        print(clust)
        for it in range(4):
            try:
                RES = router.load_result(f'{clust}_it{it}', name = mouse.mouse_name)
            #depot_scotch = mouse.frame_to_time(27020)-600 #depot scotch
                plt.plot(RES[2], RES[0]/RES[0][0], label = f'{it} pre-syn weight')
                plt.plot(RES[2], RES[1]/RES[1][0], label = f'{it} post-syn weight')
            except:
                print('missing values')
        opt_val = optim_values[f'{int(clust)}']
        plt.title(f'striatum cluster n°{clust}, Glumax = {opt_val[5:]}, {mouse.mouse_name}')
        plt.ylabel('plasticity variation (%)')
        plt.xlabel('time (s)')
        #plt.legend()
        #plt.vlines(depot_scotch, 0, 30, color = 'green')
        plt.vlines([mouse.ti, mouse.tf], 0, 30, color = ['red', 'red'])
        plt.show()
    return 0

def plot_compare_dt_sat(mouse: Mouse, router: Router):
    res = load_object(pathlib.Path(mouse.path_mouse, 'data_analyse', 'analyse_dt' ))
    intern_neuron = np.array([418, 554, 634, 155])
    cluster_striatum_sat = [18, 30, 44, 112, 131, 150, 201, 234, 239, 262, 266]
    cluster_striatum_nosat = [12, 40, 181, 186, 145, 197, 198, 212, 230, 272]
    cluster_cortex = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cluster_cortex = cluster_cortex[np.invert(cluster_cortex.isin(intern_neuron))]

    it_sat = ()
    it_nosat = ()

    for it, dt in enumerate(res):
        if np.isin(dt[1], cluster_striatum_sat):
            if np.isin(dt[0], cluster_cortex):
                it_sat += (it, )
        if np.isin(dt[1], cluster_striatum_nosat):
            if np.isin(dt[0], cluster_cortex):
                it_nosat += (it, )

    dt_sat = np.concatenate([res[it][2] for it in it_sat])
    dt_nosat = np.concatenate([res[it][2] for it in it_nosat])
    dt_sat = dt_sat[np.abs(dt_sat) < 1]
    dt_nosat = dt_nosat[np.abs(dt_nosat) < 1]

    plt.hist(dt_sat, alpha = 0.5, density=True, label = 'sat', bins = 400)
    plt.hist(dt_nosat, alpha = 0.5, density = True, label = 'nosat', bins = 400)
    plt.legend()
    plt.show()
    return 0

def plot_ID_inW(router, mouse, cluster_cortex, cluster_sitratum):

    for couple in list_of_couple:
        #print('GOT IN ONCE AT LEAST')
        #print(couple)
        RES = Router_exp.load_artifacts(couple[0], couple[1])
        mean_before = compute_int_RES(RES,  mouse.ti-200, mouse.ti-5)
        if mean_before[1] > 2.2:
            list_pre_saturated += (couple, )
        else:
            list_pre_nosat += (couple, )

    print(f'proportion of non saturated: {len(list_pre_nosat)}/{len(list_of_couple)}')
    #for couple in [list_pre_saturated[i] for i in [ 40]]: #For fig.1.pdf
    for couple in list_of_couple:
        RES = router.load_artifacts(couple[0], couple[1])
        y = Router_exp.load_artifacts(couple[0], couple[1], seg = 'sol')
        #cortex_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == couple[0]]
        #striatum_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == couple[1]]
        #FR_cortex = firing_rate(cortex_times, [0, 1100], 5, 1)
        #FR_striatum = firing_rate(striatum_times, [0, 1100], 5, 1)
        plt.plot(RES[2], RES[0][:]/RES[0][0], color = 'navy')
        plt.plot(RES[2], RES[1][:]/RES[1][0], color = 'firebrick')
        #plt.plot(np.arange(2.5, mouse.t_bounds[1]-mouse.t_bounds[0]-2, 1), FR_cortex, color = 'green')
        #plt.plot(np.arange(2.5, mouse.t_bounds[1]-mouse.t_bounds[0]-2, 1), FR_striatum, color = 'orange')
        '''taufpre=P1/((P1/1e-4)**P3+(y[24]+alphaAEACB1*y[26, :])**P3)+P4

        #Omega = (RES[0]*np.exp(RES[2]/taufpre) - y0init[25])/(np.exp(RES[2]/taufpre)-1) #WRONG dont take disontinuities into account
        Omega=(0.5-LTDMax*np.heaviside(y[24]+alphaAEACB1*y[26]-LTDstart, 1)*
                        np.heaviside(LTDstop-y[24]-alphaAEACB1*y[26], 1)+
                        np.heaviside(y[24]+alphaAEACB1*y[26]-LTPstart, 1)*LTPMax)'''

        #plt.plot(RES[2],Omega)
        #taufpre[taufpre > 100] = 100
        #plt.plot(RES[2], taufpre)
        plt.vlines([mouse.ti, mouse.tf] , 0, 30 , colors=['red', 'red'], label = 'duration of contact with tape')
        plt.legend(['pre-syn weight','post-syn weight'])#, 'fr cortex', 'fr striatum'])
        plt.ylabel('synaptic weight (%)')
        plt.xlabel('time (s) ')
        plt.title(f'Brain plasticity along time, id {couple} ')
        plt.show()
    
    return 0

#mouse_V7265 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')
#plot_func(mouse_V7265)

def main_big_cortex():
    #name = 'Blue'
    #mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    mouse_Black = load_object(f'/home/ahubert/Documents/SpikeSorting/Black/data_mouse/Black_mouse')
    mouse_Blue = load_object(f'/home/ahubert/Documents/SpikeSorting/Blue/data_mouse/Blue_mouse')
    mouse_Mouse1 = load_object(f'/home/ahubert/Documents/SpikeSorting/Mouse1/data_mouse/Mouse1_mouse')
    mouse_V4606 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4606/data_mouse/V4606_mouse')
    mouse_Red = load_object(f'/home/ahubert/Documents/SpikeSorting/Red/data_mouse/Red_mouse')
    mouse_V7265 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')

    router_Black = Router('562231799100305614','c91e543cb81b4736afbd3d05753b6d66') #Black
    router_Blue = Router('562231799100305614','8492562535d24ce8908f1fb780a80c1e') #Blue
    router_Blueshorten = Router('562231799100305614','17b5cc7fdd064b45aaaafb3daf6a95b7') #Bluesorten
    router_Mouse1 = Router('562231799100305614','25474d0ca15946309cd7385a95fbbc5d') #Mouse1 
    router_Mouse1short = Router('562231799100305614','e24a17c06075472a81c45f9d92bfbfe2') #Mouse1shorten to 20+-
    router_V4606 = Router('562231799100305614','92bab5dedf244a2cb15e8fb38c620a1d') #V4606
    router_Red = Router('562231799100305614','628286551c0b4853a0d4dc8b6a828997') #RED
    router_V7265 = Router('562231799100305614','9a5a2a7db6d843ee80c9c05e4362b64c') #V7265
    #router = Router(exp_id, '13b8924031bd48bd9d99e53140373753')
    #mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/V4606/data_mouse/V4606_mouse')

    #plot_multi_big_cortex([router_Black, router_Blue, router_Red, router_V4606, router_V7265, router_Mouse1],[mouse_Black, mouse_Blue, mouse_Red, mouse_V4606, mouse_V7265, mouse_Mouse1])

    #plot_multi_big_cortex([router_Black, router_Blue, router_Blueshorten, router_Mouse1, router_Mouse1short],[mouse_Black, mouse_Blue, mouse_Blue, mouse_Mouse1, mouse_Mouse1])

    #plot_big_cortex(router_V7265_low, mouse_V7265)

def main2():
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')
    router = Router('490892905216247639', 'e4a16bbf69a64f3ca1cdc98a2ae69b6f')
    list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= 'V7265',header=2, usecols=[0,1])
    list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
    #print(list.loc[0,:]+list.loc[,:])
    real_pair = [list.loc[it,:] for it in range( len (list)) ]
    print(real_pair)

    for couple in real_pair:
        try:
            RES = router.load_result(f'{couple[1]}_{couple[0]}')
            plt.plot(RES[2], RES[0]/RES[0][0])
            plt.plot(RES[2], RES[1]/RES[1][0])
            plt.vlines([mouse.ti, mouse.tf], 0, 20, color = 'red')
            plt.show()
        except:
            print(f'{(couple[0], couple[1])} not found')
    '''it = 0
    fig, axs = plt.subplots(3,4)
    for couple in real_pair:
        try:
            a,b = it // 4, it %  4
            RES = router.load_result(f'{couple[0]}_{couple[1]}')
            axs[a, b].plot(RES[2], RES[0]/RES[0][0])
            axs[a, b].plot(RES[2], RES[1]/RES[1][0])
            axs[a, b].vlines([mouse.ti, mouse.tf], 0, 20, color = 'red')
            axs[a, b].set_title(f'{it}')
            #plt.savefig(f"/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/490892905216247639/00e4f28b9afb4895b4cb1aff9032822f/fig/{(couple[0])}_{couple[1]}.pdf", format="pdf", bbox_inches="tight")
            #plt.show()
            it +=1
        except:
            print(f'{couple} not found {couple[0]}')
    plt.savefig(f"/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/490892905216247639/00e4f28b9afb4895b4cb1aff9032822f/fig/find_real_pairs.pdf", format="pdf", bbox_inches="tight")
    plt.show()
    '''
    return 0

def plot_ID(router:Router, mouse:Mouse, couple, with_Omega = True, with_FR = False):
    RES = router.load_result_couple(couple[0], couple[1])
    y = router.load_solution_couple(couple)#, seg = 'sol')
    '''if np.shape(y)[0]>10:
        y[0] = y[24]
        y[2] = y[26]'''
    if with_Omega == True:
        taufpre=P1/((P1/1e-4)**P3+(y[0]+alphaAEACB1*y[2])**P3)+P4
        Omega=(0.5-LTDMax*np.heaviside(y[0]+alphaAEACB1*y[0]-LTDstart, 1)*
                        np.heaviside(LTDstop-y[0]-alphaAEACB1*y[2], 1)+
                        np.heaviside(y[0]+alphaAEACB1*y[2]-LTPstart, 1)*LTPMax)
        
        taufpre[taufpre > 100] = 100
        plt.plot(RES[2], Omega, label = 'omega', alpha = 0.6)
        plt.plot(RES[2], taufpre, label = 'taufpre', alpha = 0.6)
    if with_FR == True:
        spikes_cortex = mouse.spikes_from_ID(couple[0])
        spikes_striatum = mouse.spikes_from_ID(couple[1])
        FR_cortex = firing_rate(spikes_cortex, [RES[2][0], RES[2][-1]], win_len = 0.2, win_step=0.1 )
        FR_striatum = firing_rate(spikes_striatum, [RES[2][0], RES[2][-1]], win_len = 0.2, win_step=0.1 )
        plt.plot(FR_cortex[1], FR_cortex[0], alpha = 0.8, label='FR_cortex',  linestyle = 'dashed')
        plt.plot(FR_striatum[1], FR_striatum[0],alpha = 0.8, label = 'FR_striatum', linestyle = 'dashed')

    #plt.vlines([mouse.ti, mouse.tf], 0, 20, color = 'red')
    plt.vlines([mouse.tf+30, mouse.tf+35], 0, 20, color = 'red')
    plt.plot(RES[2], RES[0]/RES[0][0], label = 'wpre')
    plt.plot(RES[2], RES[1]/RES[1][0], label = 'wpost')
    plt.title(f'{mouse.mouse_name} {couple}')
    plt.legend()
    plt.show()
    return 0

def main_plot():
    router_Red = Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    mouse_Red = load_object(f'/home/ahubert/Documents/SpikeSorting/Red/data_mouse/Red_mouse')

    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f')
    mouse_V4606 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4606/data_mouse/V4606_mouse')

    router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f')
    mouse_V7265 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')

    router_Mouse1 = Router('490892905216247639','c289df2d94674edb9c16c64e32711432')
    mouse_Mouse1 = load_object(f'/home/ahubert/Documents/SpikeSorting/Mouse1/data_mouse/Mouse1_mouse')

    router_V4607 = Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    mouse_V4607 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4607/data_mouse/V4607_mouse')

    '''#V4606___________________________________________________________
    L_V4606 = np.array([(364, 736), (370, 690), (905, 730), (948, 15), (948, 675), (906, 730), (748, 61), (279, 12), (120, 675), (807, 12), (120, 656), (256, 649), (1013, 644), (365, 690), (471, 675), (637, 645), (748, 690), (806, 728), (471, 649)])

    #V4607___________________________________________________________

    L_V4607 = np.array([(906, 53), (887, 62), (928, 765), (927, 39), (926, 267), (927, 590), (926, 263), (832, 718), (867, 62), (927, 712), (900, 644)])

    #Red___________________________________________________________

    L_Red = np.array([(558, 197), (561, 1672), (1474, 1690), (1474, 325), (594, 1795), (624, 1690),  (662, 393), (617, 109), (539, 320), (662, 743), (1474, 1590), (617, 743), (1474, 432), (539, 112), (685, 1430), (675, 1496),  (539, 131), (617, 184)])
    #(601, 230), (601, 49), (685, 395), (601, 1529), (617, 1483), (601, 938), (601, 724), (568, 268),(601, 32),(601, 440), 
    #V7265___________________________________________________________

    L_V7265 = np.array([(1142, 1338), (378, 747), (263, 1312), (212, 1312), (320, 1381), (212, 982), (1045, 826), (1153, 138), (1045, 1426), (540, 1312), (529, 1338), (1045, 922), (529, 138), (540, 786), (506, 436), (320, 1426), (1045, 1352), (1149, 1388), (1153, 823), (1045, 158),  (1045, 1416)])
    #(1190, 626), (261, 1005),(1190, 786), (304, 1381), (1190, 989), 
    #Mouse1___________________________________________________________

    L_Mouse1 = np.array([(445, 860), (378, 882), (391, 860), (424, 294), (526, 860), (391, 862), (466, 261), (1103, 273), (390, 1012), (498, 218), (445, 249),  (659, 296)])
    #(445, 294), (1190, 640), (1101, 235), (391, 941), (417, 755), (445, 755), (391, 913), (424, 913), (1097, 273), (437, 1020), (420, 969), (1159, 913), (667, 24), (1097, 68), (1097, 969), (654, 969)
    '''

    #[650, 1282, 1287, 1441, 108, 158, 1352]#striatum
    plot_FR(mouse_V7265, [297,331,227,232,243,268,294,371,575,601,732])
    '''real_post = mouse_Mouse1.get_correlated_couples('Post')
    real_post = real_post[real_post['Keep'] == 1]
    striatum_id = real_post['Striatum_ID'].to_numpy()
    cortex_id = real_post['Cortex_ID'].to_numpy()
    couples = [(cortex_id[it], striatum_id[it]) for it in range (len(striatum_id))]
    for couple in couples:
        plot_ID(router_Mouse1, mouse_Mouse1, couple, with_FR = True)'''
        
    return 0

'''
    router_V7 = Router(exp_id, '894b008f912248e2b1fc58670f6ee92b')
    router_V7_2 = Router(exp_id, '3167a5215aa44bd1889d2dbbf002c972')
    for id in router_V7.get_result_id():
        RES = router_V7.load_result(f'{id}', V7265_mouse.mouse_name)
        jump_list = dens.is_jump(RES[2], RES[0])
        for jump in jump_list:
            plt.vlines([jump.start, jump.stop], 0,np.max(RES[0]*2), color = ['orange','red'])

        plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre')
        #plt.plot(RES[2], RES[1]/RES[1][0], label = 'post')
        #plt.vlines([V7265_mouse.ti, V7265_mouse.tf], 0, np.max(RES[0]*2), color = 'red')
        #RES = router_V7_2.load_result(f'{id}', V7265_mouse.mouse_name)
        #plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre_2')
        #plt.plot(RES[2], RES[1]/RES[1][0], label = 'post_2')
        plt.legend()
        plt.title('V7265')
        plt.show()
'''

'''
router_V6 = Router(exp_id, '31b1ef6511a54be585503553ed0069d5')
router_V6_2 = Router(exp_id, 'f66295897b734bd69b9ef509391e33d4')
for id in router_V6.get_result_id():
    RES = router_V6.load_result(f'{id}', V4606_mouse.mouse_name)
    plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre')
    plt.plot(RES[2], RES[1]/RES[1][0], label = 'post')
    plt.vlines([V4606_mouse.ti, V4606_mouse.tf], 0, np.max(RES[0]*2), color = 'red')
    RES = router_V6_2.load_result(f'{id}', V4606_mouse.mouse_name)
    plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre_2')
    plt.legend()
    plt.title('V4606')
    plt.show()
'''


'''
router_red = Router(exp_id, '16df42c893b64f58b11b00941b94ef76')
router_red_2 = Router(exp_id, '9949d442049f435395a6ef7d4c6545b2')
for id in router_red.get_result_id():
    RES = router_red.load_result(f'{id}', red_mouse.mouse_name)
    plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre')
    plt.plot(RES[2], RES[1]/RES[1][0], label = 'post')
    plt.vlines([red_mouse.ti, red_mouse.tf], 0, np.max(RES[0]*2), color = 'red')
    RES = router_red_2.load_result(f'{id}', red_mouse.mouse_name)
    plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre_2')
    plt.plot(RES[2], RES[1]/RES[1][0], label = 'post_2')
    plt.legend()
    plt.title('RED')
    plt.show()
'''

'''
router_red = Router(exp_id, '0ae3470a2e334a1890166661db0599b8')
router_V7 = Router(exp_id, '13b8924031bd48bd9d99e53140373753')
router_V6 = Router(exp_id, '3e4aa9a2bf104810861967f0340664a6') 

mouse = V7265_mouse
time_vect = [mouse.ti-60, mouse.ti-10, mouse.tf, mouse.tf + 50]
w_pre_before_V7 = np.array([])
w_pre_after_V7 = np.array([])
for clust in router_V7.get_result_id():
    RES = router_V7.load_result(f'{clust}', name = mouse.mouse_name)
    mean_selec = compute_int_RES(RES, mouse.ti-60, mouse.ti-10)[1]
    mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
    mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
    if mean_selec < 2.6:
        w_pre_after_V7 = np.append(w_pre_after_V7, mean_after[0] )
        w_pre_before_V7 = np.append(w_pre_before_V7, mean_before[0] )

mouse = V4606_mouse
time_vect = [mouse.ti-60, mouse.ti-10, mouse.tf, mouse.tf + 50]
w_pre_before_V6 = np.array([])
w_pre_after_V6 = np.array([])
for clust in router_V6.get_result_id():
    RES = router_V6.load_result(f'{clust}', name = mouse.mouse_name)
    mean_selec = compute_int_RES(RES, mouse.ti-60, mouse.ti-10)[1]
    mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
    mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
    if mean_selec < 2.6:
        w_pre_after_V6 = np.append(w_pre_after_V6, mean_after[0] )
        w_pre_before_V6 = np.append(w_pre_before_V6, mean_before[0] )

mouse = red_mouse
time_vect = [mouse.ti-60, mouse.ti-10, mouse.tf, mouse.tf + 50]
w_pre_before_red = np.array([])
w_pre_after_red = np.array([])
for clust in router_red.get_result_id():
    RES = router_red.load_result(f'{clust}', name = mouse.mouse_name)
    mean_selec = compute_int_RES(RES, mouse.ti-60, mouse.ti-10)[1]
    mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
    mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
    if mean_selec < 2.6:
        w_pre_after_red = np.append(w_pre_after_red, mean_after[0] )
        w_pre_before_red = np.append(w_pre_before_red, mean_before[0] )

plt.boxplot([w_pre_after_red - w_pre_before_red,  w_pre_after_V6 - w_pre_before_V6, w_pre_after_V7 - w_pre_before_V7])
plt.show()
plt.boxplot([w_pre_after_red , w_pre_before_red,  w_pre_after_V6 , w_pre_before_V6 , w_pre_after_V7 , w_pre_before_V7])
plt.show()
'''


'''
router = Router(exp_id, '2838b64f77154046aa39f5911147b750')
plot_compare_dt_sat(red_mouse, router)

exp_id = '562231799100305614'
router = Router(exp_id, '2cd9ecadbf8f42ee9b344f84fb0ecd45')
plot_big_cortex_jitter(router, V4606_mouse)

router = Router(exp_id, '8bd63e18eb7341509877da0d252cc784')
plot_big_cortex_jitter(router, red_mouse)

router = Router(exp_id, '055e5b9bdba1450a9d2bad7f34372089')
plot_big_cortex_jitter(router, V7265_mouse)
'''



'''
analyse_dt(V7265_mouse)
analyse_dt(red_mouse)
analyse_dt(V4606_mouse, selection_inter = True)
plt.legend(['V7', 'Red', 'V4'])
plt.show()
'''

'''
RES = np.load(f'/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/e5a7f494b91e42838208114339e4fdf6/artifacts/res_Red.npy')
plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre-syn weight')
plt.plot(RES[2], RES[1]/RES[1][0], label = 'post-syn weight')
plt.ylabel('plasticity variation (%)')
plt.xlabel('time (s)')
plt.legend()
#plt.vlines([V7265_mouse.ti, V7265_mouse.tf], 0, 30, color = ['red', 'red'])
plt.show()
'''