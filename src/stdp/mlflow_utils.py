#!/usr/bin/env python3
'''
Utility functions for MLflow
'''

import logging
import pathlib

import mlflow
import numpy as np

LOGGER = logging.getLogger(__name__)


def log_numpy_data(data, path):
    '''
    Log arbitrary numpy data with MLflow

    Args:
        data:
            Any data object that can be saved with numpy.save.

        path:
            The path under which to log the data. It may be a relative path.
    '''
    path = pathlib.Path(path).with_suffix('.npy')
    LOGGER.info('Saving %s', path)
    artifact_path = None
    if len(path.parts) > 1:
        path.parent.mkdir(parents=True, exist_ok=True)
        artifact_path = str(path.parent)
    path = str(path.resolve())
    np.save(path, data)
    mlflow.log_artifact(path, artifact_path=artifact_path)