import logging
import time
import pathlib
import pickle
#from pathlib import Path

import numpy as np
import pandas as pd
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)

""" #Intro
This script is used to uniformize mouse data when loaded. Done mouse by mouse because too annoying to make a generalised function.
"""

def save_object(obj, file):
    try:
        with open(file, "wb") as f:
            pickle.dump(obj, f)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)

def load_object(filename):
    try:
        with open(filename, "rb") as f:
            return pickle.load(f)
    except Exception as ex:
        print("Error during unpickling object (Possibly unsupported):", ex)

########################################## TEAM AND TIMES #############################################################

def old_init():
    '''
    useless, just to keep a track
    '''
    
    cols_load_resume =  ['Team A', 'channel A', 'Team B+', 'channel B+', 'Team B', 'channel B', 'Team B-', 'channel B-', 'Team C', 'channel C']
    cols_name_resume =  ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B', 'channel_B', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']
    cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
    cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']

    #resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V7265/V7265_sorted.xlsx', header = 3)#, usecols = cols_load_resume) #V7265
    #resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V4606/V4606_sorted.xlsx', header = 6)#, usecols = cols_load_resume) #V4606
    resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Red/Rouge_Clustering.xlsx')#, usecols = cols_load_resume) #Red_mouse

    #resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'Unnamed: 8', 'Team B-', 'channel.2', 'Team C', 'channel.3']] #V4606
    #resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'channel.2', 'Team B-', 'channel.3', 'Team C', 'channel.4']] #V7265
    resume = resume[['Team A', 'Unnamed: 1', 'Team B+ ', 'Unnamed: 3', 'Team B-', 'Unnamed: 5', 'Team C', 'Unnamed: 7']] #RED NEED TO GET CHANNEL
    resume.columns = ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C'] #RED

    #resume.columns = cols_name_resume
    #resume = read_ods('/home/ahubert/Documents/SpikeSorting/V4606/V4606_sorted.ods', header = 6, usecols = cols_load_resume)

    spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/Spike_times_Rouge_Charlotte_Clustering.txt', usecols = cols_load_time)
    #spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/Spike_times_V7265_Charlotte_Clustering.txt', usecols = cols_load_time)
    #spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606/Spike_times_V4606_Charlotte_Clustering.txt', usecols = cols_load_time)
    spike_times.columns = cols_name_time

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
    cluster_info = cluster_info.set_index('cluster_id')
    resume = resume.drop([91, 108])

    resume.channel_A[:] = cluster_info.ch[resume.Team_A[:]]

    #synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/V7265_synchro', usecols=[0,1])
    #synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606/V4606_synchro', usecols=[0,1])
    #synchro.columns = ['time', 'frame']


    #get spike of team A of striatum
    #striatum_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A < 180])]
    striatum_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A < 210])]
    #striatum_times.index = np.arange()
    #get spike of team A of cortex
    #cortex_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A > 200])]
    #cortex_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A > 250])]

############################################# V7265 #############################################

def prepare_V7265():
    """
    LFP Not loaded because heavy (about 2Go), same for synchro (about 200+Mo)
    Spike times only for team A and B+

    OUTPUT: ('V7265', spike_times, resume, (range(0,180), range(200,383)))
    """

    resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V7265/V7265_sorted.xlsx', header = 3) #V7265
    cols_load_resume =  ['Team A', 'channel A', 'Team B+', 'channel B+', 'Team B', 'channel B', 'Team B-', 'channel B-', 'Team C', 'channel C']
    cols_name_resume =  ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B', 'channel_B', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']
    cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
    cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']

    resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'channel.2', 'Team B-', 'channel.3', 'Team C', 'channel.4']] #V7265
    resume.columns = cols_name_resume
    spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/Spike_times_V7265_Charlotte_Clustering.txt', usecols = cols_load_time)
    spike_times.columns = cols_name_time

    #synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/V7265_synchro.txt', usecols=[0,1])

    new_cluster = np.concatenate((resume.Team_A.to_numpy(), resume['Team_B+'].to_numpy()))
    new_chan = np.concatenate((resume.channel_A.to_numpy(), resume['channel_B+'].to_numpy()))
    selection_valid = np.concatenate((np.isfinite(resume.Team_A.to_numpy()), np.isfinite(resume['Team_B+'].to_numpy()))) #selection_valid is here to handle NaN
    new_groupe = ['']*np.sum(selection_valid)
    new_groupe[:np.sum(np.isfinite(resume.Team_A.to_numpy()))] = 'A'
    new_groupe[np.sum(np.isfinite(resume.Team_A.to_numpy())) :] = 'B+'
    new_cluster = new_cluster[selection_valid]
    new_chan = new_chan[selection_valid]

    resume = pd.DataFrame([new_groupe, new_cluster, new_chan])
    resume = resume.transpose()
    resume.columns = ['Team', 'Cluster_ID', 'Channel_ID']

    #return is in good order to pass it to Mouse class
    return ('V7265', spike_times, resume, (range(0,181), range(200,384)), np.array([4, 448, 1378]),  [0, 1100], [619.78, 626.70]) # 635.8, 642.7
        #  mouse_name, spike_times, resume, brain_region, t_bounds, contact_time

############################################# V4606 #############################################

def prepare_V4606():
    """
    LFP Not loaded because heavy (about 2Go), same for synchro (about 200+Mo)
    Spike times only for team A and B+

    OUTPUT: ('4606', spike_times, resume, (range(0,57), range(58,383)))
    """
    #921 bug team B+ but 0 spike

    cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
    cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']
    cols_name_resume =  ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B', 'channel_B', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']

    resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V4606/V4606_sorted.xlsx', header = 6)
    resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'Unnamed: 8', 'Team B-', 'channel.2', 'Team C', 'channel.3']] #V4606
    resume.columns = cols_name_resume
    spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606/Spike_times_V4606_Charlotte_Clustering.txt', usecols = cols_load_time)
    spike_times.columns = cols_name_time

    new_cluster = np.concatenate((resume.Team_A.to_numpy(), resume['Team_B+'].to_numpy()))
    new_chan = np.concatenate((resume.channel_A.to_numpy(), resume['channel_B+'].to_numpy()))
    selection_valid = np.concatenate((np.isfinite(resume.Team_A.to_numpy()), np.isfinite(resume['Team_B+'].to_numpy()))) #selection_valid is here to handle NaN
    new_groupe = ['']*np.sum(selection_valid)
    new_groupe[:np.sum(np.isfinite(resume.Team_A.to_numpy()))] = 'A'
    new_groupe[np.sum(np.isfinite(resume.Team_A.to_numpy())) :] = 'B'
    new_cluster = new_cluster[selection_valid]
    new_chan = new_chan[selection_valid]

    resume = pd.DataFrame([new_groupe, new_cluster, new_chan])
    resume = resume.transpose()
    resume.columns = ['Team', 'Cluster_ID', 'Channel_ID']

    #return is in good order to pass it to Mouse class
    return ('V4606', spike_times, resume, (range(0,58), range(58,384)), np.array([63, 71, 222, 197, 962, 1034]), [0, 1200], [901.48, 965.62])
          #mouse_name, spike_times, resume, brain_region, t_bounds, contact_time

#############################################  RED  #############################################

def prepare_Red():
    """
    LFP Not loaded because heavy (about 2Go), same for synchro (about 200+Mo)
    Spike times only for team A and B+

    OUTPUT: 'Red', spike_times, resume, (range(0,210), range(250,383))
    """
    cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
    cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']

    resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Red/Rouge_Clustering.xlsx')
    resume = resume[['Team A', 'Unnamed: 1', 'Team B+ ', 'Unnamed: 3', 'Team B-', 'Unnamed: 5', 'Team C', 'Unnamed: 7']] #RED NEED TO GET CHANNEL
    resume.columns = ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']
    spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/Spike_times_Rouge_Charlotte_Clustering.txt', usecols = cols_load_time)
    spike_times.columns = cols_name_time

    cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
    cluster_info = cluster_info.set_index('cluster_id')

    #clust put as Nan in resume if channel not known is cluster_info (probably hand error)
    resume['Team_B+'].loc[[10, 21, 78]] = np.NaN # , 91, 108]) #by hand selectionned
    resume.Team_A.loc[[91, 108]] = np.NaN

    new_cluster = np.concatenate((resume.Team_A.to_numpy(), resume['Team_B+'].to_numpy(), np.array([150, 266, 601, 1815])))
    selection_valid = np.isfinite(new_cluster)
    new_cluster = new_cluster[selection_valid]
    new_chan = cluster_info.ch[new_cluster]

    #resume.channel_A[:] = cluster_info.ch[resume.Team_A[:]]
    #resume['channel_B+'][:] = cluster_info.ch[np.isfinite(resume['Team_B+'][:])]

    #new_cluster = np.concatenate((resume.Team_A.to_numpy(), resume['Team_B+'].to_numpy(), np.array([150, 266, 601, 1815])))
    #new_chan = np.concatenate((resume.channel_A.to_numpy(), resume['channel_B+'].to_numpy(), np.array([80, 126, 286, 223])))
    #selection_valid = np.concatenate((np.isfinite(resume.Team_A.to_numpy()), np.isfinite(resume['Team_B+'].to_numpy()))) #selection_valid is here to handle NaN
    new_groupe =  ['']*len(new_cluster)
    new_groupe[:138] = 'A'*138
    new_groupe[138 : ] = 'B'*(len(new_groupe)-138)
    resume = pd.DataFrame([new_groupe, new_cluster, new_chan])
    resume = resume.transpose()
    resume.columns = ['Team', 'Cluster_ID', 'Channel_ID']

    #return is in good order to pass it to Mouse class
    return ('Red', spike_times, resume, (range(0,211), range(250,384)), np.array([418, 554, 634, 155]), [0, 690], [524.10, 604.18])
          #mouse_name, spike_times, resume, brain_region, t_bounds, contact_time

############################################# Class #############################################

class Mouse():
    default_dir_mouse = pathlib.Path('/home/ahubert/Documents/SpikeSorting')
    default_dir_optim_glumax = pathlib.Path('/home/ahubert/Documents/Code_STDP/model-potcadep')

    def __init__(self, mouse_name, spike_times, resume, brain_region, interneurons,t_bounds, contact_time,
                 dir_mouse = default_dir_mouse, dir_optim_glumax = default_dir_optim_glumax): #mouse_name must be the name of the directory
        '''
        Args:
            mouse_name:
                must be the name of the mouse directory

            spikes_times:
                dataframe, three columns like ['Spike_times', 'Cluster_ID', 'Channel_ID']

            resume:
                dataframe, two columns like ['Team', 'Cluster_ID', 'Channel_ID']

            brain_region:
                format like ('iterable', 'iterable'). Expected to be ('striatum', 'cortex') at the moment

            dir_mouse:
                path of all mouses data directory (parent of the specific mouse)
        '''
        self.mouse_name = mouse_name
        self.path_mouse = pathlib.Path(dir_mouse, mouse_name).resolve()
        self.path_optim_glumax = pathlib.Path(dir_optim_glumax, mouse_name, 'optim_Glumax').resolve()
        #self.LFP = Mouse.get_LFP(self) #not by default because too heavy (about 2Go)
        self.spike_times = spike_times
        self.resume = resume
        self.brain_region = brain_region
        self.interneurons = interneurons
        self.t_bounds = t_bounds
        self.ti = contact_time[0]
        self.tf = contact_time[1]
        self._clear_all()

    def _clear_all(self):
        self._LFP = (None, None)
        # Tuple of Synchro dataframe and timestamp of when it was loaded.
        self._synchro = (None, None)

    @property
    def LFP(self):
        data, last_modified = self._LFP
        modified = self.path_mouse.stat().st_mtime
        if data is None or modified >= last_modified:
            data = se.read_spikeglx(self.path_mouse, stream_id = 'imec0.lf').get_traces()
            self._LFP = (data, modified)
        return data
    
    @property
    def LFP_cortex(self):
        return self.LFP[:, self.brain_region[1]]
    
    @property
    def synchro(self):
        #path = self.path_mouse / f'{self.mouse_name}_synchro.txt'
        path = self.path_mouse / f'{self.mouse_name}_Test_Table_data.txt'
        data, last_modified = self._synchro
        modified = path.stat().st_mtime
        if data is None or modified >= last_modified:
            data = pd.read_csv(path, usecols=[0,1])
            data.columns = ['time', 'frame']
            self._synchro = (data, modified)
        return data
    
    @property
    def speed(self):
        path = self.path_mouse / f'{self.mouse_name}_synchro.txt'
        data, last_modified = self._synchro
        modified = path.stat().st_mtime
        if data is None or modified >= last_modified:
            data = pd.read_csv(path, usecols=[0,4])
            data.columns = ['time','speed']
            self._synchro = (data, modified)
        return data

    def frame_to_time(self, frame):
        '''
        link frame to time, relatively to the begin of synchro doc. In fact, return the mean of time captured during the frame 
        (because multiple time point within a frame)
        '''
        time = np.mean(self.synchro.time[self.synchro.frame == frame])# - self.synchro.time[0] cause all is already on t0 of spike recording
        return time

    def spikes_from_ID(self, clust):
        return self.spike_times.Spike_times[self.spike_times.Cluster_ID == int(clust)].to_numpy()
    
    def get_cluster(self, region, interneurons = False):
        '''
        Return a list of neuron cluster depending on parameters
        
        args:
            region: exected 'striatum' or 'cortex'
            interneurons: if False, remove cluster considered as interneurons.
            
        '''
        if region == 'striatum':
            res = self.resume.Cluster_ID[self.resume.Channel_ID.isin(self.brain_region[0]) ]
            if interneurons == False:
                res = res[np.invert(np.isin(res, self.interneurons))]
        if region == 'cortex':
            res = self.resume.Cluster_ID[self.resume.Channel_ID.isin(self.brain_region[1]) ]
            if interneurons == False:
                res = res[np.invert(np.isin(res, self.interneurons))]        
        return res

    def get_optim_values(self, id = 'pre'):
        if id == 'pre':
            path = self.path_optim_glumax / 'cortex_only' / 'Glumax_mem.npy'
        else:
            path = self.path_optim_glumax / f'{id}' / 'Glumax_mem.npy'
        try:
            values = np.load(path)
        except:
            LOGGER.error(f'Cannot find file: {path}')
        return values
    
    def get_real_pair(self, path = 'Spike_pairs_final'):
        list = pd.read_excel(f'/home/ahubert/Documents/SpikeSorting/{path}.xlsx', sheet_name= f'{self.mouse_name}',header=2, usecols=[0,1])
        list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
        real_pair = list.to_numpy()
        check_real = real_pair[:, 0]**3 + real_pair[:, 1]**3

        return real_pair, check_real
    
    def get_correlated_couples(self, sheet = 'All'):
        '''return correlated couples
        sheet allow to choose on which base the correlation has been done, choices are:
        All, Baseline, Post, Transi
        '''
        path = self.path_mouse / 'data_analyse' / 'correlated_couples' / f'correlated_couples_{self.mouse_name}.xlsx'
        if sheet == 'All':
            list = pd.read_excel(path, sheet_name= sheet, usecols=[0,1])
            list.columns = ['Striatum_ID', 'Cortex_ID']
        if sheet == 'Baseline' or sheet == 'Post':
            list = pd.read_excel(path, sheet_name= sheet, usecols=[0,3,4])
            list.columns = ['Keep','Striatum_ID', 'Cortex_ID']

        return list
    
    def selec_Team(self, list_clusters, team = ['Team_A', 'Team_Bplus']):
        '''From a list of clusters, selec thoses who are from a certain quality team.
        team: ['Team_A', 'Team_Bplus', 'Team_Bmoins']
        '''
        path = Mouse.default_dir_mouse / 'Clusters_sorted_Team.xlsx'
        ref_tab = pd.read_excel(path, usecols = team, sheet_name = self.mouse_name).to_numpy()
        list_clusters = list_clusters[np.isin(list_clusters, ref_tab)]

        return list_clusters