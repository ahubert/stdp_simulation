import logging

import stdp.VectorAccessor
import stdp.synapseModel
#import stdp.synapseModel_omega
import numpy as np
import scipy
from hydronaut.hydra.omegaconf import get_container

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)

def integration(params, t_pre, t_post, y0init, t_end = 0, glu_release = None, scipy_options = None):
    '''
    INPUT VAR:
        t_pre: spike timing of pre-synaps, np.array.
        t_post: spike timing of post-synapse, np.array
        t_end: duration of stabilization, float, (optional)
        integration method: choice of methode in scipy.integrate.solve_ivp.
            Radau is default becauses adapted for stiff problems.
        params: parameters of the experience

    LOCAL VAR:
        it_pre, it_post : suivi en indice des spikes pre/post

    OUTPUT :
        [fpre_inf,CaMKact_inf, t_sol, sol, sol_last]
        fpre_inf, CaMKact_inf, t_sol : shape (N, )
        sol : shape (len(y), N)
        sol_last : shape(len(y),) donne la sol avant stabilisation
    '''

    currstepon = 0
    
    """
    tsdt=0.01499 #start of depolarization before post spike.
    DPdur = 0.03 # duration of the postsyn depolarisation
    Glumax = 2000
    APmax = 7800 # peak amplitude of the postsyn action potential.
    """

    #region ####################################### initialisations #####################################

    fpre_inf = 0 # proxy for wpre
    CaMKact_inf = 0 # proxy for wpost

    #initial conditions
    y0_values = np.zeros(y0init.shape)
    y0_values[:] = y0init.copy()
    y0 = stdp.VectorAccessor.VectorAccessor(y0_values)

    #initialization of iterator
    it_pre = 0
    it_post = 0
    it_start_depol = 0
    it_stop_depol = 0
    #initialization of times for integration
    t_start = 0
    t_stop = 0

    #some tricks
    t_pre = np.append(np.sort(t_pre), np.infty)
    t_post = np.append(np.sort(t_post), np.infty)
    t_start_depol = t_post - params.integration.tsdt #times start of depol
    t_stop_depol = t_post - params.integration.tsdt + params.integration.DPdur #times end of depol

    #init where to keep results
    t_sol = np.array([np.min([t_post[it_post], t_pre[it_pre], t_start_depol[it_start_depol], t_stop_depol[it_stop_depol]])]) #temps de solutions calculées
    sol = np.transpose(np.array([y0init])) #solutions calculées
    sol_last = np.array([])

    #endregion ################################### end of initialisations #####################################
    if not(len(t_pre) == 1 and len(t_post) == 1):

        #First time integration
        trigger = np.argmin([t_post[it_post], t_pre[it_pre], t_start_depol[it_start_depol], t_stop_depol[it_stop_depol]]) #search the next event
        if trigger == 0: #event is post spike
            t_stop = t_post[it_post]
            next_add = ["post", ""]
            it_post = it_post + 1
        elif trigger == 1: #event is pre spike
            t_stop = t_pre[it_pre]
            next_add = ["pre", ""]
            it_pre = it_pre + 1
        elif trigger == 2: #event is start of depol
            t_stop = t_start_depol[it_start_depol]
            next_add = ["", "on"]
            it_start_depol = it_start_depol + 1
        elif trigger == 3: #event is end of depol
            t_stop = t_stop_depol[it_stop_depol]
            if t_stop_depol[it_stop_depol] < t_start_depol[it_start_depol-1] + params.integration.DPdur :
                next_add = ["", ""]
            else:
                next_add = ["", "off"]
            it_stop_depol = it_stop_depol + 1
        else: #event is not recognized
            LOGGER.info("trigger failed. Event like spike or depol not recognize at first step")

        # time loop, sum of len -4 because of infty add
        for it in range(len(t_pre)+3*len(t_post)-5): #-5 instead of -4 cause there has already been a first step

            add = next_add
            t_start = t_stop
            trigger = np.argmin([t_post[it_post], t_pre[it_pre], t_start_depol[it_start_depol], t_stop_depol[it_stop_depol]])
            if trigger == 0:
                t_stop = t_post[it_post]
                next_add = ["post", ""]
                it_post = it_post + 1
            elif trigger == 1:
                t_stop = t_pre[it_pre]
                next_add = ["pre", ""]
                it_pre = it_pre + 1
            elif trigger == 2:
                t_stop = t_start_depol[it_start_depol]
                next_add = ["", "on"]
                it_start_depol = it_start_depol + 1
            elif trigger == 3:
                t_stop = t_stop_depol[it_stop_depol]
                if t_stop_depol[it_stop_depol] < t_start_depol[it_start_depol-1] + params.integration.DPdur :
                    next_add = ["", ""]
                else:
                    next_add = ["", "off"]
                it_stop_depol = it_stop_depol + 1
            else:
                LOGGER.info("trigger failed. Event like spike or depol not recognized at the " + str(it+1) + "th" + " event at time " + str(t_sol[-1]) )

            #apply the effect of triggers
            if (add[1]=="on"): #start depol
                currstepon = 1
            elif(add[1]=="off"): #end depol
                currstepon = 0
            if (add[0]=="pre"): #pre spike release Glumax
                if str(type(glu_release)) == '<class \'NoneType\'>':
                    y0.Glu = y0.Glu + params.integration.Glumax #Glumax_temp
                else:
                    if trigger == 1:
                        y0.Glu = y0.Glu + glu_release[it_pre - 2]
                    else:
                        y0.Glu = y0.Glu + glu_release[it_pre - 1]
            elif (add[0]=="post"): #post spike increase the post current of APmax
                y0.Curr_spikepost = params.integration.APmax # +y0.Curr_spikepost we dont want that residuals sum up and exceed APmax

            # integrates bewteen two spike event
            in_work = True
            while (in_work == True):
                #LOGGER.info("integration start at: " + str(t_start) + " and end at " + str(t_stop) + '\n' + "iterations: " + str(it_post) + " " + str(it_pre) + " " + str(it_start_depol) + " " + str(it_stop_depol) )
                sol_curr = scipy.integrate.solve_ivp(stdp.synapseModel.STDP, [t_start, t_stop] , y0_values, args = (currstepon,), events = [event_Ca_cyt], method='RK45')#, **scipy_options)
                
                # concatenate the result of the current integration, between two events
                sol = np.concatenate((sol , sol_curr.y[:,1:]) , axis=1)
                t_sol = np.concatenate(( t_sol , sol_curr.t[1:]))

                # set initial conditions of the next integration round as the final conditions of the previous one
                y0_values[:] = sol_curr.y[:,-1]
            
                if len(sol_curr.t_events[0]) == 0: #if no event, go to the next trigger event
                    in_work = False
                else: #fix Ca_cyt at 1e-10 and continue integration
                    #LOGGER.info('We got an event')
                    y0_values[13] = 1e-10
                    t_start = sol_curr.t[-1]

            #mem = sol[0,:] + 2*np.sum(sol[1:4, :], axis=0) + 3*np.sum(sol[4:8, :], axis=0) + 4*np.sum(sol[8:11, :], axis=0) + 5*sol[11,:] + 6*sol[12,:]
            #if (t_sol[-1] < 550 and np.max(1+2*mem/164.6) > 3):
            #    return ('fail')

        sol_last = sol[:, -1] #to get easier the last sol during stimulation time if stabilisation
    else:
        t_sol = np.array([0])
    if (t_end > 0):
        # integrates at the end for stabilisation
        currstepon = 0
        sol_curr = scipy.integrate.solve_ivp(stdp.synapseModel.STDP, [t_sol[-1], t_sol[-1] + t_end] , y0_values, args = (currstepon,), events = [event_Ca_cyt], method='RK45')#**scipy_options)

        # concatentate the result of the current integration, between two events

        sol = np.concatenate((sol , sol_curr.y[:,1:]) , axis=1)
        t_sol = np.concatenate(( t_sol , sol_curr.t[1:]))
    #final output variables
    fpre_inf = sol[25,:] #pre_inf is the presynaptic weight
    CaMKact_inf = sol[0,:] + 2*np.sum(sol[1:4, :], axis=0) + 3*np.sum(sol[4:8, :], axis=0) + 4*np.sum(sol[8:11, :], axis=0) + 5*sol[11,:] + 6*sol[12,:]

    #Normalization of the proxy variables to obtain the real pre and post weights
    CaMKact_inf=1+2*CaMKact_inf/164.6

    return((fpre_inf, CaMKact_inf, t_sol, sol, sol_last))

def event_Ca_cyt(t,y,a):
    return(y[13])
event_Ca_cyt.terminal = True