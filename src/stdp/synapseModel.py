import numpy as np
from numba import jit
from numba.extending import overload, register_jitable

#region ################################################# PARAMS FOR STDP ############################################
# Declare dynamically at the point of import. Yes, it's a hack.

#Stimulation parameters

#Glumax=2000#set in the main function cause it's an event parameter


tauGlu=5e-3

# Parameters of the Ca influx through VDCC (bAP)
# set so as to mimick the Fino et al paper, Fig 1 C
# ie the depol brings \DeltaV = 45 mV, 
# and the bAP  \DeltaV = 60 mV in supplement
# hence the total is around 100 mV, as reported by Sabatini-Carter 2004
#APdur=0.03#set in the main function
#APmax=7800#set in the main function

DPmax = 550
ksivdcc=0.1#µM.s-1.A-1
#set so that the bAP make \Delta Ca = 400 nM (Sabatini Carter 2004)
tausbAP=0.001


#Parameters of the AMPAR
gAMPAmax=0.28 #in nS, set so that each pre spike increases V from around 1 mV
alphaAMPA=1.10#s-1µM-1
betaAMPA=190#s-1

#Parameters of the NMDAR
gNMDAmax=0.28#0.28#nS
ksi=3040 # in µM.s-1.A-1
# gNMDAmax and ksi are set so that each pre spike increases Ca of circa
# 0.17 µM at -70 mV (Sabatini et al, 2002)
alphaNMDA=0.072#s-1µM-1
betaNMDA=100#s-1
#alpha and beta NMDA are set so that the time-change of NMDAR-transported 
#Ca behaves in agreement with corresponding experimental observation 
#in Sabatini et al, 2002 ie a very rapide rise ( 2ms) then decay phase 
#that needs around 200 ms to converge back to baseline.

#External Mg concentration (mM)
Mg=1.0

#Parameters for the CAMKII activation (ionotrop plasticity)
CaMKT=16.6#fixed by experiments see Graupner-Brunel 2007
#phosphorylation
k5=0.1 #fixed by experiments see Graupner-Brunel 2007
k6=6
k7=6 # same values as Graupner-Brunel 2007
#dephosporylation
k12=6000
KM=0.4 # same values as Graupner-Brunel 2007
#Calmodulin
CaMT=0.07342
Ka1=0.1
Ka2=0.025
Ka3=0.32
Ka4=0.40 #fixed by experiments see Graupner-Brunel 2007
#PP1 / I1
PP10=0.2 # same values as Graupner-Brunel 2007
k11=500
km11=0.1
I10=1# same values as Graupner-Brunel 2007
#CaN / PKA parameters on I1
KdcanI1=0.053
ncanI1=3 #fixed by experiments see Graupner-Brunel 2007
kcan0I1=0.05
kcanI1=20.5
KdpkaI1=0.159 
npkaI1=3 
kpka0I1=0.0025 
kpkaI1=4.67
#changed the too large value npka=8 in Graupner-Brunel 2007 to a more 
#conservative 3 for pka

#Parameters for CICR
ver=8.0
ker=0.05
rhoER=.30
rc=4
rl=0.1
d1=0.13
d2=3.049
d3=0.9434
d5=0.12
a2=0.5

#Parameters of the IP3 metabolism subsystem
#Agonist-dependent production
vbeta=0.80
kr=1.3
kp=10
kpi=0.6
#Agonist-independent production
vdelta=0.02
kappad=1.5
kdelta=0.1
#IP3 degradation
v3k=1
kd=1.5
k3=1
n3=1
r5p=0.25

#Parameters for Kinase K on DAGlipase a
rK=1e6
r0K=0.0
nK=8
KmK=3.0
# Parameters for Posphastase P on DAGLIPASE a
rP=1e3
r0P=0
nP=3
KmP=0.053
#get_container(config, 'experiment.scipy_options', default={})
#DGL kinetics
rDGL=2e4
KDGL=30

#DAG and 2-AG degradation/export
kMAGL=2
kDAGK=2


#Parameters for AEA and TRPV1
vATAEA=0.2
Kact=300#Natarajan1986,Hansen1998
vFAAH=4.0
KFAAH=1#Okamoto2004
Gmax_TRPV1=0.1#0.150##nS
ksiTRPV1=250#µM.s-1.A-1
alphaAEACB1=0.10


LTDstart=5e-3 #5e-3
LTDstop=10e-3 #13e-3
LTPstart=19e-3
LTDMax=0.20 #0.25
LTPMax=15
P1=1e-10 #1e-3
P3=5 #2
P4=2 #2e-3

#Parameters of the dendrite
Cm=0.1 # nF
gL=10.0 # nS
EL=-70 # mV

#Parameters for Ca Buffering
BT=4
KdB=0.50
tauCab=0.0070
Cab=0.10


no17 = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,23,24,25,26,27,29])

#endregion ##################################################### END PARAMS FOR STDP #################################################

@overload(np.heaviside)
def np_heaviside(x1, x2):
    @register_jitable
    def heaviside_impl(x1, x2):
        if x1 < 0:
            return 0.0
        elif x1 > 0:
            return 1.0
        else:
            return x2

    return heaviside_impl

@jit(nopython=True)
def STDP(t,y, currstepon):
    '''

    '''

    y[no17][y[no17]<0] = 0
    dy = np.zeros((30))

    #CICR intermediate functions
    minf=y[16]/(y[16]+d1)
    ninf=y[13]/(y[13]+d5)
    JIP3R=rc*(minf*ninf*y[15])**3*(y[14]-y[13])
    Jserca=(ver*y[13]**2)/(ker**2+y[13]**2)
    Jleak=rl*(y[14]-y[13])
    Hill1=y[13]/(y[13]+kpi)


    #ionotropic Glutamate Receptors
    IAMPA=gAMPAmax*y[18]*y[17]
    B=1/(1+Mg/3.57*np.exp(-0.062*y[17])) # corresponds to mg=1mM
    INMDA=gNMDAmax*y[19]*y[17]*B

    #Specific Ca buffer
    tauCa=(1+BT/KdB/(1+y[13]/KdB)**2)
    tauCaER=(1+BT/KdB/(1+y[14]/KdB)**2)

    #Stimulations
    #Stimulation-dependent IP3 production
    vglu=vbeta*y[27]/(y[27]+(kr*(1+kp/kr*Hill1)))
    #Postsynaptic Stim (current step + bAP)
    
    IVDCC=currstepon*DPmax + y[28]

    #TRPV1
    ITRPV1=-Gmax_TRPV1*G_TRPV1(y[26],y[17])

    ##ODEs

    #dCa_cyt/dt
    #with event for positivity
    if (np.heaviside(y[13]-1e-10, 0) == 0) and (((JIP3R-Jserca+Jleak+ksivdcc*IVDCC-(y[13]-Cab)/tauCab-ksi*0.10*INMDA-ksiTRPV1*ITRPV1)/tauCa) <0) :
        dy[13] = 0
    else:
        dy[13]=(JIP3R-Jserca+Jleak+ksivdcc*IVDCC-(y[13]-Cab)/tauCab-ksi*0.10*INMDA-ksiTRPV1*ITRPV1)/tauCa
    
    #dy[13]=(JIP3R-Jserca+Jleak+ksivdcc*IVDCC-(y[13]-Cab)/tauCab-ksi*0.10*INMDA-ksiTRPV1*ITRPV1)/tauCa
    #dCa_ER/dt
    dy[14]=-(JIP3R-Jserca+Jleak)*rhoER/tauCaER
    #dh/dr
    dy[15]=(a2*d2*(y[16]+d1)/(y[16]+d3))*(1-y[15])-a2*y[13]*y[15]
    #dIP3/dt
    dy[16]=vglu+vdelta/(1+y[16]/kappad)*y[13]**2/(y[13]**2+kdelta**2)-v3k*y[13]**4/(y[13]**4+kd**4)*y[16]**n3/(k3**n3+y[16]**n3)-r5p*y[16]
    #dV/dt
    dy[17]=1/Cm*(-gL*(y[17]-EL)-IAMPA-INMDA+IVDCC-ITRPV1)
    #drAMPA/dt
    #dy(19)=alphaAMPA*Glu*(1-y[18])-betaAMPA*y[18]
    dy[18]=alphaAMPA*y[27]*(1-y[18])-betaAMPA*y[18]
    #drNMDA/dt
    #dy(20)=alphaNMDA*Glu*(1-y[19])-betaNMDA*y[19]
    dy[19]=alphaNMDA*y[27]*(1-y[19])-betaNMDA*y[19]


    ###############################
    # Ionotropic NMDA plasticity #
    ###############################

    #Taken from the Brunel and Graupner model

    # occupied receptors on the CaMKII
    rr=np.sum(y[0:13])
    # B0 is whats left from total
    B0=2*CaMKT-rr
    # kinetic equations
    phossum=y[0] + 2*np.sum(y[1:4]) + 3*np.sum(y[4:8]) + 4*np.sum(y[8:11]) + 5*y[11] + 6*y[12]
    k10=k12*y[20]/(KM + phossum)
    CaM=CaMT/(1 + Ka4/y[13] + Ka3*Ka4/(y[13]**2) + Ka2*Ka3*Ka4/(y[13]**3) + Ka1*Ka2*Ka3*Ka4/(y[13]**4))
    gamma=CaM/(k5+CaM) 
    vPKA=kpka0I1 + kpkaI1/(1 + (KdpkaI1/CaM)**npkaI1)
    vCaN=kcan0I1 + kcanI1/(1 + (KdcanI1/CaM)**ncanI1)

    #dBi/dt
    '''
    dy[0] = 6*k6*gamma**2*B0 - 4*k6*gamma**2*y[0] - k7*gamma*y[0] - k10*y[0] + 2*k10*np.sum(y[1:4])
    [-4*g2k6 - gk7 - k10, 2*k10, 2*k10, 2*k10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6*g2k6*B0]
    dy[1] = k7*gamma*y[0] + k6*gamma**2*y[0] - 3*k6*gamma**2*y[1] - k7*gamma*y[1] - 2*k10*y[1] + k10*(2*y[4] + y[5] + y[6])
    [gk7 + g2k6, -3*g2k6 - gk7 - 2*k10, 0, 0, 2*k10, k10, k10, 0, 0, 0, 0, 0, 0, 0]
    dy[2] = 2*k6*gamma**2*y[0] - 2*k7*gamma*y[2] - 2*k6*gamma**2*y[2] - 2*k10*y[2] + k10*(y[4] + y[5] + y[6] + 3*y[7]) 
    [2*g2k6, 0, -2*gk7-2*g2k6-2*k10, 0, k10, k10, k10, 3*k10, 0, 0, 0, 0, 0, 0]
    dy[3] = k6*gamma**2*y[0] - 2*k7*gamma*y[3] - 2*k6*gamma**2*y[3] - 2*k10*y[3] + k10*(y[5] + y[6])
    [g2k6, 0, 0, -2*gk7 - 2*g2k6 - 2*k10, 0, k10, k10, 0, 0, 0, 0, 0, 0, 0]
    dy[4] = k7*gamma*y[1] + k6*gamma**2*y[1] + k7*gamma*y[2] - k7*gamma*y[4] - 2*k6*gamma**2*y[4] - 3*k10*y[4] + k10*(2*y[8] + y[9])
    [0, gk7 + g2k6, gk7, 0, -gk7 - 2*g2k6 - 3*k10, 0, 0, 0, 2*k10, k10, 0, 0, 0, 0]
    dy[5] = k6*gamma**2*y[1] + k6*gamma**2*y[2]  + 2*k7*gamma*y[3] - k6*gamma**2*y[5] - 2*k7*gamma*y[5] - 3*k10*y[5] + k10*(y[8] + y[9] + 2*y[10])
    [0, g2k6, g2k6, 2*gk7, 0, -g2k6-2*gk7-3*k10, 0, 0, k10, k10, 2*k10, 0, 0, 0 ]
    dy[6] = k6*gamma**2*y[1] + k7*gamma*y[2] + 2*k6*gamma**2*y[3] - k6*gamma**2*y[6] - 2*k7*gamma*y[6] - 3*k10*y[6] + k10*(y[8] + y[9] + 2*y[10])
    [0, g2k6, gk7, 2*g2k6, 0, 0,  -g2k6 - 2*gk7 - 3*k10, 0, k10, k10, 2*k10, 0, 0, 0]
    dy[7] = k6*gamma**2*y[2] - 3*k7*gamma*y[7] - 3*k10*y[7] + k10*y[9]
    [0, 0, g2k6, 0, 0, 0, 0, -3*gk7 - 3*k10, 0, k10, 0, 0, 0, 0]
    dy[8] = k7*gamma*y[4] + k6*gamma**2*y[4] + k7*gamma*y[5] + k7*gamma*y[6] - k6*gamma**2*y[8] - k7*gamma*y[8] - 4*k10*y[8] + 2*k10*y[11]
    [0, 0, 0, 0, gk7 + g2k6,  gk7, gk7, 0, -g2k6 - gk7 - 4*k10, 0, 0, 2*k10, 0, 0]
    dy[9] = k6*gamma**2*y[4] + k6*gamma**2*y[5] + k7*gamma*y[6] + 3*k7*gamma*y[7] - 2*k7*gamma*y[9] - 4*k10*y[9] + 2*k10*y[11]
    [0, 0, 0, 0, g2k6, g2k6, gk7, 3*gk7, 0,  -2*gk7 - 4*k10, 0, 2*k10, 0, 0]
    dy[10] = k7*gamma*y[5] +  k6*gamma**2*y[6] - 2*k7*gamma*y[10] - 4*k10*y[10] + k10*y[11]
    [0, 0, 0, 0, 0, gk7, g2k6, 0, 0, 0, -2*gk7 - 4*k10, k10, 0, 0]
    dy[11] = k7*gamma*y[8] + k6*gamma**2*y[8] + 2*k7*gamma*y[9] + 2*k7*gamma*y[10] - k7*gamma*y[11] - 5*k10*y[11] + 6*k10*y[12]
    [0, 0, 0, 0, 0, 0, 0, 0, gk7 + g2k6, 2*gk7, 2*gk7, -gk7 - 5*k10, 6*k10, 0]
    dy[12] = k7*gamma*y[11] - 6*k10*y[12]
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, gk7, -6*k10, 0]
    '''

    g2k6 = k6*gamma**2
    gk7 = k7*gamma

    mat = np.array([[-4*g2k6 - gk7 - k10, 2*k10, 2*k10, 2*k10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6*g2k6*B0],#0
              [gk7 + g2k6, -3*g2k6 - gk7 - 2*k10, 0, 0, 2*k10, k10, k10, 0, 0, 0, 0, 0, 0, 0],#1
              [2*g2k6, 0, -2*gk7-2*g2k6-2*k10, 0, k10, k10, k10, 3*k10, 0, 0, 0, 0, 0, 0],#2
              [g2k6, 0, 0, -2*gk7 - 2*g2k6 - 2*k10, 0, k10, k10, 0, 0, 0, 0, 0, 0, 0],#3
              [0, gk7 + g2k6, gk7, 0, -gk7 - 2*g2k6 - 3*k10, 0, 0, 0, 2*k10, k10, 0, 0, 0, 0],#4
              [0, g2k6, g2k6, 2*gk7, 0, -g2k6-2*gk7-3*k10, 0, 0, k10, k10, 2*k10, 0, 0, 0 ],#5
              [0, g2k6, gk7, 2*g2k6, 0, 0,  -g2k6 - 2*gk7 - 3*k10, 0, k10, k10, 2*k10, 0, 0, 0],#6
              [0, 0, g2k6, 0, 0, 0, 0, -3*gk7 - 3*k10, 0, k10, 0, 0, 0, 0],#7
              [0, 0, 0, 0, gk7 + g2k6,  gk7, gk7, 0, -g2k6 - gk7 - 4*k10, 0, 0, 2*k10, 0, 0],#8
              [0, 0, 0, 0, g2k6,g2k6, gk7, 3*gk7, 0,  -2*gk7 - 4*k10, 0, 2*k10, 0, 0],#9
              [0, 0, 0, 0, 0, gk7, g2k6, 0, 0, 0, -2*gk7 - 4*k10, k10, 0, 0],#10
              [0, 0, 0, 0, 0, 0, 0, 0, gk7 + g2k6, 2*gk7, 2*gk7, -gk7 - 5*k10, 6*k10, 0],#11
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, gk7, -6*k10, 0]])#12

    tmp_y = np.zeros(14)
    tmp_y[0:13] = y[0:13]
    tmp_y[13] = 1
    dy[0:13] = np.dot(mat, tmp_y)

    #PP1 And I1P
    dy[20]= -k11*y[21]*y[20] + km11*(PP10 - y[20])
    dy[21]= -k11*y[21]*y[20] + km11*(PP10 - y[20]) + vPKA*I10 - vCaN*y[21]

    #DAG
    dy[22]=vglu+vdelta/(1+y[16]/kappad)*y[13]**2/(y[13]**2+kdelta**2)-rDGL*y[23]*y[22]/(y[22]+KDGL)-kDAGK*y[22]

    #Phosphorylated DAGLipase Fraction
    dy[23]=(r0K+rK*y[13]**nK/(KmK**nK+y[13]**nK))*(1-y[23])-(r0P+rP*y[13]**nP/(KmP**nP+y[13]**nP))*y[23]
    
    #2-AG-post
    dy[24]=rDGL*y[23]*y[22]/(y[22]+KDGL)-kMAGL*y[24]

    #2-AG controled aKP cycle in pre
    Omega=(0.5-LTDMax*np.heaviside(y[24]+alphaAEACB1*y[26]-LTDstart, 1)*
                      np.heaviside(LTDstop-y[24]-alphaAEACB1*y[26], 1)+
                      np.heaviside(y[24]+alphaAEACB1*y[26]-LTPstart, 1)*LTPMax)
    
    #Omega=0.5-LTDMax/(1 + np.exp(-1e8*(y[24]+alphaAEACB1*y[26]-LTDstart)))/(1 + np.exp(-1e8*(LTDstop-y[24]-alphaAEACB1*y[26])))+LTPMax/(1 + np.exp(-1e8*(y[24]+alphaAEACB1*y[26]-LTPstart)))
    #taufpre=P1/((P1/1e4)**P3+(y[24]+alphaAEACB1*y[26])**P3)+P4
    taufpre=P1/((P1/1e-4)**P3+(y[24]+alphaAEACB1*y[26])**P3)+P4
    dy[25]=(Omega-y[25])/taufpre

    #dAEA/dt
    dy[26]=vATAEA*y[13]-vFAAH*y[26]/(KFAAH+y[26]) #vAT*y[0]/(Kact+y[0])-vFAAH*y[1]/(KF+y[1])

    dy[27]=-y[27]/tauGlu
    dy[28]=-y[28]/tausbAP

    '''
    if sum(np.logical_and(dy[no17] < 0, y[no17] < 0) > 0):
        print('neeeeegatiiiif \n heeeeeeereeee______________ \n encore une liiiiiiiiiiiigne_________________________')
        print(np.logical_and(dy[no17] < 0, y[no17] < 0))
    '''
    dy[no17][np.logical_and(dy[no17] < 0, y[no17] <= 0)] = 0#1e-16

    return(dy)

#Implementation of the allosteric TRPV1 model by Matta & Ahern, 2007 (their eq 3)

TC=25 #temp in °C
#constants
L=4.2e-4
J0=0.0169
DH=205e3 #J/mol
T=273.15 + TC #K
DS=615 #J/mol
D=1100
C=23367
z=0.6
KD=0.5 #10.0#µM - KD for caps 0.5 µM for AEA (Marzo2002,Starowicz2007)
P=750
F=96.5 #1e3 C/mol
R=8.31 #J/mol/K

@jit(nopython=True)
def G_TRPV1(AEA,V):

    J=J0*np.exp(z*F*V/(R*T))
    K=np.exp(-(DH-T*DS)/(R*T))
    Q=AEA/KD

    Popen=1/(1+(1+J+K+Q+J*K+J*Q+K*Q+J*K*Q)/(L*(1+J*D+K*C+Q*P+J*K*C*D+J*Q*D*P+K*Q*C*P+J*K*Q*D*C*P)))
    #plot(V,Popen)

    return (Popen)