import logging
import pathlib
#from pathlib import Path

from multiprocessing import Pool

import math
import numpy as np
import pandas as pd
import tslearn
from tslearn.metrics import dtw, cdist_dtw
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

import matplotlib.pyplot as plt
import matplotlib as mpl

import mlflow

import stdp.integration
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot
import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from stdp.Class_Analysis import Router

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)


def compute_int_RES(RES, ti, tf):
    '''compute the integrale of a non regulare array (with left rectangle method).
    RES is the array, expected to be like (array, array, array_time)
    ti, tf respectivly start time and stop time.
    '''
    ind = np.logical_and(RES[2]>=(ti), RES[2]<=(tf))
    w_pre = np.sum( RES[0][ind][:-1]*(RES[2][ind][1:] - RES[2][ind][:-1])) /(tf-ti)
    w_post =np.sum( RES[1][ind][:-1]*(RES[2][ind][1:] - RES[2][ind][:-1])) /(tf-ti)
    #w_pre = np.sum( RES[0][ind][:-1]*((RES[2][1:] - RES[2][:-1])[ind[:-1]]) )/(tf-ti)
    #w_post =np.sum( RES[1][ind][:-1]*((RES[2][1:] - RES[2][:-1])[ind[:-1]]) )/(tf-ti)
    return (w_pre, w_post)

def compute_int_X(T, RES, ti, tf):
    '''compute the integrale of a non regulare array (with left rectangle method).
    RES is the array, expected to be like (array, array, array_time)
    ti, tf respectivly start time and stop time.
    '''
    W = np.zeros(len(RES))
    for it in range(len(RES)):
        ind = np.logical_and(T>=(ti), T<=(tf))
        W[it] = np.sum( RES[0][ind][:-1]*(T[ind][1:] - T[ind][:-1])) /(T[-1]-T[0])

    return (W)

def compute_int_RES_Var(RES, ti, tf):
    '''compute the integrale of a non regulare array (with left rectangle method).
    RES is the array, expected to be like (array, array, array_time)
    ti, tf respectivly start time and stop time.
    '''
    ind = np.logical_and(RES[2]>=(ti), RES[2]<=(tf))
    w_pre_temp = RES[0][ind][:-1]*(RES[2][ind][1:] - RES[2][ind][:-1])
    w_post_temp = RES[1][ind][:-1]*(RES[2][ind][1:] - RES[2][ind][:-1])
    
    w_pre_mean = np.sum( w_pre_temp ) /(tf-ti)
    w_post_mean =np.sum( w_post_temp ) /(tf-ti)

    w_pre_var = np.sum( (((RES[0][ind][:-1]-w_pre_mean)**2)*(RES[2][ind][1:] - RES[2][ind][:-1])) )
    w_post_var = np.sum((w_post_temp-w_post_mean)**2)/w_post_mean
    #w_pre = np.sum( RES[0][ind][:-1]*((RES[2][1:] - RES[2][:-1])[ind[:-1]]) )/(tf-ti)
    #w_post =np.sum( RES[1][ind][:-1]*((RES[2][1:] - RES[2][:-1])[ind[:-1]]) )/(tf-ti)
    return (w_pre_var, w_post_var)

def moving_window(array, t_bounds, win_len, win_step, func = None):
    '''
    INPUT VAR:
        array:
        win_len: length of the windows on wich to apply the function func
        win_step: delta between two windows
        func: the function to apply on each windows

    OUTPUT VAR:
        res: if func is given, an array containing the result of func applied on each window
             else, an array of array containing the windows.
    '''
    it = 0
    q = (t_bounds[1]-t_bounds[0]-win_len)/win_step
    if q.is_integer == False:
        new_win_step = win_step + (q-math.floor(q))/math.floor(q)
        LOGGER.info(f'new win_step to fit the interval is {new_win_step} instead of {win_step}')
        win_step = new_win_step

    if func == None:
        res = np.zeros( (math.floor(q)+1 , len(array) ))
        for t in np.linspace(t_bounds[0], t_bounds[1]-win_len, math.floor(q)+1):
            res[it, :] = (array >= t ) & (array <= t + win_len)
            it = it + 1
    else:
        res = np.zeros( math.floor(q)+1 )
        for t in np.linspace(t_bounds[0], t_bounds[1]-win_len, math.floor(q)+1):
            win_ind = (array >= t ) & (array <= t + win_len)
            res[it] = func(array[win_ind])
            it = it + 1
    return res, np.linspace(t_bounds[0], t_bounds[1]-win_len, math.floor(q)+1)

def moving_window_timed(array, t_bounds, win_len, win_step, func = None):
    '''
    INPUT VAR:
        array: 2 dim, dim 1 is time, dim 2 is value
        win_len: length of the windows on wich to apply the function func
        win_step: delta between two windows
        func: the function to apply on each windows

    OUTPUT VAR:
        res: if func is given, an array containing the result of func applied on each window
             else, an array of array containing the windows.
    '''
    it = 0
    q = (t_bounds[1]-t_bounds[0]-win_len)/win_step
    if q.is_integer == False:
        new_win_step = win_step + (q-math.floor(q))/math.floor(q)
        LOGGER.info(f'new win_step to fit the interval is {new_win_step} instead of {win_step}')
        win_step = new_win_step

    if func == None:
        res = np.zeros( (math.floor(q)+1 , len(array[0]) ))
        for t in np.linspace(t_bounds[0], t_bounds[1]-win_len, math.floor(q)+1):
            res[it, :] = (array[0] >= t ) & (array[0] <= t + win_len)
            it = it + 1
    else:
        res = np.zeros( math.floor(q)+1 )
        for t in np.linspace(t_bounds[0], t_bounds[1]-win_len, math.floor(q)+1):
            win_ind = (array[0] >= t ) & (array[0] <= t + win_len)
            res[it] = func(array[1][win_ind])
            it = it + 1
    return res

def firing_rate(spike_array, t_bounds, win_len, win_step):
    '''
    donne le firing rate sur une fenêtre glissante de taille win_len
    '''
    #LOGGER.debug('clust array: ' + str(clust_array) + '  tbound:' + str(*t_bounds))
    res = moving_window(spike_array, t_bounds, win_len, win_step, len)
    return res

def np_moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

def random_jitter(spikes, params_jitter, n_it = 1):
    '''
    Apply a gaussian jitter on an array of time
    args:
        spikes:
            the array on to apply the jitter.
        params_jitter:
            [mean for normale law, std for normal law].
        n_it: default = 1
            return multiple random jitterised vector.

    output:
        RES: a matrix, each line is a jittered "spikes" vector.
    '''
    RES = np.zeros((n_it, len(spikes)))
    for it in range(n_it):
        jitter_vect = np.random.normal(params_jitter[0], params_jitter[1], len(spikes))
        RES[it, :] = spikes + jitter_vect

    return RES

def compute_Omega(router: Router, couple):
    P1=1e-10 #1e-3
    P3=5 #2
    P4=2 #2e-3
    alphaAEACB1=0.10
    LTDstart=5e-3 #5e-3
    LTDstop=10e-3 #13e-3
    LTPstart=19e-3
    LTDMax=0.20 #0.25
    LTPMax=15

    y = router.load_solution_couple(couple)
    if y.shape[0]>10:
        y = y[24:27, :]
    taufpre=P1/((P1/1e-4)**P3+(y[0]+alphaAEACB1*y[2, :])**P3)+P4
    Omega=(0.5-LTDMax*np.heaviside(y[0]+alphaAEACB1*y[2]-LTDstart, 1)*
            np.heaviside(LTDstop-y[0]-alphaAEACB1*y[2], 1)+
            np.heaviside(y[0]+alphaAEACB1*y[2]-LTPstart, 1)*LTPMax)
    return (Omega, taufpre)
