import numpy as np
import stdp.integration
import stdp.integration_sat
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot
import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from computation_tools import *
from Class_optiGlu import optiGlu
from stdp.Class_Analysis import Router
import stdp.density_stdp as dens

""" #en attente
def Glu_curve(array, tauGlu, Glumax):
    lambda t: np.exp(-t*tauGlu)
    
    glu = 
    return glu
"""

def pre_optim_Glumax_badway(params, mouse: Mouse, Glumax_win):
    '''
    Search the limit value for post-syn weight saturation with pre-spike only
    Glumax_win: windows value for the search.
    '''
    ############################# PREPARATION ############################

    y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])
    path = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/only_cortex')
    pathlib.Path.mkdir(path, parents=True, exist_ok=True )

    cortex_cluster = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cortex_cluster = cortex_cluster[~cortex_cluster.isin(mouse.interneurons)]
    t_pre = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)].to_numpy()
    t_pre = t_pre[t_pre < (mouse.ti-10)]
    ######################################################################
    z = 0
    it = 0
    Glu_mem = np.zeros(13)
    Glumax_curr = np.mean(Glumax_win)
    Glu_mem[it] = Glumax_curr
    LOGGER.info(f' computing {mouse.mouse_name}')

    while it < 12-z :
        
        no_sat = True
        it_no_sat = 0
        y0 = y0init
        fpre_inf = y0[25] #pre_inf is the presynaptic weight
        CaMKact_inf = y0[0] + 2*np.sum(y0[1:4], axis=0) + 3*np.sum(y0[4:8], axis=0) + 4*np.sum(y0[8:11], axis=0) + 5*y0[11] + 6*y0[12]
        #Normalization of the proxy variables to obtain the real pre and post weights
        CaMKact_inf=1+2*CaMKact_inf/164.6
        RES = (fpre_inf, CaMKact_inf, [0], np.array([y0]).T, y0)
        print(f'len RES[0]:{RES[0]}')
        print(f'len RES[1]:{RES[1]}')
        print(f'len RES[2]:{RES[2]}')
        while no_sat and it_no_sat < 4:
            selec = [int(len(t_pre)/4*it_no_sat), np.min((int(len(t_pre)/4*(it_no_sat+1)), len(t_pre)-1))]
            print(f'im into repetition, {len(t_pre[selec[0]:selec[1]])}')
            RES_new = stdp.integration.integration(params, Glumax_curr, t_pre[selec[0]:selec[1]], np.array([]), t_end = 0, y0init = y0 )
            print(f'OK interne {it_no_sat}')
            t_sol = np.concatenate((RES[2] , RES_new[2][1:]))
            sol = np.concatenate((RES[3] , RES_new[3][:, 1:]), axis = 1)
            y0 = RES_new[4]
            RES = (fpre_inf, CaMKact_inf, t_sol, sol, y0)
            fpre_inf = RES[3][25,:] #pre_inf is the presynaptic weight
            CaMKact_inf = RES[3][0,:] + 2*np.sum(RES[3][1:4, :], axis=0) + 3*np.sum(RES[3][4:8, :], axis=0) + 4*np.sum(RES[3][8:11, :], axis=0) + 5*RES[3][11,:] + 6*RES[3][12,:]
            #Normalization of the proxy variables to obtain the real pre and post weights
            CaMKact_inf=1+2*CaMKact_inf/164.6
            print(f'into center fpre is {fpre_inf.shape}')
            RES = (fpre_inf, CaMKact_inf, t_sol, sol, y0)
            print(f'len RES[0]:{len(RES[0])}')
            print(f'len RES[1]:{len(RES[1])}')
            print(f'len RES[2]:{len(RES[2])}')
            print(f'time for mean {(t_pre[selec[0]], RES[2][-1])}')
            mean_selec = compute_int_RES(RES, t_pre[selec[0]], RES[2][-1])[1]#mouse.ti-115, mouse.ti-15)[1]
            if mean_selec > 2:
                it_no_sat = 100 #Will stop while and taking val 6 differentiate from 'classic end with 4 iteration
                print("got in fast sat")
            else:
                it_no_sat += 1
            
        if it_no_sat == 4:
            Glumax_win[0] = Glumax_curr
        elif it_no_sat == -100:
            Glumax_win[1] = Glumax_curr

        LOGGER.info(f'{mouse.mouse_name} pre optim Glumax, iteration {it} done')
        RES = RES[0:3]

        np.save(path / f'{it}_res.npy', RES)

        Glumax_curr = np.mean(Glumax_win)
        it = it+1
        Glu_mem[it] = Glumax_curr #for next step.
    
    np.save( path / 'Glu_mem', Glu_mem)

    return Glu_mem

def pre_optim_Glumax(params, mouse: Mouse, Glumax_win):
    '''
    Search the limit value for post-syn weight saturation with pre-spike only
    Glumax_win: windows value for the search.
    '''
    ############################# PREPARATION ############################

    y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])
    
    path = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/only_cortex')
    pathlib.Path.mkdir(path, parents=True, exist_ok=True )

    cortex_cluster = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cortex_cluster = cortex_cluster[~cortex_cluster.isin(mouse.interneurons)]
    t_pre = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)].to_numpy()
    t_pre = t_pre[t_pre < (mouse.ti-10)]
    ######################################################################
    z = 0
    it = 0
    Glu_mem = np.zeros(13)
    Glumax_curr = np.mean(Glumax_win)
    Glu_mem[it] = Glumax_curr
    LOGGER.info(f' computing {mouse.mouse_name}')

    while it < 12-z :
        
        y0 = y0init
        
        RES = stdp.integration_sat.integration(params, Glumax_curr, t_pre, np.array([]), t_end = 0, y0init = y0 )
        
        if RES[5] == 0:
            Glumax_win[0] = Glumax_curr
        elif RES[5] == 1:
            Glumax_win[1] = Glumax_curr

        LOGGER.info(f'{mouse.mouse_name} pre optim Glumax, iteration {it} done')
        RES = RES[0:3]

        np.save(path / f'{it}_res.npy', RES)

        Glumax_curr = np.mean(Glumax_win)
        it = it+1
        Glu_mem[it] = Glumax_curr #for next step.
    
    np.save( path / 'Glu_mem', Glu_mem)

    return Glu_mem

def optim_Glumax_badway(params, mouse: Mouse, clust):
    '''
    Search the limit value for post-syn weight saturation for cluster clust.
    Glumax_win: windows value for the search.
    '''
    ############################# PREPARATION ############################

    y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])
    
    path_pre = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/only_cortex')
    Glumem_pre = np.load(path_pre / 'Glu_mem.npy')
    Glumax_win = [0, 1.5*Glumem_pre[-1]]

    path = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/{clust}')
    pathlib.Path.mkdir(path, parents=True, exist_ok=True )

    cortex_cluster = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cortex_cluster = cortex_cluster[~cortex_cluster.isin(mouse.interneurons)]
    t_pre = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)].to_numpy()
    t_post = mouse.spikes_from_ID(int(clust))

    t_pre = t_pre[t_pre < (mouse.ti-10)]
    t_post = t_post[t_post < (mouse.ti-10)]

    ######################################################################

    it = 0
    Glu_mem = np.zeros(9)
    Glumax_curr = np.mean(Glumax_win)

    while it < 8 :
        
        Glu_mem[it] = Glumax_curr    
        no_sat = True
        it_no_sat = 0
        max_it_no_sat = int(mouse.t_bounds[1]/100)
        y0 = y0init
        fpre_inf = y0[25] #pre_inf is the presynaptic weight
        CaMKact_inf = y0[0] + 2*np.sum(y0[1:4], axis=0) + 3*np.sum(y0[4:8], axis=0) + 4*np.sum(y0[8:11], axis=0) + 5*y0[11] + 6*y0[12]
        #Normalization of the proxy variables to obtain the real pre and post weights
        CaMKact_inf=1+2*CaMKact_inf/164.6
        RES = (fpre_inf, CaMKact_inf, [0], np.array([y0]).T, y0)
        LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')

        while no_sat and it_no_sat < max_it_no_sat:

            ################# OLD ##########################
            #Old selection mode on pre-syn spike. Use post is better not to cut the plateau depol
            '''selec = [int(len(t_pre)/4*it_no_sat), int(len(t_pre)/4*(it_no_sat+1))]
            RES_new = stdp.integration.integration(params, Glumax_curr, t_pre[selec[0]:selec[1]],
                                                   t_post[np.logical_and(t_post<t_pre[selec[1]], t_post>t_pre[selec[0]]) ], t_end = 0, y0init = y0 )
            '''
            
            selec = np.array([int(len(t_post)/max_it_no_sat*it_no_sat), np.min([int(len(t_post)/max_it_no_sat*(it_no_sat+1)), len(t_post)-1] )])
            RES_new = stdp.integration.integration(params, Glumax_curr, t_pre[np.logical_and(t_pre<t_post[selec[1]], t_pre>t_post[selec[0]])],
                                                   t_post[selec[0]:selec[1]], t_end = 0, y0init = y0 )
            
            #LOGGER.debug(f' {mouse.mouse_name}_{int(clust)} compute done')

            t_sol = np.concatenate((RES[2] , RES_new[2][1:]))
            sol = np.concatenate((RES[3] , RES_new[3][:, 1:]), axis = 1)
            y0 = RES_new[4]
            print(y0.shape)
            RES = (fpre_inf, CaMKact_inf, t_sol, sol, y0)
            fpre_inf = RES[3][25,:] #pre_inf is the presynaptic weight
            CaMKact_inf = RES[3][0,:] + 2*np.sum(RES[3][1:4, :], axis=0) + 3*np.sum(RES[3][4:8, :], axis=0) + 4*np.sum(RES[3][8:11, :], axis=0) + 5*RES[3][11,:] + 6*RES[3][12,:]
            #Normalization of the proxy variables to obtain the real pre and post weights
            CaMKact_inf=1+2*CaMKact_inf/164.6
            RES = (fpre_inf, CaMKact_inf, t_sol, sol, y0)

            print(f'time for mean {(t_post[selec[0]], RES[2][-1])}')
            mean_selec = compute_int_RES(RES, t_post[selec[0]], RES[2][-1])[1]#mouse.ti-115, mouse.ti-15)[1]
            if mean_selec > 2:
                it_no_sat = 100 #Will stop while and taking val 'sat' differentiate from 'classic end with 4 iteration
            else:
                it_no_sat += 1
    
        if it_no_sat == max_it_no_sat:
            Glumax_win[0] = Glumax_curr
        elif it_no_sat == 100:
            Glumax_win[1] = Glumax_curr

        Glumax_curr = np.mean(Glumax_win)
        it = it+1

        LOGGER.info(f'{mouse.mouse_name} optim Glumax, iteration {it} done')
        RES = RES[0:3]

        np.save(path / f'{it}_res_{clust}.npy', RES)
    
    LOGGER.info(f'{mouse.mouse_name}_{int(clust)} done')
    np.save(path / 'Glu_mem', Glu_mem)

    return Glu_mem

def optim_Glumax(params, mouse: Mouse, clust):
    '''
    Search the limit value for post-syn weight saturation for cluster clust.
    Glumax_win: windows value for the search.
    '''
    ############################# PREPARATION ############################

    y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])
    
    #path_pre = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/only_cortex')
    #Glumem_pre = np.load(path_pre / 'Glu_mem.npy')
    #Glumax_win = [0, 2*Glumem_pre[-1]]
    Glumax_win = [0,2]

    path = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/{clust}')
    pathlib.Path.mkdir(path, parents=True, exist_ok=True )

    cortex_cluster = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cortex_cluster = cortex_cluster[~cortex_cluster.isin(mouse.interneurons)]
    t_pre = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)].to_numpy()
    t_post = mouse.spikes_from_ID(int(clust))

    t_pre = t_pre[t_pre < (mouse.ti-10)]
    t_pre = t_pre[t_pre > (mouse.ti-200)]
    t_post = t_post[t_post > (mouse.ti-200)]

    ######################################################################

    it = 0
    Glu_mem = np.zeros(9)
    Glumax_curr = np.mean(Glumax_win)

    while it < 8 :
        
        Glu_mem[it] = Glumax_curr    
        y0 = y0init

        LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')

        RES = stdp.integration_sat.integration(params, Glumax_curr, t_pre, t_post, t_end = 0, y0init = y0 )
 
        if RES[5] == 0:
            Glumax_win[0] = Glumax_curr
        elif RES[5] == 1:
            Glumax_win[1] = Glumax_curr

        Glumax_curr = np.mean(Glumax_win)
        it = it+1

        LOGGER.info(f'{mouse.mouse_name} optim Glumax, iteration {it} done')
        RES = RES[0:3]

        np.save(path / f'{it}_res_{clust}.npy', RES)
    
    LOGGER.info(f'{mouse.mouse_name}_{int(clust)} done')
    np.save(path / 'Glu_mem', Glu_mem)

    return Glu_mem

def compute_with_Glumax(params, mouse: Mouse, computation_options: dict = None):
    ################################# PREP ###################################

    ##########################################################################
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(mouse.interneurons))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID.isin(mouse.brain_region[1])]
    cortex_spikes = cortex_spikes[cortex_spikes<mouse.tf+200] #choose stop point
    #cortex_spikes = cortex_spikes[cortex_spikes>mouse.ti-20] #choose start point

    cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[0]) ]
    #cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, mouse.interneurons_list))]

    #Glumem = np.zeros((2,len(cluster_striatum)))
    #it = 0

    #for clust in cluster_striatum:
    args = [(params, mouse, cortex_spikes, clust ) for clust in cluster_striatum]
    with Pool(int(os.cpu_count()-4)) as pool:
        pool.starmap(compute_from_ID, args)

    return 0

def compute_from_ID(params, mouse, cortex_spikes, clust ):
    y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
                     0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
                     0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])
    
    path_save =  pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/results/{clust}')
    
    path_opti = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/optim_Glumax/by_striatum')
    striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
    striatum_spikes = striatum_spikes[striatum_spikes<mouse.tf+200] #choose stop point
    #striatum_spikes = striatum_spikes[striatum_spikes>mouse.ti-20] #choose start point
    Glumax_curr = np.load(path_opti / str(clust) / 'Glu_mem.npy')[:-1]
    Glumax_curr = Glumax_curr[Glumax_curr > 0]
    glu0, glu1 = Glumax_curr[-2:]
    if glu0 < glu1: #if no sat then dont know, we want to keep when it dont saturates
        Glumax_curr = 0.95*glu0
    elif glu0 > glu1: #inverse case
        Glumax_curr = 0.95*glu1 # np.max( 2*glu1 - glu0 , 0)

    LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')
    RES = stdp.integration.integration(params, Glumax_curr, cortex_spikes, striatum_spikes, t_end = 100, y0init = y0init )
    LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)} done')
    for prefix, data in (
    ('res_', RES[0:3]),
    ('sol_', RES[3])):
        name = f'{prefix}{mouse.mouse_name}_{int(clust)}.npy'
        path_save =  pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/{mouse.mouse_name}/results/{name}')
        np.save(path_save, data)
        log_numpy_data(data, name)
    #Glumem[1, it] = Glumax_curr
    #Glumem[0, it] = int(clust)
    #it = it + 1
    #log_numpy_data(Glumem, 'Glumem')
    return 0

