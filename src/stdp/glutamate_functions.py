import logging
import pathlib
#from pathlib import Path

from multiprocessing import Pool

import numpy as np
import pandas as pd
import pickle
import tslearn
from tslearn.metrics import dtw, cdist_dtw, dtw_path
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as mpatches

import mlflow

import stdp.integration
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot
import stdp.computation_center
import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from stdp.Class_Analysis import Router
from stdp.computation_tools import *
from stdp.Class_optiGlu import optiGlu


logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)

######################### Init part ##########################
'''
if __name__ == "__main__":
    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    V4606_mouse = Mouse(*stdp.create_mouse_data.prepare_V4606())
    V7265_mouse = Mouse(*stdp.create_mouse_data.prepare_V7265())
'''
######################## Functions def ########################
                            #Utility

def save_object(obj, file):
    try:
        with open(file, "wb") as f:
            pickle.dump(obj, f)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)

def load_object(filename):
    try:
        with open(filename, "rb") as f:
            return pickle.load(f)
    except Exception as ex:
        print("Error during unpickling object (Possibly unsupported):", ex)

###############################################################

def LFP_contact():
    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    Mouse.set_synchro(red_mouse)
    ti = red_mouse.frame_to_time(27243)-600 #start contact
    tf = red_mouse.frame_to_time(29168)-600 #end contact
    ti = tf
    tf = ti + 50
    red_mouse.set_LFP()
    red_LFP_cut = red_mouse.LFP[range(1,5), range(1,5)]
    red_LFP_cut = red_mouse.LFP[np.ceil(ti*2500).astype(int) : np.ceil(tf*2500).astype(int), 1:4]
    plt.hist(np.ravel(red_LFP_cut), bins = 50)
    plt.show()

    V7265_mouse = Mouse(*stdp.create_mouse_data.prepare_V7265())
    Mouse.set_synchro(V7265_mouse)
    ti = V7265_mouse.frame_to_time(21257)-700 #start contact
    tf = V7265_mouse.frame_to_time(21361)-700 #end contact
    ti = tf
    tf = ti + 50
    print( f'ti = {ti} while max is {np.max(V7265_mouse.synchro.time)}')
    V7265_mouse.set_LFP()
    V7265_LFP_cut = V7265_mouse.LFP[np.ceil(ti*2500).astype(int): np.ceil(tf*2500).astype(int), V7265_mouse.brain_region[1]]
    plt.hist(np.ravel(V7265_LFP_cut), bins = 50)
    plt.show()

    exit() #not the info of contact for this  mouse
    V4606_mouse = Mouse(*stdp.create_mouse_data.prepare_V4606())
    Mouse.set_synchro(V4606_mouse)
    ti = V4606_mouse.frame_to_time(27243) #start contact
    tf = V4606_mouse.frame_to_time(29168) #end contact
    V4606_mouse.set_LFP()
    V4606_LFP_cut = V4606_mouse.LFP[range(np.ceil(ti*2500).astype(int), np.ceil(tf*2500).astype(int)), V4606_mouse.brain_region[1]]
    plt.hist(np.ravel(V4606_LFP_cut), bins = 50)
    plt.show()

def fit_norm_LFP():
    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    red_mouse_LFP = red_mouse.LFP[:, red_mouse.brain_region[1] ]
    red_LFP_vector = np.ravel(red_mouse_LFP)
    red_mu = np.mean(red_LFP_vector)
    red_sigma = np.std(red_LFP_vector)
    red_x = np.linspace(red_mu - 4*red_sigma, red_mu + 4*red_sigma, 300)

    red_LFP_vector_cut = red_LFP_vector[np.logical_and(red_LFP_vector < red_mu + 4*red_sigma , red_LFP_vector > red_mu - 4*red_sigma )]
    red_mu_cut = np.mean(red_LFP_vector_cut)
    red_sigma_cut = np.std(red_LFP_vector_cut)
    print(f'red mu: {red_mu_cut}, red_sigma: {red_sigma_cut}')

    #

    V4606_mouse = Mouse(*stdp.create_mouse_data.prepare_V4606())
    V4606_mouse_LFP = V4606_mouse.LFP[:, V4606_mouse.brain_region[1] ]
    V4606_LFP_vector = np.ravel(V4606_mouse_LFP)
    V4_mu = np.mean(V4606_LFP_vector)
    V4_sigma = np.std(V4606_LFP_vector)
    V4_x = np.linspace(V4_mu - 4*V4_sigma, V4_mu + 4*V4_sigma, 300)

    V4606_LFP_vector_cut = V4606_LFP_vector[np.logical_and(V4606_LFP_vector < V4_mu + 4*V4_sigma , V4606_LFP_vector > V4_mu - 4*V4_sigma )]
    V4_mu_cut = np.mean(V4606_LFP_vector_cut)
    V4_sigma_cut = np.std(V4606_LFP_vector_cut)
    print(f'V4 mu: {V4_mu_cut}, V4_sigma: {V4_sigma_cut}')

    #

    V7265_mouse = Mouse(*stdp.create_mouse_data.prepare_V7265())
    V7265_mouse_LFP = V7265_mouse.LFP[:, V7265_mouse.brain_region[1] ]
    V7265_LFP_vector = np.ravel(V7265_mouse_LFP)
    V7_mu = np.mean(V7265_LFP_vector)
    V7_sigma = np.std(V7265_LFP_vector)
    V7_x = np.linspace(V7_mu - 4*V7_sigma, V7_mu + 4*V7_sigma, 300)

    V7265_LFP_vector_cut = V7265_LFP_vector[np.logical_and(V7265_LFP_vector < V7_mu + 4*V7_sigma , V7265_LFP_vector > V7_mu - 4*V7_sigma )]
    V7_mu_cut = np.mean(V7265_LFP_vector_cut)
    V7_sigma_cut = np.std(V7265_LFP_vector_cut)
    print(f'V7 mu: {V7_mu_cut}, V7_sigma: {V7_sigma_cut}')

     #plot part
    print(f'Red params: mu = {red_mu_cut}, sigma = {red_sigma_cut}')
    print(f'V7 params: mu = {V7_mu_cut}, sigma = {V7_sigma_cut}')
    print(f'V4 params: mu = {V4_mu_cut}, sigma = {V4_sigma_cut}')

    plt.hist(red_LFP_vector, bins = 100, density=True)
    plt.plot(red_x, stats.norm.pdf(red_x, red_mu, red_sigma))
    plt.plot(red_x, stats.norm.pdf(red_x, red_mu_cut, red_sigma_cut))
    plt.legend(['raw normal low', 'outsider cuted'])
    plt.show()

    plt.hist(V7265_LFP_vector, bins = 100, density=True)
    plt.plot(V7_x, stats.norm.pdf(V7_x, V7_mu, V7_sigma))
    plt.plot(V7_x, stats.norm.pdf(V7_x, V7_mu_cut, V7_sigma_cut))
    plt.legend(['raw normal low', 'outsider cuted'])
    plt.show()

    plt.hist(V4606_LFP_vector, bins = 100, density=True)
    plt.plot(V4_x, stats.norm.pdf(V4_x, V4_mu, V4_sigma))
    plt.plot(V4_x, stats.norm.pdf(V4_x, V4_mu_cut, V4_sigma_cut))
    plt.legend(['raw normal low', 'outsider cuted'])
    plt.show()

def repartition_norm_law(mouse: Mouse, out_coef = 3):
    '''
    return the params for fit a gauss law on LFP data. Give it raw, and when not looking at "outsider" values
    '''
    mean_all = np.mean(mouse.LFP, axis = 1)
    mean_cortex = np.mean(mouse.LFP_cortex, axis = 1)

    mu_all = np.mean(mean_all)
    sigma_all = np.std(mean_all)

    mu_cortex = np.mean(mean_cortex)
    sigma_cortex = np.std(mean_cortex)

    selec_all = mean_all[np.logical_and(mean_all > mu_all - out_coef*sigma_all , mean_all < mu_all + out_coef*sigma_all )]
    selec_cortex = mean_cortex[np.logical_and(mean_cortex > mu_cortex - out_coef*sigma_cortex , mean_cortex < mu_cortex + out_coef*sigma_cortex )]

    selec_mu_all = np.mean(selec_all)
    selec_sigma_all = np.std(selec_all)

    selec_mu_cortex = np.mean(selec_cortex)
    selec_sigma_cortex = np.std(selec_cortex)

    return (mu_all, sigma_all, mu_cortex, sigma_cortex, selec_mu_all, selec_sigma_all, selec_mu_cortex, selec_sigma_cortex)

def plot_fig_normal_law(mouse: Mouse, out_coef = 3):
    ''' Fit a normal law on LFP density, then fit again but only on LFP inside mean +- out_coef*variance.
    outsider values might be considered outsider.
    '''
    #(mu_all, sigma_all, mu_cortex, sigma_cortex, selec_mu_all, selec_sigma_all, selec_mu_cortex, selec_sigma_cortex) output params
    mouse_full_LFP = mouse.LFP
    mean_full_LFP = np.mean(mouse_full_LFP, axis=1 )
    mouse_cortex_LFP = mouse.LFP_cortex
    mean_cortex_LFP = np.mean(mouse_cortex_LFP, axis = 1)
    param = repartition_norm_law(mouse, out_coef = out_coef)

    plt.hist(mean_full_LFP, bins = 150, alpha = 0.5, label = 'full brain', density=True)
    plt.hist(mean_cortex_LFP, bins = 150, alpha = 0.5, label = 'cortex region', density = True)
    
    print( f'proba to see exterior:{stats.norm.cdf(param[0]-out_coef*param[1], loc = param[0], scale = param[1])} while proportion is: {np.sum(mean_full_LFP< param[0]-out_coef*param[1])/len(mean_full_LFP) } ' )
    print( f'proba to see exterior:{stats.norm.cdf(param[2]-out_coef*param[3], loc = param[2], scale = param[3])} while proportion is: {np.sum(mean_cortex_LFP< param[2]-out_coef*param[3])/len(mean_cortex_LFP)} ' )
    x_full = np.linspace(param[0]-out_coef*param[1]-300, param[0]+out_coef*param[1], 600)
    plt.plot(x_full, stats.norm.pdf(x_full, param[0], param[1]), color = 'blue' , label = 'normal law fit')
    plt.plot(x_full, stats.norm.pdf(x_full, param[4], param[5]), color = 'red', label = 'limited normal law fit')

    x_cortex = np.linspace(param[2]-out_coef*param[3]-300, param[2]+out_coef*param[3], 600)
    plt.plot(x_cortex, stats.norm.pdf(x_cortex, param[2], param[3]), color = 'blue')
    plt.plot(x_cortex, stats.norm.pdf(x_cortex, param[6], param[7]), color = 'red')
    plt.title(f'LFP distribution of {mouse.mouse_name} mouse')
    plt.legend()
    
    plt.show()

def when_outsider_LFP(mouse, out_coef = 3):
    ''' Plot when outsider value appears in timeline, mean on cortex
    '''
    #(mu_all, sigma_all, mu_cortex, sigma_cortex, selec_mu_all, selec_sigma_all, selec_mu_cortex, selec_sigma_cortex) output params
    mean_trace = np.mean(mouse.LFP_cortex, axis = 1)
    mu_cortex, sigma_cortex = repartition_norm_law(mouse)[6:8]
    time_cortex = np.where(mean_trace < mu_cortex - out_coef*sigma_cortex )[0]
    y, x, _ = plt.hist(time_cortex/2500, bins = 50, color = 'navy')
    plt.title(f'{mouse.mouse_name} outsider time repartition of mean LFP on Cortex')
    plt.vlines([mouse.ti, mouse.tf], 0, np.max(x), colors = 'red')
    plt.show()

def where_outsider_LFP(mouse, out_coef = 3):
    #(mu_all, sigma_all, mu_cortex, sigma_cortex, selec_mu_all, selec_sigma_all, selec_mu_cortex, selec_sigma_cortex) output params
    mouse_LFP = mouse.LFP
    mu, sigma = repartition_norm_law(mouse, out_coef = out_coef)[4:6]
    mouse_time = np.where( mouse_LFP < mu - out_coef*sigma)

    fig, ax = plt.subplots()
    N, bins, patches = ax.hist(mouse_time[1], bins = mouse_LFP.shape[1], color = 'black')
    for i in mouse.brain_region[0]:
        patches[i].set_facecolor('navy')
    for i in mouse.brain_region[1]:    
        patches[i].set_facecolor('firebrick')
    ax.set_xlabel('channels')
    ax.set_title(f'{mouse.mouse_name} mouse, outsider repartition on channels')
    striatum_patch = mpatches.Patch(color='navy', label='striatum region')
    cortex_patch = mpatches.Patch(color='firebrick', label='cortex region')
    remaining_patch = mpatches.Patch(color='black', label='other region or not sure about')
    plt.legend(handles=[striatum_patch, cortex_patch, remaining_patch])
    plt.show()

def where_outsider_LFP_cortex(mouse, out_coef = 3):
    #(mu_all, sigma_all, mu_cortex, sigma_cortex, selec_mu_all, selec_sigma_all, selec_mu_cortex, selec_sigma_cortex) output params
    mouse_LFP = mouse.LFP_cortex
    mu, sigma = repartition_norm_law(mouse, out_coef = out_coef)[6:8]
    mouse_time = np.where( mouse_LFP < mu - out_coef*sigma)
    fig, ax = plt.subplots()
    N, bins, patches = ax.hist(mouse_time[1], bins = mouse_LFP.shape[1])
    ax.set_xlabel('channels')
    ax.set_title(f'{mouse.mouse_name} mouse, outsider repartition on cortex channel')
    plt.show()

def boxplot_bef_aft(time_vect):
    Router_10 = Router('562231799100305614','2b5a317a9f024521af21d2bb754beec9')
    list_of_couple = Router_10.get_couple_id()
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    for couple in list_of_couple:
        RES = Router_10.load_artifacts(couple[0], couple[1])
        mean_selec = compute_int_RES(RES, V7265_mouse.ti-350, V7265_mouse.ti-10)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.6:
            list_pre_saturated += (couple, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (couple, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )

    plt.boxplot([w_pre_saturated_before, w_pre_saturated_after, w_pre_nosat_before, w_pre_nosat_after])
    plt.title(f'compare {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()

def boxplot_bef_aft_V2(time_vect):
    Router_10 = Router('562231799100305614','2b5a317a9f024521af21d2bb754beec9')
    list_of_couple = Router_10.get_couple_id()
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    for couple in list_of_couple:
        RES = Router_10.load_artifacts(couple[0], couple[1])
        mean_selec = compute_int_RES(RES, V7265_mouse.ti-350, V7265_mouse.ti-10)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.6:
            list_pre_saturated += (couple, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (couple, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )

    plt.boxplot([w_pre_saturated_before - w_pre_saturated_after, w_pre_nosat_before - w_pre_nosat_after], positions=[1, 1.2], notch = True)
    plt.xticks([1,1.2], ['saturated', 'not saturated'])
    plt.title(f'compare {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()

def LFP_FR(mouse: Mouse):
    mouse_LFP = np.mean(mouse.LFP_cortex, axis = 1)
    len_LFP = len(mouse_LFP)
    #mouse_LFP = mouse_LFP[:int(21200/21996*len_LFP)] #V7
    #mouse_LFP = mouse_LFP[int(3200/23996*len_LFP):] #V4
    #mouse.t_bounds[1] = mouse.t_bounds[0]+(mouse.t_bounds[1]-mouse.t_bounds[0])*21200/21996 #V7
    #mouse.t_bounds[0] = mouse.t_bounds[0]+(mouse.t_bounds[1]-mouse.t_bounds[0])*3200/23996 #V4
    '''new_LFP = np.zeros(17250) #1725000/100
    for k in range(17250):
        new_LFP[k] = np.mean(mouse_LFP[k*100:(k+1)*100])
    new_LFP = new_LFP/np.sum(new_LFP)
    '''
    res = ()
    list_shift = [0, 5, 10, 15, 20, 25, 30]
    my_res = np.zeros((4, 7))
    it = 0
    list_FR_options = [ ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 0.2, 0.05),
                        ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 1, 0.2),
                        ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 5, 1),
                        ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 20, 4)]
    cortex_cluster = mouse.resume.Cluster_ID.loc[mouse.resume.Channel_ID.isin(mouse.brain_region[1])].to_numpy()
    for FR_options in list_FR_options:
        FR = firing_rate(mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)], *FR_options)
        lr = len(FR)
        print(lr)
        step = len(mouse_LFP)/lr
        new_LFP = np.zeros(lr)
        for k in range(lr):
            new_LFP[k] = np.mean(mouse_LFP[int(k*step):int((k+1)*step)])
        new_LFP = new_LFP/np.std(new_LFP)
        FR = FR/np.std(FR)
        j=0
        for shift in [0, 5, 10, 15, 20, 25, 30]:
            my_res[it, j] = np.mean( (FR[(30-shift) : (-1-shift)]-np.mean(FR))*(new_LFP[15: -16] - np.mean(new_LFP)) )
            j = j+1
        shift = list_shift[np.argmax(my_res[it, :])]
        plt.plot(new_LFP[15: -16]-np.mean(new_LFP), color = 'blue')
        plt.plot(FR[(30-shift) : (-1-shift)]-np.mean(FR), color = 'red')
        plt.show()
        #res = (res) + (np.cov(new_LFP, FR),)
        
    
        """ind0 = np.array([k[0] for k in res_dtw[0]])
        ind1 = np.array([k[1] for k in res_dtw[0]])
        plt.scatter(new_LFP[ind0], FR[ind1], alpha = 0.7)
        plt.plot(ind0/np.max(ind0)*np.max(new_LFP), ind1/np.max(ind1)*np.max(FR), color = 'red')
        plt.plot([0,np.max(new_LFP)], [0, np.max(FR)], color = 'black')
        plt.show()
        """
        
        it = it+1
    return (my_res)

def corr_speed_cortex(mouse: Mouse, interneurons_list):
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    interneurons_spikes = mouse.spike_times[mouse.spike_times.Cluster_ID.isin(interneurons_list)]
    interneurons_spikes = interneurons_spikes.Spike_times[interneurons_spikes.Channel_ID > 199]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID > 199]
    FR_cortex = firing_rate(cortex_spikes, [0, 1100], 1, 0.25)
    FR_inter = firing_rate(interneurons_spikes, [0, 1100], 1, 0.25)

    speed = mouse.speed[np.logical_and(mouse.speed.time < 1800, mouse.speed.time > 700)]
    speed_FR = moving_window_timed((speed.time, speed.speed), [700, 1800], 1, 0.25, np.mean)    
    plt.plot( FR_cortex-np.mean(FR_cortex), label = 'FR cortex')
    plt.plot( FR_inter, label = 'FR inter')
    plt.plot( speed_FR, label = 'speed')
    plt.legend()
    plt.show()
    return 0

def corr_speed_striatum(mouse: Mouse, striatum_list ,interneurons_list):
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    interneurons_spikes = mouse.spike_times[mouse.spike_times.Cluster_ID.isin(interneurons_list)]
    interneurons_spikes = interneurons_spikes.Spike_times[interneurons_spikes.Channel_ID > 249]
    speed = mouse.speed[np.logical_and(mouse.speed.time < 1800, mouse.speed.time > 700)]

    speed_FR = firing_rate(speed.speed, [0, 1100], 1, 0.25)

    for clust in striatum_list:
        striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
        FR = firing_rate(striatum_spikes, [0, 1100], 1, 0.25)
    
    
    plt.plot( np.linspace(0, 1100, len(FR)) , FR, label = 'FR cortex')
    plt.plot(speed.time-700, speed.speed, label = 'speed')
    plt.legend()
    plt.show()
    return 0

def match_timed(t_pre, t_post):
    '''
    for each time of t_pre, give the delta time of nearest time of t_post
    '''
    res = np.zeros(len(t_pre))
    for it, t in enumerate(t_pre):
        diff = t - t_post
        res[it] = diff[diff.abs().idxmin()]
    return res

def analyse_dt(mouse: Mouse):
    '''
    Apply match timed on all team A and B+ cluster of a mouse.

    output:
        a tuple (clust_pre, clust_post, dt based on pre)
    '''
    cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[0])]
    cluster_cortex = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cortex_len = len(cluster_cortex)
    it = 1
    tab_res = ()

    for pre_clust in cluster_cortex:
        LOGGER.info(f'preclust {it} / {cortex_len}')
        t_pre = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == pre_clust]
        for post_clust in cluster_striatum:
            t_post = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == post_clust]
            tab_res += ([pre_clust, post_clust, match_timed(t_pre, t_post)], )
        it += 1

    save_object(tab_res, pathlib.Path(mouse.path_mouse, 'data_analyse', 'analyse_dt' ).resolve())

    return tab_res

def verif_no_zeros(exp_id, run_id, mouse_name, mouse_path = None):
    '''
    verify if supposed positive variable of the synaptic model indeed stay positive during integration.
    This is possible because each step of integration has been saved during all simulations.
    '''
    VECTOR_ELEMENT_NAMES = ['B1', 'B2', 'B3' , 'B4' , 'B5' , 'B6' , 'B7' , 'B8' , 'B9' , 'B10' , 'B11' , 'B12' , 'B13', 'Ca_cyt' , 'Ca_ER' , 'h' , 'IP3' , 'V' , 'rAMPA' , 'rNMDA', 'PP1' , 'I1P' , 'DAG', 'DAGLP' , '2-AG' , 'fpre' , 'AEA' , 'Glu' , 'Curr_spikepost', 'cst']
    #                         0     1     2      3      4      5      6      7      8      9        10      11     12      13          14     15     16     17      18        19      20      21      22      23        24       25      26      27           28           29
    no17 = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,18,19,20,21,22,23,24,25,26,27,29])
    expe = Router(exp_id, run_id, mouse_path)
    result_id = expe.get_result_id()
    for clust_id in result_id:
        sol = np.load(expe.get_artifacts_dir() / f'sol_{mouse_name}_{clust_id}.npy')
        res = np.sum(sol[no17] < 0 , axis = 1)
        sol_min = np.min(np.abs(sol[no17]) , axis = 1)
        #print(f'cluster {clust_id}, {res}/{len(sol[0])}')
        print(sol_min)
    return 0

def compare_dt_sat(mouse: Mouse):
    '''
    Apply match timed on all team A and B+ cluster of a mouse.
    '''
    cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[0])]
    cluster_cortex = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[1])]
    cortex_len = len(cluster_cortex)
    it = 1
    tab_res = ()

    for pre_clust in cluster_cortex:
        LOGGER.info(f'preclust {it} / {cortex_len}')
        t_pre = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == pre_clust]
        for post_clust in cluster_striatum:
            t_post = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == post_clust]
            tab_res += ([pre_clust, post_clust, match_timed(t_pre, t_post)], )
        it += 1

    save_object(tab_res, pathlib.Path(mouse.path_mouse, 'data_analyse', 'analyse_dt' ).resolve())

    return tab_res
#verif_no_zeros('562231799100305614', 'd7792f0df3e84581b6a9293a363a4440', 'V7265')


######################## Call functions ##########################
def lookat_opti(mouse: Mouse):
    mouse_glu = optiGlu(mouse)
    optim_values = mouse_glu.get_optim_values()
    it = 0
    mat_res = np.zeros((len(optim_values.keys()), 9))
    for clust_id in optim_values.keys():
        mat_res[it, :] = optim_values[clust_id]
        it = it+1
        plt.plot(optim_values[clust_id][:8], label = clust_id)
    #plt.legend()
    plt.title(mouse.mouse_name)
    plt.show()
    print(np.unique(mat_res[:, 4], return_counts=True))
    print(np.unique(mat_res[:, 7], return_counts=True))
    return 0

##################################################################
#name = 'Blue'
#"path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
#mouse = load_object(path)

#pd.read_csv(mouse.path_mouse / 'Red_Test_Table_data.txt', usecols=[0,1])


''' #random mouse
time_vect = [V7265_mouse.ti - 350 , V7265_mouse.ti - 10, V7265_mouse.tf  , V7265_mouse.tf + 100 ]

Router_10 = Router('562231799100305614','d95a4d2f64524ad29631675a17501518')
list_of_id = Router_10.get_result_id()
list_pre_saturated = ( )
list_pre_nosat = ( )
w_pre_saturated_after = np.array([])
w_pre_nosat_after = np.array([])
w_pre_saturated_before = np.array([])
w_pre_nosat_before = np.array([])
for id in list_of_id:
    RES = Router_10.load_result(id)
    mean_selec = compute_int_RES(RES, V7265_mouse.ti-350, V7265_mouse.ti-10)[1]
    mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
    mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
    if mean_selec > 2.6:
        list_pre_saturated += (id, )
        w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
        w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
    else:
        list_pre_nosat += (id, )
        w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
        w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )

plt.boxplot([w_pre_saturated_before - w_pre_saturated_after, w_pre_nosat_before - w_pre_nosat_after], positions=[1, 1.2])#, notch = True)
plt.xticks([1,1.2], ['saturated', 'not saturated'])
plt.title(f'compare {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
plt.show()

print(len(list_pre_nosat)/500)'''

#boxplot_bef_aft_V2([V7265_mouse.ti - 410 , V7265_mouse.ti - 210, V7265_mouse.ti - 210  , V7265_mouse.ti - 10 ])

'''
boxplot_bef_aft_V2([V7265_mouse.ti - 200-10, V7265_mouse.ti - 200, V7265_mouse.ti - 200-5, V7265_mouse.ti - 200+5])
boxplot_bef_aft_V2([V7265_mouse.ti - 200, V7265_mouse.ti - 200+10, V7265_mouse.ti - 200+5, V7265_mouse.ti - 200+15])
boxplot_bef_aft_V2([V7265_mouse.ti - 200+10, V7265_mouse.ti - 200+20, V7265_mouse.ti - 200+15, V7265_mouse.ti - 200+25])
boxplot_bef_aft_V2([V7265_mouse.ti - 200+25, V7265_mouse.ti - 200+35, V7265_mouse.ti - 200+30, V7265_mouse.ti - 200+40])
'''



'''
[0.14878479 0.1492572  0.18932347 0.58887654] #RED
[0.12048502 0.11959754 0.13080367 0.26446793] #V7
[0.0348183  0.03406956 0.04103351 0.06984857] #V4
'''

'''
plot_fig_normal_law(red_mouse)
plot_fig_normal_law(V7265_mouse)
plot_fig_normal_law(V4606_mouse)
'''

'''
where_outsider_LFP(red_mouse)
where_outsider_LFP(V7265_mouse)
where_outsider_LFP(V4606_mouse)

where_outsider_LFP_cortex(red_mouse)
where_outsider_LFP_cortex(V7265_mouse)
where_outsider_LFP_cortex(V4606_mouse)
'''