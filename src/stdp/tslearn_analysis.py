import logging

import numpy as np
import pandas as pd
import tslearn
#from tslearn.metrics import dtw, cdist_dtw
from tslearn.clustering import TimeSeriesKMeans
from tslearn.utils import to_time_series
import dtaidistance.dtw
#from dtaidistance import dtw, clustering

import matplotlib.pyplot as plt
import matplotlib as mpl

import stdp.Analysis_spike

LOGGER = logging.getLogger(__name__)

cols_load_resume =  ['Team A', 'channel A', 'Team B+', 'channel B+', 'Team B', 'channel B', 'Team B-', 'channel B-', 'Team C', 'channel C']
cols_name_resume =  ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B', 'channel_B', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']
cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']

resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V7265_final_data/V7265_sorted.xlsx', header = 3) #V7265
resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'channel.2', 'Team B-', 'channel.3', 'Team C', 'channel.4']] #V7265
resume.columns = cols_name_resume

spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265_final_data/Spike_times_V7265_Charlotte_Clustering.txt', usecols = cols_load_time)
spike_times.columns = cols_name_time

synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265_final_data/V7265_synchro', usecols=[0,1])
synchro.columns = ['time', 'frame']

id_clust_cortex = np.concatenate((resume.Team_A[resume.channel_A > 200], resume['Team_B+'][resume['channel_B+'] > 200]))
id_clust_cortex = id_clust_cortex.astype(int)
id_clust_striatum = np.concatenate((resume.Team_A[resume.channel_A < 180], resume['Team_B+'][resume['channel_B+'] < 180]))
id_clust_striatum = id_clust_striatum.astype(int)
id_clust_striatum = np.delete(id_clust_striatum, np.where(id_clust_striatum == 4))

path = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/f11e6da016ef45ad9a6a07f386214813/artifacts/'

#args_fr = [t_bounds, win_len, win_step]
def matcorr_cortex(id_cortex_list, args_fr):
    mat_corr = np.zeros((len(id_cortex_list), len(id_cortex_list)))
    cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == id_cortex_list[0]]
    fr_cortex = np.zeros( (len(id_cortex_list), len(stdp.Analysis_spike.moving_window(cortex_times, *args_fr, len)) ) )
    for i, id in enumerate(id_cortex_list):
        cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == id]
        fr_cortex[i, :] = np.sqrt(stdp.Analysis_spike.moving_window(cortex_times, *args_fr, len))

    for i, id1 in enumerate(id_cortex_list):
        for j, id2 in enumerate(id_cortex_list[i:]):
            mat_corr[i, i + j] = dtw.distance(fr_cortex[i, :], fr_cortex[i + j, :])
        #print(i)

    #plt.matshow(mat_corr)
    #plt.colorbar()
    #plt.show()
    return(mat_corr)

def clustering_neurons(id_cortex_list, args_fr):
    cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == id_cortex_list[0]]
    fr_cortex = np.zeros( (len(id_cortex_list), len(stdp.Analysis_spike.moving_window(cortex_times, *args_fr, len)) ) )
    for i, id in enumerate(id_cortex_list):
        cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == id]
        fr_cortex[i, :] = np.sqrt(stdp.Analysis_spike.moving_window(cortex_times, *args_fr, len))

    return tslearn.clustering.TimeSeriesKMeans(n_clusters=3, max_iter = 200, n_init = 4, metric="dtw", max_iter_barycenter = 200).fit(fr_cortex)
    #return tslearn.clustering.KShape(n_clusters=2, max_iter = 200, n_init = 4).fit(fr_cortex)

def compare_2_list(gr1, gr2):
    gr1_match = np.sum(np.isin(gr1, gr2))/len(gr1)
    gr2_match = np.sum(np.isin(gr2, gr1))/len(gr2)
    return [gr1_match, gr2_match]

def compare_group_list(gr1, gr2):
    mat_res12 = np.zeros((len(gr1), len(gr2)))
    mat_res21 = np.zeros((len(gr1), len(gr2)))
    for i in range(len(gr1)):
        for j in range(len(gr2)):
            mat_res12[i, j], mat_res21[i, j] = compare_2_list(gr1[i], gr2[j])
    return ((mat_res12, mat_res21))

def compare_multi_group():
    for i in range(5):
        for j in range(5-i-1):
            print(f'groups {i} VS {i+j+1} \n')
            res = compare_group_list(groups_cortex[i], groups_cortex[i+j+1])
            print(res[0])
            print(res[1])
    return 0

#region #DATA GROUP FOR COMPARE
group_1_cortex = [[616, 1187,331,602,617,183,619,325,309,345, 1103, 1064,603,232,239,684,243],
                  [ 224,1153,685,196,540,532,529, 1248,358,208,212,489, 1198 ,1190,1123,506, 1148,457, 1142,320 ,1054, 1045,400,378,240,255,261,279,1149, 1135],
                  [ 297,304, 1021,371, 1036,294,268,324, 1132,192,411,362, 1211,732,1091 ,1161]]

group_1_striatum =[[ 638, 50, 30,786, 1005,111, 1282,968,148,747,771,650,626, 37],
                    [ 143, 1288,908,982,108, 1416,110 ,1388 ,1337,158, 1338 ,1312, 86,628,826,828, 11 ,1287,970,832,922, 1434, 1381,436, 1352],
                    [1378,849,657,95,636]]

group_2_cortex = [[ 602,1161,732,192,1132,324,371,1021,268,1036,1211,1091,297,331,304,294,1187,362,411],
                   [ 532,540,1142,1190,506,685,1248,1153,358,1123,529,1148,279,1198,489,212,208,196,224,1054,320,457,1045,378,1135,240,255,261,1149,400],
                   [ 616,183,619,325,309,345,1103,1064,243,684,239,232,617,603]] 

group_2_striatum = [[1338,108, 1416,110,1388,1337,143,158,1287,1312,1288,1352,436,1381,1434,982,922,832,11,828,826,628,111, 86, 37,747,650,771,908,970],
                     [1005,1282, 50,638,1378,148,849],
                     [ 30,786,968,626,657,95,636]]

group_3_cortex = [[ 602,371,331,1021,268,1211,362,1036,1103,294,1187,1161,1091,411,1132],
                   [ 685,1142,1123,358,1153,529,540,532,1149,1190,1248,304,506,279,1148,255,297,1198,489,212,208,196,224,1054,320,324,457,192,1045,378,732,1135,240,261,400],
                   [ 616,183,619,325,309,345,1064,243,684,239,232,617,603]]

group_3_striatum = [[1338,908,771,1282,1005,50,1312,628,111,638,37,650,849,747,86,148],
                     [ 657,626,968,786,30, 95,636,1378],
                     [1287,826,828,11,832,970,922,1434,1288,1381,982,108,1416,110,1388,1352,143,158,436,1337]]

group_4_cortex = [[1148,378,1190,400,1135,240,255,261,1045,279,529,1142,1123,358,1153,685,506,1149,192,1248,457,297,1198,489,212,304,540,208,196,224,1054,320,324,532],
                   [ 325,619,183,617,345,309,616,603,1064,232,239,684],
                   [1091,371,1036,243,294,1187,732,602,411,362,1161,1211,268,1021,1132,1103,331]]
 
group_4_striatum = [[ 828,1312,143,1352,1337,1381,1434,922,970,832,11,1287,158,1388,110,1416,108,982,1288,826],
                     [50,1282,771,908,1338,638,436,628,111,86,37,148,747,650],
                     [ 657,626,1005,849,786,30,1378,95,968,636]]

group_5_cortex = [[1148,1045,378,529,400,1135,240,255,192,261,1149,532,1142,1123,358,1153,685,279,506,1190,1248,297,1198,489,212,304,457,208,540,196,224,1054,320],
                   [309,183,617,345,325,619,616,603,232,1064,239,684],
                   [1091,371,1036,243,294,1187,732,602,411,362,1161,1211,268,1021,324,1132,1103,331]]

group_5_striatum = [[1338,908,771,1282,1005,50,970,628,111,638,37,650,849,148,747, 86],
                     [657,626,968,786,30,95,636,1378],
                     [1287,826,828,11,832,922,1434,1381,1288,436,1352,982,108,1416,110,1388,143,158,1312,1337]]

groups_cortex = [group_1_cortex, group_2_cortex, group_3_cortex, group_4_cortex, group_5_cortex]
groups_striatum = [group_1_striatum, group_2_striatum, group_3_striatum, group_4_striatum, group_5_striatum]

#endregion

#print(dtaidistance.dtw.distance(np.array([1,2,3]), np.array([1,2,3])))


'''
s = matcorr_cortex(id_clust_cortex, [[0, 1200], 10, 2])
model = clustering.KMedoids(dtw.distance_matrix_fast, {}, k=3)
res = model.fit(s)
labels_cortex = res.labels_
id_labels_cortex = labels_cortex.argsort()

len_lab_cortex = np.cumsum(np.array([sum(labels_cortex == 0), sum(labels_cortex == 1), sum(labels_cortex == 2)]))
print(id_clust_cortex[id_labels_cortex[:len_lab_cortex[0]]])
print(id_clust_cortex[id_labels_cortex[len_lab_cortex[0]:len_lab_cortex[1]]])
print(id_clust_cortex[id_labels_cortex[len_lab_cortex[1]:]])
'''


#compare_multi_group()

'''
print(np.concatenate((id_clust_cortex, id_clust_striatum)).shape)
mat_corr= matcorr_cortex(np.concatenate((id_clust_cortex, id_clust_striatum)) , [[0, 1200], 10, 2])
plt.matshow(mat_corr)
plt.show()

quit()
'''

''' # CLUSTERING
res_cortex = clustering_neurons(id_clust_cortex, [[0, 1200], 10, 2])
res_striatum = clustering_neurons(id_clust_striatum, [[0, 1200], 10, 2])

labels_cortex = res_cortex.labels_
id_labels_cortex = labels_cortex.argsort()
#mat_corr_cortex = matcorr_cortex(id_clust_cortex[id_labels_cortex], [[0, 1200], 10, 2])

labels_striatum = res_striatum.labels_
id_labels_striatum = labels_striatum.argsort()

len_lab_cortex = np.cumsum(np.array([sum(labels_cortex == 0), sum(labels_cortex == 1), sum(labels_cortex == 2)]))
print(id_clust_cortex[id_labels_cortex[:len_lab_cortex[0]]])
print(id_clust_cortex[id_labels_cortex[len_lab_cortex[0]:len_lab_cortex[1]]])
print(id_clust_cortex[id_labels_cortex[len_lab_cortex[1]:]])

len_lab_striatum = np.cumsum(np.array([sum(labels_striatum == 0), sum(labels_striatum == 1), sum(labels_striatum == 2)]))
print(id_clust_striatum[id_labels_striatum[:len_lab_striatum[0]]])
print(id_clust_striatum[id_labels_striatum[len_lab_striatum[0]:len_lab_striatum[1]]])
print(id_clust_striatum[id_labels_striatum[len_lab_striatum[1]:]])

print(len_lab_cortex)

mat_corr= matcorr_cortex(np.concatenate((id_clust_cortex[id_labels_cortex], id_clust_striatum[id_labels_striatum])) , [[0, 1200], 10, 2])
'''

'''
len_lab0_c = sum(labels_cortex == 0)
len_lab1_c = sum(labels_cortex == 1)
len_lab2_c = sum(labels_cortex == 2)
'''

''' # CROSS COMPARE CORTEX STRIATUM
len_lab = np.array([sum(labels_cortex == 0), sum(labels_cortex == 1), sum(labels_cortex == 2),
                    sum(labels_striatum == 0), sum(labels_striatum == 1), sum(labels_striatum == 2)])
len_lab = np.cumsum([len_lab])

print(len_lab)
print(mat_corr.shape)
#plt.matshow(mat_corr)
mat_corr_c0s0 = mat_corr[:len_lab[0], len_lab[2]:len_lab[3]]
mat_corr_c0s1 = mat_corr[:len_lab[0], len_lab[3]:len_lab[4]]
mat_corr_c0s2 = mat_corr[:len_lab[0], len_lab[4]:]
mat_corr_c1s0 = mat_corr[len_lab [0]:len_lab[1], len_lab[2]:len_lab[3]]
mat_corr_c1s1 = mat_corr[len_lab [0]:len_lab[1], len_lab[3]:len_lab[4]]
mat_corr_c1s2 = mat_corr[len_lab [0]:len_lab[1], len_lab[4]:]
mat_corr_c2s0 = mat_corr[len_lab[1]:len_lab[2], len_lab[2]:len_lab[3]]
mat_corr_c2s1 = mat_corr[len_lab[1]:len_lab[2], len_lab[3]:len_lab[4]]
mat_corr_c2s2 = mat_corr[len_lab[1]:len_lab[2], len_lab[4]:]

plt.boxplot([np.ravel(mat_corr_c0s0[np.where(mat_corr_c0s0>0)]),
             np.ravel(mat_corr_c0s1[np.where(mat_corr_c0s1>0)]),
             np.ravel(mat_corr_c0s2[np.where(mat_corr_c0s2>0)]),
             np.ravel(mat_corr_c1s0[np.where(mat_corr_c1s0>0)]),
             np.ravel(mat_corr_c1s1[np.where(mat_corr_c1s1>0)]),
             np.ravel(mat_corr_c1s2[np.where(mat_corr_c1s2>0)]),
             np.ravel(mat_corr_c2s0[np.where(mat_corr_c2s0>0)]),
             np.ravel(mat_corr_c2s1[np.where(mat_corr_c2s1>0)]),
             np.ravel(mat_corr_c2s2[np.where(mat_corr_c2s2>0)])])
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9], ['0x0', '0x1', '0x2', '1x0' , '1x1', '1x2', '2x0', '2x1', '2x3'])
plt.show()
'''

''' #for 4 Cluster
len_lab3 = sum(labels == 3)
mat_corr_lab0 = mat_corr[:len_lab0, :len_lab0]
mat_corr_lab1 = mat_corr[len_lab0:(len_lab0+len_lab1), len_lab0:(len_lab0+len_lab1)]
mat_corr_lab2 = mat_corr[(len_lab0+len_lab1):(len_lab0+len_lab1+len_lab2), (len_lab0+len_lab1):(len_lab0+len_lab1+len_lab2)]
mat_corr_lab3 = mat_corr[(len_lab0+len_lab1+len_lab2):, (len_lab0+len_lab1+len_lab2):]

mat_cross_01 = mat_corr[:len_lab0, len_lab0:(len_lab0+len_lab1)]
mat_cross_02 = mat_corr[:len_lab0, (len_lab0+len_lab1):(len_lab0+len_lab1+len_lab2)]
mat_cross_12 = mat_corr[len_lab0:(len_lab0+len_lab1), len_lab0+len_lab1:(len_lab0+len_lab1+len_lab2)]
mat_cross_03 = mat_corr[:len_lab0, (len_lab0+len_lab1+len_lab2):]
mat_cross_13 = mat_corr[len_lab0:(len_lab0+len_lab1), (len_lab0+len_lab1+len_lab2):]
mat_cross_23 = mat_corr[len_lab0+len_lab1:(len_lab0+len_lab1+len_lab2), (len_lab0+len_lab1+len_lab2):]

plt.boxplot([np.ravel(mat_corr_lab0[np.where(mat_corr_lab0>0)]),
             np.ravel(mat_corr_lab1[np.where(mat_corr_lab1>0)]),
             np.ravel(mat_corr_lab2[np.where(mat_corr_lab2>0)]),
             np.ravel(mat_corr_lab3[np.where(mat_corr_lab3>0)]),
             np.ravel(mat_cross_01[np.where(mat_cross_01>0)]),
             np.ravel(mat_cross_02[np.where(mat_cross_02>0)]),
             np.ravel(mat_cross_12[np.where(mat_cross_12>0)]),
             np.ravel(mat_cross_03[np.where(mat_cross_03>0)]),
             np.ravel(mat_cross_13[np.where(mat_cross_13>0)]),
             np.ravel(mat_cross_23[np.where(mat_cross_23>0)])])
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], ['0', '1', '2', '3' , '0x1', '0x2', '1x2', '0x3', '1x3', '2x3'])
plt.show()
'''

''' #for 3 Cluster
mat_corr_lab0 = mat_corr[:len_lab0, :len_lab0]
mat_corr_lab1 = mat_corr[len_lab0:(len_lab0+len_lab1), len_lab0:(len_lab0+len_lab1)]
mat_corr_lab2 = mat_corr[(len_lab0+len_lab1):, (len_lab0+len_lab1):]

mat_cross_01 = mat_corr[:len_lab0, len_lab0:(len_lab0+len_lab1)]
mat_cross_02 = mat_corr[:len_lab0, (len_lab0+len_lab1):]
mat_cross_12 = mat_corr[len_lab0:(len_lab0+len_lab1), len_lab0+len_lab1:]

plt.boxplot([np.ravel(mat_corr_lab0[np.where(mat_corr_lab0>0)]),
             np.ravel(mat_corr_lab1[np.where(mat_corr_lab1>0)]),
             np.ravel(mat_corr_lab2[np.where(mat_corr_lab2>0)]),
             np.ravel(mat_cross_01[np.where(mat_cross_01>0)]),
             np.ravel(mat_cross_02[np.where(mat_cross_02>0)]),
             np.ravel(mat_cross_12[np.where(mat_cross_12>0)])])
plt.xticks([1,2,3,4,5,6], ['0', '1', '2' , '0x1', '0x2', '1x2'])
plt.show()
'''

''' #for 2 Cluster
mat_corr_lab0 = mat_corr[:len_lab0, :len_lab0]
mat_corr_lab1 = mat_corr[len_lab0:, len_lab0:]

mat_cross = mat_corr[:len_lab0, len_lab0:]

plt.boxplot([np.ravel(mat_corr_lab0[np.where(mat_corr_lab0>0)]),
             np.ravel(mat_corr_lab1[np.where(mat_corr_lab1>0)]),
             np.ravel(mat_cross[np.where(mat_cross>0)])])
plt.xticks([1,2,3], ['0', '1', 'cross'])
plt.show()
'''