import pickle
import scipy.stats
from sklearn.cluster import KMeans

import plotly.express as px
import matplotlib.pyplot as plt
import numpy as np
from stdp.create_mouse_data import Mouse
from stdp.Class_Analysis import Router
from computation_tools import *
import pandas as pd


########################## INIT VARIABLES ####################################

P1=1e-10 #1e-3
P3=5 #2
P4=2 #2e-3
alphaAEACB1=0.10
LTDstart=5e-3 #5e-3
LTDstop=10e-3 #13e-3
LTPstart=19e-3
LTDMax=0.20 #0.25
LTPMax=15

##############################################################################

class cat_res():
    def __init__(self, wpre_nAG, wpre_tAG, wpre_tCa, wpre_Ca, wpre_tAGCa):
        self.wpre_nAG = wpre_nAG
        self.wpre_tAG = wpre_tAG
        self.wpre_tCa = wpre_tCa
        self.wpre_Ca = wpre_Ca
        self.wpre_tAGCa = wpre_tAGCa
    """
    wpre_nAG: number of switch to Omega LTP
    wpre_tAG: time past in Omega LTP
    wpre_tCa: time past in hight Ca (taufpre<100)
    wpre_Ca: integral of 1/taufpre, to give info on how much low
    wpre_tAGCa: time past with Omega LTP while hight Ca
    """
    def f(self):
        return 0

def compute_Omega(router: Router, couple):
    y = router.load_solution_couple(couple)
    if y.shape[0]>10:
        y = y[24:27, :]
    taufpre=P1/((P1/1e-4)**P3+(y[0]+alphaAEACB1*y[2, :])**P3)+P4
    Omega=(0.5-LTDMax*np.heaviside(y[0]+alphaAEACB1*y[2]-LTDstart, 1)*
            np.heaviside(LTDstop-y[0]-alphaAEACB1*y[2], 1)+
            np.heaviside(y[0]+alphaAEACB1*y[2]-LTPstart, 1)*LTPMax)
    return (Omega, taufpre)

def compute_carCat(T , Omega, taufpre):
    '''compute the variables of a result for creating cat_res class object
    '''
    IOmega = (Omega > 2)*1
    Itaufpre = (taufpre < 100)*1
    dIOmega = IOmega[1:]-IOmega[:-1]
    int_cat = compute_int_X(T, np.array([IOmega, 1/taufpre, Itaufpre, IOmega*Itaufpre]), T[0], T[-1])
    wpre_nAG = np.sum(dIOmega == 1)
    wpre_tAG = int_cat[0]*(T[-1]-T[0])
    wpre_Ca = int_cat[1]
    wpre_tCa = int_cat[2]
    wpre_tAGCa = int_cat[3]
    W = [wpre_nAG, wpre_tAG, wpre_tCa, wpre_Ca, wpre_tAGCa]
    return W

def res_to_carCat(router:Router, mouse:Mouse):
    """This function want to sort results into category
    """
    
    result_id = router.get_couple_id()
    W_cat = np.zeros((len(result_id), 5))


    for it, couple in enumerate(result_id):
        T = router.load_result_couple(*couple)[2]
        Omega, taufpre = compute_Omega(router, couple)
        W_cat[it, :] = compute_carCat(T, Omega, taufpre)

    return W_cat

def Kmean_cat(X, n_clust=4, options = (0, 'auto')):
    kmean = KMeans(n_clust).fit(X) #n_clusters, random_state, n_init)
    labels = kmean.labels_
    lab_dict = {'x':'wpre_nAG',
            'y':'wpre_tAG',
            'z':'wpre_tCa',
            'color': 'cat'}
    #[wpre_nAG, wpre_tAG, wpre_tCa, wpre_Ca, wpre_tAGCa]
    fig = px.scatter_3d(x = X[:, 1], y = X[:, 2], z = X[:, 4],
                        color = labels, labels=lab_dict)#, labels=lab_dict)#, x='sepal_length', y='sepal_width', z='petal_width', color='species')
    fig.show()
    fig = px.scatter_3d(x = X[:, 0], y = X[:, 1], z = X[:, 2],
                        color = labels, labels=lab_dict)#, labels=lab_dict)#, x='sepal_length', y='sepal_width', z='petal_width', color='species')
    fig.show()


############### UTILITIES #######################

def save_object(obj, file):
    try:
        with open(file, "wb") as f:
            pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)

def load_object(filename):
    try:
        with open(filename, "rb") as f:
            return pickle.load(f)
    except Exception as ex:
        print("Error during unpickling object (Possibly unsupported):", ex)

#################################################
        
def compare_wpre_tAG(router:Router, mouse:Mouse):

        #Part related to real pair
    list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
    list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
    real_pair = list.to_numpy()
    check_real = real_pair[:, 0]**3 + real_pair[:, 1]**3

    result_id = router.get_couple_id()
    t_win_before = [mouse.ti-310, mouse.ti-10]
    t_win_after = [mouse.ti, mouse.tf+5]
    ratio_mouse = np.zeros(len(real_pair))
    it = 0
    for couple in result_id:
        if np.isin(couple[0]**3+couple[1]**3, check_real):
            T = router.load_result_couple(*couple)[2]
            Omega = compute_Omega(router, couple)[0]
            IOmega = (Omega > 2)*1
            #dIOmega = IOmega[1:]-IOmega[:-1]
            wpre_tAG_before = compute_int_X(T, np.array([IOmega]), *t_win_before)[0]/(t_win_before[1]-t_win_before[0])
            wpre_tAG_after = compute_int_X(T, np.array([IOmega]), *t_win_after)[0]/(t_win_after[1]-t_win_after[0])
            ratio_mouse[it] = wpre_tAG_after/wpre_tAG_before
            it += 1

    ratio_mouse = ratio_mouse[np.isfinite(ratio_mouse)]
    plt.boxplot(ratio_mouse, showmeans=True)
    plt.show()

    return 0

#################################################

def big_cortex(router: Router, mouse: Mouse):
    ''' We want to make basic analyse like mean pre weight comparison before/after task.
        This is in cas big cortex, ie when all cortex neuron are merged
    
    '''
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    time_vect = np.array([mouse.ti-20, mouse.ti, mouse.tf, mouse.tf + 20])
    result_id = router.get_result_id()
    #result_id = np.array([ 11,108,110,143,158])

    for clust in result_id:
        RES = router.load_result(clust, name = f'{mouse.mouse_name}')
        y = router.load_solution(clust, name = f'{mouse.mouse_name}')
        mean_selec = compute_int_RES(RES, mouse.ti-20, mouse.ti)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.2:
            list_pre_saturated += (clust, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (clust, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )

        plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre-syn weight')
        plt.plot(RES[2], RES[1]/RES[1][0], label = 'post-syn weight')
        plt.title(f'striatum cluster n°{clust}, {mouse.mouse_name}')
        plt.ylabel('plasticity variation (%)')
        plt.xlabel('time (s)')
        plt.legend()
        plt.vlines([mouse.ti, mouse.tf], 0, 30, color = ['red', 'red'])
        plt.show()
        

    print(f'len no sat: {len(list_pre_nosat)}, len sat: {len(list_pre_saturated)} ')
    plt.boxplot([w_pre_saturated_before, w_pre_saturated_after,w_pre_nosat_before , w_pre_nosat_after, -w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after])#, notch = True)
    #plt.boxplot([-w_pre_saturated_before + w_pre_saturated_after, -w_pre_nosat_before + w_pre_nosat_after], positions=[1, 1.4])#, notch = True)
    plt.xticks([1,1.4], ['saturated', 'not saturated'])
    plt.title(f'compare btwn times {int(time_vect[0])}-{int(time_vect[1])} and  {int(time_vect[2])}-{int(time_vect[3])}')
    plt.show()
    return 0

def big_cortex_random(router: Router):
    ''' We want to make basic analyse like mean pre weight comparison before/after task.
        This is in cas big cortex, ie when all cortex neuron are merged
    
    '''
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    time_vect = np.array([170, 190, 205, 225])
    result_id = router.get_result_id_random()

    for clust in result_id:
        RES = router.load_result(clust, name = None)
        mean_selec = compute_int_RES(RES, 150,190)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.2:
            list_pre_saturated += (clust, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (clust, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )

        plt.plot(RES[2], RES[0]/RES[0][0], label = 'pre-syn weight')
        plt.plot(RES[2], RES[1]/RES[1][0], label = 'post-syn weight')
        plt.title(f'striatum cluster n°{clust}')
        plt.ylabel('plasticity variation (%)')
        plt.xlabel('time (s)')
        plt.legend()
        plt.vlines([200, 205], 0, 30, color = ['red', 'red'])
        plt.show()
    #plt.boxplot([ w_pre_saturated_after-w_pre_saturated_before, w_pre_nosat_after-w_pre_nosat_before])
    #plt.show()

def poisson_random():
    router = Router('490892905216247639', '045481cadc67438a9b98a70f255d47e9')
    #7dcf8350397a433ab70c5bade1840b3c 5Hz
    #9368de0defbd49dd8441a9441344808a 1Hz
    #045481cadc67438a9b98a70f255d47e9 2.5Hz
    big_cortex_random(router)

def boxplot_cross_mouse(router: Router, mouse: Mouse, time_vect = None):
    """ Make boxplot of mean pre-weight before and after (time vect give that time) with some selection (saturation etc...)
    """
    if time_vect == None:
        time_vect = np.array([mouse.ti-50, mouse.ti-10, mouse.tf, mouse.tf + 50])
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])

    w_pre_nosat_real_before = np.array([])
    w_pre_nosat_real_after = np.array([])
    w_pre_saturated_real_before = np.array([])
    w_pre_saturated_real_after = np.array([])
    result_id = router.get_couple_id()


    list_id = np.unique(result_id)
    wdiff_max = np.zeros((len(list_id)))-10
    wdiff_mat = np.zeros((len(list_id), len(list_id)))

    #Part related to real pair
    list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
    list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
    real_pair = list.to_numpy()
    #real_pair = [list.loc[it,:] for it in range( len (list)) ]
    check_real = np.sum(real_pair**2, axis = 1) #tip for finding if a couple is in the list

    for couple in result_id:
        RES = router.load_result_couple(*couple)
        mean_selec = compute_int_RES(RES, mouse.ti-30, mouse.ti-5)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.2:
            list_pre_saturated += (couple, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
            if np.isin(couple[0]**2+couple[1]**2, check_real):
                w_pre_saturated_real_after = np.append(w_pre_saturated_real_after, mean_after[0] )
                w_pre_saturated_real_before = np.append(w_pre_saturated_real_before, mean_before[0] )   
        else:
            list_pre_nosat += (couple, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
            if np.isin(couple[0]**2+couple[1]**2, check_real):
                w_pre_nosat_real_after = np.append(w_pre_nosat_real_after, mean_after[0] )
                w_pre_nosat_real_before = np.append(w_pre_nosat_real_before, mean_before[0] )

        #We look for the best match of each cluster
        wdiff_max[np.where(list_id == couple[0])] = np.max([wdiff_max[np.where(list_id == couple[0])][0], mean_after[0]-mean_before[0]])
        wdiff_max[np.where(list_id == couple[1])] = np.max([wdiff_max[np.where(list_id == couple[1])][0], mean_after[0]-mean_before[0]])
        wdiff_mat[np.where(list_id == couple[1])[0], np.where(list_id == couple[0])[0]] = mean_after[0]-mean_before[0]
        '''if np.isin(couple[0]**2+couple[1]**2, check_real):
            w_pre_real_before = np.append(w_pre_real_before, mean_before[0])
            w_pre_real_after = np.append(w_pre_real_after, mean_after[0])
        '''
    #print(wdiff_max)
    #print(f'{np.unique(list_pre_nosat, return_counts=True)}')
    #print(f'{np.unique(list_pre_saturated, return_counts=True))
    print(f'no sat: {len(list_pre_nosat)}, sat: {len(list_pre_saturated)}')
    striatum_list = mouse.get_cluster('striatum')
    print(f'len striatum:{len(striatum_list)}')
    cortex_list = mouse.get_cluster('cortex')
    print(f'len cortex:{len(cortex_list)}')
    print(f'n of real pair: {len(real_pair)/2}, {len(w_pre_nosat_real_after)} not sat and {len(w_pre_saturated_real_after)} sat')
    wdiff_striatum = wdiff_max[ np.isin(list_id, striatum_list) ]
    wdiff_cortex = wdiff_max[ np.isin(list_id, cortex_list) ]
    w_nosat_real_diff = w_pre_nosat_real_after - w_pre_nosat_real_before
    w_saturated_real_diff = w_pre_saturated_real_after - w_pre_saturated_real_before
    #plt.scatter(np.tile(np.arange(1,len(list_id)+1), len(list_id)), wdiff_mat.flatten())
    #plt.show()
    plt.boxplot([ w_pre_saturated_after-w_pre_saturated_before, w_pre_nosat_after-w_pre_nosat_before, wdiff_striatum, wdiff_cortex,
                  w_saturated_real_diff, w_nosat_real_diff], positions=[1, 1.5, 2, 2.5, 3, 3.5])
    plt.xticks([1,1.5, 2, 2.5, 3, 3.5], ['sat', 'no_sat', 'best match striatum', 'best match cortex', 'real_pair sat', 'real pair nosat' ])
    plt.title(f'{mouse.mouse_name}')

    print(scipy.stats.ttest_rel( w_pre_nosat_after-w_pre_nosat_before ,w_nosat_real_diff))

    plt.show()

def boxplot_cross_mouse_perfect(router: Router, mouse: Mouse, time_vect = None):
    """ Make boxplot of mean pre-weight before and after (time vect give that time) with some selection (saturation etc...)
    add a selection on perfect case, like nothing but LTP.
    """
    if time_vect == None:
        time_vect = np.array([mouse.ti-50, mouse.ti-10, mouse.tf, mouse.tf + 50])
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])

    w_pre_nosat_real_before = np.array([])
    w_pre_nosat_real_after = np.array([])
    w_pre_saturated_real_before = np.array([])
    w_pre_saturated_real_after = np.array([])
    result_id = router.get_couple_id()

    list_best = ()
    list_best_real = ()

    list_id = np.unique(result_id)
    wdiff_max = np.zeros((len(list_id)))-10
    wdiff_mat = np.zeros((len(list_id), len(list_id)))

    #Part related to real pair
    list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Spike_pairs_final.xlsx', sheet_name= f'{mouse.mouse_name}',header=2, usecols=[0,1])
    list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
    real_pair = list.to_numpy()
    #real_pair = [list.loc[it,:] for it in range( len (list)) ]
    check_real = np.sum(real_pair**2, axis = 1) #tip for finding if a couple is in the list

    for couple in result_id:
        RES = router.load_result_couple(*couple)
        mean_selec = compute_int_RES(RES, mouse.ti-30, mouse.ti-5)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])

        var_before = compute_int_RES_Var(RES, time_vect[0], time_vect[1])
        var_after = compute_int_RES(RES, time_vect[2], time_vect[3])

        if var_before[0]<2 and var_after[0]<2 and (mean_after[0]-mean_before[0])>1:
            list_best += (couple, )
            if np.isin(couple[0]**2+couple[1]**2, check_real):
                list_best_real += (couple,)
            RES = router.load_result_couple(*couple)
            '''plt.plot(RES[2], RES[0])
            plt.vlines([mouse.ti, mouse.tf], 0, 20, color = 'red')
            plt.hlines( [mean_before[0]], time_vect[0], time_vect[1], color = 'red')
            plt.hlines([mean_before[0]-var_before[0], mean_before[0]+var_before[0]], time_vect[0], time_vect[1], color = 'green')
            plt.hlines( [mean_after[0]], time_vect[2], time_vect[3], color = 'red')
            plt.hlines([mean_after[0]-var_after[0], mean_after[0]+var_after[0]], time_vect[2], time_vect[3], color = 'green')
            plt.show()
            '''

        '''        w_pre_saturated_real_after = np.append(w_pre_saturated_real_after, mean_after[0] )
                w_pre_saturated_real_before = np.append(w_pre_saturated_real_before, mean_before[0] )   
        else:
            list_pre_nosat += (couple, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
            if np.isin(couple[0]**2+couple[1]**2, check_real):
                w_pre_nosat_real_after = np.append(w_pre_nosat_real_after, mean_after[0] )
                w_pre_nosat_real_before = np.append(w_pre_nosat_real_before, mean_before[0] )
        '''
    #print(list_best)
    print(f'{mouse.mouse_name}___________________________________________________________\n')
    print(list_best)
    #print(f'{len(list_best_real)} are real over {len(list_best)} perfect')
    return 0


################################################ MAIN CALLABLE #########################################################

#from stdp.plot_center import plot_ID

def L_perfect():
        #V4606___________________________________________________________
    L_V4606 = ( (364, 736), (370, 690), (905, 730), (948, 15), (948, 675), (906, 730), (748, 61), (279, 12), (120, 675), (807, 12), (120, 656), (256, 649), (1013, 644), (365, 690), (471, 675), (637, 645), (748, 690), (806, 728), (471, 649))

    #V4607___________________________________________________________

    L_V4607 = ((906, 53), (887, 62), (928, 765), (927, 39), (926, 267), (927, 590), (926, 263), (832, 718), (867, 62), (927, 712), (900, 644))

    #Red___________________________________________________________

    L_Red = np.array([(558, 197), (561, 1672), (1474, 1690), (1474, 325), (594, 1795), (624, 1690),  (662, 393), (617, 109), (539, 320), (662, 743), (1474, 1590), (617, 743), (1474, 432), (539, 112), (685, 1430), (675, 1496),  (539, 131), (617, 184)])
    #(601, 230), (601, 49), (685, 395), (601, 1529), (617, 1483), (601, 938), (601, 724), (568, 268),(601, 32),(601, 440), 
    #V7265___________________________________________________________

    L_V7265 = ((1142, 1338), (378, 747), (263, 1312), (212, 1312), (320, 1381), (212, 982), (1045, 826), (1153, 138), (1045, 1426), (540, 1312), (529, 1338), (1045, 922), (529, 138), (540, 786), (506, 436), (320, 1426), (1045, 1352), (1149, 1388), (1153, 823), (1045, 158),  (1045, 1416))
    #(1190, 626), (261, 1005),(1190, 786), (304, 1381), (1190, 989), 
    #Mouse1___________________________________________________________

    L_Mouse1 = (  (445, 860), (378, 882), (391, 860), (424, 294), (526, 860), (391, 862), (466, 261), (1103, 273), (390, 1012), (498, 218), (445, 249),  (659, 296))
    #(445, 294), (1190, 640), (1101, 235), (391, 941), (417, 755), (445, 755), (391, 913), (424, 913), (1097, 273), (437, 1020), (420, 969), (1159, 913), (667, 24), (1097, 68), (1097, 969), (654, 969)
    return 0

def main1():
    router_red = Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    mouse_red = load_object(f'/home/ahubert/Documents/SpikeSorting/Red/data_mouse/Red_mouse')

    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f')
    mouse_V4606 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4606/data_mouse/V4606_mouse')

    router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f')
    mouse_V7265 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')

    router_Mouse1 = Router('490892905216247639','c289df2d94674edb9c16c64e32711432')
    mouse_Mouse1 = load_object(f'/home/ahubert/Documents/SpikeSorting/Mouse1/data_mouse/Mouse1_mouse')

    router_V4607 = Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    mouse_V4607 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4607/data_mouse/V4607_mouse')

    L_V7265 = ((1142, 1338), (378, 747), (263, 1312), (212, 1312), (320, 1381), (212, 982), (1045, 826), (1153, 138), (1045, 1426), (540, 1312), (529, 1338), (1045, 922), (529, 138), (540, 786), (506, 436), (320, 1426), (1045, 1352), (1149, 1388), (1153, 823), (1045, 158),  (1045, 1416))

    '''for couple in L_Red:
        plot_ID(router_red, mouse_red, couple)
    for couple in L_V4606:
        plot_ID(router_V4606, mouse_V4606, couple)'''
    
    for couple in L_V7265:
        plot_ID(router_V7265, mouse_V7265, couple, with_FR=True)

    '''for couple in L_Mouse1:
        plot_ID(router_Mouse1, mouse_Mouse1, couple)
    for couple in L_V4607:
        plot_ID(router_V4607, mouse_V4607, couple)'''

    '''boxplot_cross_mouse_perfect(router_V4606, mouse_V4606)

    boxplot_cross_mouse_perfect(router_V4607, mouse_V4607)

    boxplot_cross_mouse_perfect(router_red, mouse_red)

    boxplot_cross_mouse_perfect(router_V7265, mouse_V7265)

    boxplot_cross_mouse_perfect(router_Mouse1, mouse_Mouse1)'''
    
    return 0

'''router_Red = Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
mouse_Red = load_object(f'/home/ahubert/Documents/SpikeSorting/Red/data_mouse/Red_mouse')

router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f')
mouse_V4606 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4606/data_mouse/V4606_mouse')

router_V7265 = Router('490892905216247639','e4a16bbf69a64f3ca1cdc98a2ae69b6f')
mouse_V7265 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7265/data_mouse/V7265_mouse')

router_Mouse1 = Router('490892905216247639','c289df2d94674edb9c16c64e32711432')
mouse_Mouse1 = load_object(f'/home/ahubert/Documents/SpikeSorting/Mouse1/data_mouse/Mouse1_mouse')

router_V4607 = Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
mouse_V4607 = load_object(f'/home/ahubert/Documents/SpikeSorting/V4607/data_mouse/V4607_mouse')

router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924')
mouse_V7260 = load_object(f'/home/ahubert/Documents/SpikeSorting/V7260/data_mouse/V7260_mouse')
'''


#compare_wpre_tAG(router_V4607, mouse_V4607)


def detritus():
    name = 'Red'
    X = res_to_carCat(router_Red, mouse_Red)
    np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/Red_cat_res', X)
    X = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/data/Red_cat_res.npy')

    R = np.zeros((5,5))
    P = np.zeros((5,5))
    n_out = np.zeros((5,5))

    lab_names = ['wpre_nAG', 'wpre_tAG', 'wpre_tCa', 'wpre_Ca', 'wpre_tAGCa']
    for i in range(5):
        for j in range(5-i):
            selec = np.logical_or(X[:,j]==0, X[:,i]==0)
            X = X[np.invert(selec), :]
            L = scipy.stats.linregress(X[:, i],X[:,j])
            #err = X[:,j]-(X[:, i]*L[0]+L[1])
            #plt.hist(err, bins = 100)
            #plt.title(f'{lab_names[i]}, {lab_names[j]}')
            #plt.show()
            R[i,j] = np.mean(X[:, i]/X[:,j])
            P[i,j] = L.rvalue
            #n_out[i,j] = np.sum( (np.abs(X[:, i]/X[:,j]/R[i,j])-1) >0.01

    print(f'{name}')
    print(f'Ratio: \n {R}')
    print(f'Rvalue: \n {P}')
    #print(n_out)

    #[wpre_nAG, wpre_tAG, wpre_tCa, wpre_Ca, wpre_tAGCa]
    return 0



'''
plt.hist(X[:, 0], bins = 100, alpha = 0.5)
plt.hist(X[:, 0], bins = 20, alpha = 0.5)
plt.title('wpre_nAG')
plt.show()
plt.hist(X[:, 1], bins = 100, alpha = 0.5)
plt.hist(X[:, 1], bins = 20 , alpha = 0.5)
plt.title('wpre_tAG')
plt.show()
plt.hist(X[:, 2], bins = 100, alpha = 0.5)
plt.hist(X[:, 2], bins = 20 , alpha = 0.5)
plt.title('wpre_tCa')
plt.show()
plt.hist(X[:, 3], bins = 100, alpha = 0.5)
plt.hist(X[:, 3], bins = 20 , alpha = 0.5)
plt.title('wpre_Ca')
plt.show()
plt.hist(X[:, 4], bins = 100, alpha = 0.5)
plt.hist(X[:, 4], bins = 20 , alpha = 0.5)
plt.title('wpre_tAGCa')
plt.show()
'''

#Kmean_cat(X)


'''
for name in ['V7265', 'V4606', 'Red', 'Black', 'Blue', 'Mouse1']:
    mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')
    resume = pd.read_csv(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_resume.csv')
    times = pd.read_csv(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_spike_times.csv')

    print(np.sum(mouse.resume.Channel_ID != resume.Channel_ID))
    #print(np.sum(mouse.spike_times.Cluster_ID != times.Cluster_ID))
    print(np.sum((times.Spike_times - mouse.spike_times.Spike_times)>0.0001))

    n_false = 0
    for id in resume.Cluster_ID:
        #spikes = spikes_from_ID(id)
        spikes = times.Spike_times[times.Cluster_ID == id]
        spikes = spikes.to_numpy()
        #print(spikes[1:])
        #print(spikes[:-1])
        diff = spikes[1:]-spikes[:-1]
        #print(len(diff) - np.sum(diff >= 0))
        #print(f'mouse {name}, cluster {id}, n double is {np.sum(diff<0.0015)}')
        if np.sum(diff<0.002) > 100:
            n_false += 1
    print(n_false)

'''