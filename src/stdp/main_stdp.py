import logging
import os
import pathlib
import sys

print(sys.path)

from multiprocessing import Pool

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd

#import mlflow
from hydronaut.decorator import with_hydronaut
from hydronaut.hydra.omegaconf import get_container

#my own package
import stdp.integration
import stdp.synapseModel
import stdp.Analysis_spike
import stdp.VectorAccessor
import stdp.mlflow_utils
#import stdp.jitter_stats
import stdp.computation_center
import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from stdp.Class_Analysis import Router


@with_hydronaut()
def main(config=None):
    params = config.experiment.params

    for param_set in ('STDP', 'G_TRPV1'):
        for key, value in get_container(params, param_set, default={}).items():
            setattr(stdp.synapseModel, key, value)

    #for key, value in get_container(params, 'integration', default={}).items():
    #    setattr(stdp.integration, key, value)

    scipy_options = get_container(config, 'experiment.params.scipy_options', default={})

    #red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    #stdp.computation_center.compute_from_mouse_glumax_jitter(params, red_mouse, np.array([418, 554, 634, 155]))

    #V7 = Mouse(*stdp.create_mouse_data.prepare_V7265())
    #stdp.computation_center.compute_from_mouse_glumax_jitter(params, V7, np.array([4, 448, 1378]))

    #exp_id = '562231799100305614'
    #router = Router(exp_id, '2cd9ecadbf8f42ee9b344f84fb0ecd45')

    V6 = Mouse(*stdp.create_mouse_data.prepare_V4606())
    '''cluster_striatum = V4.get_cluster('striatum')
    cluster_striatum = set(cluster_striatum) - set(router.get_result_id_jitter())
    args = [(params, V4, np.array([clust]), np.array([63, 71, 222, 197, 962, 1034])) for clust in cluster_striatum ]
    '''
    stdp.computation_center.compute_from_mouse_glumax_nobaseline(params, V6, np.array([63, 71, 222, 197, 962, 1034]))

    '''
    striatum_list_V7 = [636,657,626,650,849,747,148,37,86,111,628,826,828,11,832,970,922,1434,1381,436,1352,1338,
    1312,1287,158,143,1337,1388,110,1416,108,982,95,50,1288,30,786,968,1005,1282,771,908,638]
    striatum_list_Red = [3, 12, 18, 30, 40, 44, 67, 112, 131, 145, 150, 181, 186, 197, 198, 201, 212, 230, 234, 239, 262, 266, 268, 270, 272,
    275, 281, 293, 303, 309, 312, 315, 320, 329, 350, 354, 366, 367, 371, 377, 380, 393, 403, 405, 406, 407, 410, 419, 424,
    434, 442, 723, 724, 743, 759, 880, 904, 918, 969, 1043, 1089, 1103, 1124, 1137, 1152, 1430, 1483, 1517, 1529, 1585, 1599,
    1625, 1630, 1651, 1669, 1671, 1672, 1674, 1690, 1694, 1701, 1709, 1713, 1716, 1755, 1756, 1799, 25, 32, 49, 109, 148,
    154, 162, 184, 218, 242, 274, 278, 325, 332, 351, 395, 400, 401, 408, 432, 435, 440, 706, 714, 737, 740, 753, 755, 827,
    843, 898, 920, 935, 938, 1105, 1140, 1254, 1335, 1369, 1428, 1435, 1496, 1590, 1597, 1658, 1661, 1673, 1682, 1698, 1714,
    1715, 1735, 1740, 1742, 1743, 1766, 1783, 1795, 1802, 150, 266]
    striatum_list_V4 = [649, 656, 12, 644, 645, 35, 61, 736, 738, 83, 86, 16, 25,
    684, 690, 36, 730, 15, 675, 53, 69, 81, 82]
    striatum_list_Red_sat = [3, 18, 30, 44, 112, 131, 150, 201, 234, 239, 262, 266, 270, 275, 281, 303, 309, 312, 315, 320, 350, 366,
                         367, 371, 405, 406, 407, 410, 419, 724, 743, 759, 880, 904, 918, 969, 1430, 1529, 1585, 1716, 25, 32, 49,
                         148, 184, 218, 274, 278, 332, 351, 395, 435, 440, 737, 843, 898, 920, 935, 938, 1105, 1254, 1435, 1698,
                         1714, 1766, 1783, 1795, 150, 266] #the one that had saturated
    striatum_list_Red_nosat = [12, 40, 67, 145, 181, 186, 197, 198, 212, 230, 268, 272, 293, 329, 354, 377, 380, 393, 403, 424, 434, 442, 723, 1043,
                         1089, 1103, 1124, 1137, 1152, 1483, 1517, 1599, 1625, 1630, 1651, 1669, 1671, 1672, 1674, 1690, 1694, 1701, 1709, 1713,
                         1755, 1756, 1799, 109, 154, 162, 242, 325, 400, 401, 408, 432, 706, 714, 740, 753, 755, 827, 1140, 1335, 1369, 1428, 1496,
                         1590, 1597, 1658, 1661, 1673, 1682, 1715, 1735, 1740, 1742, 1743, 1802] #the one not saturated

    V4606 = Mouse(*stdp.create_mouse_data.prepare_V4606())
    striatum_list_V4_sat = [656, 35, 736, 738, 83, 86, 15, 675, 53, 69, 81, 82]
    striatum_list_V4_nosat = [649, 12, 644, 645, 61, 16, 25, 684, 690, 36, 730]
    args_sat = [(params, V4606,  np.array([63, 71, 222, 197, 962, 1034]), 'sat', 0.1, [0,6], clust) for clust in striatum_list_V4_sat]
    args_nosat = [(params, V4606,  np.array([63, 71, 222, 197, 962, 1034]), 'nosat', 0.1, [0,6], clust) for clust in striatum_list_V4_nosat]

    #V7265 = Mouse(*stdp.create_mouse_data.prepare_V7265())
    
    #args = [(params, red_mouse,  np.array([418, 554, 634, 155]), clust) for clust in striatum_list_Red]
    #args = [(params, V4606,  np.array([63, 71, 222, 197, 962, 1034]), clust) for clust in striatum_list_V4]
    #stdp.computation_center.compute_from_mouse(*args[0])
    
    #args = [(params, V7265, V4606, np.array([4, 448, 1378]), np.array([63, 71]), clust) for clust in striatum_list_V4 ]
    #args = [(params, V7265,  np.array([4, 448, 1378]), clust) for clust in striatum_list_V7 ]
    
    with Pool(int(os.cpu_count()-2)) as pool:
        #pool.starmap(stdp.computation_center.compute_from_mouse, args)
        pool.starmap(stdp.computation_center.optim_Glumax, args_nosat)
    
    with Pool(int(os.cpu_count()-2)) as pool:
        #pool.starmap(stdp.computation_center.compute_from_mouse, args)
        pool.starmap(stdp.computation_center.optim_Glumax, args_sat)


    V7265 = Mouse(*stdp.create_mouse_data.prepare_V7265())
    striatum_list_V7_sat = [636, 657, 626, 747, 37, 628, 95, 50, 30, 786, 968, 908]
    striatum_list_V7_nosat = [650, 849, 148, 86, 111, 826, 828, 11, 832, 970, 922, 1434, 1381, 436, 1352, 1338, 1312, 1287, 158, 143, 1337, 1388, 110, 1416, 108, 982, 1288, 1005, 1282, 771, 638]
    args_sat = [(params, V7265,  np.array([4, 448, 1378]), 'sat', 0.06, [0,6], clust) for clust in striatum_list_V7_sat]
    args_nosat = [(params, V7265,  np.array([4, 448, 1378]), 'nosat', 0.06, [0,6], clust) for clust in striatum_list_V7_nosat]


    with Pool(int(os.cpu_count()-2)) as pool:
        #pool.starmap(stdp.computation_center.compute_from_mouse, args)
        pool.starmap(stdp.computation_center.optim_Glumax, args_nosat)
    
    with Pool(int(os.cpu_count()-2)) as pool:
        #pool.starmap(stdp.computation_center.compute_from_mouse, args)
        pool.starmap(stdp.computation_center.optim_Glumax, args_sat)


    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    args_sat = [(params, red_mouse, np.array([418, 554, 634, 155]), 'sat', 0.05, [0,6], clust) for clust in striatum_list_Red_sat]
    args_nosat = [(params, red_mouse, np.array([418, 554, 634, 155]), 'nosat', 0.05, [0,6], clust) for clust in striatum_list_Red_nosat]


    with Pool(int(os.cpu_count()-2)) as pool:
        #pool.starmap(stdp.computation_center.compute_from_mouse, args)
        pool.starmap(stdp.computation_center.optim_Glumax, args_nosat)
    
    with Pool(int(os.cpu_count()-2)) as pool:
        #pool.starmap(stdp.computation_center.compute_from_mouse, args)
        pool.starmap(stdp.computation_center.optim_Glumax, args_sat)
    '''

    #stdp.computation_center.compute_from_mouse(params, V7265, np.array([4, 448, 1378]))

    #stdp.computation_center.parallel_random_mouse(params, [0, 1100], 1, 1, 500)

    #print(config)
    
    #WRONG SIDE c_pre_list = [650, 849, 747, 148, 37, 86, 111, 628, 826, 828, 11, 832, 970, 922, 1434, 1381, 436, 1352, 1338, 1312, 1287, 158, 143, 1337, 1388, 110, 1416, 108 ,982]
    #WRONG SIDE c_post_list = [616, 1091 , 243, 411, 362, 1211, 208, 268, 1021, 196, 224, 1054, 320, 324, 457, 1132, 192, 1045, 378, 732, 400, 1135, 240, 255, 261, 279, 1149, 1148, 1142, 1123, 358, 1153, 685, 506, 540, 532, 529, 1190, 1248]
    
    #clust_list = stdp.Analysis_spike.pass_id_clust()
    #c_pre_list = clust_list[0]
    #c_post_list = clust_list[1]
    #c_pre_list = [1091,1211,362, 208,268,1021,196,224,1054,320]#,324,457,1132,192,1045,378,732,400,1135,240,255,261,279,1149,1148,1142,1123,358,1153,685,506,540,532,529,1190,1248, 239,684,243,411]
    #c_post_list = [650,849,747,148, 37,86,111,628,826,828]#,11,832,970,922,1434,1381,436,1352,1338,1312,1287,158,143,1337,1388,110,1416,108,982,636,626,657]
    
    #c_pre_list = [324,457,1132,192,1045,378,732,400,1135,240,255,261,279,1149,1148,1142,1123,358,1153,685,506,540,532,529,1190,1248,1091,1211,362, 208,268,1021,196,224,1054,320, 239,684,243,411]
    #c_post_list = [11,832,970,922,1434,1381,436,1352,1338,1312,1287,158,143,1337,1388,110,650,849,747,148, 37,86,111,628,826,828,1416,108,982,636,626,657]

    '''args = [(params, c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list ]
    with Pool(os.cpu_count()-2) as pool:
        pool.starmap(stdp.Analysis_spike.analyse3, args)'''
    
    #stdp.jitter_stats.apply_jitter(params, 650, 378)
#   args = [(params, np.arange(1, 100), np.arange(-40, 40)/1000+0.5/1000, freq, 50) for freq in np.arange(1, 21, 2)]

    '''args = [(params, np.arange(1, 100, 10), np.array([-120/freq, 120/freq]), freq, 50) for freq in np.arange(1, 21, 2)]
    with Pool(os.cpu_count()) as pool:
        pool.starmap(stdp.Analysis_spike.compute_map_V2, args)
    '''
    #RES = stdp.Analysis_spike.analyse2(params, 826, 268)
    #path_res = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/9cd83d3e24a24d89a1f497319dce8990/artifacts/'
    #RES_ref = np.load(f'{path_res}sol_1287_685.npy')
    #print(np.min(np.abs((RES[3])), axis = 1))
    #print(np.min(np.abs((RES_ref)), axis = 1))

    
    '''y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])
    
    t_pre = np.arange(80)
    t_post = t_pre - 0.015
    RES = stdp.integration.integration(params, t_pre, t_post, y0init, t_end = 50)
    print(np.min(np.abs((RES[3])), axis = 1))
    '''
    return 0


def configure_logging(level=logging.INFO):
    '''Configure logging'''
    logging.basicConfig(
        style='{',
        format='[{asctime}] {levelname} {message}',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=level
    )
    logging.getLogger('matplotlib').setLevel(logging.INFO)
    logging.getLogger('PIL').setLevel(logging.WARNING)

if __name__ == '__main__':
    configure_logging(level=logging.DEBUG)
    main()
