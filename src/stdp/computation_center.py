import logging
import pathlib
import gc
#from pathlib import Path

from multiprocessing import Pool

import numpy as np
import pandas as pd
import pathlib
import tslearn
from tslearn.metrics import dtw, cdist_dtw
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

import matplotlib.pyplot as plt
import matplotlib as mpl

import mlflow

import stdp.integration
import stdp.integration_sat
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot
import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from computation_tools import *
from Class_optiGlu import optiGlu
from stdp.Class_Analysis import Router
import stdp.density_stdp as dens

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)

y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])

if __name__ == "__main__":
    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    V4606_mouse = Mouse(*stdp.create_mouse_data.prepare_V4606())
    V7265_mouse = Mouse(*stdp.create_mouse_data.prepare_V7265())
    exp_id = '562231799100305614'

def compute_from_ID(params, mouse: Mouse, clust_pre: int, clust_post: int, computation_options: dict = None):
    '''
    2500 is the frequence of acquisition, ceil done to select on lfp times.

    Args:
        params:
            params to pass for integration
        
        mouse:
            mouse class

        clust_pre, clust_post:
            respectively the ID of cluster to consider as presynaptic and postsynaptic neuron.

        lfp_modulation: False(default) or True
            active or not lfp modulation

        glu_func:
            if lfp_modulation is True, the function that gives glutamate release from lfp vector values
    '''
    if computation_options is None:
         computation_options = {}
    '''lfp_modulation = computation_options.get('lfp_modulation', False)
    glu_func = computation_options.get('glu_func', None)

    lfp_freq = 2500
    '''
    cortex_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust_pre]
    cortex_times = cortex_times[cortex_times > (mouse.ti-300)]
    cortex_times = cortex_times[cortex_times < (mouse.tf+300)]
    #spike_times_vect = np.ceil(cortex_times*lfp_freq).to_numpy().astype(int)
    #glu_array = 10*np.ones(len(cortex_times))
    #mean_trace = np.mean(mouse.get_LFP()[:, mouse.brain_region[1]], axis = 1)
    #lfp_array = mean_trace[spike_times_vect]
    #glu_array[lfp_array < - 100] = -2.703*lfp_array[lfp_array < -100] + 730
    #glu_array[lfp_array < - 100] = -4.054*lfp_array[lfp_array < -100] + 94.5
    #glu_array[lfp_array < - 100] = -5.135*lfp_array[lfp_array < -100] - 413.5
    #glu_array[lfp_array < - 120] = -5.6857*lfp_array[lfp_array < -120] - 672
    #glu_array = glu_func(lfp_array)
    striatum_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust_post]
    striatum_times = striatum_times[striatum_times > (mouse.ti-300)]
    striatum_times = striatum_times[striatum_times < (mouse.tf+300)]
    LOGGER.info('Computing  res_%s_%s', clust_pre, clust_post)
    RES = stdp.integration.integration(params, cortex_times, striatum_times, y0init, t_end = 20)#, glu_release = glu_array)#, scipy_options= {'method': 'Radau'})

    for prefix, data in (
        #('Ca_', RES[13])):
        ('res_', RES[0:3]),
        ('sol_', RES[3][24:27])):
            name = f'{prefix}{clust_pre}_{clust_post}'
            log_numpy_data(data, name)
    LOGGER.info('Saved  res_%s_%s', clust_pre, clust_post)
    
    del RES
    del striatum_times
    del cortex_times
    gc.collect()
    return 0

def compute_from_ID_sat(params, mouse: Mouse, clust_pre: int, clust_post: int, time_baseline = 300, computation_options: dict = None):
    '''
    2500 is the frequence of acquisition, ceil done to select on lfp times.

    Args:
        params:
            params to pass for integration
        
        mouse:
            mouse class

        clust_pre, clust_post:
            respectively the ID of cluster to consider as presynaptic and postsynaptic neuron.

        lfp_modulation: False(default) or True
            active or not lfp modulation

        glu_func:
            if lfp_modulation is True, the function that gives glutamate release from lfp vector values
    '''
    if computation_options is None:
         computation_options = {}
    '''lfp_modulation = computation_options.get('lfp_modulation', False)
    glu_func = computation_options.get('glu_func', None)

    lfp_freq = 2500
    '''
    cortex_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust_pre]
    cortex_times = cortex_times[cortex_times > (mouse.ti-time_baseline)]
    cortex_times = cortex_times[cortex_times < (mouse.tf+300)]
    #spike_times_vect = np.ceil(cortex_times*lfp_freq).to_numpy().astype(int)
    #glu_array = 10*np.ones(len(cortex_times))
    #mean_trace = np.mean(mouse.get_LFP()[:, mouse.brain_region[1]], axis = 1)
    #lfp_array = mean_trace[spike_times_vect]
    #glu_array[lfp_array < - 100] = -2.703*lfp_array[lfp_array < -100] + 730
    #glu_array[lfp_array < - 100] = -4.054*lfp_array[lfp_array < -100] + 94.5
    #glu_array[lfp_array < - 100] = -5.135*lfp_array[lfp_array < -100] - 413.5
    #glu_array[lfp_array < - 120] = -5.6857*lfp_array[lfp_array < -120] - 672
    #glu_array = glu_func(lfp_array)
    striatum_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust_post]
    striatum_times = striatum_times[striatum_times > (mouse.ti-time_baseline)]
    striatum_times = striatum_times[striatum_times < (mouse.tf+300)]
    LOGGER.info('Computing  res_%s_%s', clust_pre, clust_post)
    RES = stdp.integration_sat.integration(params, cortex_times, striatum_times, y0init, t_end = 20)#, glu_release = glu_array)#, scipy_options= {'method': 'Radau'})

    if len(RES) == 6:
        if RES[5] == 1:
            nmda_vect = np.concatenate((RES[0] , [-1] ) )
            ecb_vect = np.concatenate((RES[1] , [-1] ) )
            time_vect = np.concatenate(( RES[2] , [mouse.tf + 299]))
            RES_tosave = (nmda_vect, ecb_vect, time_vect)
    else:
        RES_tosave = RES[0:3]
    for prefix, data in (
        #('Ca_', RES[13])):
        ('res_', RES_tosave),
        ('sol_', RES[3][24:27])):
            name = f'{prefix}{clust_pre}_{clust_post}'
            log_numpy_data(data, name)
    LOGGER.info('Saved  res_%s_%s', clust_pre, clust_post)
    
    del RES
    del striatum_times
    del cortex_times
    gc.collect()
    return 0

def compute_from_mouse(params, mouse: Mouse, interneurons_list, clust, computation_options: dict = None):

    #valid_clust = mouse.resume.Cluster_ID[np.invert(mouse.resume.Cluster_ID.isin(interneurons_list))]
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID.isin(mouse.brain_region[1])]

    #cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID < 58]
    #cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, interneurons_list))]
    #striatum_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID < 181]

    #for clust in cluster_striatum[10:]:
    striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
    #striatum_spikes = np.array([])
    LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')
    RES = stdp.integration.integration(params, cortex_spikes, striatum_spikes, t_end = 100, y0init = y0init )

    for prefix, data in (
    ('res_', RES[0:3]),
    ('sol_', RES[3])):
        name = f'{prefix}{mouse.mouse_name}_{int(clust)}'
        log_numpy_data(data, name)

    return 0

def compute_cross_mouse(params, mouse_cortex, mouse_striatum, interneurons_cortex, interneurons_striatum, clust):
    ''' Cross spike of cortex of mouse_cortex with neurons of striatum from mouse_striatum
     Args:
        mouse_cortex:
            the mouse to take cortex spikes from.
        mouse_striatum:
            the mouse to take striatum neurons from.
        interneurons_cortex/striatum:
            neurons considered as interneurons that wont be used.
        clust_striatum:
        cluster to take as striatum
        
    '''
    #valid_clust = mouse_cortex.resume.Cluster_ID[np.invert(mouse_cortex.resume.Cluster_ID.isin(interneurons_cortex))]
    pyramidal_spikes = mouse_cortex.spike_times[np.invert(mouse_cortex.spike_times.Cluster_ID.isin(interneurons_cortex))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID > 199]

    cluster_striatum = mouse_striatum.resume.Cluster_ID[mouse_striatum.resume.Channel_ID < 58]
    cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, interneurons_striatum))]
    #striatum_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID < 181]

    #for clust in cluster_striatum:
    striatum_spikes = mouse_striatum.spike_times.Spike_times[mouse_striatum.spike_times.Cluster_ID == clust]

    RES = stdp.integration.integration(params, cortex_spikes, striatum_spikes, t_end = 100, y0init = y0init )

    for prefix, data in (
        ('res_', RES[0:3]),
        ('sol_', RES[3])):
        name = f'{prefix}V7265_V4606_{int(clust)}'
        log_numpy_data(data, name)
        
    return 0

def select_cortex_attr(mouse: Mouse, attr: str):
    '''
    access some spike_times attribute selected on cortex region

    Args:
        mouse:
            mouse object to get attr on

        attr:
            wich attr to get
    '''
    l = getattr(mouse, attr)
    return l[mouse.spike_times.channel.isin( mouse.brain_region[1] ) ]

def select_striatum_attr(mouse: Mouse, attr: str):
    '''
    access some spike_times attribute selected on cortex region

    Args:
        mouse:
            mouse object to get attr on

        attr:
            wich attr to get
    '''
    l = getattr(mouse, attr)
    return l[mouse.spike_times.channel.isin( mouse.brain_region[0] ) ]

def compute_all_cross(params, mouse: Mouse, computation_options: dict):
    '''
    compute all cross match
    Args:
        params:
            parameter for model, used in stdp.synapseModel
        
        computation_options:
            dict of args to pass to compute_from_ID
    '''
    cluster_cortex = select_cortex_attr(mouse, 'Cluster_ID')
    cluster_striatum = select_striatum_attr(mouse, 'Cluster_ID')
    args = [(params, mouse, c_pre, c_post, computation_options) for c_pre in cluster_cortex for c_post in cluster_striatum ]
    with Pool(os.cpu_count()-2) as pool:
        pool.starmap(compute_from_ID, args)

def compute_cross_match(params, mouse: Mouse, cluster_cortex, cluster_striatum):#, computation_options):
    '''
    compute cross match from 2 given list
    Args:
        params:
            parameter for model, used in stdp.synapseModel
        
        cluster_cortex/striatum:
            list of cluster_ID that gonna be considered as Cortex/Striatum neuron in the model

        computation_options:
            dict of args to pass to compute_from_ID. IS UNPACKED WITH * SO MUST BE WELL ORDERED DEPENDING ON "compute_from_ID" func
    '''
    args = [(params, mouse, c_pre, c_post) for c_pre in cluster_cortex for c_post in cluster_striatum ]
    
    list_pair = np.array([(c_pre, c_post) for c_pre in cluster_cortex for c_post in cluster_striatum])
    Router_exp = Router('490892905216247639','c22b39c7b56b4e54ad2e34582d80430d')
    list_computed = Router_exp.get_couple_id()
    print(list_computed)
    print(len(list_computed))
    new_args = ()
    it = 0
    for couple in args:
        drop = False
        for computed in list_computed:
            if (couple[2],couple[3]) == computed:
                drop = True
                print(f'{(couple[2],couple[3])} already computed')
        if drop == False:
            new_args += (couple,)

    with Pool(int(os.cpu_count()-6)) as pool:
        pool.starmap(compute_from_ID, new_args)

def compute_cross_match_sat(params, mouse: Mouse, cluster_cortex, cluster_striatum, time_baseline = 300):#, computation_options):
    '''
    compute cross match from 2 given list
    Args:
        params:
            parameter for model, used in stdp.synapseModel
        
        cluster_cortex/striatum:
            list of cluster_ID that gonna be considered as Cortex/Striatum neuron in the model

        computation_options:
            dict of args to pass to compute_from_ID. IS UNPACKED WITH * SO MUST BE WELL ORDERED DEPENDING ON "compute_from_ID" func
    '''
    args = [(params, mouse, c_pre, c_post, time_baseline) for c_pre in cluster_cortex for c_post in cluster_striatum ]
    
    list_pair = np.array([(c_pre, c_post) for c_pre in cluster_cortex for c_post in cluster_striatum])
    Router_exp = Router('490892905216247639','c22b39c7b56b4e54ad2e34582d80430d')
    list_computed = Router_exp.get_couple_id()
    print(list_computed)
    print(len(list_computed))
    new_args = ()
    it = 0
    for couple in args:
        drop = False
        for computed in list_computed:
            if (couple[2],couple[3]) == computed:
                drop = True
                print(f'{(couple[2],couple[3])} already computed')
        if drop == False:
            new_args += (couple,)
    log_numpy_data(np.array([time_baseline]), 'time_baseline')
    with Pool(int(os.cpu_count()-4)) as pool:
        pool.starmap(compute_from_ID_sat, args)

def parallel_random_mouse(params, t_bounds, fr_cortex, fr_striatum, nrep):
    args = [(params, t_bounds, fr_cortex, fr_striatum, it_name) for it_name in range(nrep)]
    with Pool(os.cpu_count()-4) as pool:
        pool.starmap(random_mouse, args)
    return 'nothing'

def random_mouse(params, t_bounds, fr_cortex, fr_striatum, it_name):
    cortex_times = np.random.uniform(t_bounds[0], t_bounds[1], np.random.poisson((t_bounds[1]-t_bounds[0])*fr_cortex))
    striatum_times = np.random.uniform(t_bounds[0], t_bounds[1], np.random.poisson((t_bounds[1]-t_bounds[0])*fr_striatum))
    RES = stdp.integration.integration(params, cortex_times, striatum_times, y0init, t_end = 100)#, glu_release = glu_array)
    
    for prefix, data in (
        ('res_', RES[0:3]),
        ('sol_', RES[3])):
            name = f'{prefix}{it_name}'
            log_numpy_data(data, name)
            LOGGER.info(f'{it_name} done')
    return RES

def selec_subclust(mouse: Mouse, exp_id, run_id):
        
    list_pre_saturated = ( )
    list_pre_nosat = ( )
    w_pre_saturated_after = np.array([])
    w_pre_nosat_after = np.array([])
    w_pre_saturated_before = np.array([])
    w_pre_nosat_before = np.array([])
    #time_vect = [V4606_mouse.ti-60, V4606_mouse.ti-10, V4606_mouse.tf, V4606_mouse.tf + 50]
    #time_vect = [red_mouse.ti-60, red_mouse.ti-10, red_mouse.tf, red_mouse.tf + 50]
    time_vect = [mouse.ti-60, mouse.ti-10, mouse.tf, mouse.tf + 50]

    #4a403505ce2f4de994ee75ffbaa97ad2 #complete Red
    #4fe67572c10c401e924a289af1ef4bf2 #complete V4606
    #d7792f0df3e84581b6a9293a363a4440 #complete V7
    
    #ede5a3d11f0f4da3b88e2fa939476afe in FACT V4606!!
    striatum_list = [3, 12, 18, 30, 40, 44, 67, 112, 131, 145, 150, 181, 186, 197, 198, 201, 212, 230, 234, 239, 262, 266, 268, 270, 272,
        275, 281, 293, 303, 309, 312, 315, 320, 329, 350, 354, 366, 367, 371, 377, 380, 393, 403, 405, 406, 407, 410, 419, 424,
        434, 442, 723, 724, 743, 759, 880, 904, 918, 969, 1043, 1089, 1103, 1124, 1137, 1152, 1430, 1483, 1517, 1529, 1585, 1599,
        1625, 1630, 1651, 1669, 1671, 1672, 1674, 1690, 1694, 1701, 1709, 1713, 1716, 1755, 1756, 1799, 25, 32, 49, 109, 148,
        154, 162, 184, 218, 242, 274, 278, 325, 332, 351, 395, 400, 401, 408, 432, 435, 440, 706, 714, 737, 740, 753, 755, 827,
        843, 898, 920, 935, 938, 1105, 1140, 1254, 1335, 1369, 1428, 1435, 1496, 1590, 1597, 1658, 1661, 1673, 1682, 1698, 1714,
        1715, 1735, 1740, 1742, 1743, 1766, 1783, 1795, 1802, 150, 266] 
    
    for clust in striatum_list:
        RES = np.load(f'/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/{exp_id}/{run_id}/artifacts/res_{mouse.mouse_name}_{clust}.npy') #0
        #RES = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/0b3850a9af1d430db75e6f230875209b/artifacts/res_V7265.npy') #1
        #RES = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/8019d89ae2b54d8a98d5001e233e3409/artifacts/res_V7265.npy') #5
        #RES2 = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/1f2f0f1389af4cc4a962952360cea477/artifacts/res_V7265.npy')

        mean_selec = compute_int_RES(RES, mouse.ti-350, mouse.ti-10)[1]
        mean_before = compute_int_RES(RES, time_vect[0], time_vect[1])
        mean_after = compute_int_RES(RES, time_vect[2], time_vect[3])
        if mean_selec > 2.6:
            list_pre_saturated += (clust, )
            w_pre_saturated_after = np.append(w_pre_saturated_after, mean_after[0] )
            w_pre_saturated_before = np.append(w_pre_saturated_before, mean_before[0] )
        else:
            list_pre_nosat += (clust, )
            w_pre_nosat_after = np.append(w_pre_nosat_after, mean_after[0] )
            w_pre_nosat_before = np.append(w_pre_nosat_before, mean_before[0] )
    print(list_pre_saturated)
    print(list_pre_nosat)
    
    return 0

def optim_Glumax(params, mouse: Mouse, interneurons_list, res_init, Glumax_init, Glumax_win, clust):
    path = pathlib.Path(f'/home/ahubert/Documents/Code_STDP/model-potcadep/optim_Glumax/{mouse.mouse_name}/{clust}')
    pathlib.Path.mkdir(path, parents=True, exist_ok=True )
    tol = 0.005
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    t_pre = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID.isin(mouse.brain_region[1])]
    t_post = mouse.spikes_from_ID(int(clust))

    t_pre = t_pre[t_pre < mouse.ti]
    t_post = t_post[t_post < mouse.ti]

    Glumax_curr = Glumax_init
    Glu_mem = np.zeros(9)
    Glu_mem[0] = Glumax_init
    it = 0

    if it == 0:
        if res_init == 'sat':
            Glumax_win[1] = Glumax_init
        elif res_init == 'nosat' :
            Glumax_win[0] = Glumax_init

        Glumax_curr = np.mean(Glumax_win)
        #setattr(stdp.synapseModel, 'Glumax', Glumax_curr)
        it += 1
        Glu_mem[it] = Glumax_curr

    while it < 8 and (np.abs(Glu_mem[it]-Glu_mem[it-1]) > tol) :
        LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')
        RES = stdp.integration.integration(params, Glumax_curr, t_pre, t_post, t_end = 100, y0init = y0init )
        LOGGER.info(f'{mouse.mouse_name}_{int(clust)} done')
        RES = RES[0:3]
        np.save(f'/home/ahubert/Documents/Code_STDP/model-potcadep/optim_Glumax/{mouse.mouse_name}/{clust}/{it}_res_{clust}.npy', RES)

        mean_selec = compute_int_RES(RES, mouse.ti-200, mouse.ti-10)[1]
        if mean_selec > 2:
            Glumax_win[1] = Glumax_curr
        else:
            Glumax_win[0] = Glumax_curr

        Glumax_curr = np.mean(Glumax_win)
        #setattr(stdp.synapseModel, 'Glumax', Glumax_curr)
        it = it+1
        Glu_mem[it] = Glumax_curr #for next step.
    
    np.save(f'/home/ahubert/Documents/Code_STDP/model-potcadep/optim_Glumax/{mouse.mouse_name}/{clust}/Glu_mem', Glu_mem)

    return Glu_mem

def compute_from_mouse_glumax(params, mouse: Mouse, interneurons_list, computation_options: dict = None):

    #valid_clust = mouse.resume.Cluster_ID[np.invert(mouse.resume.Cluster_ID.isin(interneurons_list))]
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID.isin(mouse.brain_region[1])]

    cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[0]) ]
    cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, interneurons_list))]
    #striatum_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID < 181]
    optiGlu_mouse = optiGlu(mouse)
    dict_glumax = optiGlu_mouse.get_optim_values()
    Glumem = np.zeros((2,len(cluster_striatum)))
    it = 0

    if mouse.mouse_name == 'Red':
        exp_id = '562231799100305614'
        router = Router(exp_id, 'fda4f8868f034997978359425f5dac10')
        computed = router.get_result_id()
        cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, computed))]
        router = Router(exp_id, 'bf6ecf6efaca4e4b8c60a685b1ad12dc')
        computed = router.get_result_id()
        cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, computed))]

    for clust in cluster_striatum:
        striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
        Glumax_curr = dict_glumax[f'{int(clust)}']
        Glumax_curr = Glumax_curr[Glumax_curr > 0]
        glu0, glu1 = Glumax_curr[-2:]
        if glu0 < glu1:
            Glumax_curr = glu0
        elif glu0 > glu1:
            Glumax_curr = np.max(glu1-0.008, 0)
        LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')
        RES = stdp.integration.integration(params, Glumax_curr, cortex_spikes, striatum_spikes, t_end = 100, y0init = y0init )

        for prefix, data in (
        ('res_', RES[0:3]),
        ('sol_', RES[3])):
            name = f'{prefix}{mouse.mouse_name}_{int(clust)}'
            log_numpy_data(data, name)
        Glumem[1, it] = Glumax_curr
        Glumem[0, it] = int(clust)
        it = it + 1
    log_numpy_data(Glumem, 'Glumem')
    return 0

def compute_from_mouse_glumax_nobaseline(params, mouse: Mouse, interneurons_list, computation_options: dict = None):
    '''
    Same as compute_from_mouse_glumax but start from ti, the contact time.
    '''
    #valid_clust = mouse.resume.Cluster_ID[np.invert(mouse.resume.Cluster_ID.isin(interneurons_list))]
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID.isin(mouse.brain_region[1])]
    cortex_spikes = cortex_spikes[cortex_spikes > (mouse.tf-2)]
    cortex_spikes = cortex_spikes[cortex_spikes < (mouse.tf+50)]
    cluster_striatum = mouse.resume.Cluster_ID[mouse.resume.Channel_ID.isin(mouse.brain_region[0]) ]
    cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, interneurons_list))]
    #striatum_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID < 181]
    optiGlu_mouse = optiGlu(mouse)
    dict_glumax = optiGlu_mouse.get_optim_values()
    Glumem = np.zeros((2,len(cluster_striatum)))
    it = 0

    if mouse.mouse_name == 'Red':
        exp_id = '562231799100305614'
        router = Router(exp_id, 'fda4f8868f034997978359425f5dac10')
        computed = router.get_result_id()
        cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, computed))]
        router = Router(exp_id, 'bf6ecf6efaca4e4b8c60a685b1ad12dc')
        computed = router.get_result_id()
        cluster_striatum = cluster_striatum[np.invert(np.isin(cluster_striatum, computed))]

    for clust in cluster_striatum:
        striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
        striatum_spikes = striatum_spikes[striatum_spikes > (mouse.tf-2)]
        striatum_spikes = striatum_spikes[striatum_spikes < (mouse.tf+50)]
        Glumax_curr = dict_glumax[f'{int(clust)}']
        Glumax_curr = Glumax_curr[Glumax_curr > 0]
        glu0, glu1 = Glumax_curr[-2:]
        if glu0 < glu1:
            Glumax_curr = glu0
        elif glu0 > glu1:
            Glumax_curr = np.max(glu1-0.008, 0)
        LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')
        RES = stdp.integration.integration(params, Glumax_curr, cortex_spikes, striatum_spikes, t_end = 100, y0init = y0init )

        for prefix, data in (
        ('res_', RES[0:3]),
        ('sol_', RES[3])):
            name = f'{prefix}{mouse.mouse_name}_{int(clust)}'
            log_numpy_data(data, name)
        Glumem[1, it] = Glumax_curr
        Glumem[0, it] = int(clust)
        it = it + 1
    log_numpy_data(Glumem, 'Glumem')
    return 0

def compute_from_mouse_glumax_jitter(params, mouse: Mouse, cluster_striatum, interneurons_list, computation_options: dict = None):

    #valid_clust = mouse.resume.Cluster_ID[np.invert(mouse.resume.Cluster_ID.isin(interneurons_list))]
    pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
    cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID.isin(mouse.brain_region[1])]

    optiGlu_mouse = optiGlu(mouse)
    dict_glumax = optiGlu_mouse.get_optim_values()
    Glumem = np.zeros((2,len(cluster_striatum)))
    it = 0

    for clust in cluster_striatum:
        striatum_spikes = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust]
        Glumax_curr = dict_glumax[f'{int(clust)}']
        Glumax_curr = Glumax_curr[Glumax_curr > 0]
        glu0, glu1 = Glumax_curr[-2:]
        if glu0 < glu1:
            Glumax_curr = glu0
        elif glu0 > glu1:
            Glumax_curr = np.max(glu1-0.008, 0)
        mat_striatum_spikes = random_jitter(striatum_spikes, [0, 0.015], n_it = 4)
        for it in range(4):
            striatum_spikes = mat_striatum_spikes[it, :]
            LOGGER.info(f' computing {mouse.mouse_name}_{int(clust)}')
            RES = stdp.integration.integration(params, Glumax_curr, cortex_spikes, striatum_spikes, t_end = 100, y0init = y0init )

            for prefix, data in (
            ('res_', RES[0:3]),
            ('sol_', RES[3])):
                name = f'{prefix}{mouse.mouse_name}_{int(clust)}_it{int(it)}'
                log_numpy_data(data, name)
        log_numpy_data(mat_striatum_spikes, f'mat_jitter_{mouse.mouse_name}_{int(clust)}')
        Glumem[1, it] = Glumax_curr
        Glumem[0, it] = int(clust)
        it = it + 1
    log_numpy_data(Glumem, f'Glumem_{int(cluster_striatum)}')
    return 0

def compute_jump_density(router: Router, mouse: Mouse):
    jump_list = ()
    for id in router.get_result_id():
        RES = router.load_result(f'{id}', mouse.mouse_name)
        #print( type(dens.is_jump(RES[2], RES[0])) )
        jump_list = jump_list + (*dens.is_jump(RES[2], RES[0]),)
    plt.hist([jump.height for jump in [jump_clust for jump_clust in jump_list]], density=True, bins = 50, alpha = 0.5, label=f'{mouse.mouse_name}')
    return(jump_list)

def compute_jump_density_contact(router: Router, mouse: Mouse):
    jump_list_baseline = ()
    jump_list_contact = ()
    for id in router.get_result_id():
        RES = router.load_result(f'{id}', mouse.mouse_name)
        RES_baseline = RES[:, RES[2]<mouse.ti]
        RES_contact = RES[:, np.logical_and(RES[2]>mouse.ti, RES[2]<mouse.tf)]
        #print( type(dens.is_jump(RES[2], RES[0])) )
        jump_list_baseline = jump_list_baseline + (*dens.is_jump(RES_baseline[2], RES_baseline[0]),)
        jump_list_contact = jump_list_contact + (*dens.is_jump(RES_contact[2], RES_contact[0]),)
    plt.hist([jump.height for jump in [jump_clust for jump_clust in jump_list_baseline]],  bins = 50, alpha = 0.5, label = 'baseline')
    plt.hist([jump.height for jump in [jump_clust for jump_clust in jump_list_contact]],  bins = 50, alpha = 0.5, label = 'contact')
    plt.legend()
    plt.show()
    return 0

def fit_jump_density(router: Router, mouse: Mouse):
    jump_list = ()
    for id in router.get_result_id():
        RES = router.load_result(f'{id}', mouse.mouse_name)
        jump_list = jump_list + (*dens.is_jump(RES[2], RES[0]),)
        jump_height = [jump.height for jump in [jump_clust for jump_clust in jump_list]]
        res = scipy.stats.fit(scipy.stats.gamma, jump_height, bounds=[(0, 20),(0, 20)])

    return res

def compute_real_pair():
    list = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Red/Spike_pairs_final.xlsx', header=2, usecols=[0,1])
    list.columns = ['Striatal_Cluster_ID', 'Cortical_Cluster_ID']
    #print(list.loc[0,:]+list.loc[,:])
    real_pair = [list.loc[it,:] for it in range( len (list)) ]
    return 0

def compute_sat_nmda_ID(params, mouse: Mouse, clust_pre: int, clust_post: int, computation_options: dict = None):
    '''
    2500 is the frequence of acquisition, ceil done to select on lfp times.

    Args:
        params:
            params to pass for integration
        
        mouse:
            mouse class

        clust_pre, clust_post:
            respectively the ID of cluster to consider as presynaptic and postsynaptic neuron.

        lfp_modulation: False(default) or True
            active or not lfp modulation

        glu_func:
            if lfp_modulation is True, the function that gives glutamate release from lfp vector values
    '''
    if computation_options is None:
         computation_options = {}
    '''lfp_modulation = computation_options.get('lfp_modulation', False)
    glu_func = computation_options.get('glu_func', None)

    lfp_freq = 2500
    '''
    
    t_len = 10
    cortex_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust_pre]
    striatum_times = mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID == clust_post]
    sat_mouse = np.zeros(int((mouse.tf - mouse.ti + 320)/t_len))
    LOGGER.info('Computing  res_%s_%s', clust_pre, clust_post)
    for k in range(int((mouse.tf - mouse.ti + 60)/t_len)):

        sub_cortex_times = cortex_times[cortex_times > (mouse.ti-20+t_len*k)]
        sub_cortex_times = sub_cortex_times[sub_cortex_times < (mouse.ti-20+t_len*(k+1))]
        sub_striatum_times = striatum_times[striatum_times > (mouse.ti-20+t_len*k)]
        sub_striatum_times = sub_striatum_times[sub_striatum_times < (mouse.tf-20+t_len*(k+1))]
        #spike_times_vect = np.ceil(cortex_times*lfp_freq).to_numpy().astype(int)
        #glu_array = 10*np.ones(len(cortex_times))
        #mean_trace = np.mean(mouse.get_LFP()[:, mouse.brain_region[1]], axis = 1)
        #lfp_array = mean_trace[spike_times_vect]
        #glu_array[lfp_array < - 100] = -2.703*lfp_array[lfp_array < -100] + 730
        #glu_array[lfp_array < - 100] = -4.054*lfp_array[lfp_array < -100] + 94.5
        #glu_array[lfp_array < - 100] = -5.135*lfp_array[lfp_array < -100] - 413.5
        #glu_array[lfp_array < - 120] = -5.6857*lfp_array[lfp_array < -120] - 672
        #glu_array = glu_func(lfp_array)

        if len(sub_cortex_times) >= 0 or len(sub_striatum_times) >= 0:
            RES = stdp.integration.integration(params, sub_cortex_times, sub_striatum_times, y0init, t_end = 0)#, glu_release = glu_array)#, scipy_options= {'method': 'Radau'})
            if compute_int_RES(RES, mouse.ti-20+t_len*k+5, mouse.ti-20+t_len*k+10)[1]>2 :
                sat_mouse[k] = 1

    name = f'satnmda_{clust_pre}_{clust_post}'
    log_numpy_data(sat_mouse, name)
    LOGGER.info('Saved  res_%s_%s', clust_pre, clust_post)

    del RES
    del striatum_times
    del cortex_times
    gc.collect()
    
    return sat_mouse

def test_sat_nmda(params, mouse: Mouse, cluster_cortex, cluster_striatum):#, computation_options):
    '''
    compute cross match from 2 given list
    Args:
        params:
            parameter for model, used in stdp.synapseModel
        
        cluster_cortex/striatum:
            list of cluster_ID that gonna be considered as Cortex/Striatum neuron in the model

        computation_options:
            dict of args to pass to compute_from_ID. IS UNPACKED WITH * SO MUST BE WELL ORDERED DEPENDING ON "compute_from_ID" func
    '''
    args = [(params, mouse, c_pre, c_post) for c_pre in cluster_cortex for c_post in cluster_striatum ]
    
    list_pair = np.array([(c_pre, c_post) for c_pre in cluster_cortex for c_post in cluster_striatum])
    
    with Pool(int(os.cpu_count()-4)) as pool:
        pool.starmap(compute_sat_nmda_ID, args)

#router = Router(exp_id, '13b8924031bd48bd9d99e53140373753')
#print(fit_jump_density(router, V7265_mouse))

'''
router = Router(exp_id, '3e4aa9a2bf104810861967f0340664a6')
compute_jump_density_contact(router, V4606_mouse)
router = Router(exp_id, '13b8924031bd48bd9d99e53140373753')
compute_jump_density_contact(router, V7265_mouse)
router = Router(exp_id, '0ae3470a2e334a1890166661db0599b8')
compute_jump_density_contact(router, red_mouse)
'''

'''#V7265
x = np.linspace(0,15, 100)
sa = 0.8
sb = 0.22
a=0.8
b=10
'''
'''
#red
x = np.linspace(0,15, 100)
sa = 0.6
sb = 0.5
a=2
b=5


plt.plot(x, (stats.gamma.pdf(x/sa, a)/sa + stats.gamma.pdf(x/sb, b)/sb)/2 , label = f'{a}, {b}')
'''

'''
a=1
b =0.5 
plt.plot(x*s, stats.gamma.pdf(x, a)/s + stats.gamma.pdf(x, b)/s, label = f'{a}, {b}')

plt.plot(x*s, stats.gamma.pdf(x, a)/s + stats.gamma.pdf(x, b)/s, label = f'{a}, {b}')

plt.plot(x*s, stats.gamma.pdf(x, a)/s + stats.gamma.pdf(x, b)/s, label = f'{b}, {b}')

plt.plot(x*s, stats.gamma.pdf(x, a)/s + stats.gamma.pdf(x, b)/s, label = f'{b}, {b}')
'''

'''
router = Router(exp_id, '13b8924031bd48bd9d99e53140373753')
compute_jump_density(router, V7265_mouse)
router = Router(exp_id, '3e4aa9a2bf104810861967f0340664a6')
compute_jump_density(router, V4606_mouse)
router = Router(exp_id, '0ae3470a2e334a1890166661db0599b8')
compute_jump_density(router, red_mouse)
plt.legend()
plt.show()
'''

#selec_subclust(red_mouse, '562231799100305614', 'b130b821493e47b0b2cff79b86e4980c')



'''
mouse = Mouse(*stdp.create_mouse_data.prepare_Red())

chan_cortex = 249
interneurons_list = np.array([418, 554, 634, 155])

pyramidal_spikes = mouse.spike_times[np.invert(mouse.spike_times.Cluster_ID.isin(interneurons_list))]
cortex_spikes = pyramidal_spikes.Spike_times[pyramidal_spikes.Channel_ID > chan_cortex]

print(len(cortex_spikes))
'''