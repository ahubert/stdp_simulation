import logging
import pathlib
#from pathlib import Path

import numpy as np
import pandas as pd
import tslearn
from tslearn.metrics import dtw, cdist_dtw
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

import matplotlib.pyplot as plt
import matplotlib as mpl

import mlflow

import stdp.integration
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)


#######################################################################################################################

class Router():
    mlruns_dir = pathlib.Path('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns')
    
    #To get informations and link from all the good files
    def __init__(self, exp_id, run_id, mouse_path = None):
        self.exp_id = exp_id
        self.run_id = run_id
        if mouse_path != None:
            self.mouse_id = mouse_path

    def get_artifacts_dir(self):
        mlruns_dir = pathlib.Path('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns')
        return mlruns_dir / f'{self.exp_id}/{self.run_id}/artifacts'

    def get_couple_id(self):
        '''
        return couple id of cluster for experiment of a cortex neuron vs a striatum neuron
        '''
        couple_id = ()
        #for path in self.mlruns_dir.glob(f'{self.get_artifacts_dir()}/res_*_*.npy'):
        for path in self.get_artifacts_dir().glob('res_*_*.npy'):
            stem = path.stem
            _, n1, n2 = stem.split('_', 2)
            couple_id += ((int(n1),int(n2)),)
        return(couple_id)

    def load_result_couple(self, cortex_id: int, striatum_id:int, seg = 'res'):
        return np.load(f'{self.get_artifacts_dir()}/{seg}_{cortex_id}_{striatum_id}.npy')

    def get_result_id(self):
        '''
        return the id of striatum cluster for experiment of global cortex activitie vs a striatum neuron
        '''
        couple_id = ()
        #for path in self.mlruns_dir.glob(f'{self.get_artifacts_dir()}/res_*_*.npy'):
        for path in self.get_artifacts_dir().glob('res_*.npy'):
            stem = path.stem
            _, _, n2 = stem.split('_', 2)
            couple_id += (int(n2),)
        return(couple_id)
    
    def get_result_id_random(self):
        '''
        return the id of striatum cluster for experiment of global cortex activitie vs a striatum neuron
        '''
        couple_id = ()
        #for path in self.mlruns_dir.glob(f'{self.get_artifacts_dir()}/res_*_*.npy'):
        for path in self.get_artifacts_dir().glob('res_*.npy'):
            stem = path.stem
            _, n2 = stem.split('_', 1)
            couple_id += (int(n2),)
        return(couple_id)

    def get_result_id_jitter(self):
        '''
        return the id of striatum cluster for experiment of global cortex activitie vs a striatum neuron
        '''
        couple_id = ()
        #for path in self.mlruns_dir.glob(f'{self.get_artifacts_dir()}/res_*_*.npy'):
        for path in self.get_artifacts_dir().glob('res_*.npy'):
            stem = path.stem
            _, _, n2, _ = stem.split('_', 3)
            couple_id += (int(n2),)
        return(couple_id)

    def load_result(self, id, name = None):
        if name == None:
            return np.load(f'{self.get_artifacts_dir()}/res_{id}.npy')
        else:
            try:
                return np.load(f'{self.get_artifacts_dir()}/res_{name}_{id}.npy')
            except FileNotFoundError:
                LOGGER.error(f'{self.get_artifacts_dir()}/res_{name}_{id}.npy not found')

    def load_solution(self, id, name = None):
        if name == None:
            return np.load(f'{self.get_artifacts_dir()}/sol_{id}.npy')
        else:
            try:
                return np.load(f'{self.get_artifacts_dir()}/sol_{name}_{id}.npy')
            except FileNotFoundError:
                LOGGER.error(f'{self.get_artifacts_dir()}/sol_{name}_{id}.npy not found')

    def load_solution_couple(self, couple, name = None):
        if name == None:
            return np.load(f'{self.get_artifacts_dir()}/sol_{couple[0]}_{couple[1]}.npy')
        else:
            try:
                return np.load(f'{self.get_artifacts_dir()}/sol_{name}_{id}.npy')
            except FileNotFoundError:
                LOGGER.error(f'{self.get_artifacts_dir()}/sol_{name}_{id}.npy not found')