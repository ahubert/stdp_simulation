#python3 -  module lfp
import numpy as np
import matplotlib.pyplot as plt
import pathlib
import tslearn
import pandas as pd

import spikeinterface.full as si
import spikeinterface.sorters as ss
import spikeinterface.comparison as sc
import spikeinterface.widgets as sw
import spikeinterface.extractors as se

import stdp.Analysis_spike
import stdp.create_mouse_data
from stdp.computation_tools import *
from stdp.Class_Analysis import Router
from stdp.create_mouse_data import Mouse

#384 channels - 2.5kHz - 1 segments - 2,750,000 samples
#1,100.00s (18.33 min)

if __name__ == "__main__":
    red_mouse = Mouse(*stdp.create_mouse_data.prepare_Red())
    V4606_mouse = Mouse(*stdp.create_mouse_data.prepare_V4606())
    V7265_mouse = Mouse(*stdp.create_mouse_data.prepare_V7265())
    
    red_mouse.ti = red_mouse.frame_to_time(27243)-600 #start contact
    red_mouse.tf = red_mouse.frame_to_time(29168)-600 #end contact

    V7265_mouse.ti = V7265_mouse.frame_to_time(21257)-700 #start contact
    V7265_mouse.tf = V7265_mouse.frame_to_time(21361)-700 #end contact

    V4606_mouse.ti = V4606_mouse.frame_to_time(27243) #start contact
    V4606_mouse.tf = V4606_mouse.frame_to_time(29168) #end contact


def LFP_FR_drift(mouse: Mouse):
    mouse_LFP = np.mean(V7265_mouse.LFP_cortex, axis = 1)
    #x = scipy.signal.wiener(x, noise = 50)
    sos = scipy.signal.butter(2, [49.8, 50.2], 'bandstop', fs=2500, output='sos')
    filtered = scipy.signal.sosfilt(sos, mouse_LFP)
    fs = 2500
    '''
    f, Pxx_den = scipy.signal.periodogram(filtered, fs)
    plt.semilogy(f, Pxx_den)
    plt.xlabel('frequency [Hz]')
    plt.ylabel('PSD [V**2/Hz]')
    plt.show()
    '''
    len_LFP = len(mouse_LFP)
    #mouse_LFP = mouse_LFP[:int(21200/21996*len_LFP)] #V7
    #mouse_LFP = mouse_LFP[int(3200/23996*len_LFP):] #V4
    #mouse.t_bounds[1] = mouse.t_bounds[0]+(mouse.t_bounds[1]-mouse.t_bounds[0])*21200/21996 #V7
    #mouse.t_bounds[0] = mouse.t_bounds[0]+(mouse.t_bounds[1]-mouse.t_bounds[0])*3200/23996 #V4
    '''new_LFP = np.zeros(17250) #1725000/100
    for k in range(17250):
        new_LFP[k] = np.mean(mouse_LFP[k*100:(k+1)*100])
    new_LFP = new_LFP/np.sum(new_LFP)
    '''
    res = ()
    list_shift = [0, 5, 10, 15, 20, 25, 30]
    my_res = np.zeros((4, 7))
    it = 0
    list_FR_options = [ ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 0.2, 0.05),
                        ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 1, 0.2),
                        ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 5, 1),
                        ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 20, 4)]
    cortex_cluster = mouse.resume.Cluster_ID.loc[mouse.resume.Channel_ID.isin(mouse.brain_region[1])].to_numpy()
    for FR_options in list_FR_options:
        FR = firing_rate(mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)], *FR_options)
        lr = len(FR)
        print(lr)
        step = len(mouse_LFP)/lr
        new_LFP = np.zeros(lr)
        for k in range(lr):
            new_LFP[k] = np.mean(mouse_LFP[int(k*step):int((k+1)*step)])
        new_LFP = new_LFP/np.std(new_LFP)
        FR = FR/np.std(FR)
        j=0
        for shift in [0, 5, 10, 15, 20, 25, 30]:
            my_res[it, j] = np.mean( (FR[(30-shift) : (-1-shift)]-np.mean(FR))*(new_LFP[15: -16] - np.mean(new_LFP)) )
            j = j+1
        shift = list_shift[np.argmax(my_res[it, :])]
        plt.plot(new_LFP[15: -16]-np.mean(new_LFP), color = 'blue')
        plt.plot(FR[(30-shift) : (-1-shift)]-np.mean(FR), color = 'red')
        plt.show()
        #res = (res) + (np.cov(new_LFP, FR),)
        
    
        """ind0 = np.array([k[0] for k in res_dtw[0]])
        ind1 = np.array([k[1] for k in res_dtw[0]])
        plt.scatter(new_LFP[ind0], FR[ind1], alpha = 0.7)
        plt.plot(ind0/np.max(ind0)*np.max(new_LFP), ind1/np.max(ind1)*np.max(FR), color = 'red')
        plt.plot([0,np.max(new_LFP)], [0, np.max(FR)], color = 'black')
        plt.show()
        """
        
        it = it+1
    return (my_res)

def LFP_FR(mouse: Mouse):
    mouse_LFP = np.mean(mouse.LFP_cortex, axis = 1)

    #x = scipy.signal.wiener(x, noise = 50)
    sos = scipy.signal.butter(2, 30, 'lowpass', fs=2500, output='sos')
    #sos = scipy.signal.butter(2, [49.8, 50.2], 'bandstop', fs=2500, output='sos')
    filtered = scipy.signal.sosfilt(sos, mouse_LFP)
    plt.hist(filtered, bins = 150)
    plt.show()
    '''plt.plot(filtered)
    plt.plot(mouse_LFP)
    plt.legend(['filtered', 'raw'])
    plt.show()'''
    fs = 2500

    FR_options = ([0,mouse.t_bounds[1]-mouse.t_bounds[0]], 0.1, 0.05)
    cortex_cluster = mouse.resume.Cluster_ID.loc[mouse.resume.Channel_ID.isin(mouse.brain_region[1])].to_numpy()
    FR = firing_rate(mouse.spike_times.Spike_times[mouse.spike_times.Cluster_ID.isin(cortex_cluster)], *FR_options)
    lr = len(FR)

    step = len(filtered)/lr
    new_LFP = np.zeros(lr)
    for k in range(lr):
        new_LFP[k] = np.mean(filtered[int(k*step):int((k+1)*step)])
    new_LFP = new_LFP/np.std(new_LFP)
    FR = FR/np.std(FR)

    #res_dtw = tslearn.metrics.dtw_path(FR, new_LFP, global_constraint = 'sakoe_chiba', sakoe_chiba_radius = 5)
    #ind0 = np.array([k[0] for k in res_dtw[0]])
    #ind1 = np.array([k[1] for k in res_dtw[0]])

    plt.plot(new_LFP-np.mean(new_LFP), color = 'blue')
    plt.plot(FR-np.mean(FR), color = 'red')
    plt.legend(['LFP', 'FR'])
    plt.show()
    #res = (res) + (np.cov(new_LFP, FR),)
    #plt.scatter(new_LFP[ind0], FR[ind1], alpha = 0.7)
    #plt.plot(ind0/np.max(ind0)*np.max(new_LFP), ind1/np.max(ind1)*np.max(FR), color = 'red')
    #plt.plot([0,np.max(new_LFP)], [0, np.max(FR)], color = 'black')
    #plt.show()

    return 0

#pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'depth'])
load_name = ['cluster_id', 'depth']

plt.grid()
red_count = np.bincount( red_mouse.spike_times.Cluster_ID ) #690
#plt.scatter(np.arange(0, len(red_count)), red_count/690)
#plt.show()
print(np.where(red_count/690 > 20))
info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'depth'])

for clust in red_mouse.resume.Cluster_ID:
    spike_times = red_mouse.spike_times.Spike_times[red_mouse.spike_times.Cluster_ID == clust]
    FR = firing_rate(spike_times, [0, 690], 2, 0.5)
    if np.max( FR > 100):
        chan = red_mouse.resume.Channel_ID[red_mouse.resume.Cluster_ID == clust]
        depth = info.depth[info.cluster_id == clust]
        print(f'max of cluster {int(clust)} is {int(np.max(FR))}, \n channel {int(chan)}, depth {int(depth)}, average is {red_count[int(clust)]/690}Hz')


V7_count = np.bincount( V7265_mouse.spike_times.Cluster_ID )
#plt.scatter(np.arange(0, len(V7_count)), V7_count/1100)
#plt.show()
print(np.where(V7_count/1100 > 20))
info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'depth'])
for clust in V7265_mouse.resume.Cluster_ID:
    spike_times = V7265_mouse.spike_times.Spike_times[V7265_mouse.spike_times.Cluster_ID == clust]
    FR = firing_rate(spike_times, [0, 1100], 2, 0.5)
    if np.max( FR > 100):
        chan = V7265_mouse.resume.Channel_ID[V7265_mouse.resume.Cluster_ID == clust]
        depth = info.depth[info.cluster_id == clust]
        print(f'max of cluster {int(clust)} is {int(np.max(FR))}, \n channel {int(chan)}, depth {int(depth)}, average is {V7_count[int(clust)]/1100}Hz')

V4_count = np.bincount( V4606_mouse.spike_times.Cluster_ID )
#plt.scatter(np.arange(0, len(V4_count)), V4_count/1200)
#plt.show()
print(np.where(V4_count/1200 > 20))
info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606/data_KS/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'depth'])
for clust in V4606_mouse.resume.Cluster_ID:
    spike_times = V4606_mouse.spike_times.Spike_times[V4606_mouse.spike_times.Cluster_ID == clust]
    FR = firing_rate(spike_times, [0, 1200], 2, 0.5)
    if np.max( FR > 100):
        chan = V4606_mouse.resume.Channel_ID[V4606_mouse.resume.Cluster_ID == clust]
        depth = info.depth[info.cluster_id == clust]
        print(f'max of cluster {clust} is {np.max(FR)}, \n channel {int(chan)}, depth {int(depth)}, average is {V4_count[int(clust)]/1200}Hz')

