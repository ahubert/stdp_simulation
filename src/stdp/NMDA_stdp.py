#NMDA LTP study
import numpy as np
import scipy
import scipy.signal
import scipy.stats
import matplotlib.pyplot as plt
import sklearn.cluster

from stdp.Class_Analysis import Router
from stdp.create_mouse_data import Mouse
from stdp.computation_tools import *
from stdp.utility_tools import *
from stdp.fig_paper_Charlotte import around_ratio


def compute_when_nmda(T , nmda, ti, delta, len_dim):
    '''compute the times where LTP happens.
    LTD_type can be Neutral if it come from Ca increase without LTD threshold activated
    '''

    vect_LTP = np.zeros(len_dim)
    Inmda = (nmda > 2)*1
    T_LTP = T[Inmda == 1]
    vect_LTP[np.array((T_LTP - ti)/delta).astype(int)] = 1

    return (vect_LTP)

def NMDA_variations(router: Router, mouse: Mouse, delta, len_dim, id_selection = 'pairing_increase'):

    #delta:  precision, in s

    if id_selection == 'pairing_increase':
        result_id = around_ratio(mouse)
        print(f'number of id with pairing increase selection: {len(result_id)}')
    elif id_selection == 'all':
        result_id = np.array(router.get_couple_id())
    elif id_selection == 'mid':
        result_id = around_ratio(mouse, 'mid')
    else:
        print(f'selection id as: {id_selection}, is not recognized')

    print(f'{id_selection} selection, {len(result_id)} id.')
    #corr_id = mouse.get_correlated_couples()
    #result_id = np.array([(corr_id['Cortex_ID'][it],corr_id['Striatum_ID'][it]) for it in range(len(corr_id['Cortex_ID']))])
    
    '''#Checker for Team A and B+
    cortex_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    #print((cortex_clusters))
    striatum_clusters = np.array([result_id[it][1] for it in range(len(result_id))])
    selec_cortex = np.isin(cortex_clusters, mouse.selec_Team(cortex_clusters))
    selec_striatum = np.isin(striatum_clusters, mouse.selec_Team(striatum_clusters))
    result_id = result_id[np.logical_and(selec_cortex, selec_striatum)]'''
    
    T_mouse_LTP = np.zeros((len(result_id), len_dim))

    T_plot = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    it = 0
    for couple in result_id:
        try:
            RES = router.load_result_couple(*couple)
            T = RES[2]
            RES = RES[1]
            res_nmda = compute_when_nmda(T, RES, mouse.ti-300, delta, len_dim)
            #plt.plot(T_plot, res[0])
            #if np.sum(res[0])>0:
            T_mouse_LTP[it, :] = res_nmda#/np.sqrt(np.sum(res[0]))
            it += 1
        except FileNotFoundError:
            LOGGER.warning('%s not found', couple)
    #print(T_mouse_LTP)
    return (T_mouse_LTP)

def main_sepRes():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''
    name = 'Blue'
    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    #router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    #router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    #router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    #router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    #router_Black =  Router('490892905216247639','1cb6b6e3c29e45dd83f8710f2463942e')
    #router_Red =  Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    #router_V4607 =  Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    router_Blue = Router('490892905216247639','aa6bdd6ad7b9415a8f06513209b90b75') #1K

    #router_V4606 = Router('490892905216247639','1cd46c41f92040e189a9812930273d17') #2K start contact
    
    mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')

    '''#RELATED TO SPEED
    path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
    synchro = pd.read_csv(path, usecols=[0, 4])
    #print(synchro.index)
    speed = synchro.to_numpy()
    #length = speed.shape[0]/10000
    #print(speed.shape[0])
    speed = speed[np.arange(0, speed.shape[0], 500), :]
    speed = speed.T
    recallage = 0
    if name == 'Black':
        speed = speed.astype(float)
    speed = speed[:, np.logical_and(speed[0]>(mouse.ti-300+recallage), speed[0]<(mouse.tf+320+recallage))]
    plt.plot(speed[0]-recallage, speed[1], alpha = 0.3, label = 'mouse speed') '''

    delta = 0.2
    len_dim = int((mouse.tf-mouse.ti+620)/delta +1)

    router = router_Blue
    '''RES_all = NMDA_variations(router, mouse, delta, len_dim, 'all')
    RES_mid = NMDA_variations(router, mouse, delta, len_dim, 'mid')
    RES_selected = NMDA_variations(router, mouse, delta, len_dim)'''

    try:
        RES_selected = np.load(mouse.path_mouse / 'RES_selected_NMDA.npy')
        RES_all = np.load(mouse.path_mouse / 'RES_all_NMDA.npy')
        RES_mid = np.load(mouse.path_mouse / 'RES_mid_NMDA.npy')
    except:
        RES_all = NMDA_variations(router, mouse, delta, len_dim, 'all')
        RES_mid = NMDA_variations(router, mouse, delta, len_dim, 'mid')
        RES_selected = NMDA_variations(router, mouse, delta, len_dim)
        np.save(mouse.path_mouse / 'RES_selected_NMDA.npy', RES_selected)
        np.save(mouse.path_mouse / 'RES_all_NMDA.npy', RES_all)
        np.save(mouse.path_mouse / 'RES_mid_NMDA.npy', RES_mid)

    #ind_Kmean = np.load('/home/ahubert/Documents/SpikeSorting/Kmean_ind/V7265indKmean.npy')
    plt.vlines([mouse.ti, mouse.tf], 0, 1, color = 'red')


    T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    #plt.plot(T, np.sum(RES_all[ind_Kmean == 1], axis = 0)/np.sum(ind_Kmean == 1), label = "learning")
    #plt.plot(T, np.sum(RES_all[ind_Kmean != 1], axis = 0)/np.sum(ind_Kmean != 1), label = "speed")
    print(RES_all.shape)
    plt.plot(T, np.sum(RES_all, axis = 0)/RES_all.shape[0], label = 'all')
    plt.plot(T, np.sum(RES_mid, axis = 0)/RES_mid.shape[0], label = 'mid')
    plt.plot(T, np.sum(RES_selected, axis = 0)/RES_selected.shape[0], label = 'increase')

    #plt.plot(T, np.sum(RES_selected[0], axis = 0), label = '0', alpha = 0.6)
    #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 0], axis = 0), label = 'speed artefacts', alpha = 1)
    #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 1], axis = 0), label = 'learning?', alpha = 1)
    #plt.plot(T, np.sum(RES_all[0][ind_Kmean == 2], axis = 0), label = '2', alpha = 0.6)  

    plt.legend()
    plt.show()
    #T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
    return 0

def main_heatmap():
    '''We look to time distrib of LTP events. In each bin time is counted number of LTP and LTD that are active.
    '''
    #name = 'Red'
    #router_V7265 = Router('490892905216247639','1b6f1bee208e4d34a15f257f6bea48a8') #2K
    router_V7265 = Router('490892905216247639','a9095fdc9b45419ca1eae69f062aaa61') #1K
    router_V4606 = Router('490892905216247639','00e4f28b9afb4895b4cb1aff9032822f') #1K
    #router_V4606 = Router('490892905216247639','c770e58062d54a83878b764637835047') #2K
    router_Mouse1 = Router('490892905216247639','1c5d50000358446aafaf5ea04ecef9e4') #1K
    #router_Mouse1 = Router('490892905216247639','b4dd767690f94a09a7ff47eec17d93d7') #2K
    router_V7260 = Router('490892905216247639','675dc4eb9be241d3bf9df8ef860e1924') #1K
    #router_V7260 = Router('490892905216247639','6fceb9cc477d4c80b1705205746a81d3') #2K
    router_Black =  Router('490892905216247639','1cb6b6e3c29e45dd83f8710f2463942e')
    router_Red =  Router('490892905216247639','c4889ab0b9754ddfa87c63a031920ba1')
    router_V4607 =  Router('490892905216247639','3201eeba20a640dcbc7899f84a98cefb')
    router_Blue = Router('490892905216247639','aa6bdd6ad7b9415a8f06513209b90b75') #1K

    L_mouse = [('V7265', router_V7265), ('V4606', router_V4606), ('Mouse1', router_Mouse1), ('V7260', router_V7260),
               ('Black', router_Black), ('Red', router_Red), ('V4607', router_V4607)]
    L_mouse = [('Blue', router_Blue)]

    for name, router in L_mouse:
    
        mouse: Mouse = load_object(f'/home/ahubert/Documents/SpikeSorting/{name}/data_mouse/{name}_mouse')

        '''#RELATED TO SPEED
        path = pathlib.Path(f'/home/ahubert/Documents/SpikeSorting/{name}/{name}_Test_Table_data.txt')
        synchro = pd.read_csv(path, usecols=[0, 4])
        #print(synchro.index)
        speed = synchro.to_numpy()
        #length = speed.shape[0]/10000
        #print(speed.shape[0])
        speed = speed[np.arange(0, speed.shape[0], 500), :]
        speed = speed.T
        recallage = 0
        if name == 'Black':
            speed = speed.astype(float)
        speed = speed[:, np.logical_and(speed[0]>(mouse.ti-300+recallage), speed[0]<(mouse.tf+320+recallage))]
        plt.plot(speed[0]-recallage, speed[1], alpha = 0.3, label = 'mouse speed') '''

        delta = 0.2
        len_dim = int((mouse.tf-mouse.ti+620)/delta +1)


        '''RES_all = NMDA_variations(router, mouse, delta, len_dim, 'all')
        RES_mid = NMDA_variations(router, mouse, delta, len_dim, 'mid')
        RES_selected = NMDA_variations(router, mouse, delta, len_dim)'''

        try:
            RES_selected = np.load(mouse.path_mouse / 'RES_selected_NMDA.npy')
            RES_all = np.load(mouse.path_mouse / 'RES_all_NMDA.npy')
            RES_mid = np.load(mouse.path_mouse / 'RES_mid_NMDA.npy')
        except:
            RES_all = NMDA_variations(router, mouse, delta, len_dim, 'all')
            RES_mid = NMDA_variations(router, mouse, delta, len_dim, 'mid')
            RES_selected = NMDA_variations(router, mouse, delta, len_dim)
            np.save(mouse.path_mouse / 'RES_selected_NMDA.npy', RES_selected)
            np.save(mouse.path_mouse / 'RES_all_NMDA.npy', RES_all)
            np.save(mouse.path_mouse / 'RES_mid_NMDA.npy', RES_mid)

        #ind_Kmean = np.load('/home/ahubert/Documents/SpikeSorting/Kmean_ind/V7265indKmean.npy')


        T = np.linspace(mouse.ti-300, mouse.tf+320, len_dim)
        

        RES_selected = np.sum(RES_selected, axis = 0)/RES_selected.shape[0]
        RES_mid = np.sum(RES_mid, axis = 0)/RES_mid.shape[0]
        RES_all = np.sum(RES_all, axis = 0)/RES_all.shape[0]
        var_selected = RES_selected[1:]-RES_selected[:-1]
        var_mid = RES_mid[1:]-RES_mid[:-1]
        var_all = RES_all[1:]-RES_all[:-1]

        zscore_selected = scipy.stats.zscore( var_selected[int((150)/delta):int((mouse.tf-mouse.ti+550)/delta)] )
        zscore_all = scipy.stats.zscore( var_all[int((150)/delta):int((mouse.tf-mouse.ti+550)/delta)] )
        zscore_mid = scipy.stats.zscore( var_mid[int((150)/delta):int((mouse.tf-mouse.ti+550)/delta)] )

        zscore_selected[zscore_selected>2] = 2
        zscore_mid[zscore_mid>2] = 2
        zscore_all[zscore_all>2] = 2

        zscore_selected[zscore_selected<0] = 0
        zscore_mid[zscore_mid<0] = 0
        zscore_all[zscore_all<0] = 0

        X = np_moving_average(zscore_selected, 10)
        X = X[np.arange(0, len(X), 5)]
        d = len(X)
        X = np.tile(X, (int(len(X)/20),1))
        X = np.concatenate((X,))

        Y = np_moving_average(zscore_mid, 10)
        Y = Y[np.arange(0, len(Y), 5)]
        Y = np.tile(Y, (int(len(Y)/20),1))

        Z = np_moving_average(zscore_all, 10)
        Z = Z[np.arange(0, len(Z), 5)]
        Z = np.tile(Z, (int(len(Z)/20),1))

        W = np.ones(d)*np.min([X,Y,Z])
        W = np.tile(W, (int(d/100),1))

        XYZ = np.concatenate((X,W,Y,W,Z), axis = 0)
        plt.imshow(XYZ, cmap = 'hot', label = 'zscore')
        plt.title(f'Z score, {name}')
        plt.colorbar(orientation = 'horizontal')

        #plt.vlines([mouse.ti-150, mouse.tf-150], 0, 100, color = 'red')

        plt.show()
    return 0

#main_sepRes()
main_heatmap()