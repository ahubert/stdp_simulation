import logging

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import scipy

LOGGER = logging.getLogger(__name__)

def plot_weight(name):
    '''
    
    '''
    RES = np.load(name)
    plt.plot(RES[2], RES[0][:]/RES[0][0] * RES[1][:]/RES[1][0])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 100, colors=['green', 'green', 'green', 'red', 'red', 'green'])
    plt.title('produits des poids. ' + name)
    plt.show()
    plt.plot(RES[2], RES[0][:]/RES[0][0])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 100, colors=['green', 'green', 'green', 'red', 'red', 'green'])
    plt.title('poids pre. ' + name)
    plt.show()
    plt.plot(RES[2], RES[1][:]/RES[1][0])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 100, colors=['green', 'green', 'green', 'red', 'red', 'green'])
    plt.title('poids post. ' + name)
    plt.show()

def heatmap2d(arr: np.ndarray):
    cmap_heat = mpl.cm.seismic
    norm_heat = mpl.colors.PowerNorm(gamma = 0.5, vmin = 0.5, vmax = 2.5, clip = True)
    #x_position = np.arange(0, len(x_axis), step = 5)
    #new_x_axis = [x_axis[i] for i in x_position]
    plt.imshow(arr, cmap = cmap_heat, norm = norm_heat)
    plt.colorbar()
    #plt.yticks(x_position, new_x_axis)
    plt.xlabel('Number of pairing')
    plt.ylabel('Time between pre and post (s)')
    plt.title('map of synaptic weight change' + '\n' + 'depending on number of pairing and time of pairing.')
    plt.show()

def plot_gaussian_heatmap(Z, sigma = 1):
    init1 = y0init[25] #pre_inf is the presynaptic weight
    init2 = y0init[0] + 2*np.sum(y0init[1:4]) + 3*np.sum(y0init[4:8]) + 4*np.sum(y0init[8:11]) + 5*y0init[11] + 6*y0init[12]
    init2 = 1+2*init2/164.6
    Z = Z/(init1 * init2)
    #Z = scipy.ndimage.gaussian_filter(Z, sigma = sigma)
    Z = scipy.ndimage.gaussian_filter1d(Z, sigma = sigma, axis = 0)
    heatmap2d(np.flip(Z, axis=0))

def compare_matlab_python(dtstdp, ns_max, t_end = 100):

    T_data = scipy.io.loadmat('/home/ahubert/Documents/model_stdp/T_dtstdp-15_ns30.mat')
    Y_data = scipy.io.loadmat('/home/ahubert/Documents/model_stdp/Y_dtstdp-15_ns30.mat')
    T_mat = np.array((T_data['T']))
    T_mat = T_mat - 1 - 0.01499 + dtstdp
    Y_mat = np.transpose(np.array((Y_data['Y'])))
    RES = integration(dtstdp = dtstdp, ns_max = ns_max, t_end = t_end)
    #RES_interpol = np.interp(T_mat, RES[2], RES[3][25, :])[:, 0]
    #plt.plot(T_mat, RES_interpol-Y_mat[25, :])
    print(T_mat[0])
    for i in range(29):
        plt.plot(RES[2], RES[3][i, :], color = 'red')
        plt.plot(T_mat, Y_mat[i, :], color = 'blue')
        plt.title(str(VECTOR_ELEMENT_NAMES[i]))
        plt.show()


VECTOR_ELEMENT_NAMES = ['B1', 'B2', 'B3' , 'B4' , 'B5' , 'B6' , 'B7' , 'B8' , 'B9' , 'B10' , 'B11' , 'B12' , 'B13',
                        'Ca_cyt' , 'Ca_ER' , 'h' , 'IP3' , 'V' , 'rAMPA' , 'rNMDA' ,
                        'PP1' , 'I1P' , 'DAG', 'DAGLP' , '2-AG' , 'fpre' , 'AEA' , 'Glu' , 'Curr_spikepost', 'cst']

y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])

'''
c_post_list = [1091, 232, 239, 684, 243, 362, 208, 268, 1021, 196, 1054, 320, 1064, 1132, 192, 1045, 378, 255, 261, 1149, 1148, 1153, 506, 540, 529, 1248]

c_post_list = [616, 1091, 232, 239, 684, 243, 411, 362, 1211, 208, 268, 1021, 196,
                    224, 1054, 320, 1064, 324, 457, 1132, 192, 1045, 378, 732, 400, 1135,
                      240, 255, 261, 279, 1149, 1148, 1142, 1123, 358, 1153, 685, 506, 540, 532, 1190, 1248]
for c_post in c_post_list:
    name = '/home/ahubert/Documents/Code_STDP/model-potcadep/res_650_' + str(c_post) + '.npy'
    plot_weight(name)
'''