import logging

import numpy as np
import pandas as pd
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods

import matplotlib.pyplot as plt
import matplotlib as mpl

import mlflow

import stdp.integration
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)


cols_load_resume =  ['Team A', 'channel A', 'Team B+', 'channel B+', 'Team B', 'channel B', 'Team B-', 'channel B-', 'Team C', 'channel C']
cols_name_resume =  ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B', 'channel_B', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']
cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']
resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V7265_final_data/V7265_sorted.xlsx', header = 3)#, usecols = cols_load_resume)
#resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'Unnamed: 8', 'Team B-', 'channel.2', 'Team C', 'channel.3']]
resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'channel.2', 'Team B-', 'channel.3', 'Team C', 'channel.4']]
resume.columns = cols_name_resume
#resume = read_ods('/home/ahubert/Documents/SpikeSorting/V4606_final_data/V4606_sorted.ods', header = 6, usecols = cols_load_resume)

spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265_final_data/Spike_times_V7265_Charlotte_Clustering.txt', usecols = cols_load_time)
spike_times.columns = cols_name_time

synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265_final_data/V7265_synchro', usecols=[0,1])
synchro.columns = ['time', 'frame']

#get spike of team A of striatum
#striatum_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A < 180])]
#get spike of team A of cortex
#cortex_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A > 200])]

y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])

def random_jitter(params, clust_pre, clust_post, n_it):
    cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_pre]
    cortex_jitter = np.random.normal(0, 0.02, len(cortex_times))
    cortex_times = cortex_times + cortex_jitter
    striatum_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_post]
    striatum_jitter = np.random.normal(0, 0.004, len(striatum_times))
    striatum_times = striatum_times + striatum_jitter

    RES = stdp.integration.integration(params, cortex_times, striatum_times, y0init, t_end = 100)
    for prefix, data in (
        ('res_', RES[0:3]),
        ('sol_', RES[3])
    ):
        name = f'{prefix}{clust_pre}_{clust_post}_{n_it}'
        log_numpy_data(data, name)
    return RES

def apply_jitter(params, clust_pre, clust_post):
    RES = ()
    for i in range(20):
        print('hello')
        RES = (*RES , random_jitter(params, clust_pre, clust_post, i))
        plt.plot(RES[i][2], RES[i][0])
        plt.plot(RES[i][2], RES[i][1])
        plt.legend(['wpre', 'wpost'])
        plt.show()
    