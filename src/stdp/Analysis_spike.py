import logging
import pathlib

import math
import numpy as np
import pandas as pd
import tslearn
from tslearn.metrics import dtw, cdist_dtw
import scipy
import scipy.stats as stats
#pip install pandas-ods-reader
from pandas_ods_reader import read_ods
import os
import spikeinterface.extractors as se

import matplotlib.pyplot as plt
import matplotlib as mpl
import plotly.express as px

import mlflow

import stdp.integration
import stdp.synapseModel
from stdp.mlflow_utils import log_numpy_data
import stdp.plot

logging.getLogger('matplotlib').setLevel(logging.INFO)
logging.getLogger('PIL').setLevel(logging.INFO)
logging.getLogger('numba').setLevel(logging.INFO)
LOGGER = logging.getLogger(__name__)

VECTOR_ELEMENT_NAMES = ['B1', 'B2', 'B3' , 'B4' , 'B5' , 'B6' , 'B7' , 'B8' , 'B9' , 'B10' , 'B11' , 'B12' , 'B13',
                        'Ca_cyt' , 'Ca_ER' , 'h' , 'IP3' , 'V' , 'rAMPA' , 'rNMDA' ,
                        'PP1' , 'I1P' , 'DAG', 'DAGLP' , '2-AG' , 'fpre' , 'AEA' , 'Glu' , 'Curr_spikepost', 'cst']

y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])

#pd.read_excel
#pd.read_csv

#V4606 header 6
#'/home/ahubert/Documents/SpikeSorting/V4606_final_data/V4606_sorted.xlsx'
#'/home/ahubert/Documents/SpikeSorting/V4606_final_data/Spike_times_V4606_Charlotte_Clustering.txt'
#V7265 header 3
#red header 0

#resume key:   'Team A', 'channel A', 'Unnamed: 2', 'Team B+', 'channel B+', 'Unnamed: 5',
#              'Unnamed: 6', 'Team B', 'channel B', 'Unnamed: 9', 'Unnamed: 10',
#              'Team B-', 'channel B-', 'Unnamed: 13', 'Unnamed: 14', 'Unnamed: 15',
#              'Team C', 'channel C', 'Unnamed: 18', 'Unnamed: 19', 'Unnamed: 20',
#              'NB'
#time key      'Spike times', 'Cluster ID', 'Channel ID', 'Channel Depth', 'Spike Amplitude'
#synchro organisation (not key): time, frame, x pos, y pos, speed (x and y ?)

#def glideWin(cluster_win ,win_len, path):


########################################## TEAM AND TIMES #############################################################

cols_load_resume =  ['Team A', 'channel A', 'Team B+', 'channel B+', 'Team B', 'channel B', 'Team B-', 'channel B-', 'Team C', 'channel C']
cols_name_resume =  ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B', 'channel_B', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C']
cols_load_time = ['Spike times', 'Cluster ID', 'Channel ID']
cols_name_time = ['Spike_times', 'Cluster_ID', 'Channel_ID']

resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V7265/V7265_sorted.xlsx', header = 3)#, usecols = cols_load_resume) #V7265
#resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/V4606_final_data/V4606_sorted.xlsx', header = 6)#, usecols = cols_load_resume) #V4606
#resume = pd.read_excel('/home/ahubert/Documents/SpikeSorting/Red_mouse_final_data/Rouge_Clustering.xlsx')#, usecols = cols_load_resume) #Red_mouse

#resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'Unnamed: 8', 'Team B-', 'channel.2', 'Team C', 'channel.3']] #V4606
resume = resume[['Team A', 'channel', 'Team B+', 'channel.1', 'Team B', 'channel.2', 'Team B-', 'channel.3', 'Team C', 'channel.4']] #V7265
#resume = resume[['Team A', 'Unnamed: 1', 'Team B+ ', 'Unnamed: 3', 'Team B-', 'Unnamed: 5', 'Team C', 'Unnamed: 7']] #RED NEED TO GET CHANNEL
#resume.columns = ['Team_A', 'channel_A', 'Team_B+', 'channel_B+', 'Team_B-', 'channel_B-', 'Team_C', 'channel_C'] #RED

resume.columns = cols_name_resume
#resume = read_ods('/home/ahubert/Documents/SpikeSorting/V4606_final_data/V4606_sorted.ods', header = 6, usecols = cols_load_resume)

#spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red_mouse_final_data/Spike_times_Rouge_Charlotte_Clustering.txt', usecols = cols_load_time)
spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/Spike_times_V7265_Charlotte_Clustering.txt', usecols = cols_load_time)
#spike_times = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606_final_data/Spike_times_V4606_Charlotte_Clustering.txt', usecols = cols_load_time)
spike_times.columns = cols_name_time

"""cluster_info = pd.read_csv('/home/ahubert/Documents/SpikeSorting/Red_mouse_final_data/cluster_info.tsv', sep ='\t', usecols = ['cluster_id', 'ch'])
cluster_info = cluster_info.set_index('cluster_id')
resume = resume.drop([91, 108])

resume.channel_A[:] = cluster_info.ch[resume.Team_A[:]]"""

synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V7265/V7265_synchro.txt', usecols=[0,1])
#synchro = pd.read_csv('/home/ahubert/Documents/SpikeSorting/V4606_final_data/V4606_synchro', usecols=[0,1])
#synchro.columns = ['time', 'frame']


#get spike of team A of striatum
striatum_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A < 180])]
#striatum_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A < 210])]
#striatum_times.index = np.arange()
#get spike of team A of cortex
cortex_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A > 200])]
#cortex_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A > 250])]

####################################################################################################################


################################################# LFP ###############################################
def LFP_things():
    path = '/home/ahubert/Documents/SpikeSorting/V7265_final_data/data_recording'
    #path = '/home/ahubert/Documents/SpikeSorting/V4606_final_data/data_recording'
    #path = '/home/ahubert/Documents/SpikeSorting/Red_mouse_final_data/data_recording'
    recording = se.read_spikeglx(path, stream_id = 'imec0.lf')
    traces = recording.get_traces()

    #mean_trace = np.mean(traces[:, 200:], axis = 1)
    mean_trace = np.mean(traces[:, 200:], axis = 1)
    mean_trace = mean_trace[np.logical_and(mean_trace>-150, mean_trace<150)]
    #spike_times_lfp = stdp.Analysis_spike.pass_times()
    #spike_times_vect = np.ceil(spike_times_lfp*2500).to_numpy().astype(int)
    mu = np.mean(mean_trace)
    sigma = np.std(mean_trace)
    x = np.linspace(mu - 4*sigma, mu + 4*sigma, 300)
    z = mu - 2*sigma
    print(math.erf((z-mu)/math.sqrt(sigma*2)))
    print( f'actual proportion: {np.sum(mean_trace < (mu - 2*sigma))/len(mean_trace)}')
    print(f'and the probability {0.5*(1+scipy.special.erf((z-mu)/math.sqrt(sigma*2)) )}')

    fit = scipy.stats.fit(scipy.stats.norm , mean_trace)
    Ktest = scipy.stats.kstest(mean_trace, cdf = "norm", args=fit.params, alternative='two-sided', method='auto')
    print(Ktest.pvalue)
    print(Ktest.statistic)

    print(f'Comparison of params: ({mu}, {sigma}), {fit.params} \n Was it good? {fit.success}')
    plt.plot(x, stats.norm.pdf(x, *fit.params))
    plt.plot(x, stats.norm.pdf(x, mu, sigma))
    plt.hist(mean_trace, bins=300, density = True)
    plt.show()
#####################################################################################################

def blabla(t_bounds, win_len, win_step):
    striatum_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A < 180]) | 
                                             spike_times.Cluster_ID.isin(resume['Team_B+'][resume['channel_B+'] < 180])]
    #striatum_times.index = np.arange()
    #get spike of team A of cortex
    cortex_times = spike_times.Spike_times[spike_times.Cluster_ID.isin(resume.Team_A[resume.channel_A > 200]) | 
                                            spike_times.Cluster_ID.isin(resume['Team_B+'][resume['channel_B+'] > 200])]
    
    fr_cortex = moving_window(cortex_times, t_bounds, win_len, win_step, len)
    fr_striatum = moving_window(striatum_times, t_bounds, win_len, win_step, len)

    ''' #PLOT PART
    plt.plot(np.arange(*t_bounds, win_step), fr_cortex)
    plt.plot(np.arange(*t_bounds, win_step), fr_striatum)
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, np.max(fr_cortex), colors=['black', 'black', 'black', 'red', 'red', 'black'])
    plt.show()
    '''
    return

def blabla2(t_bounds = [1315-700, 1350-700], win_len = 5, win_step = 2.5):
    id_clust_cortex = np.concatenate((resume.Team_A[resume.channel_A > 200], resume['Team_B+'][resume['channel_B+'] > 200],
                        resume['Team_B'][resume['channel_B'] > 200], resume['Team_B+'][resume['channel_B+'] > 200]))
    id_clust_striatum = np.concatenate((resume.Team_A[resume.channel_A < 180], resume['Team_B+'][resume['channel_B+'] < 180],
                        resume['Team_B'][resume['channel_B'] < 180], resume['Team_B+'][resume['channel_B+'] < 180]))
    
    fr_cortex = np.zeros((len(id_clust_cortex), len(np.arange(0, t_bounds[1]-t_bounds[0], win_step))))
    fr_striatum = np.zeros((len(id_clust_striatum), len(np.arange(0, t_bounds[1]-t_bounds[0], win_step))))
    for it, clust_cortex in enumerate(id_clust_cortex):
        cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_cortex]
        fr_cortex[it, :] = moving_window(cortex_times, t_bounds, win_len, win_step, len)
    for it, clust_striatum in enumerate(id_clust_striatum):
        striatum_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_striatum]
        fr_striatum[it, :] = moving_window(striatum_times, t_bounds, win_len, win_step, len)

    binf_cortex = np.array([60, 60, 80, 100, 60, 80, 60, 60, 60, 60, 60, 80, 60, 80, 80])
    binf_striatum = np.array([40, 100, 80, 100, 100, 60-260, 60, 80, 80, 60, 80, 120, 120, 80, 60])
    res_cortex = np.array([])
    res_striatum = np.array([])
    x_time = np.arange(0, t_bounds[1]-t_bounds[0], win_step)
    for it in range(len(x_time)):
        print(np.where(fr_cortex[:, it] > binf_cortex[it])[0])
        res_cortex = np.concatenate((res_cortex, np.where(fr_cortex[:, it] > binf_cortex[it])[0]))
        res_striatum = np.concatenate((res_striatum, np.where(fr_striatum[:, it] > binf_striatum[it])[0]))
        '''
        plt.hist(fr_cortex[:, it], bins = np.arange(0, np.max(fr_cortex[:, it])+20, 20))
        plt.title(f'cortex time btwn after {x_time[it]}')
        plt.show()
        plt.hist(fr_striatum[:, it], bins = np.arange(0, np.max(fr_striatum[:, it])+20, 20))
        plt.title(f' striatum time btwn after {x_time[it]}')
        plt.show()
        '''
        
    print('\n ____________________________________COUNTING_______________________ \n')
    unique, counts = np.unique(res_cortex, return_counts=True)
    print(dict(zip(unique, counts)))
    unique, counts = np.unique(res_striatum, return_counts=True)
    print(dict(zip(unique, counts)))
    return 0

def compute_map_V2(params, ns_values, dtstdp_values, freq, t_end = 0):
    LOGGER.info('computing map for %s hz', freq)
    Z_pre = np.zeros((len(dtstdp_values), len(ns_values)))
    Z_post = np.zeros((len(dtstdp_values), len(ns_values)))
    x = 0
    y0 = np.zeros((len(y0init), 1))

    for dtstdp in dtstdp_values:
        y = 0
        t_pre = np.arange(0, ns_values[-1])/freq
        t_post = t_pre + dtstdp
        RES_max = stdp.integration.integration(params, t_pre, t_post, y0init, t_end = t_end)
        Z_pre[x,-1] = RES_max[0][-1]
        Z_post[x,-1] = RES_max[1][-1]
        plt.plot(RES_max[2], RES_max[0])
        plt.plot(RES_max[2], RES_max[1])
        plt.vlines([t_pre[0], t_pre[-1], t_post[0], t_post[-1]], 0, 5, colors = ['black', 'black', 'red', 'red'])
        plt.title(f'freq: {freq}, ns: {ns_values[-1]}, dtstdp: {dtstdp}')
        plt.show()
        for ns in ns_values[:-1]:
            y0[:] = RES_max[3][: , RES_max[2] == min(ns/freq, ns/freq + dtstdp - params.integration.tsdt)]
            RES = stdp.integration.integration(params, [], [], y0[:,0], t_end = t_end)
            plt.plot(RES[2], RES[0])
            plt.plot(RES[2], RES[1])
            plt.vlines([t_pre[0], t_pre[-1], t_post[0], t_post[-1]], 0, 5, colors = ['black', 'black', 'red', 'red'])
            plt.title(f'freq: {freq}, ns: {ns}, dtstdp: {dtstdp}')
            plt.show()
            Z_pre[x,y] = RES[0][-1]
            Z_post[x,y] = RES[1][-1]
            y = y + 1
        x = x + 1

    path = '/home/ahubert/Documents/Code_STDP/model-potcadep/data/map_stdp_hz/'
    np.save('{}map_pre_{}hz_mono'.format(path, freq), Z_pre)
    np.save('{}map_post_{}hz_mono'.format(path, freq), Z_post)
    return ((Z_pre, Z_post))

def compute_map_mem(params, ns_bounds = [1, 100], ns_step = 1, dtstdp_values = np.array([-0.015]), t_end = 0):
    x = 0
    k = 0
    ns_values = np.arange(*ns_bounds, ns_step)
    RES_tot = []
    y0_next = np.zeros(len(y0init))
    Z_pre = np.zeros((len(dtstdp_values), len(ns_values)))
    Z_post = np.zeros((len(dtstdp_values), len(ns_values)))

    for dtstdp in dtstdp_values:
        y= 0
        for ns_max in ns_values:
            if (y == 0):
                t_pre = np.arange(0, ns_bounds[0])
                t_post = t_pre + dtstdp
                RES = stdp.integration.integration(params, t_pre, t_post, y0init, t_end = 1 + dtstdp - params.integration.tsdt)
                y0_next[:] = RES[3][:, -1]
                plt.plot(RES[2], RES[0])
                plt.plot(RES[2], RES[1])
                plt.show()
                RES = stdp.integration.integration(params, [], [], y0_next, t_end)
                y0_next[:] = RES[3][:, -1]

            else:
                t_pre = np.arange(0,ns_step)
                t_post = t_pre + dtstdp
                RES = stdp.integration.integration(params, t_pre, t_post, y0_next, t_end = 1)
                y0_next[:] = RES[3][:, -1]
                plt.plot(RES[2], RES[0])
                plt.plot(RES[2], RES[1])
                plt.show()
                RES = stdp.integration.integration(params, [], [], y0_next, t_end)
            Z_pre[x,y] = RES[0][-1]
            Z_post[x,y] = RES[1][-1]
            RES_tot.append( RES )
            LOGGER.info("dtstdp: " + str(dtstdp))
            LOGGER.info("ns_max: " + str(ns_max))
            y = y+1
            k = k+1
        x = x + 1
    #np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/verif_pos/line_test_pre_-15ms', Z_pre)
    #np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/verif_pos/line_test_post_-15ms', Z_post)
    #np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/verif_pos/map_full_creneau_res_test_pos', RES_tot)

def compute_map(params, ns_bounds = [1, 100], ns_step = 1, dtstdp_values = np.array([-0.015]), t_end = 0):
    x = 0
    ns_values = np.arange(*ns_bounds, ns_step)
    RES_tot = []
    Z_pre = np.zeros((len(dtstdp_values), len(ns_values)))
    Z_post = np.zeros((len(dtstdp_values), len(ns_values)))

    for dtstdp in dtstdp_values:
        y= 0
        for ns_max in ns_values:
            t_pre = np.arange(0,ns_max)
            t_post = t_pre + dtstdp
            RES = stdp.integration.integration(params, t_pre, t_post, y0init, t_end = t_end )
            Z_pre[x,y] = RES[0][-1]
            Z_post[x,y] = RES[1][-1]
            RES_tot.append( RES )
            LOGGER.info("dtstdp: " + str(dtstdp))
            LOGGER.info("ns_max: " + str(ns_max))
            y = y+1
        x = x + 1
    #np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/verif_pos/line_test_pre_-15ms', Z_pre)
    #np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/verif_pos/line_test_post_-15ms', Z_post)

def moving_window(array, t_bounds, win_len, win_step, func = None):
    '''
    INPUT VAR:
        array:
        win_len: length of the windows on wich to apply the function func
        win_step: delta between two windows
        func: the function to apply on each windows

    OUTPUT VAR:
        res: an array containing the result of func applied on each window
    '''
    it = 0
    
    if func == None:
        res = np.zeros( (len(np.arange(t_bounds[0], t_bounds[1] - win_len, win_step)) , len(array)) )
        for t in np.arange(t_bounds[0], t_bounds[1] - win_len, win_step):
            res[it, :] = (array >= t ) & (array <= t + win_len)
            it = it + 1
    else:
        res = np.zeros( len(np.arange(t_bounds[0], t_bounds[1] - win_len, win_step)) )
        for t in np.arange(t_bounds[0], t_bounds[1] - win_len, win_step):
            win_ind = (array >= t ) & (array <= t + win_len)
            res[it] = func(array[win_ind])
            it = it + 1
    return res

def match_pre_post(t_pre, t_post, win_len):
    '''
    INPUT VAR:
        t_pre:
        t_post:
        win_len:
    '''
    it = 0
    res = np.zeros(len(t_post))
    for t in t_post:
        res[it] = sum( (t_pre > t - win_len/2) & (t_pre < t + win_len/2) )
        it = it + 1
    return res

def match_timed(t_pre, t_post):
    res = np.zeros(len(t_post))
    for it, t in enumerate(t_post):
        diff = t - t_pre
        res[it] = diff[diff.abs().idxmin()]
    return res

def analyse_match():
    it_post = 1
    n_post = len((resume.Team_A[resume.channel_A > 200]))
    n_pre = len(resume.Team_A[resume.channel_A < 180])
    res_match = np.zeros((n_post, n_pre))
    for clust_post in (resume.Team_A[resume.channel_A > 200]):
        it_pre = 1
        t_post = spike_times.Spike_times[spike_times.Cluster_ID ==  clust_post]
        LOGGER.info('t_post number ' + str(it_post) + ' out of ' + str(n_post))
        it_post = it_post + 1
        for clust_pre in (resume.Team_A[resume.channel_A < 180]):
            LOGGER.info('t_pre number ' + str(it_pre) + ' out of ' + str(n_pre))
            it_pre = it_pre + 1
            t_pre = spike_times.Spike_times[spike_times.Cluster_ID ==  clust_pre]
            res = match_pre_post(t_pre, t_post, win_len = 0.05)
            res_match[it_post - 2, it_pre - 2] = sum(res)
            '''
            if sum(res) > 10:
                plt.scatter(t_post, res)
                plt.title('pre: ' + str(clust_pre) + ', post: ' + str(clust_post))
                plt.show()
            '''
    #np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/data/mat_match_groupA_V7265', res_match)
    return

''' #test about colorbar
fig, ax = plt.subplots()
c = ax.matshow(res_match, fignum=False)
cmap = mpl.cm.seismic
bounds = [0, 100, 500, 2000, 10000, 50000]
norm = mpl.colors.BoundaryNorm(bounds, cmap.N, extend='both')
fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), cax = ax )
'''

def plot_matshow(mat, v_bounds, title = 'Unnamed'):
    fig, ax = plt.subplots()
    c = ax.matshow(mat, cmap ='seismic',
                vmin = v_bounds[0],
                vmax = v_bounds[1],
                interpolation ='nearest',
                origin ='lower')
    
    fig.colorbar(c, ax = ax)
    ax.set_title(title)
    ax.set_xlabel('cortex cluster')
    ax.set_ylabel('striatum cluster')
    plt.show()

def frame_to_time(frame):
    '''
    for a given frame, return the mean of time captured during the frame (because multiple time point within a frame)
    '''
    time = np.mean(synchro.time[synchro.frame == frame])
    return time

def analyse1(params):
    #print('HEEEEEEEEEEEEEEEEEEEEERE ' + '\n' , clust_pre_A[1:])
    for clust_pre in clust_pre_A[1:][np.where(res_match > 1000)[1]]:
        striatum_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_pre]
        for clust_post in clust_post_A[np.where(res_match > 250)[0]]:
            cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_post]
            RES = stdp.integration.integration(cortex_times, striatum_times, y0init, params, 100, scipy_options= {'method': 'Radau'})
            plt.plot(RES[2], RES[0][:]/RES[0][0] * RES[1][:]/RES[1][0])
            plt.title('striatum: ' + str(clust_pre) + ', cortex: ' + str(clust_post))
            plt.show()

def analyse2(params, clust_pre, clust_post):
    if not any(f'{clust_pre}_{clust_post}' in name for name in computed_pair ):
        cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_pre]
        striatum_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_post]
        LOGGER.info('Computing  res_%s_%s', clust_pre, clust_post)
        RES = stdp.integration.integration(params, cortex_times, striatum_times, y0init, t_end = 100)#, scipy_options= {'method': 'Radau'})

        for prefix, data in (
            ('res_', RES[0:3]),
            ('sol_', RES[3])):
                name = f'{prefix}{clust_pre}_{clust_post}'
                log_numpy_data(data, name)

        return RES
    else:
        print(f'cluster {clust_pre}_{clust_post} already computed')

def analyse3(params, clust_pre, clust_post):
    cortex_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_pre]
    spike_times_vect = np.ceil(cortex_times*2500).to_numpy().astype(int)
    glu_array = 10*np.ones(len(cortex_times))
    lfp_array = mean_trace[spike_times_vect]
    #glu_array[lfp_array < - 100] = -2.703*lfp_array[lfp_array < -100] + 730
    #glu_array[lfp_array < - 100] = -4.054*lfp_array[lfp_array < -100] + 94.5
    #glu_array[lfp_array < - 100] = -5.135*lfp_array[lfp_array < -100] - 413.5
    glu_array[lfp_array < - 120] = -5.6857*lfp_array[lfp_array < -120] - 672
    striatum_times = spike_times.Spike_times[spike_times.Cluster_ID == clust_post]
    LOGGER.info('Computing  res_%s_%s', clust_pre, clust_post)
    RES = stdp.integration.integration(params, cortex_times, striatum_times, y0init, t_end = 100)#, glu_release = glu_array)#, scipy_options= {'method': 'Radau'})

    for prefix, data in (
        ('res_', RES[0:3]),
        ('sol_', RES[3])):
            name = f'{prefix}{clust_pre}_{clust_post}'
            log_numpy_data(data, name)

    return RES

def firing_rate(clust_array, t_bounds, win_len, win_step):
    '''
    donne le firing rate sur une fenêtre glissante de taille win_len
    '''
    #LOGGER.debug('clust array: ' + str(clust_array) + '  tbound:' + str(*t_bounds))
    res = np.zeros((len(clust_array), len(np.arange(t_bounds[0], t_bounds[1] - win_len, win_step))))
    it = 0
    for clust in clust_array:
        spike_array = spike_times.Spike_times[spike_times.Cluster_ID == clust]
        res[it, :] = moving_window(spike_array, t_bounds, win_len, win_step, len)
        it = it + 1
    return res

    '''
    plt.plot(RES[2], RES[0][:]/RES[0][0] * RES[1][:]/RES[1][0])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 100, colors=['green', 'green', 'green', 'red', 'red', 'green'])
    plt.title('produits des poids.  striatum: ' + str(clust_pre) + ', cortex: ' + str(clust_post))
    plt.show()

    plt.plot(RES[2], RES[0][:]/RES[0][0])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 100, colors=['green', 'green', 'green', 'red', 'red', 'green'])
    plt.title('Wpre.  striatum: ' + str(clust_pre) + ', cortex: ' + str(clust_post))
    plt.show()

    plt.plot(RES[2], RES[1][:]/RES[1][0])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 100, colors=['green', 'green', 'green', 'red', 'red', 'green'])
    plt.title('Wpost.  striatum: ' + str(clust_pre) + ', cortex: ' + str(clust_post))
    plt.show()
    '''

def num_firing(clust_array):
    res = np.zeros(len(clust_array))
    it = 0
    for clust in clust_array:
        res[it] = len(spike_times.Spike_times[spike_times.Cluster_ID == clust])
        it= it + 1
    return res

def plot_weight(path, c_pairing, args_fr = [[0, 1200], 5, 1]):
    for load_name in c_pairing:
        try:
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
        except:
            print(f'cluster {load_name[0]}_{int(load_name[1])} not found')
        else:
            #print('ready to plot {}_{}'.format(*load_name))
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < 2.7:
                RES_fr = firing_rate([*load_name[:]], *args_fr)
                res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)
                plt.plot(RES[2], RES[1][:]/RES[1][0])
                plt.plot(RES[2], RES[0][:]/RES[0][0])
                #plt.scatter(spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]][res_match >0], res_match[res_match >0]*5)#, c = res_match[res_match >0], cmap = 'seismic')
                #plt.plot(np.arange(args_fr[1]/2, 1200-args_fr[1]/2, args_fr[2]), RES_fr[0,:])#/np.max(RES_fr[0,:])*100)
                #plt.plot(np.arange(args_fr[1]/2, 1200-args_fr[1]/2, args_fr[2]), RES_fr[1,:])#/np.max(RES_fr[0,:])*100)
                plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 30, colors=['black', 'black', 'black', 'red', 'red', 'black'])
                plt.legend(['post weight', 'pre_weight', 'n spike that match', 'FR_cortex', 'FR_striatum'])#, 'firing rate post', 'firing_rate pre'])
                #plt.legend(['post weight', 'pre weight', '5n spike that match'])#, 'FR_cortex', 'FR_striatum'])#, 'firing rate post', 'firing_rate pre'])
                plt.title('cortex {}, striatum{}'.format(*load_name))
                plt.show()
            else:
                print('saturation')

def plot_weight_compare(path, path_compare, c_pairing, args_fr = [[0, 1200], 5, 1], selection = 2.7):
    for load_name in c_pairing:
        try:
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            RES_compare = np.load(f'{path_compare}res_{load_name[0]}_{int(load_name[1])}.npy')
        except:
            print(f'cluster {load_name[0]}_{int(load_name[1])} not found')
        else:
            #print('ready to plot {}_{}'.format(*load_name))
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            RES_compare = np.load(f'{path_compare}res_{load_name[0]}_{int(load_name[1])}.npy')
            if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < selection:
                RES_compare = np.load(f'{path_compare}res_{load_name[0]}_{int(load_name[1])}.npy')
                RES_fr = firing_rate([*load_name[:]], *args_fr)
                res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)
                plt.plot(RES[2], RES[1][:]/RES[1][0], color = 'black')
                plt.plot(RES[2], RES[0][:]/RES[0][0], color = 'black')

                plt.plot(RES_compare[2], RES_compare[1][:]/RES_compare[1][0], color = 'red')
                plt.plot(RES_compare[2], RES_compare[0][:]/RES_compare[0][0], color = 'blue')

                plt.scatter(spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]][res_match >0], res_match[res_match >0]*5)#, c = res_match[res_match >0], cmap = 'seismic')
                plt.plot(np.arange(args_fr[1]/2, 1200-args_fr[1]/2, args_fr[2]), RES_fr[0,:], color = 'green', alpha=0.7)#/np.max(RES_fr[0,:])*100)
                plt.plot(np.arange(args_fr[1]/2, 1200-args_fr[1]/2, args_fr[2]), RES_fr[1,:], color = 'orange', alpha = 0.7)#/np.max(RES_fr[0,:])*100)
                plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, 30, colors=['black', 'black', 'black', 'red', 'red', 'black'])
                #plt.legend(['post weight', 'pre_weight', 'n spike that match', 'FR_cortex', 'FR_striatum'])#, 'firing rate post', 'firing_rate pre'])
                #plt.legend(['post weight', 'pre weight', '5n spike that match'])#, 'FR_cortex', 'FR_striatum'])#, 'firing rate post', 'firing_rate pre'])
                plt.title('cortex {}, striatum{}'.format(*load_name))
                plt.show()
            else:
                print('saturation')

def verif_stdp(path_verif):
    w_pre = np.load('{}/line_test_pre_-15ms.npy'.format(path_verif))
    w_post = np.load('{}/line_test_post_-15ms.npy'.format(path_verif))

    plt.plot(np.arange(0, 100, 4) , w_pre[0], color = 'black')
    plt.plot(np.arange(0, 100, 4), w_post[0], color = 'black')

    path_verif = '/home/ahubert/Documents/Code_STDP/model-potcadep/data/model_verif' #/map_full_creneau_pre_test_pos'

    w_pre = np.load('{}/line_-15ms.npy'.format(path_verif))
    w_post = np.load('{}/line_-15ms.npy'.format(path_verif))

    plt.plot(w_pre[0], color = 'red')
    plt.plot(w_pre[1], color = 'red')

    w_pre = np.load('{}/map_full_creneau_pre.npy'.format(path_verif))
    w_post = np.load('{}/map_full_creneau_post.npy'.format(path_verif))
    plt.plot(w_pre[40], color = 'green')
    plt.plot(w_post[40], color = 'green')
    plt.plot(w_pre[41], color = 'blue')
    plt.plot(w_post[41], color = 'blue')    
    plt.show()

def inde_cluster_nosat(path, all_clust, args_fr = [[620, 720], 100, 100]):
    c_pre_list = all_clust[0]
    c_post_list = all_clust[1]
    res_striatum = np.zeros(len(c_pre_list))
    res_cortex = np.zeros(len(c_post_list))
    res_mat = np.zeros((len(c_pre_list), len(c_post_list)))

    fr_striatum = np.mean(firing_rate(c_pre_list, *args_fr), axis = 1)
    fr_sort_striatum = fr_striatum.argsort()
    fr_cortex = np.mean(firing_rate(c_post_list, *args_fr), axis = 1)
    fr_sort_cortex = fr_cortex.argsort()

    print(f'HERRRRRRRRRRRRRRE {len(resume.channel_A[resume.Team_A.isin(c_pre_list)])}')
    print(f'HERRRRRRRRRRRRRRE {len(resume.channel_A[resume.Team_A.isin(c_post_list)])}')
    chan_striatum = np.concatenate((resume.channel_A[resume.Team_A.isin(c_pre_list)].argsort(), resume['channel_B+'][resume['Team_B+'].isin(c_pre_list)].argsort()))
    chan_cortex = np.concatenate((resume['channel_A'][resume['Team_A'].isin(c_post_list)].argsort(), resume['channel_B+'][resume['Team_B+'].isin(c_post_list)].argsort()))
    #chan_sort_striatum = chan_striatum.argsort()
    #chan_sort_cortex = chan_cortex.argsort()

    print(fr_sort_cortex)
    print(fr_sort_striatum)
    for it_pre, c_pre in enumerate(c_pre_list):
        for it_post, c_post in enumerate(c_post_list):
            try:
                RES = np.load(f'{path}res_{c_pre}_{int(c_post)}.npy')
            except:
                a = 0
                #print(f'loading {c_pre}_{int(c_post)} failed')
            else:
                RES = np.load(f'{path}res_{c_pre}_{int(c_post)}.npy')
                if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < 2.8:
                    res_striatum[it_pre] += 1
                    res_cortex[it_post] += 1
                    #res_mat[fr_sort_striatum[it_pre], fr_sort_cortex[it_post]] = 1 #sorting by firing rate
                    #res_mat[chan_sort_striatum[it_pre], chan_sort_cortex[it_post]] = 1
                    res_mat[chan_striatum[it_pre], chan_cortex[it_post]] = 1
                    #res_mat[it_pre, it_post] = 1
    unique, counts = np.unique(res_striatum, return_counts=True)
    print(len(c_post_list))
    print(res_striatum)
    #print(unique)
    #print(counts)
    unique, counts = np.unique(res_cortex, return_counts=True)
    print(len(c_pre_list))
    print(res_cortex)
    #print(unique)
    #print(counts)
    plt.matshow(res_mat)
    plt.show()
    return ((res_striatum, res_cortex))

def pass_id_clust():
    id_clust_cortex = np.concatenate((resume.Team_A[resume.channel_A > 200], resume['Team_B+'][resume['channel_B+'] > 200]))
    id_clust_cortex = id_clust_cortex.astype(int)
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 616))
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 232))
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 1064))
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 345))
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 309))
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 325))
    id_clust_cortex = np.delete(id_clust_cortex, np.where(id_clust_cortex == 619))

    id_clust_striatum = np.concatenate((resume.Team_A[resume.channel_A < 180], resume['Team_B+'][resume['channel_B+'] < 180]))
    id_clust_striatum = id_clust_striatum.astype(int)
    id_clust_striatum = np.delete(id_clust_striatum, np.where(id_clust_striatum == 4))
    id_clust_striatum = np.delete(id_clust_striatum, np.where(id_clust_striatum == 95))
    id_clust_striatum = np.delete(id_clust_striatum, np.where(id_clust_striatum == 1378))

    return((id_clust_cortex, id_clust_striatum))

def analyse_pairing(path1, path2, all_clust):
    c_pre_list = all_clust[0]
    c_post_list = all_clust[1]
    res_striatum = np.zeros(len(c_pre_list))
    res_cortex = np.zeros(len(c_post_list))
    res_mat = np.zeros((len(c_pre_list), len(c_post_list)))

    for it_pre, c_pre in enumerate(c_pre_list):
        for it_post, c_post in enumerate(c_post_list):
            try:
                RES = np.load(f'{path1}res_{c_pre}_{int(c_post)}.npy')
            except:
                try:
                    RES = np.load(f'{path2}res_{c_pre}_{int(c_post)}.npy')
                except:
                    a = 0
                else:
                    RES = np.load(f'{path2}res_{c_pre}_{int(c_post)}.npy')
                    t_pre = spike_times.Spike_times[spike_times.Cluster_ID ==  c_pre]
                    t_post = spike_times.Spike_times[spike_times.Cluster_ID ==  c_post]
                    spike_matched = match_timed(t_pre, t_post)
                    id_match = np.where(spike_matched < 0.03)[0]
                    w_pre_var = np.zeros(len(id_match))
                    w_post_var = np.zeros(len(id_match))
                    
                    for it, i in enumerate(id_match):
                        ind_start = np.max(np.where(RES[2] == max(t_post[t_post.index[i]] - spike_matched[i], t_post[t_post.index[i]]) )[0])
                        ind_stop = np.max(np.where(RES[2] < (max(t_post[t_post.index[i]] - spike_matched[i], t_post[t_post.index[i]])+1))[0])
                        w_pre_var[it] = RES[0][ind_stop]-RES[0][ind_start]
                        w_post_var[it] = RES[1][ind_stop]-RES[1][ind_start]
                    plt.scatter(spike_matched[id_match], w_pre_var)
                    plt.show()
            else:
                RES = np.load(f'{path1}res_{c_pre}_{int(c_post)}.npy')
                t_pre = spike_times.Spike_times[spike_times.Cluster_ID ==  c_pre]
                t_post = spike_times.Spike_times[spike_times.Cluster_ID ==  c_post]
                spike_matched = match_timed(t_pre, t_post)
                id_match = np.where(spike_matched < 0.03)[0]
                w_pre_var = np.zeros(len(id_match))
                w_post_var = np.zeros(len(id_match))
                
                for it, i in enumerate(id_match):
                    ind_start = np.where(RES[2] == max(t_post[t_post.index[i]] - spike_matched[i], t_post[t_post.index[i]]) )[0]
                    ind_stop = np.max(np.where(RES[2] < (max(t_post[t_post.index[i]] - spike_matched[i], t_post[t_post.index[i]])+1) ))
                    w_pre_var[it] = RES[0][ind_stop]-RES[0][ind_start]
                    w_post_var[it] = RES[1][ind_stop]-RES[1][ind_start]
                plt.scatter(spike_matched[id_match], w_pre_var)
                plt.show()
    return

def pass_times():
    clust_array = pass_id_clust()[0]
    spike_times_array = spike_times.Spike_times[spike_times.Cluster_ID.isin(clust_array)]
    return spike_times_array

def pass_no_sat(path, c_pairing, selection):
    no_sat_list = ((),)
    for load_name in c_pairing:
        try:
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
        except:
            print(f'cluster {load_name[0]}_{int(load_name[1])} not found')
        else:
            #print('ready to plot {}_{}'.format(*load_name))
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < selection:
                no_sat_list += (load_name,)
    return no_sat_list[1:]

def pass_no_sat_bool(path, c_pairing, selection):
    no_sat_list = [False]*(len(c_pairing))
    for it, load_name in enumerate(c_pairing):
        try:
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
        except:
            print(f'cluster {load_name[0]}_{int(load_name[1])} not found')
        else:
            #print('ready to plot {}_{}'.format(*load_name))
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < selection:
                no_sat_list[it] = True
    return no_sat_list

def pass_no_sat_wpre_bool(path, c_pairing, selection):
    no_sat_list = [False]*(len(c_pairing))
    for it, load_name in enumerate(c_pairing):
        try:
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
        except:
            print(f'cluster {load_name[0]}_{int(load_name[1])} not found')
        else:
            #print('ready to plot {}_{}'.format(*load_name))
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            ind = np.logical_and(RES[2]>(1319-700-400), RES[2]<(1319-700))
            ind_0 = np.logical_and(RES[2]>(1319-700-400), RES[2]<(1319-700))
            ind_1 = np.logical_and(RES[2]>(1350-700), RES[2]<(1350-700+100))
            #if np.sum(RES[0][ind_0]*(RES[2][1:] - RES[2][:-1])[ind_0[:-1]])/400 < selection:
            if (np.sum(RES[0][ind_0]*(RES[2][1:] - RES[2][:-1])[ind_0[:-1]])/400)**1.5 < (np.sum(RES[0][ind_1]*(RES[2][1:] - RES[2][:-1])[ind_1[:-1]])/100) :
                #if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < 2.7:
                #print('hello')
                no_sat_list[it] = True
    return no_sat_list

def plasticity_dependant_task(c_pairing):
    for c_pre, c_post in c_pairing:
        print('nothing')
    return 0

################################################################ UTILS #####################################################
''' #SHOW CLASS
class Test():
    FIRING_RATE = 1000

    def __init__(self, param1, param2, param3='foo', firing_rate=None):
        self.param1 = param1
        self.param2 = param2

        if firing_rate is None:
            firing_rate = self.FIRING_RATE
        self.firing_rate = firing_rate

    def change_param1(self, value):
        self.param1 = value

    def do_something_with_param1(self):
        print(self.param1)

test_obj = Test(1, 2, param=3)
test_obj.param2 = 6
test_obj2 = Test(6,7)
test_obj2.FIRING_RATE = 200
print(Test.FIRING_RATE)
print(test_obj.FIRING_RATE)

def get_artifacts_dir(exp_id, run_id):
    mlruns_dir = pathlib.Path('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns')
    return mlruns_dir / f'{exp_id}/{run_id}/artifacts'

mlruns_dir = pathlib.Path('/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns')
for path in mlruns_dir.glob('*/*/artifacts/res_*_*.ext'):
    stem = path.stem
    _, n1, n2 = stem.split('_', 2)

#path_1000 = get_artifacts_dir('562231799100305614', 'bc97e6b979ee4fce88657cb526b77053')
'''

#[ 616 ,232 ,1064 ,345 ,309 ,325 ,619 , 4 , 95, 1378] #cluster très gros (>9000)
#path = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/9cd83d3e24a24d89a1f497319dce8990/artifacts/' #first compute
#path_res = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/c0e5c86086094414845dacbe345155f4/artifacts/' #the good one (with pos)
#path = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/26d13bd714b2413a853c44a64b7d31ce/artifacts/'
path_zero = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/11e16d7c84494374b4a09c63d29de957/artifacts/' #juste to ref, zeros
#path = '/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_mean_post_V2_pos.npy' #path to map of min w post
#path1 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/ae955f676b8643d183b4bfb86f0c15a0/artifacts/' #clust A and B+ cut w_pre no sat
#path2 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/dcd9c193275040ca9c7aae651f30f199/artifacts/' #remainning clust from A and B+
#path = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/33b990cf455c4d059d5f3ea6dce2bf7f/artifacts/'

#path_ref = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/f11e6da016ef45ad9a6a07f386214813/artifacts/' #Brain in good way(I thought)
path_1000 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/bc97e6b979ee4fce88657cb526b77053/artifacts/' #LFP MODULATION 1000
path_500 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/69fb0c65db64494090d0134ed21f0fc0/artifacts/' #LFP 500
path_100 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/10569951cd564531a32218e1a4805372/artifacts/' #LFP 100
path_10_compare = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/7b58d6f9671945e2ab509bc2f6e8a473/artifacts/' # no LFP
#path_10 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/2e4f5a0abcc4477d8b6f8806da766565/artifacts/' #LFP 10
#path_10 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/72e7ec229fbc41b2b3111e1654a82146/artifacts' #LFP 10 mais plus
path_10 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/2b5a317a9f024521af21d2bb754beec9/artifacts/' #LFP 10 all
#computed_pair = os.listdir(path)

#path_verif = '/home/ahubert/Documents/Code_STDP/model-potcadep/data/verif_pos'
#path = '/home/ahubert/Documents/Code_STDP/model-potcadep/data/map_stdp_hz/'

#WRONG SIDE c_pre_list = np.array([650, 849, 747, 148, 37, 86, 111, 628, 826, 828, 11, 832, 970, 922, 1434, 1381, 436, 1352, 1338, 1312, 1287, 158, 143, 1337, 1388, 110, 1416, 108 ,982])
#WRONG SIDE c_post_list = np.array([616, 1091 , 243, 411, 362, 1211, 208, 268, 1021, 196, 224, 1054, 320, 324, 457, 1132, 192, 1045, 378, 732, 400, 1135, 240, 255, 261, 279, 1149, 1148, 1142, 1123, 358, 1153, 685, 506, 540, 532, 529, 1190, 1248])

#c_pre_list = [1091,239,684,243,411,1211, 362, 208,268,1021,196,224,1054,320,324,457,1132,192,1045,378,732,400,1135,240,255,261,279,1149,1148,1142,1123,358,1153,685,506,540,532,529,1190,1248]
#c_post_list = [636,626,650,849,747,657, 148, 37,86,111,628,826,828,11,832,970,922,1434,1381,436,1352,1338,1312,1287,158,143,1337,1388,110,1416,108,982]

#c_pre_list = [1091,1211,362, 208,268,1021,196,224,1054,320] #72e7ec229fbc41b2b3111e1654a82146
#c_post_list = [650,849,747,148, 37,86,111,628,826,828] #72e7ec229fbc41b2b3111e1654a82146

c_pre_list = [324,457,1132,192,1045,378,732,400,1135,240,255,261,279,1149,1148,1142,1123,358,1153,685,506,540,532,529,1190,1248,1091,1211,362, 208,268,1021,196,224,1054,320, 239,684,243,411]
c_post_list = [11,832,970,922,1434,1381,436,1352,1338,1312,1287,158,143,1337,1388,110,650,849,747,148, 37,86,111,628,826,828,1416,108,982,636,626,657]

c_pairing = [(c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list]

#box_col = ['pink', 'lightgreen', 'lightblue']

############################################################## MAIN APPEL ##################################################
#mat_max_post = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_max_post.npy')
#mat_mean_post = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_mean_post.npy')
#print(np.where(mat_max_post<2.8))

#PLOT OF SUPER BOXPLOT TO SEND
#args_fr = [[0, 1200], 10, 1]
#mat_wpre = np.zeros((3, len(c_pre_list), len(c_post_list)))
#mat_wpost = np.zeros((3, len(c_pre_list), len(c_post_list)))

#[0,1200], 0.2, 0.04 # params for blabla1
#blabla([0,1200], 0.4, 0.08 )
#c_pairing = [(650, 378), (650, 400)]

#plot_weight(path, c_pairing, args_fr = [[0, 1200], 5, 1])

#plot_weight_compare(path_10, path_10_compare, c_pairing, args_fr = [[0, 1200], 5, 1], selection=5)

#inde_cluster_nosat(path1, pass_id_clust())
#analyse_pairing(path1, path2, pass_id_clust())
#verif_stdp(path_verif)


w_pre = np.zeros((2, len(c_pairing)))
w_mid = np.zeros((2, len(c_pairing)))
w_post = np.zeros((2, len(c_pairing)))


''' #Only increase
for it, load_name in enumerate(c_pairing):
    try:
        RES = np.load('{}res_{}_{}.npy'.format(path_10, *load_name))

        ind = np.array(RES[2]<(1319-700))
        w_pre[0, it] = RES[0][ind][-1] - RES[0][ind][0]
        w_pre[1, it] = RES[1][ind][-1] - RES[1][ind][0]

        ind = np.logical_and(RES[2]>(1319-700), RES[2]<(1350-700))
        w_mid[0, it] = RES[0][ind][-1] - RES[0][ind][0]
        w_mid[1, it] = RES[1][indf pass_no_sat_wpre_bool(path, c_pairing, selection):
    no_sat_list = [False]*(len(c_pairing))
    for it, load_name in enumerate(c_pairing):
        try:
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
        except:
            print(f'cluster {load_name[0]}_{int(load_name[1])} not found')
        else:
            #print('ready to plot {}_{}'.format(*load_name))
            RES = np.load(f'{path}res_{load_name[0]}_{int(load_name[1])}.npy')
            ind = np.logical_and(RES[2]>(1319-700-400), RES[2]<(1319-700))
            ind_0 = np.logical_and(RES[2]>(1319-700-400), RES[2]<(1319-700))
            ind_1 = np.logical_and(RES[2]>(1350-700), RES[2]<(1350-700+100))
            #if np.sum(RES[0][ind_0]*(RES[2][1:] - RES[2][:-1])[ind_0[:-1]])/400 < selection:
            if (np.sum(RES[0][ind_0]*(RES[2][1:] - RES[2][:-1])[ind_0[:-1]])/400)**1.5 < (np.sum(RES[0][ind_1]*(RES[2][1:] - RES[2][:-1])[ind_1[:-1]])/100) :
                #if np.max(RES[1][np.where(RES[2]< 600)])/RES[1][0] < 2.7:
                #print('hello')
                no_sat_list[it] = True
    return no_sat_list][-1] - RES[1][ind][0]

        ind = np.logical_and(RES[2]>(1350-700), RES[2]<1100)
        w_post[0, it] = RES[0][ind][-1] - RES[0][ind][0]
        w_post[1, it] = RES[1][ind][-1] - RES[1][ind][0]

    except:
        print('HERE IS SOME EXCEPTION THAT I DONT LIKE')
        print('bug on {}_{} pairing'.format(*load_name))
        w_pre[0, it] = np.NaN
        w_mid[0, it] = np.NaN
        w_post[0, it] = np.NaN
        w_pre[1, it] = np.NaN
        w_mid[1, it] = np.NaN
        w_post[1, it] = np.NaN
'''

 #CLUST FIXED IE BOXPLOT FOR MAX SAT & NOT SAT
#fig, (ax1, ax2) = plt.subplots(1,2)
def presentation_plot3D():
    for it, load_name in enumerate(c_pairing):
        try:
            RES = np.load('{}res_{}_{}.npy'.format(path_10, *load_name))
            #RES_fr = firing_rate([*load_name[:]], *args_fr)
            #res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)

            #ind = np.array(RES[2]<(1319-700))
            ind = np.logical_and(RES[2]>(1319-700-400), RES[2]<(1319-700))
            w_pre[0, it] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/400#(1319-700)
            w_pre[1, it] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/400#(1319-700)

            ind = np.logical_and(RES[2]>(1319-700), RES[2]<(1350-700))
            w_mid[0, it] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(31)
            w_mid[1, it] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(31)

            ind = np.logical_and(RES[2]>(1350-700), RES[2]<1350-700+100)
            w_post[0, it] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/100 #(450-100)
            w_post[1, it] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/100 #(450-100)

        except:
            print('bug on {}_{} pairing'.format(*load_name))
            w_pre[0, it] = np.NaN
            w_mid[0, it] = np.NaN
            w_post[0, it] = np.NaN
            w_pre[1, it] = np.NaN
            w_mid[1, it] = np.NaN
            w_post[1, it] = np.NaN
    
    lab_dict = {'x':'mean weight post syn before contact',
                'y':'mean weight pre syn before contact',
                'z':'mean weight pre syn, after contact',
                'color': 'mean weight post syn, after contact'}
    #df = pd.DataFrame( (w_pre[1, ~np.isnan(w_pre[1,:])], w_pre[0, ~np.isnan(w_pre[0,:])], w_post[0, ~np.isnan(w_post[0,:])]))#, columns=['wpre1, wpre0, wpost0'])
    fig = px.scatter_3d(x = w_pre[1, ~np.isnan(w_pre[1,:])], y = w_pre[0, ~np.isnan(w_pre[0,:])], z = w_post[0, ~np.isnan(w_post[0,:])],
                        color = w_post[1, ~np.isnan(w_post[1,:])], labels=lab_dict)#, x='sepal_length', y='sepal_width', z='petal_width', color='species')
    fig.show()

''' #SCATTER 3D MATPLOTLIB
fig = plt.figure()
ax = fig.add_subplot(projection='3d')

xs = w_pre[1, :]
ys = w_post[0, :]
zs = w_post[0, :]

ax.scatter(xs, ys, zs)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
plt.show()
'''

#fig, (ax1, ax2) = plt.subplots(1,2)
'''
pre_all = [w_pre[0, ~np.isnan(w_pre[0,:])], w_pre[1, ~np.isnan(w_pre[1,:])]]
mid_all = [w_mid[0, ~np.isnan(w_mid[0,:])], w_mid[1, ~np.isnan(w_mid[1,:])]]
post_all = [w_post[0, ~np.isnan(w_post[0,:])], w_post[1, ~np.isnan(w_post[1,:])]]
box1 = ax1.boxplot( [pre_all[0], mid_all[0], post_all[0]])
box2 = ax2.boxplot( [pre_all[1], mid_all[1], post_all[1]])
ax1.set_title('mean of presynaptic weight')
ax2.set_title('mean of postsynaptic weight')
plt.show()
'''

'''
#selec_max3_list =  selec_max3[0]*len(c_post_list) + selec_max3[1]
#selec_list_2 = pass_no_sat_bool(path_10, c_pairing, 2.7)
selec_list = pass_no_sat_wpre_bool(path_10, c_pairing, 3)
pre_sat = [w_pre[0, ~np.isnan(w_pre[0,:])], w_pre[1, ~np.isnan(w_pre[1,:])]]
#pre_sat = [w_pre[0, selec_list_2], w_pre[1, selec_list_2]]
pre_all = [w_pre[0, selec_list], w_pre[1, selec_list]]

mid_sat = [w_mid[0, ~np.isnan(w_mid[0,:])], w_mid[1, ~np.isnan(w_mid[1,:])]]
#mid_sat = [w_mid[0, selec_list_2], w_mid[1, selec_list_2]]
mid_all = [w_mid[0, selec_list], w_mid[1, selec_list]]

post_sat = [w_post[0, ~np.isnan(w_post[0,:])], w_post[1, ~np.isnan(w_post[1,:])]]
#post_sat = [w_post[0, selec_list_2], w_post[1, selec_list_2]]
post_all = [w_post[0, selec_list], w_post[1, selec_list]]

box1 = ax1.boxplot( [pre_sat[0], pre_all[0], mid_sat[0], mid_all[0], post_sat[0], post_all[0]], positions=[1,2,4,5,7,8])
box2 = ax2.boxplot( [pre_sat[1], pre_all[1], mid_sat[1], mid_all[1], post_sat[1], post_all[1]], positions=[1,2,4,5,7,8])
plt.show()
plt.scatter(pre_sat[0], post_sat[0], color = 'blue')
plt.scatter(pre_all[0], post_all[0], color = 'red')
plt.show()
plt.scatter(pre_sat[1], post_sat[1], color = 'blue')
plt.scatter(pre_all[1], post_all[1], color = 'red')
plt.show()
'''

'''
ind_650 = np.where(c_pre_list == 650)[0]
ind_378 = ind_650*len(c_post_list) + np.where(c_post_list == 378)[0]
ind_400 = ind_650*len(c_post_list) + np.where(c_post_list == 400)[0]

pre_650_378 = [w_pre[0, ind_378], w_pre[1, ind_378]]
pre_650_400 = [w_pre[0, ind_400], w_pre[1, ind_400]]

mid_650_378 = [w_mid[0, ind_378], w_mid[1, ind_378]]
mid_650_400 = [w_mid[0, ind_400], w_mid[1, ind_400]]

post_650_378 = [w_post[0, ind_378], w_post[1, ind_378]]
post_650_400 = [w_post[0, ind_400], w_post[1, ind_400]]

ax1.hlines(pre_65id_labels_striatum0_378[0], 0.5, 2.5, color = 'red')
ax1.hlines(mid_650_378[0], 3.5, 5.5, color = 'red')
ax1.hlines(post_650_378[0], 6.5, 8.5, color = 'red')
ax1.hlines(pre_650_400[0], 0.5, 2.5, color = 'blue')
ax1.hlines(mid_650_400[0], 3.5, 5.5, color = 'blue')
ax1.hlines(post_650_400[0], 6.5, 8.5, color = 'blue')

ax2.hlines(pre_650_378[1], 0.5, 2.5, color = 'red')
ax2.hlines(mid_650_378[1], 3.5, 5.5, color = 'red')
ax2.hlines(post_650_378[1], 6.5, 8.5, color = 'red')
ax2.hlines(pre_650_400[1], 0.5, 2.5, color = 'blue')
ax2.hlines(mid_650_400[1], 3.5, 5.5, color = 'blue')
ax2.hlines(post_650_400[1], 6.5, 8.5, color = 'blue')

ax1.set_xticks([1.5,3.5,5.5],['before', 'during', 'after'])
ax1.set_title('mean of presynaptic weight')
ax2.set_xticks([1.5,3.5,5.5], ['before' , 'during', 'after'])
ax2.set_title('mean of postsynaptic weight')
'''
plt.show()



'''#MEAN WEIGHT ALL CLUST
for count, load_name in enumerate(c_pairing):
    try:
        RES = np.load('{}res_{}_{}.npy'.format(path, *load_name))
        #RES_fr = firing_rate([*load_name[:]], *args_fr)
        #res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)

        ind = np.array(RES[2]<(1319-700))
        mat_wpre[0, count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(1319-700)
        mat_wpost[0, count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(1319-700)

        ind = np.logical_and(RES[2]>(1319-700), RES[2]<(1350-700))
        mat_wpre[1, count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(31)
        mat_wpost[1, count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(31)

        ind = np.logical_and(RES[2]>(1350-700), RES[2]<1100)
        mat_wpre[2, count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(450)
        mat_wpost[2, count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(450)
        
    except:
        print('HERE IS SOME EXCEPTION THAT I DONT LIKE')
        print('bug on {}_{} pairing'.format(*load_name))
        mat_wpre[:, count//len(c_post_list), count%len(c_post_list)] = np.NaN
        mat_wpost[:, count//len(c_post_list), count%len(c_post_list)] = np.NaN

#plt.scatter(np.ravel(mat_wpre[0,:,:])[~np.isnan(np.ravel(mat_wpre[0,:,:]))], np.ravel(mat_wpost[0,:,:])[~np.isnan(np.ravel(mat_wpost[0,:,:]))], color = 'black')
#plt.scatter(np.ravel(mat_wpre[1,:,:])[~np.isnan(np.ravel(mat_wpre[1,:,:]))], np.ravel(mat_wpost[1,:,:])[~np.isnan(np.ravel(mat_wpost[1,:,:]))], color = 'red')
#plt.scatter(np.ravel(mat_wpre[2,:,:])[~np.isnan(np.ravel(mat_wpre[2,:,:]))], np.ravel(mat_wpost[2,:,:])[~np.isnan(np.ravel(mat_wpost[2,:,:]))], color = 'blue')
plt.show()
'''

'''# CLUST FIXED IE BOXPLOT
for rep in range(3):
    if rep == 0:
        c_pairing = [(650, c_post) for c_post in c_post_list]
        w_pre = np.zeros((2, len(c_post_list)))
        w_mid = np.zeros((2, len(c_post_list)))
        w_post = np.zeros((2, len(c_post_list)))
    if rep == 1:
        c_pairing = [(c_pre, 378) for c_pre in c_pre_list]
        w_pre = np.zeros((2, len(c_pre_list)))
        w_mid = np.zeros((2, len(c_pre_list)))
        w_post = np.zeros((2, len(c_pre_list)))
    if rep == 2:
        c_pairing = [(c_pre, 400) for c_pre in c_pre_list]
        w_pre = np.zeros((2, len(c_pre_list)))
        w_mid = np.zeros((2, len(c_pre_list)))
        w_post = np.zeros((2, len(c_pre_list)))

    for it, load_name in enumerate(c_pairing):
        try:
            RES = np.load('{}res_{}_{}.npy'.format(path, *load_name))
            RES_fr = firing_rate([*load_name[:]], *args_fr)
            res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)

            ind = np.array(RES[2]<(1319-700))
            w_pre[0, it] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(1319-700)
            w_pre[1, it] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(1319-700)

            ind = np.logical_and(RES[2]>(1319-700), RES[2]<(1350-700))
            w_mid[0, it] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(31)
            w_mid[1, it] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(31)

            ind = np.logical_and(RES[2]>(1350-700), RES[2]<1100)
            w_post[0, it] = np.sum(RES[0][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(450)
            w_post[1, it] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(450)
            
        except:
            print('HERE IS SOME EXCEPTION THAT I DONT LIKE')
            print('bug on {}_{} pairing'.format(*load_name))
            w_pre[0, it] = np.NaN
            w_mid[0, it] = np.NaN
            w_post[0, it] = np.NaN
            w_pre[1, it] = np.NaN
            w_mid[1, it] = np.NaN
            w_post[1, it] = np.NaN
    if rep == 0:
        pre_650_378 = [w_pre[0, np.where(c_post_list == 378)], w_pre[1, np.where(c_post_list == 378)]]
        pre_650_400 = [w_pre[0, np.where(c_post_list == 400)], w_pre[1, np.where(c_post_list == 400)]]

        mid_650_378 = [w_mid[0, np.where(c_post_list == 378)], w_mid[1, np.where(c_post_list == 378)]]
        mid_650_400 = [w_mid[0, np.where(c_post_list == 400)], w_mid[1, np.where(c_post_list == 400)]]

        post_650_378 = [w_post[0, np.where(c_post_list == 378)], w_post[1, np.where(c_post_list == 378)]]
        post_650_400 = [w_post[0, np.where(c_post_list == 400)], w_post[1, np.where(c_post_list == 400)]]

    box1 = ax1.boxplot( [w_pre[0, ~np.isnan(w_pre[0,:]) ], w_mid[0, ~np.isnan(w_mid[0, :])], w_post[0, ~np.isnan(w_post[0, :])]], positions=[1+rep, 5+rep, 9+rep], labels = ['650', '378', '400'], patch_artist=True )
    box1['boxes'][0].set_facecolor(box_col[rep])
    box1['boxes'][1].set_facecolor(box_col[rep])
    box1['boxes'][2].set_facecolor(box_col[rep])
    box2 = ax2.boxplot( [w_pre[1, ~np.isnan(w_pre[1,:]) ], w_mid[1, ~np.isnan(w_mid[1, :])], w_post[1, ~np.isnan(w_post[1, :])]], positions=[1+rep, 5+rep, 9+rep], labels = ['650', '378', '400'], patch_artist=True )
    box2['boxes'][0].set_facecolor(box_col[rep])
    box2['boxes'][1].set_facecolor(box_col[rep])
    box2['boxes'][2].set_facecolor(box_col[rep])

ax1.set_xticks([2,6,10],['before task', 'during task', 'after task'])
ax1.set_title('mean of presynaptic weight')
ax1.hlines(pre_650_378[0], 0.5, 3.5, color = 'red')
ax1.hlines(mid_650_378[0], 4.5, 7.5, color = 'red')
ax1.hlines(post_650_378[0], 8.5, 11.5, color = 'red')
ax1.hlines(pre_650_400[0], 0.5, 3.5, color = 'blue')
ax1.hlines(mid_650_400[0], 4.5, 7.5, color = 'blue')
ax1.hlines(post_650_400[0], 8.5, 11.5, color = 'blue')
ax2.set_xticks([2,6,10], ['before task', 'during task', 'after task'])
ax2.set_title('mean of postsynaptic weight')
ax2.hlines(pre_650_378[1], 0.5, 3.5, color = 'red')
ax2.hlines(mid_650_378[1], 4.5, 7.5, color = 'red')
ax2.hlines(post_650_378[1], 8.5, 11.5, color = 'red')
ax2.hlines(pre_650_400[1], 0.5, 3.5, color = 'blue')
ax2.hlines(mid_650_400[1], 4.5, 7.5, color = 'blue')
ax2.hlines(post_650_400[1], 8.5, 11.5, color = 'blue')
plt.show()
'''

############################################################################################################################

''' #PLOT DIFF ON DERIVATIVE WHEN NEG FIX TO 0 
no17 = np.array([0,1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,18,19,20,21,22,23,24,25,26,27,29])
VECTOR_ELEMENT_NAMES = ['B1', 'B2', 'B3' , 'B4' , 'B5' , 'B6' , 'B7' , 'B8' , 'B9' , 'B10' , 'B11' , 'B12' , 'B13',
                        'Ca_cyt' , 'Ca_ER' , 'h' , 'IP3' , 'V' , 'rAMPA' , 'rNMDA' ,
                        'PP1' , 'I1P' , 'DAG', 'DAGLP' , '2-AG' , 'fpre' , 'AEA' , 'Glu' , 'Curr_spikepost', 'cst']
for load_name in c_pairing:
    #try:
    RES = np.load('{}res_{}_{}.npy'.format(path, *load_name))
    SOL = np.load('{}sol_{}_{}.npy'.format(path, *load_name))
    ind_list = np.where(np.min(SOL[no17, :], axis = 0) < 0)[0]
    deriv_on = np.zeros((30, len(ind_list)))
    deriv_off = np.zeros((30, len(ind_list)))
    deriv_pos_on = np.zeros((30, len(ind_list))) 
    deriv_pos_off = np.zeros((30, len(ind_list)))
    for it, ind in enumerate(ind_list):
        deriv_off[: ,it] = stdp.synapseModel.STDP(0, SOL[:, ind], 0)
        deriv_on[:, it] = stdp.synapseModel.STDP(0, SOL[:, ind], 1)
        y0 = np.zeros(30)
        y0[:] = SOL[:, ind]
        y0[np.where(y0<0)] = 1e-100
        deriv_pos_off[: ,it] = stdp.synapseModel.STDP(0, y0, 0)
        deriv_pos_on[: ,it] = stdp.synapseModel.STDP(0, y0, 1)
    for it in range(30):
        plt.plot(deriv_on[it, :])
        plt.plot(deriv_off[it, :])
        plt.plot(deriv_pos_on[it, :])
        plt.plot(deriv_pos_off[it, :])
        plt.title(VECTOR_ELEMENT_NAMES[it])
        plt.legend(['on', 'off', 'pos on', 'pos off'])
        plt.show()

    #except:
    #    print('{}_{}'.format(*load_name))
'''

'''#PLOT FREQ MAPS
path = '/home/ahubert/Documents/Code_STDP/model-potcadep/data/map_stdp_hz/'
for freq in np.arange(1, 21, 2):
    mat_res_pre = np.load('{}map_pre_{}hz_mono.npy'.format(path, freq))
    plot_matshow(mat_res_pre*2, v_bounds = [0,2], title = f'frequence {freq}')
    mat_res_post = np.load('{}map_post_{}hz_mono.npy'.format(path, freq))
    plot_matshow(mat_res_post, v_bounds = [0,2], title = f'frequence {freq}')
'''

'''# COMPUTE MAP OF MAX WPOST
t_bounds = [0, 1200]
win_len = 10
win_step = 1
#c_pairing = ((c_pre_list[selec_mean[0, it]], c_post_list[selec_mean[1, it]]) for it in range(selec_mean.shape[1]))
#c_pairing = [(650, 378), (650, 400), (650, 255), (650, 685)]
mat_max_post = np.zeros((len(c_pre_list),len(c_post_list)))
#for load_name in ((path_res,c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list):
for count, load_name in enumerate((c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list):

#mat_max_post = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_max_post.npy')

    
#for load_name in c_pairing:
    #RES = np.load('{}res_{}_{}.npy'.format(*load_name))
    #RES = np.load('{}res_{}_{}.npy'.format(path_res, *load_name))
    #SOL = np.load('{}sol_{}_{}.npy'.format(path_res, *load_name))
    try:
        RES = np.load('{}res_{}_{}.npy'.format(path, *load_name))
        SOL = np.load('{}sol_{}_{}.npy'.format(path, *load_name))
        #RES_fr = firing_rate([*load_name[:]], t_bounds, win_len, win_step)
        #res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)
        if RES[2][-1]>1100:
            ind = np.array(RES[2]<(1319-700))
            mat_max_post[count//len(c_post_list), count%len(c_post_list)] = np.max(RES[1])
        else:
            mat_max_post[count//len(c_post_list), count%len(c_post_list)] = np.max(RES[1])
    except:
        mat_max_post[count//len(c_post_list), count%len(c_post_list)] = 5
np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_max_post', mat_max_post)
plot_matshow(mat_max_post, v_bounds=[0,5], title='max value of post weight before the experience')
print(np.where(mat_max_post < 3))
'''


'''# COMPUTE MAP OF MEAN WPOST
t_bounds = [0, 1200]
win_len = 10
win_step = 1
#c_pairing = ((c_pre_list[selec_mean[0, it]], c_post_list[selec_mean[1, it]]) for it in range(selec_mean.shape[1]))
#c_pairing = [(650, 378), (650, 400), (650, 255), (650, 685)]
mat_mean_post = np.zeros((len(c_pre_list),len(c_post_list)))
#for load_name in ((path_res,c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list):
for count, load_name in enumerate((c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list):

mat_mean_post = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_mean_post.npy')

    
#for load_name in c_pairing:
    #RES = np.load('{}res_{}_{}.npy'.format(*load_name))
    #RES = np.load('{}res_{}_{}.npy'.format(path_res, *load_name))
    #SOL = np.load('{}sol_{}_{}.npy'.format(path_res, *load_name))
    try:
        RES = np.load('{}res_{}_{}.npy'.format(path, *load_name))
        SOL = np.load('{}sol_{}_{}.npy'.format(path, *load_name))
        #RES_fr = firing_rate([*load_name[:]], t_bounds, win_len, win_step)
        #res_match = match_pre_post(spike_times.Spike_times[spike_times.Cluster_ID == load_name[0]], spike_times.Spike_times[spike_times.Cluster_ID == load_name[1]], 0.05)
        if RES[2][-1]>1100:
            ind = np.array(RES[2]<(1319-700))
            mat_mean_post[count//len(c_post_list), count%len(c_post_list)] = np.sum(RES[1][ind]*(RES[2][1:] - RES[2][:-1])[ind[:-1]])/(1319-700)
        else:
            mat_mean_post[count//len(c_post_list), count%len(c_post_list)] = 5
    except:
        mat_mean_post[count//len(c_post_list), count%len(c_post_list)] = 5
np.save('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_mean_post', mat_mean_post)
plot_matshow(mat_mean_post, v_bounds=[0,5], title='mean value of post weight before the experience')
print(np.where(mat_mean_post<2.5))
'''

''' #PLOT MATLAB
dtstdp = -0.015
T_data = scipy.io.loadmat('/home/ahubert/Documents/model_stdp/T_dtstdp-15_ns45.mat')
Y_data = scipy.io.loadmat('/home/ahubert/Documents/model_stdp/Y_dtstdp-15_ns45.mat')
T_mat = np.array((T_data['T']))
T_mat = T_mat - 1 - 0.01499 + dtstdp
Y_mat = np.transpose(np.array((Y_data['Y'])))
fpre_inf = Y_mat[25,:] #pre_inf is the presynaptic weight
CaMKact_inf = Y_mat[0,:] + 2*np.sum(Y_mat[1:4, :], axis=0) + 3*np.sum(Y_mat[4:8, :], axis=0) + 4*np.sum(Y_mat[8:11, :], axis=0) + 5*Y_mat[11,:] + 6*Y_mat[12,:]
CaMKact_inf=1+2*CaMKact_inf/164.6
plt.plot(T_mat, fpre_inf, color = 'red')
plt.plot(T_mat, CaMKact_inf, color = 'red')

t_pre = np.arange(0,45)
t_post = t_pre +dtstdp
res = stdp.integration.integration(t_pre, t_post, y0init, 100)
plt.plot(res[2], res[0], color = 'black')
plt.plot(res[2], res[1], color = 'black')
'''


''' #path of the big experiment:
path_res2 = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/9cd83d3e24a24d89a1f497319dce8990/artifacts/' #first compute
#path_res = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/9e64b92026104c8db2333986328ec16a/artifacts/'
path_res = '/home/ahubert/Documents/Code_STDP/model-potcadep/mlruns/562231799100305614/c0e5c86086094414845dacbe345155f4/artifacts/' #the good one (with pos)

t_bounds = [0, 1200]
c_pre_list = np.array([650, 849, 747, 148, 37, 86, 111, 628, 826, 828, 11, 832, 970, 922, 1434, 1381, 436, 1352, 1338, 1312, 1287, 158, 143, 1337, 1388, 110, 1416, 108 ,982])
c_post_list = np.array([616, 1091 , 243, 411, 362, 1211, 208, 268, 1021, 196, 224, 1054, 320, 324, 457, 1132, 192, 1045, 378, 732, 400, 1135, 240, 255, 261, 279, 1149, 1148, 1142, 1123, 358, 1153, 685, 506, 540, 532, 529, 1190, 1248])
#c_pairing = [(c_pre_list[0], c_post_list[36]), (c_pre_list[5], c_post_list[11]), (c_pre_list[5], c_post_list[1]), (c_pre_list[16], c_post_list[1]),
#            (c_pre_list[15], c_post_list[1]),(c_pre_list[17], c_post_list[1]), (c_pre_list[10], c_post_list[34]), (c_pre_list[5], c_post_list[11])]

selec_mean = np.array([[ 8,  8, 15, 15, 15, 15, 15, 15, 15], [21, 30, 22, 24, 28, 29, 30, 33, 34]])

c_pairing = ((c_pre_list[selec_mean[0, it]], c_post_list[selec_mean[1, it]]) for it in range(selec_mean.shape[1]))
#c_pairing = [(650, 378), (650, 400), (650, 255), (650, 685)]
mat_mean_post = np.zeros((16,39))
#for load_name in ((path_res,c_pre, c_post) for c_pre in c_pre_list for c_post in c_post_list):
#for count, load_name in enumerate((c_pre, c_post) for c_pre in c_pre_list[:16] for c_post in c_post_list):

mat_mean_post = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/tmp/mat_max_post.npy')
'''

''' #COMPUTE PEARSON COEFF BTWN FIRING RATE AND PLASTICITY
res_corr = np.zeros((len(c_pre_list), len(c_post_list)))
it_x = 0
for c_pre in c_pre_list:
    it_y = 0
    for c_post in c_post_list:
        RES = np.load('{}res_{}_{}.npy'.format(path_res, c_pre, c_post))
        RES_fr = firing_rate([c_pre], t_bounds, win_len, win_step)
        RES_fr_inter = np.interp(RES[2], np.arange(*t_bounds, win_step), RES_fr[0,:] )
        res_corr[it_x, it_y] = stats.linregress(RES[0][:]/RES[0][0] * RES[1][:]/RES[1][0], RES_fr_inter).rvalue
        it_y = it_y + 1
    it_x = it_x + 1
'''

'''#PLOT MAT PEARSON COEF FR-PLAST
res_corr = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/data/mat_corr_20s.npy')
plot_matshow(np.power(res_corr, 2)*np.sign(res_corr), [0,1], 'corr between firing rate 20s and plasticity')
res_corr = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/data/mat_corr_10s.npy')
plot_matshow(np.power(res_corr, 2)*np.sign(res_corr), [0,1], 'corr between firing rate 10s and plasticity')
res_corr = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/data/mat_corr_2s.npy')
plot_matshow(np.power(res_corr, 2)*np.sign(res_corr), [0,1], 'corr between firing rate 2s and plasticity')
'''

""" PLOT 2 FIRING RATE

for clust in c_post_list:
    RES_fr = firing_rate([clust], t_bounds, win_len, win_step)
    plt.plot(np.arange(*t_bounds, win_step), RES_fr[0,:])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, np.max(RES_fr[0,:]), colors=['black', 'black', 'black', 'red', 'red', 'black'])
    plt.title('cluster ' + str(clust))
    plt.show()
win_len = 2
win_step = 0.5
for clust in c_post_list:
    RES_fr = firing_rate([clust], t_bounds, win_len, win_step)
    plt.plot(np.arange(*t_bounds, win_step), RES_fr[0,:])
    plt.vlines([1172.9553- 700, 1300.7363- 700, 1303.7256- 700, 1319.78- 700, 1326.70- 700, 1350.4688- 700 ] , 0, np.max(RES_fr[0,:]), colors=['black', 'black', 'black', 'red', 'red', 'black'])
    plt.title('cluster ' + str(clust))
    plt.show()
"""

'''
for c_pre in c_pre_list:
    RES_fr = firing_rate([c_pre], t_bounds, 20, 5)
    plt.plot(np.arange(*t_bounds, 5), RES_fr[0, :])
plt.show()
for c_post in c_post_list:
    RES_fr = firing_rate([c_post], t_bounds, 20, 5)
    plt.plot(np.arange(*t_bounds, 5), RES_fr[0, :])
plt.show()


res_match = np.load('/home/ahubert/Documents/Code_STDP/model-potcadep/data/mat_match_groupA_V7265.npy')
#x_axis = resume.Team_A[resume.channel_A < 180]
#y_axis = resume.Team_A[resume.channel_A > 200]
#plot_matshow(res_match, [0,1000])

#pre_len = np.zeros(clust_pre_A.size)
#post_len = np.zeros(clust_post_A.size)

ind_match = np.where(res_match > 100)
clust_pre_A = resume.Team_A[resume.channel_A < 180]
#clust_pre_A.index = np.arange(-1, clust_pre_A.size-1)
clust_post_A = resume.Team_A[resume.channel_A > 200]
#clust_post_A.index = np.arange(clust_post_A.size)
'''

'''
y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000, 1])


test = pd.Series(np.concatenate((num_firing(clust_pre_A),num_firing(clust_post_A))))
test.index = np.concatenate((clust_pre_A, clust_post_A))
print(test[test>6000])

c_post_list = [616, 1091, 232, 239, 684, 243, 411, 362, 1211, 208, 268, 1021, 196,
                    224, 1054, 320, 1064, 324, 457, 1132, 192, 1045, 378, 732, 400, 1135,
                      240, 255, 261, 279, 1149, 1148, 1142, 1123, 358, 1153, 685, 506, 540, 532, 529, 1190, 1248]
it = 0
for c_post in c_post_list:
    print(c_post)
    print( len(spike_times.Spike_times[spike_times.Cluster_ID == c_post]))
    print( res_match[it, 4]/len(spike_times.Spike_times[spike_times.Cluster_ID == c_post]))
    print('\n')
    it = it + 1


19051 = 1172.9553 s depot scotch
20971 = 1300.7363 s mise en mouvement
21016-21031 = 1303.7256 - 1304.7138 s sniff scotch
21257 = 1319.78 s contact 
21361 = 1326.7000 s fin contact
21718 = 1350.4688 s retrait scotch arene

temps spike sorting: 700-1800 s
'''

############################################################## END BLABLA ##############################################
#plt.xticks(np.arange(len(x_axis)), x_axis)
#plt.yticks(np.arange(len(y_axis)), y_axis)
#fig.savefig('test.png')

#max at 11, post, 8 pre, V4606
#chan_striatum = [0,57]
#chan_cortex = [58, 336]

#index_striatum = resume.Team_A[resume.channel_A > 200].index
#print('l autre cluster est  ' +str(resume.Team_A[resume.channel_A < 180][8]))

#pre_max = spike_times.Spike_times[spike_times.Cluster_ID == (resume.Team_A[resume.channel_A < 180][8])]
#post_max = spike_times.Spike_times[spike_times.Cluster_ID == (resume.Team_A[resume.channel_A > 200][index_striatum[11]])]
#res = match_pre_post(pre_max, post_max, 0.05)
#print(sum(res))
#plt.scatter(post_max, res)

#print(striatum_times.groupby('Cluster_ID'))
#y = moving_window(striatum_times, 0.05, 0.001, len)

"""
cluster_A = resume.Team_A[resume.channel_A < 58]
y = np.zeros((len(cluster_A), len(np.arange(0, 370, 0.05))))
it = 0
for clust in cluster_A:
    y[it, :] = moving_window(spike_times.Spike_times[spike_times.Cluster_ID == clust], 0.1, 0.05, len)
    it = it + 1
    print(it)
plt.plot(np.mean(y, axis = 0), color = 'red')
plt.plot(np.mean(y, axis = 0) - np.var(y, axis = 0), '--', color = 'blue')
plt.plot(np.mean(y, axis = 0) + np.var(y, axis = 0), '--', color = 'blue' )
plt.show()
"""

"""
pd_striatum_times = pd.to_datetime(striatum_times, unit='s')
y = pd_striatum_times.rolling(0.05).mean()
plt.plot(y)
plt.show()
"""