import stdp.create_mouse_data
from stdp.create_mouse_data import Mouse
from stdp.computation_tools import *

import matplotlib.pyplot as plt
import numpy as np
import pathlib
import os.path


class optiGlu():
    default_dir = pathlib.Path('/home/ahubert/Documents/Code_STDP/model-potcadep/optim_Glumax')

    def __init__(self, mouse: Mouse, dir = default_dir):
        self.mouse = mouse
        self.dir = dir / self.mouse.mouse_name

    def get_id(self):
        clust_id = ()
        for path in os.listdir(self.dir):
            clust_id += (path, )

        return clust_id
    
    def get_computed_id(self):
        clust_id = ()
        for path in self.get_id():
            if len(os.listdir(self.dir / path)) > 2:
                clust_id += (path,)
        return clust_id
    
    def get_optim_values(self):
        optim_values = dict()
        for path in self.get_computed_id():
            values = np.load(self.dir / path / 'Glu_mem.npy')
            optim_values.update({path : values})
        return optim_values
    
    def plot_from_ID(self, clust, n_rep):
        '''
        n_rep the Glumem to use.
        '''
        res = np.load(self.dir / f'{clust}' / f'{n_rep}_res_{clust}.npy')
        spikes = self.mouse.spikes_from_ID(int(clust))
        spikes = spikes[spikes < self.mouse.ti]
        FR = firing_rate(spikes, [0, self.mouse.ti], 1, 0.25)
        plt.plot(FR[1], FR[0], label = 'FR', alpha = 0.6)
        plt.plot(res[2], res[0]/res[0][0], label = 'pre-syn weight')
        plt.plot(res[2], res[1]/res[1][0], label = 'post-syn weight')
        plt.plot()
        plt.legend()
        plt.title(f'clust{clust}')
        plt.show()