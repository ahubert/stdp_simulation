import logging
import pathlib
import sys

logging.basicConfig(level=logging.DEBUG)
LOGGER = logging.getLogger(__name__)
logging.getLogger('matplotlib').setLevel(logging.WARNING)

#from decimal import Decimal, getcontext
import math
import scipy
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import yaml

class VectorAccessor():
    '''
    Accessor class for a indexable object.

    In this example, suppose we have a vector with 3 elements that we will refer to as
    foo, bar and baz.
    '''
    def __init__(self, vector):
        '''
        Args:
            vector:
                The indexable object (e.g. a numpy vector or a simple list.).
        '''
        self.vector = vector

VECTOR_ELEMENT_NAMES = ['B1', 'B2', 'B3' , 'B4' , 'B5' , 'B6' , 'B7' , 'B8' , 'B9' , 'B10' , 'B11' , 'B12' , 'B13',
                        'Ca_cyt' , 'Ca_ER' , 'h' , 'IP3' , 'V' , 'rAMPA' , 'rNMDA' ,
                        'PP1' , 'I1P' , 'DAG', 'DAGLP' , '2-AG' , 'fpre' , 'AEA' , 'Glu' , 'Curr_spikepost']

# Dynamically add properties.
def VectorAccessor_add_property(VECTOR_ELEMENT_NAMES):
  for i, name in enumerate(VECTOR_ELEMENT_NAMES):
      def getter(self, i=i):
          return self.vector[i]

      def setter(self, value, i=i):
          self.vector[i] = value

      setattr(VectorAccessor, name, property(getter, setter))

VectorAccessor_add_property(VECTOR_ELEMENT_NAMES)

y0init=np.array([0.1920, 0.0025, 0.0002, 0.0001, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,
        0.1141, 64.5246, 0.8248, 0.0439, -69.9992, 0.0000, 0.0000,
        0.0009, 0.0438 , 0.0055, 0.0000, 0.0000, 0.5000, 0.0057, 0.0000, 0.0000])

def integration(t_pre, t_post, y0init = y0init, t_end = 0, integration_method = 'Radau'): #tstim_pre, tstim_post):
    '''
    INPUT VAR:
        t_pre, t_post: temps de spike, np.array.
        ns_max: number of pairings, int.
        t_end: duration of stabilization, float.
        freq: frequence des pairings, float.
        integration method: choice of methode in scipy.integrate.solve_ivp.
            Radau is default becauses adapted for stiff problems.

    LOCAL VAR:
        it_pre, it_post : suivi en indice des spikes pre/post

    OUTPUT :
        [fpre_inf,CaMKact_inf, t_sol, sol, sol_last]
        fpre_inf, CaMKact_inf, t_sol : shape (N, )
        sol : shape (len(y), N)
        sol_last : shape(len(y),) donne la sol avant stabilisation
    '''
    global currstepon
    currstepon = 0
    tsdt=0.01499 #start of depolarization before post spike.
    DPdur = 0.03 # duration of the postsyn depolarisation
    global Glumax # peak amplitude of each glutamate release peaks by the presyn
    Glumax = 2000
    #amplitude of the depol
    
    APmax = 7800 # peak amplitude of the postsyn action potential.

    #initialisations
    fpre_inf = 0 # proxy for wpre
    CaMKact_inf = 0 # proxy for wpost

    #initial conditions
    y0_values = np.zeros(y0init.shape)
    y0_values[:] = y0init.copy()
    y0 = VectorAccessor(y0_values)

    #initialise le suivit des spikes
    it_pre = 0
    it_post = 0
    it_start_depol = 0
    it_stop_depol = 0
    #initialise les temps d'integration
    t_start = 0
    t_stop = 0
    #initialization

    t_pre = np.append(t_pre, np.infty)
    t_post = np.append(t_post, np.infty)
    t_start_depol = t_post - tsdt
    t_stop_depol = t_post - tsdt + DPdur

    t_sol = np.array([np.min([t_post[it_post], t_pre[it_pre], t_start_depol[it_start_depol], t_stop_depol[it_stop_depol]])]) #temps de solutions calculées
    print(t_sol)
    sol = np.transpose(np.array([y0init])) #solutions calculées

    #First time
    trigger = np.argmin([t_post[it_post], t_pre[it_pre], t_start_depol[it_start_depol], t_stop_depol[it_stop_depol]])
    if trigger == 0:
        t_stop = t_post[it_post]
        next_add = ["post", ""]
        it_post = it_post + 1
    elif trigger == 1:
        t_stop = t_pre[it_pre]
        next_add = ["pre", ""]
        it_pre = it_pre + 1
    elif trigger == 2:
        t_stop = t_start_depol[it_start_depol]
        next_add = ["", "on"]
        it_start_depol = it_start_depol + 1
    elif trigger == 3:
        t_stop = t_stop_depol[it_stop_depol]
        next_add = ["", "off"]
        it_stop_depol = it_stop_depol + 1
    else:
        LOGGER.INFO("trigger bug. Event like spike or depol not recognize")

    # time loop
    #LOGGER.debug("length : " + str (len(t_pre)+len(t_post)) )
    for it in range(len(t_pre)+3*len(t_post)-5): #-5 et non -4 car déjà une étape faite en initialisation

        add = next_add
        t_start = t_stop
        trigger = np.argmin([t_post[it_post], t_pre[it_pre], t_start_depol[it_start_depol], t_stop_depol[it_stop_depol]])
        if trigger == 0:
            t_stop = t_post[it_post]
            next_add = ["post", ""]
            it_post = it_post + 1
        elif trigger == 1:
            t_stop = t_pre[it_pre]
            next_add = ["pre", ""]
            it_pre = it_pre + 1
        elif trigger == 2:
            t_stop = t_start_depol[it_start_depol]
            next_add = ["", "on"]
            it_start_depol = it_start_depol + 1
        elif trigger == 3:
            t_stop = t_stop_depol[it_stop_depol]
            next_add = ["", "off"]
            it_stop_depol = it_stop_depol + 1
        else:
            LOGGER.INFO("trigger bug. Event like spike or depol not recognize")
        
        if (add[1]=="on"):
            currstepon = 1
        elif(add[1]=="off"):
            currstepon = 0
        if (add[0]=="pre"):
            y0.Glu = y0.Glu + Glumax
        elif (add[0]=="post"):
            y0.Curr_spikepost = y0.Curr_spikepost + APmax

        # integrates bewteen two spike event
        in_work = True
        while (in_work == True):
            #LOGGER.debug("integration start at: " + str(t_start) + " and end at " + str(t_stop) + '\n' + "iterations: " + str(it_post) + " " + str(it_pre) + " " + str(it_start_depol) + " " + str(it_start_depol) )
            sol_curr = scipy.integrate.solve_ivp(STDP, [t_start, t_stop] , y0_values, method = integration_method, events = [event_Ca_cyt]) #event2, event3]) option event, pour heaviside par exemple.
            
            # concatentate the result of the current integration, between two events
            #LOGGER.debug("concatenation temps: " + str(sol_curr.t))
            #LOGGER.debug("concatenation sol: " + str(sol_curr.y[:,-1]))
            sol = np.concatenate((sol , sol_curr.y[:,1:]) , axis=1)
            t_sol = np.concatenate(( t_sol , sol_curr.t[1:]))  # - sol_curr.t[0] + t_sol[-1] ))

            # set initial conditions of the next integration round
            # as the final conditions of the previous one
            
            y0_values[:] = sol_curr.y[:,-1]
            if len(sol_curr.t_events[0]) == 0:
                in_work = False
            else:
                #LOGGER.debug(str(sol_curr.t_events[0]))
                #LOGGER.debug('ENDING INFORMATION:  t_event: ' + str(sol_curr.t_events) + " last t_sol: " + str(t_sol[-1]) + " last curr t: " + str(sol_curr.t[-1]))
                y0_values[13] = 1e-10
                t_start = sol_curr.t[-1]
                #quit()

            #LOGGER.info("Number of spike: " + str(it+1) + " out of " + str(len(t_pre)+len(t_post)-2))
            #LOGGER.debug("it_pre: " + str(it_pre) + ", it_post: " + str(it_post) + ", it: " + str(it) + '\n')

    sol_last = sol[:, -1]
    
    if (t_end > 0):
        # integrates at the end for stabilisation
        currstepon = 0
        sol_curr = scipy.integrate.solve_ivp(STDP, [t_sol[-1], t_sol[-1] + t_end] , y0_values, method =  integration_method ) #, events = event_cond) #option event, pour heaviside par exemple.

        # concatentate the result of the current integration, between two events

        sol = np.concatenate((sol , sol_curr.y[:,1:]) , axis=1)
        t_sol = np.concatenate(( t_sol , sol_curr.t[1:]))
    
    #final output variables
    fpre_inf = sol[25,:] #pre_inf is the presynaptic weight
    CaMKact_inf = sol[0,:] + 2*np.sum(sol[1:4, :], axis=0) + 3*np.sum(sol[4:8, :], axis=0) + 4*np.sum(sol[8:11, :], axis=0) + 5*sol[11,:] + 6*sol[12,:]

    #Normalization of the proxy variables to obtain the real pre and post weights
    CaMKact_inf=1+2*CaMKact_inf/164.6

    return([fpre_inf,CaMKact_inf, t_sol, sol, sol_last])

#region ################################################# PARAMS FOR STDP ############################################

#Stimulation parameters
#presyn

#Glumax=2000#set in the main function cause it's an event parameter

tauGlu=5e-3

# Parameters of the Ca influx through VDCC (bAP)
# set so as to mimick the Fino et al paper, Fig 1 C
# ie the depol brings \DeltaV = 45 mV, 
# and the bAP  \DeltaV = 60 mV in supplement
# hence the total is around 100 mV, as reported by Sabatini-Carter 2004
#APdur=0.03#set in the main function
#APmax=7800#set in the main function

DPmax = 550
ksivdcc=0.1#µM.s-1.A-1
#set so that the bAP make \Delta Ca = 400 nM (Sabatini Carter 2004)
tausbAP=0.001


#Parameters of the AMPAR
gAMPAmax=0.28 #in nS, set so that each pre spike increases V from around 1 mV
alphaAMPA=1.10#s-1µM-1
betaAMPA=190#s-1

#Parameters of the NMDAR
gNMDAmax=0.28#0.28#nS
ksi=3040 # in µM.s-1.A-1
# gNMDAmax and ksi are set so that each pre spike increases Ca of circa
# 0.17 µM at -70 mV (Sabatini et al, 2002)
alphaNMDA=0.072#s-1µM-1
betaNMDA=100#s-1
#alpha and beta NMDA are set so that the time-change of NMDAR-transported 
#Ca behaves in agreement with corresponding experimental observation 
#in Sabatini et al, 2002 ie a very rapide rise ( 2ms) then decay phase 
#that needs around 200 ms to converge back to baseline.

#External Mg concentration (mM)
Mg=1.0

#Parameters for the CAMKII activation (ionotrop plasticity)
CaMKT=16.6#fixed by experiments see Graupner-Brunel 2007
#phosphorylation
k5=0.1 #fixed by experiments see Graupner-Brunel 2007
k6=6
k7=6 # same values as Graupner-Brunel 2007
#dephosporylation
k12=6000
KM=0.4 # same values as Graupner-Brunel 2007
#Calmodulin
CaMT=0.07342
Ka1=0.1
Ka2=0.025
Ka3=0.32
Ka4=0.40 #fixed by experiments see Graupner-Brunel 2007
#PP1 / I1
PP10=0.2 # same values as Graupner-Brunel 2007
k11=500
km11=0.1
I10=1# same values as Graupner-Brunel 2007
#CaN / PKA parameters on I1
KdcanI1=0.053
ncanI1=3 #fixed by experiments see Graupner-Brunel 2007
kcan0I1=0.05
kcanI1=20.5
KdpkaI1=0.159 
npkaI1=3 
kpka0I1=0.0025 
kpkaI1=4.67
#changed the too large value npka=8 in Graupner-Brunel 2007 to a more 
#conservative 3 for pka

#Parameters for CICR
ver=8.0
ker=0.05
rhoER=.30
rc=4
rl=0.1
d1=0.13
d2=3.049
d3=0.9434
d5=0.12
a2=0.5

#Parameters of the IP3 metabolism subsystem
#Agonist-dependent production
vbeta=0.80
kr=1.3
kp=10
kpi=0.6
#Agonist-independent production
vdelta=0.02
kappad=1.5
kdelta=0.1
#IP3 degradation
v3k=1
kd=1.5
k3=1
n3=1
r5p=0.25

#Parameters for Kinase K on DAGlipase a
rK=1e6
r0K=0.0
nK=8
KmK=3.0
# Parameters for Posphastase P on DAGLIPASE a
rP=1e3
r0P=0
nP=3
KmP=0.053

#DGL kinetics
rDGL=2e4
KDGL=30

#DAG and 2-AG degradation/export
kMAGL=2
kDAGK=2


#Parameters for AEA and TRPV1
vATAEA=0.2
Kact=300#Natarajan1986,Hansen1998
vFAAH=4.0
KFAAH=1#Okamoto2004
Gmax_TRPV1=0.1#0.150##nS
ksiTRPV1=250#µM.s-1.A-1
alphaAEACB1=0.10


LTDstart=5e-3 #5e-3
LTDstop=10e-3 #13e-3
LTPstart=19e-3
LTDMax=0.20 #0.25
LTPMax=15
P1=1e-10 #1e-3
P3=5 #2
P4=2 #2e-3

#Parameters of the dendrite
Cm=0.1 # nF
gL=10.0 # nS
EL=-70 # mV

#Parameters for Ca Buffering
BT=4
KdB=0.50
tauCab=0.0070
Cab=0.10

#endregion ##################################################### END PARAMS FOR STDP #################################################

def STDP(t,y):
    '''

    '''

    dy=np.zeros((29))

    #CICR intermediate functions
    minf=y[16]/(y[16]+d1)
    ninf=y[13]/(y[13]+d5)
    JIP3R=rc*(minf*ninf*y[15])**3*(y[14]-y[13])
    Jserca=(ver*y[13]**2)/(ker**2+y[13]**2)
    Jleak=rl*(y[14]-y[13])
    Hill1=y[13]/(y[13]+kpi)


    #ionotropic Glutamate Receptors
    IAMPA=gAMPAmax*y[18]*y[17]
    B=1/(1+Mg/3.57*np.exp(-0.062*y[17])) # corresponds to mg=1mM
    INMDA=gNMDAmax*y[19]*y[17]*B

    #Specific Ca buffer
    tauCa=(1+BT/KdB/(1+y[13]/KdB)**2)
    tauCaER=(1+BT/KdB/(1+y[14]/KdB)**2)

    #Stimulations
    #Stimulation-dependent IP3 production
    vglu=vbeta*y[27]/(y[27]+(kr*(1+kp/kr*Hill1)))
    #Postsynaptic Stim (current step + bAP)
    
    IVDCC=currstepon*DPmax + y[28]

    #TRPV1
    ITRPV1=-Gmax_TRPV1*G_TRPV1(y[26],y[17])

    ##ODEs

    #dCa_cyt/dt
    if (np.heaviside(y[13]-1e-10, 0) == 0) and (((JIP3R-Jserca+Jleak+ksivdcc*IVDCC-(y[13]-Cab)/tauCab-ksi*0.10*INMDA-ksiTRPV1*ITRPV1)/tauCa) <0) :
        dy[13] = 0 
    else:
        dy[13]=(JIP3R-Jserca+Jleak+ksivdcc*IVDCC-(y[13]-Cab)/tauCab-ksi*0.10*INMDA-ksiTRPV1*ITRPV1)/tauCa
    #dCa_ER/dt
    dy[14]=-(JIP3R-Jserca+Jleak)*rhoER/tauCaER
    #dh/dr
    dy[15]=(a2*d2*(y[16]+d1)/(y[16]+d3))*(1-y[15])-a2*y[13]*y[15]
    #dIP3/dt
    dy[16]=vglu+vdelta/(1+y[16]/kappad)*y[13]**2/(y[13]**2+kdelta**2)-v3k*y[13]**4/(y[13]**4+kd**4)*y[16]**n3/(k3**n3+y[16]**n3)-r5p*y[16]
    #dV/dt
    dy[17]=1/Cm*(-gL*(y[17]-EL)-IAMPA-INMDA+IVDCC-ITRPV1)
    #drAMPA/dt
    #dy(19)=alphaAMPA*Glu*(1-y[18])-betaAMPA*y[18]
    dy[18]=alphaAMPA*y[27]*(1-y[18])-betaAMPA*y[18]
    #drNMDA/dt
    #dy(20)=alphaNMDA*Glu*(1-y[19])-betaNMDA*y[19]
    dy[19]=alphaNMDA*y[27]*(1-y[19])-betaNMDA*y[19]


    ###############################
    # Ionotropic NMDA plasticity #
    ###############################

    #Taken from the Brunel and Graupner model

    # occupied receptors on the CaMKII
    rr=np.sum(y[0:13])
    # B0 is whats left from total
    B0=2*CaMKT-rr
    # kinetic equations
    phossum=y[0] + 2*np.sum(y[1:4]) + 3*np.sum(y[4:8]) + 4*np.sum(y[8:11]) + 5*y[11] + 6*y[12]
    k10=k12*y[20]/(KM + phossum)
    CaM=CaMT/(1 + Ka4/y[13] + Ka3*Ka4/(y[13]**2) + Ka2*Ka3*Ka4/(y[13]**3) + Ka1*Ka2*Ka3*Ka4/(y[13]**4))
    gamma=CaM/(k5+CaM) 
    vPKA=kpka0I1 + kpkaI1/(1 + (KdpkaI1/CaM)**npkaI1)
    vCaN=kcan0I1 + kcanI1/(1 + (KdcanI1/CaM)**ncanI1)

    #dBi/dt
    dy[0] = 6*k6*gamma**2*B0 - 4*k6*gamma**2*y[0] - k7*gamma*y[0] - k10*y[0] + 2*k10*np.sum(y[1:4])
    dy[1] = k7*gamma*y[0] + k6*gamma**2*y[0] - 3*k6*gamma**2*y[1] - k7*gamma*y[1] - 2*k10*y[1] + k10*(2*y[4] + y[5] + y[6])
    dy[2] = 2*k6*gamma**2*y[0] - 2*k7*gamma*y[2] - 2*k6*gamma**2*y[2] - 2*k10*y[2] + k10*(y[4] + y[5] + y[6] + 3*y[7]) 
    dy[3] = k6*gamma**2*y[0] - 2*k7*gamma*y[3] - 2*k6*gamma**2*y[3] - 2*k10*y[3] + k10*(y[5] + y[6])
    dy[4] = k7*gamma*y[1] + k7*gamma*y[2] + k6*gamma**2*y[1] - k7*gamma*y[4] - 2*k6*gamma**2*y[4] - 3*k10*y[4] + k10*(2*y[8] + y[9])
    dy[5] = k6*gamma**2*y[1] + k6*gamma**2*y[2]  + 2*k7*gamma*y[3] - k6*gamma**2*y[5] - 2*k7*gamma*y[5] - 3*k10*y[5] + k10*(y[8] + y[9] + 2*y[10])
    dy[6] = k6*gamma**2*y[1] + k7*gamma*y[2] + 2*k6*gamma**2*y[3] - k6*gamma**2*y[6] - 2*k7*gamma*y[6] - 3*k10*y[6] + k10*(y[8] + y[9] + 2*y[10])
    dy[7] = k6*gamma**2*y[2] - 3*k7*gamma*y[7] - 3*k10*y[7] + k10*y[9]
    dy[8] = k7*gamma*y[4] + k6*gamma**2*y[4] + k7*gamma*y[5] + k7*gamma*y[6] - k6*gamma**2*y[8] - k7*gamma*y[8] - 4*k10*y[8] + 2*k10*y[11]
    dy[9] = k6*gamma**2*y[4] + k6*gamma**2*y[5] + k7*gamma*y[6] + 3*k7*gamma*y[7] - 2*k7*gamma*y[9] - 4*k10*y[9] + 2*k10*y[11]
    dy[10] = k7*gamma*y[5] +  k6*gamma**2*y[6] - 2*k7*gamma*y[10] - 4*k10*y[10] + k10*y[11]
    dy[11] = k7*gamma*y[8] + k6*gamma**2*y[8] + 2*k7*gamma*y[9] + 2*k7*gamma*y[10] - k7*gamma*y[11] - 5*k10*y[11] + 6*k10*y[12]
    dy[12] = k7*gamma*y[11] - 6*k10*y[12]

    #PP1 And I1P
    dy[20]= -k11*y[21]*y[20] + km11*(PP10 - y[20])
    dy[21]= -k11*y[21]*y[20] + km11*(PP10 - y[20]) + vPKA*I10 - vCaN*y[21]

    #DAG
    dy[22]=vglu+vdelta/(1+y[16]/kappad)*y[13]**2/(y[13]**2+kdelta**2)-rDGL*y[23]*y[22]/(y[22]+KDGL)-kDAGK*y[22]

    #Phosphorylated DAGLipase Fraction
    dy[23]=(r0K+rK*y[13]**nK/(KmK**nK+y[13]**nK))*(1-y[23])-(r0P+rP*y[13]**nP/(KmP**nP+y[13]**nP))*y[23]
    
    #2-AG-post
    dy[24]=rDGL*y[23]*y[22]/(y[22]+KDGL)-kMAGL*y[24]

    #2-AG controled aKP cycle in pre
    Omega=0.5-LTDMax*np.heaviside(y[24]+alphaAEACB1*y[26]-LTDstart, 1)*np.heaviside(LTDstop-y[24]-alphaAEACB1*y[26], 1)+np.heaviside(y[24]+alphaAEACB1*y[26]-LTPstart, 1)*LTPMax
    #Omega=0.5-LTDMax/(1 + np.exp(-1e8*(y[24]+alphaAEACB1*y[26]-LTDstart)))/(1 + np.exp(-1e8*(LTDstop-y[24]-alphaAEACB1*y[26])))+LTPMax/(1 + np.exp(-1e8*(y[24]+alphaAEACB1*y[26]-LTPstart)))
    #global Omega_vect
    #Omega_vect = np.append(Omega_vect, Omega)
    #taufpre=P1/((P1/1e4)**P3+(y[24]+alphaAEACB1*y[26])**P3)+P4
    taufpre=P1/((P1/1e-4)**P3+(y[24]+alphaAEACB1*y[26])**P3)+P4
    dy[25]=(Omega-y[25])/taufpre

    #dAEA/dt
    dy[26]=vATAEA*y[13]-vFAAH*y[26]/(KFAAH+y[26]) #vAT*y[0]/(Kact+y[0])-vFAAH*y[1]/(KF+y[1])

    dy[27]=-y[27]/tauGlu
    dy[28]=-y[28]/tausbAP
    return(dy)

def G_TRPV1(AEA,V):

    #Implementation of the allosteric TRPV1 model by Matta & Ahern, 2007 (their eq 3)

    TC=25 #temp in °C
    #constants
    L=4.2e-4
    J0=0.0169
    DH=205e3 #J/mol
    T=273.15 + TC #K
    DS=615 #J/mol
    D=1100
    C=23367
    z=0.6
    KD=0.5 #10.0#µM - KD for caps 0.5 µM for AEA (Marzo2002,Starowicz2007)
    P=750
    F=96.5 #1e3 C/mol
    R=8.31 #J/mol/K

    J=J0*np.exp(z*F*V/(R*T))
    K=np.exp(-(DH-T*DS)/(R*T))
    Q=AEA/KD

    Popen=1/(1+(1+J+K+Q+J*K+J*Q+K*Q+J*K*Q)/(L*(1+J*D+K*C+Q*P+J*K*C*D+J*Q*D*P+K*Q*C*P+J*K*Q*D*C*P)))
    #plot(V,Popen)

    return (Popen)

#event_cond = [event 1 , event 2] #List of callable event function. with attribute "terminal" and "direction".

"""
#generation of the jitters    
function dts=gen_dt_jit(mu,sd,minv,ns)
    absmu=abs(mu)
    # first generates 1000 random numbers with mean absmu and standard dev
    # sd
    dts=absmu+sd*randn(1000,1)
    #keep only those that are in the correct range
    dts=dts(dts>=minv)
    dts=dts(dts<=2*absmu-minv)

    #repeat until one has ns random jitter in the correct range
    while length(dts)<ns
        toto=[]
        toto=absmu+sd*randn(1000,1)
        toto=toto(toto>=minv)
        toto=toto(toto<=2*absmu-minv)
        dts=[dts toto]
    end

if mu<0
    dts=-1*dts
end
dts=dts'
dts=dts(1:ns)
"""

#Where there is discontinuities.
#heaviside(y(25)+alphaAEACB1*y(27)-LTDstart)*heaviside(LTDstop-y(25)-alphaAEACB1*y(27))+heaviside(y(25)+alphaAEACB1*y(27)-LTPstart)

#region ##################### Events functions #########################

def event_Ca_cyt(t,y):
    return(y[13])
event_Ca_cyt.terminal = True
#event_Ca_cyt.direction = -1 not really usefull cause start at positive value

def event1(t,y):
    return ( y[24]+alphaAEACB1*y[26]-LTDstart )
def event2(t,y):
    return ( LTDstop-y[24]-alphaAEACB1*y[26] )
def event3(t,y):
    return( y[24]+alphaAEACB1*y[26]-LTPstart )

#endregion ###################################################################

def plot_proxy(ns_max = 10, dtstdp = 0.025, t_end = 0 ):
    RES = integration(ns_max = ns_max, dtstdp = dtstdp, integration_method = "Radau", t_end = t_end )
    RES_proxy = np.array(RES[0:3])
    RES_sol = np.array(RES[3])
    #plt.plot(RES_proxy[2, : ], RES_sol[27,:]/Glumax )
    plt.plot(RES_proxy[2, : ], np.sum(RES_proxy[0:2, : ], axis = 0))
    plt.show()

def default_value_eval():
    #initialization
    tstart = 0
    t_sol = np.array([]) #temps de solutions calculées
    sol = np.array([[]]*29) #solutions calculées

    #initial conditions
    global currstepon #set to 0 outside postsyn depolarisation periods and to 1 during depolarisation
    currstepon = 0
    y0_values = y0init.copy()
    y0 = VectorAccessor(y0_values)

    #initialise les temps d'integration

    # integrates for stabilisation
    sol_curr = scipy.integrate.solve_ivp(STDP, [0, 40] , y0_values ) #, events = event_cond) #option event, pour heaviside par exemple.
    
    sol = sol_curr.y[:,:]
    t_sol = sol_curr.t[:]

    #final output variables
    fpre_inf = sol[25,:] #pre_inf is the presynaptic weight
    CaMKact_inf = sol[0,:] + 2*np.sum(sol[1:3, :], axis=0) + 3*np.sum(sol[4:7, :], axis=0) + 4*np.sum(sol[8:10, :], axis=0) + 5*sol[11,:] + 6*sol[12,:]

    #Normalization of the proxy variables to obtain the real pre and post weights
    fpre_inf=fpre_inf/y0init[25]
    CaMKact_inf=1+2*CaMKact_inf/164.6

    RES = [fpre_inf,CaMKact_inf, t_sol, sol]
    RES_proxy = np.array(RES[0:3])
    print(sum(RES_proxy[0:2, 0 ]))
    print(sum(RES_proxy[0:2, -1 ]))

def heatmap2d(arr: np.ndarray):
    cmap_heat = mpl.cm.seismic
    norm_heat = mpl.colors.PowerNorm(gamma = 0.5, vmin = 0.5, vmax = 2.5, clip = True)
    x_position = np.arange(0, len(x_axis), step = 5)
    new_x_axis = [x_axis[i] for i in x_position]
    plt.imshow(arr, cmap = cmap_heat, norm = norm_heat)
    plt.colorbar()
    plt.yticks(x_position, new_x_axis)
    plt.xlabel('Number of pairing')
    plt.ylabel('Time between pre and post (s)')
    plt.title('map of synaptic weight change' + '\n' + 'depending on number of pairing and time of pairing.')
    plt.show()

def compute_map_mem(ns_bounds = [1, 100], ns_step = 1, dtstdp_values = np.array([-0.015]), t_end = 0, integration_method = 'Radau'):
    x = 0
    k = 0
    ns_step = ns_step
    ns_values = np.arange(*ns_bounds, ns_step)
    RES_tot = []
    Z_pre = np.zeros((len(dtstdp_values), len(ns_values)))
    Z_post = np.zeros((len(dtstdp_values), len(ns_values)))

    for dtstdp in dtstdp_values:
        y= 0
        for ns_max in ns_values:
            if (y == 0):
                t_pre = np.arange(0,ns_step)
                t_post = t_pre +dtstdp
                RES = integration(t_pre = t_pre, t_post = t_post, t_end = t_end)
            else:
                t_pre = np.arange(0,ns_step)
                t_post = t_pre + dtstdp
                RES = integration(t_pre = t_pre, t_post = t_post, y0init = RES[4], t_end = t_end, integration_method = integration_method)
            RES_proxy = np.array(RES[0:3])
            Z_pre[x,y] = RES_proxy[0, -1]
            Z_post[x,y] = RES_proxy[1, -1]
            RES_tot.append( RES )
            LOGGER.debug("dtstdp: " + str(dtstdp))
            LOGGER.debug("ns_max: " + str(ns_max))
            y = y+1
            k = k+1
        x = x + 1
    #np.save('line_-15ms_t_sol',RES_proxy[2, : ])
    np.save('map_full_creneau_pre_test', Z_pre)
    np.save('map_full_creneau_post_test', Z_post)
    np.save('map_full_creneau_res_test', RES_tot)
    #plot_heatmap(Z_post+Z_pre)
    #plt.plot(Z[0])

def compute_map(ns_bounds = [1, 100], ns_step = 1, dtstdp_values = np.array([-0.015]), t_end = 0):
    x = 0
    ns_values = np.arange(*ns_bounds, ns_step)
    RES_tot = []
    Z_pre = np.zeros((len(dtstdp_values), len(ns_values)))
    Z_post = np.zeros((len(dtstdp_values), len(ns_values)))

    for dtstdp in dtstdp_values:
        y= 0
        for ns_max in ns_values:
            t_pre = np.arange(0,ns_max)
            t_post = t_pre + dtstdp
            RES = integration(t_pre = t_pre, t_post = t_post, integration_method = 'Radau', t_end = t_end )
            RES_proxy = np.array(RES[0:3])
            Z_pre[x,y] = RES_proxy[0, -1]
            Z_post[x,y] = RES_proxy[1, -1]
            RES_tot.append( RES )
            LOGGER.debug("dtstdp: " + str(dtstdp))
            LOGGER.debug("ns_max: " + str(ns_max))
            y = y+1
        x = x + 1
    #np.save('map_full_creneau_pre', Z_pre)
    #np.save('map_full_creneau_post', Z_post)
    #np.save('map_full_creneau_res', RES_tot)
    #plt.plot(Z[0])

def test(ns_max = 10, dtstdp = 0.005):
    RES = integration(ns_max = ns_max, dtstdp = dtstdp, integration_method = 'Radau', t_end=40)
    RES_proxy = np.array(RES[0:3])
    RES_sol = np.array(RES[3])
    plt.plot(RES_proxy[2, : ], RES_sol[23,:] )
    plt.show()

def plot_map():
    #Z = np.load('low_part_heatmap.npy')
    Z_pre = np.load('line_-30-1ms_pre.npy')
    Z_post = np.load('line_-30-1ms_post.npy')
    #heatmap2d(np.flip(Z, axis=0))
    plt.plot(Z_pre[15]/Z_pre[15][0])
    plt.plot(Z_post[15]/Z_post[15][0])
    plt.plot((Z_pre[15]+Z_post[15])/(Z_pre[15][0]+Z_post[15][0]))
    plt.show()

def plot_heatmap(Z):
    init1 = y0init[25] #pre_inf is the presynaptic weight
    init2 = y0init[0] + 2*np.sum(y0init[1:4]) + 3*np.sum(y0init[4:8]) + 4*np.sum(y0init[8:11]) + 5*y0init[11] + 6*y0init[12]
    init2 = 1+2*init2/164.6
    Z = Z/(init1*2+init2)
    heatmap2d(np.flip(Z, axis=0))

def plot_heatmap(Z):
    init1 = y0init[25] #pre_inf is the presynaptic weight
    init2 = y0init[0] + 2*np.sum(y0init[1:4]) + 3*np.sum(y0init[4:8]) + 4*np.sum(y0init[8:11]) + 5*y0init[11] + 6*y0init[12]
    init2 = 1+2*init2/164.6
    Z = Z/(init1*2+init2)
    heatmap2d(np.flip(Z, axis=0))

def plot_gaussian_heatmap(Z, sigma = 1):
    init1 = y0init[25] #pre_inf is the presynaptic weight
    init2 = y0init[0] + 2*np.sum(y0init[1:4]) + 3*np.sum(y0init[4:8]) + 4*np.sum(y0init[8:11]) + 5*y0init[11] + 6*y0init[12]
    init2 = 1+2*init2/164.6
    Z = Z/(init1 * init2)
    #Z = scipy.ndimage.gaussian_filter(Z, sigma = sigma)
    Z = scipy.ndimage.gaussian_filter1d(Z, sigma = sigma, axis = 0)
    heatmap2d(np.flip(Z, axis=0))

def debug_time():

    RES_ref = integration(ns_max = 15, dtstdp = -0.015, t_end = 10)

    RES = integration(ns_max = 10, dtstdp = -0.015, t_end = 0)
    RES_t = RES[2]
    RES = integration(ns_max = 5, dtstdp = -0.015, t_end = 10, y0init = RES[4])

    RES_t = np.concatenate(( RES_t , RES[2][:]))
    ind = min(np.where((RES_t == RES_ref[2]) == False)[0])
    RES_t[ind:] = RES_t[ind:] + RES_t[ind-1] - RES_t[ind]
    print(RES_t[(ind-2):(ind+2)])
    print(RES_ref[2][(ind-2):(ind+2)])

def compute_ligne(dtstdp, t_end , integration_method = 'Radau'):
    Y = np.zeros((2,len(np.arange(1,100))))
    for ns_max in np.arange(1, 100):
        RES = integration(ns_max=ns_max, dtstdp = dtstdp, t_end = t_end, integration_method = integration_method)
        RES_proxy = np.array(RES[0:3])
        Y[:,ns_max-1] = np.array([RES_proxy[0, -1] , RES_proxy[1, -1]])
    #file_name = "line_ns-1-100_dtstdp-" + str(round(dtstdp*1000))
    #np.save(Y, file_name)
    return Y

def compute_ligne_mem(dtstdp, t_end , integration_method = 'Radau'):
    Y = np.zeros((2, len(np.arange(1,100))))
    for ns_max in np.arange(1, 100):
        if ns_max == 1 :
            RES = integration(ns_max=1, dtstdp = dtstdp, t_end = t_end, integration_method = integration_method)
            RES_proxy = np.array(RES[0:3])
            Y[:, ns_max-1] = np.array([RES_proxy[0, -1] , RES_proxy[1, -1]])
        else:
            RES = integration(ns_max=1, dtstdp = dtstdp, t_end = t_end, y0init = RES[4], integration_method = integration_method )
            RES_proxy = np.array(RES[0:3])
            Y[:, ns_max-1] = np.array([RES_proxy[0, -1],RES_proxy[1, -1]])
    #file_name = "line_ns-1-100_dtstdp-" + str(round(dtstdp*1000))
    #np.save(Y, file_name)
    return Y

############################################################### MAIN APPEL ZONE #########################################################

x_axis = np.flip(np.arange(-35.25, 40, 0.5)/1000)

#compute_map( ns_step = 1, dtstdp_values = np.arange(-35.25, 40, 0.5)/1000, t_end = 100 )
w_pre = np.load('map_full_creneau_pre.npy')
w_post = np.load('map_full_creneau_post.npy')
w_tot =  w_pre * w_post
plot_gaussian_heatmap(w_tot, sigma = 0.5)


"""
init1 = y0init[25] #pre_inf is the presynaptic weight
init2 = y0init[0] + 2*np.sum(y0init[1:4]) + 3*np.sum(y0init[4:8]) + 4*np.sum(y0init[8:11]) + 5*y0init[11] + 6*y0init[12]
init2 = 1+2*init2/164.6
w_pre = np.load('./data/map_full-low_pre.npy')
w_post = np.load('./data/map_full-low_post.npy')
w_tot =  w_pre/init1 * w_post/init2
#plot_gaussian_heatmap(w_tot, sigma = 0.1)
heatmap2d(np.flip(w_tot, axis=0))

t_pre = np.arange(0,70)
t_post = t_pre - 0.0005
LOGGER.info("initial t post: " + str(t_post))
RES = integration(t_pre, t_post, t_end = 100, integration_method='Radau')
#print( (RES[0][-1]/RES[0][0] + RES[1][-1]/RES[1][0]) )
#print(RES[0][:]/RES[0][0])
#print(RES[1][:]/RES[1][0])
plt.plot(RES[2], RES[0][:]/RES[0][0] * RES[1][:]/RES[1][0])
plt.plot(RES[2], RES[0][:]/RES[0][0])
plt.plot(RES[2], RES[1][:]/RES[1][0])
plt.legend(['tot', 'pre', 'post'])
plt.show()

for k in range(29):
    plt.plot(RES[2], RES[3][k,:])
    plt.title(VECTOR_ELEMENT_NAMES[k])
    plt.show()
"""

"""
t_pre = np.arange(0,40)
t_post = t_pre + 0.0071
RES = integration(t_pre, t_post, t_end = 100)
plt.plot(RES[2], RES[0][:]/RES[0][0] * RES[1][:]/RES[1][0])
#plt.plot(RES[2])
plt.show()
"""


"""
plt.plot(y_mem[0])
plt.plot(y_mem[1])
plt.show()
y = np.load('line_15ms.npy')

plt.plot(y_mem[0, :])
plt.plot(y[0, :])
plt.plot(y[0, :] - y_mem[0, : ])
plt.show()
plt.plot(y_mem[1, :])
plt.plot(y[1, :])
plt.plot(y[1, :] - y_mem[1, : ])
plt.show()
"""

#plot_heatmap()
#compute_map_mem(ns_step=1, dtstdp_values = np.arange(-0.0345, -0.002, 0.001), t_end=100)

#plot_heatmap()
#compute_map(ns_step=5, dtstdp_values = np.arange(-0.035, 0.035, 0.002) , t_end = 100)