SELF=$(readlink -f "${BASH_SOURCE[0]}")
DIR=${SELF%/*}
unset SELF

source "$DIR/venv/bin/activate"

src_dir=$DIR/src
if [[ ":$PYTHONPATH:" != *":$src_dir:"* ]]
then
  export PYTHONPATH="$src_dir:$PYTHONPATH"
fi
unset src_dir
echo $PYTHONPATH
